import { LightningElement,  api } from 'lwc';  
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import FORM_FACTOR from '@salesforce/client/formFactor';
import USER_ID from '@salesforce/user/Id';
import USAGE_OBJECT from '@salesforce/schema/Lightning_Usage_Metrics__c';
import COMPONENT_FIELD from '@salesforce/schema/Lightning_Usage_Metrics__c.Component__c';
import PAGE_FIELD from '@salesforce/schema/Lightning_Usage_Metrics__c.Page__c';
import FORM_FACTOR_FIELD from '@salesforce/schema/Lightning_Usage_Metrics__c.Form_Factor__c';
import LOGGED_DATE_TIME_FIELD from '@salesforce/schema/Lightning_Usage_Metrics__c.Logged_Date_Time__c';
import MODULE_FIELD from '@salesforce/schema/Lightning_Usage_Metrics__c.Module__c';
import USER_FIELD from '@salesforce/schema/Lightning_Usage_Metrics__c.User__c';
import METRIC_TYPE_FIELD from '@salesforce/schema/Lightning_Usage_Metrics__c.Metric_Type__c';

export default class LwcUsageMetricsService extends LightningElement {

    @api logUsageStatistics(module,page,component,metricType){  

        const fields = {};
        var today = new Date();

        fields[COMPONENT_FIELD.fieldApiName] = component;
        fields[PAGE_FIELD.fieldApiName] = page;
        fields[USER_FIELD.fieldApiName] = USER_ID;
        fields[MODULE_FIELD.fieldApiName] = module;
        fields[FORM_FACTOR_FIELD.fieldApiName] = FORM_FACTOR;
        fields[LOGGED_DATE_TIME_FIELD.fieldApiName] = today.toISOString();
        fields[METRIC_TYPE_FIELD.fieldApiName] = metricType;
        
        const recordInput = { apiName: USAGE_OBJECT.objectApiName, fields };
        createRecord(recordInput)
            .then(result => {
                
            })
            .catch(error => {
                window.console.log('error ===> '+JSON.stringify(error));
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error creating log record.',
                        message: error.body.message,
                        variant: 'error',
                    }),
                );
            });
   }  
}