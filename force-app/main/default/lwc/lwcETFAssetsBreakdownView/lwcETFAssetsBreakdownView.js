import { LightningElement,track } from 'lwc';
import getETFAssetsByFund from '@salesforce/apex/ETFAssetsBreakdownController.getETFAssetsByFund';
import getETFAssetsByFundChannel from '@salesforce/apex/ETFAssetsBreakdownController.getETFAssetsByFundChannel';
import getETFAssetsByFundChannelFirm from '@salesforce/apex/ETFAssetsBreakdownController.getETFAssetsByFundChannelFirm';
import getETFAssetsByFundChannelBroadridgeFirm from '@salesforce/apex/ETFAssetsBreakdownController.getETFAssetsByFundChannelBroadridgeFirm';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const fundColumns = [
    { label: 'Fund Ticker Symbol', fieldName: 'fundTickerSymbol' },
    { label: 'Fund Name', fieldName: 'fundName' },
    { label: 'Asset Date', fieldName: 'assetsDate', type: 'date' },
    { label: 'Total Assets', fieldName: 'monthlyAssets', type: 'currency' },
    { label: 'Record Count', fieldName: 'recordCount', type: 'number' },
    {type: "button", typeAttributes: {  
        label: 'Channel',  
        name: 'channel',  
        title: 'Drill Down',  
        disabled: false,  
        value: 'channel',  
        iconPosition: 'right'  
    }}
];

const channelColumns = [
    //{ label: 'Fund Ticker Symbol', fieldName: 'fundTickerSymbol' },
    { label: 'Channel Name', fieldName: 'channelName' },
    { label: 'Asset Date', fieldName: 'assetsDate', type: 'date' },
    { label: 'Total Assets', fieldName: 'monthlyAssets', type: 'currency' },
    { label: 'Record Count', fieldName: 'recordCount', type: 'number' },
    {type: "button", typeAttributes: {  
        label: 'Firm',  
        name: 'firm',  
        title: 'Drill Down',  
        disabled: false,  
        value: 'firm',  
        iconPosition: 'right'  
    }}
];

const firmColumns = [
                        { label: 'Channel Name', fieldName: 'channelName' },
                        { label: 'Firm Name', fieldName: 'accountName' },
                        { label: 'Asset Date', fieldName: 'assetsDate', type: 'date' },
                        { label: 'Total Assets', fieldName: 'monthlyAssets', type: 'currency' },
                        {type: "button", typeAttributes: {  
                            label: 'Broadridge Firm',  
                            name: 'broadridgeFirm',  
                            title: 'Drill Down',  
                            disabled: false,  
                            value: 'broadridgeFirm',  
                            iconPosition: 'right'  
                        }}
                    ];

    const broadridgeColumns = [
                        { label: 'Firm Name', fieldName: 'accountName' },
                        { label: 'Asset Date', fieldName: 'assetsDate', type: 'date' },
                        { label: 'Total Assets', fieldName: 'monthlyAssets', type: 'currency' },
                        
                    ];

export default class LwcETFAssetsBreakdownView extends LightningElement {

    @track fundData = [];
    columns = fundColumns;

    @track
    myBreadcrumbs = [
         { label: 'ETF Assets Breakdown', name: 'parent', id: 'account1' },
        // { label: 'Child Account', name: 'child', id: 'account2' },
    ];

    fundTickerSymbol;
    channelName;
    firmName;
    // breadCrumbsMap = {
    //     parent: 'http://www.example.com/account1',
    //     child: 'http://www.example.com/account2',
    // };

    handleNavigateTo(event) {
        // prevent default navigation by href
        event.preventDefault();

        const name = event.target.name;

        if (this.breadCrumbsMap[name]) {
            //window.location.assign(this.breadCrumbsMap[name]);
        }
    }

    connectedCallback(){
        this.getAssetsFundWise();
    }  

    getAssetsFundWise() {
        getETFAssetsByFund({ assetsDate: null})
        .then(result =>{
            if (result) {
                this.fundData = result;
            } else {
                this.showNotification('Error','An error has occurred while loading data','error');
            }
        })
        .catch(error =>{
            this.showNotification('Error',error.body.message,'error');
        })
    }

    getAssetsFundChannelWise(fundId) {
        getETFAssetsByFundChannel({ assetsDate: null, fundId:fundId})
        .then(result =>{
            if (result) {
                this.columns = channelColumns;
                this.fundData = result;
            } else {
                this.showNotification('Error','An error has occurred while loading data','error');
            }
        })
        .catch(error =>{
            this.showNotification('Error',error.body.message,'error');
        })
    }

    getAssetsFundChannelFirmWise(fundId,channelName) {
        getETFAssetsByFundChannelFirm({ assetsDate: null, fundId:fundId , channelName:channelName})
        .then(result =>{
            if (result) {
                console.log(result);
                this.columns = firmColumns;
                this.fundData = result;
            } else {
                this.showNotification('Error','An error has occurred while loading data','error');
            }
        })
        .catch(error =>{
            this.showNotification('Error',error.body.message,'error');
        })
    }

    getAssetsFundChannelBroadridgeFirmWise(etfAssetsId) {
        getETFAssetsByFundChannelBroadridgeFirm({ assetsDate: null, etfAssetsByFundId:etfAssetsId })
        .then(result =>{
            if (result) {
                console.log(result);
                this.columns = broadridgeColumns;
                this.fundData = result;
            } else {
                this.showNotification('Error','An error has occurred while loading data','error');
            }
        })
        .catch(error =>{
            this.showNotification('Error',error.body.message,'error');
        })
    }

    callRowAction( event ) {  
        const recId =  event.detail.row.fundId;  
        const actionName = event.detail.action.name; 
        console.log('Rec ID '+recId);
        console.log('actionName '+actionName);
        let myBreadcrumbsTemp = this.myBreadcrumbs;
        if ( actionName === 'channel' ) {  
            this.getAssetsFundChannelWise(recId);
            //let crumb =  { label: 'Account', name: 'parent', id: 'account1' };
            myBreadcrumbsTemp = [
                { label: 'ETF Assets Breakdown', name: 'parent', id: 'account1' },
                { label: event.detail.row.fundTickerSymbol, name: 'child', id: 'account2' },
           ];
            this.fundTickerSymbol = event.detail.row.fundTickerSymbol;
            this.myBreadcrumbs = myBreadcrumbsTemp;
        } else if ( actionName === 'firm') {  
            const channel =  event.detail.row.channelName;  
            myBreadcrumbsTemp = [
                { label: 'ETF Assets Breakdown', name: 'parent', id: 'account1' },
                { label: this.fundTickerSymbol, name: 'child', id: 'account2' },
                { label: channel, name: 'child', id: 'account2' },
           ];
           this.channelName = channel;
            this.myBreadcrumbs = myBreadcrumbsTemp;
            this.getAssetsFundChannelFirmWise(recId,channel);
        }else if(actionName === 'broadridgeFirm'){
            const etfAssetsByFundId =  event.detail.row.etfAssetsByFundId;  
            this.getAssetsFundChannelBroadridgeFirmWise(etfAssetsByFundId);

            myBreadcrumbsTemp = [
                { label: 'ETF Assets Breakdown', name: 'parent', id: 'account1' },
                { label: this.fundTickerSymbol, name: 'child', id: 'account2' },
                { label: this.channelName, name: 'child', id: 'account2' },
                { label: event.detail.row.accountName, name: 'child', id: 'account2' },
           ];
          
            this.myBreadcrumbs = myBreadcrumbsTemp;

        }      
  
    }  

    showNotification(title,message,variant){
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
            }),
        );
    }

}