trigger preventDeleteSubscriptionGroup on Subscription_Group__c (before delete){
	List<Id> lstSub=new List<Id>();
	for(Subscription_Group__c sub:Trigger.old)
		lstSub.add(sub.Id);
	Subscription_Group_Subscription__c[] sgs=[Select Id,Name,Subscription_Group__c from Subscription_Group_Subscription__c where Subscription_Group__c IN:lstSub];
	if(sgs.size()>0){
		for(Subscription_Group__c subs:Trigger.old)
			subs.addError('You have attempted to delete the "'+subs.Name+'" subscription group. But there are '+sgs.size()
					+' subscription(s) associated with it. You must delete the related records before attempting to delete the master record.');
	}
}