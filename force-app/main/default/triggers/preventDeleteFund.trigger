trigger preventDeleteFund on Fund__c (before delete){
	List<Id> lstFund=new List<Id>();
	for(Fund__c sub:Trigger.old)
		lstFund.add(sub.Id);
	Fund_Interest__c[] funMem=[Select Id ,Account__c,Contact__c,Fund__c from Fund_Interest__c where Fund__c IN:lstFund];
	if(funMem.size()>0)
	{
		for(Fund__c Fund:Trigger.old)
			Fund.addError('You have attempted to delete the "'+Fund.Name+'" Fund. But there are '+funMem.size()+' Fund Interest(s) associated with it. '
							+'You must delete the related records before attempting to delete the master record.');
	}
}