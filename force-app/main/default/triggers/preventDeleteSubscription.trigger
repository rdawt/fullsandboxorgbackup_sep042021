trigger preventDeleteSubscription on Subscription__c (before delete){
	List<Id> lstSub=new List<Id>();
	for(Subscription__c sub:Trigger.old)
		lstSub.add(sub.Id);
	Subscription_Member__c[] subMem=[Select Id ,Name,Subscription__c from Subscription_Member__c where Subscription__c IN:lstSub];
	Subscription_Group_Subscription__c[] sgs=[Select Id,Name,Subscription__c from Subscription_Group_Subscription__c where Subscription__c IN:lstSub];
	if(subMem.size()>0 || sgs.size()>0){
		for(Subscription__c subs:Trigger.old)
			subs.addError('You have attempted to delete the "'+subs.Name+'" subscription. But there are '+subMem.size()+' subscription member(s) and '+sgs.size()
						+' subscription group(s) associated with it. You must delete the related records before attempting to delete the master record.');
	}
}