/*
Modified By : Vidhya on 10/11/13 - Exception Handling
*/
trigger MarketoContactTrigger on CampaignMember (Before Insert,Before Update) 
{
    Map<String,Contact> ContactsMap=new Map<String,Contact>();
    public string errMsg;
    Set<Id> conids=new Set<Id>();
    Set<Id> deleteconids=new Set<Id>();
    List<Contact> updatecons=new List<Contact>();
    List<Contact> deletecons=new List<Contact>();
    List<Contact> conList=new List<Contact>();
    public SendEmailNotification se = new SendEmailNotification('Error: Marketo Contact Trigger failed');
    
    try
    {
        // Update the contacts only if the campaign member is inserted or updates
        if(trigger.isinsert || trigger.isupdate)
        {
        system.debug('******insert********');
            // loop through all the newly inserted or updated campaign members 
            for(CampaignMember cm:Trigger.new)
            {
                // check to make sure whether the member is Contact or Lead
                if(cm.contactId!=null)
                    conids.add(cm.contactId);
            }
        }
       /* else if(trigger.isdelete) // Update the contacts only if the campaign member deleting
        {
            for(CampaignMember cm:Trigger.old)
            {
                // check to make sure whether the member is Contact or Lead
                if(cm.contactId!=null) 
                    deleteconids.add(cm.contactId);
            }
        }*/
        System.debug('*********conids****'+conids);
        System.debug('*********deleteconids****'+deleteconids);
        if(conids.size()>0)
        {
            conList = [Select Id,IsMemberOfCampaign__c from Contact where Id in : conids];
            // loop through all the member contacts
            for(Contact c:conList)
            {
                c.IsMemberOfCampaign__c=true; 
                updatecons.add(c);
            }
            System.debug('*********conids****'+updatecons);
        }
        
        /*if(deleteconids.size()>0)
        {
            for(Contact c:[Select Id,IsMemberOfCampaign__c from Contact where Id in : deleteconids])
            {
                c.IsMemberOfCampaign__c=false;
                deletecons.add(c);
            }
            System.debug('*********deleteconids****'+deletecons);
        }
        */
        if(updatecons.size()>0){
            Database.Update(updatecons,false); // update all the member contacts
            conList= [select id,IsMemberOfCampaign__c from Contact where  id in : conids];
            System.debug('*********after update****'+conList);
            }
        
     /*   if(deletecons.size()>0)
            Database.Update(deletecons,false); // update all the member contacts    */       
    }
    catch(Exception e)
    {
        errMsg = 'Exception Occured in Marketo Contact Trigger for the list of contacts : '+conids+'; \n and the error is : '+e.getMessage();
        se.sendEmail(errMsg);
    }
}