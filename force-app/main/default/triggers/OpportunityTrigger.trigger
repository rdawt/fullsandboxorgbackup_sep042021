trigger OpportunityTrigger on Opportunity (before insert, before update, after insert, after update) {

    if(TriggerOnOff__c.getOrgDefaults() != NULL && TriggerOnOff__c.getOrgDefaults().isActive_Opportunity__c == true)
    {
        OpportunityTriggerHandler objHandler = new OpportunityTriggerHandler();
        
        if(trigger.isInsert && trigger.isAfter){
            objHandler.onAfterInsert(Trigger.new);
        }

        if(trigger.isUpdate && trigger.isAfter){
            objHandler.onAfterUpdate(Trigger.new, Trigger.oldmap);
        }
    }
}