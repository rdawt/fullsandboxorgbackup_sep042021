/*  
    Author: Wilson Ng
    Date: 7/02/12
    Desc: Opportunity Consultant Trigger that handles creating Account Partner record.
    	If does not exist, create new Partner record as follows:
			Account  = opportunity_consultant__c.opportunity__c.accountId
			Partner = Opportunity_consultant__c.Firm_Name__c
			Role = 'Consultant' 
*/

trigger OpportunityConsultantTrigger on Opportunity_Consultant__c (after insert, after update) {
	
	SendEmailNotification se = new SendEmailNotification('Error: Opportunity Consultant Trigger failed');
	try{
		string ROLENAME = 'Consultant';		// FUTURE - make this a custom settings
	
		// gather all accountFrom and AccountTo ids
		// note - for accountFromIds, retrieve opportunity's parent account ids
		set<id> toAccts = new set<id>();
		map<id, id> fromOppAccts = new map<id, id>();
		for(Opportunity_Consultant__c oc : Trigger.new) {
			fromOppAccts.put(oc.Opportunity__c, null);
			toAccts.add(oc.Firm_Name__c);
		}
		
		// query opportunity parent account ids and store results into same map
		for(Opportunity opp : [select Id, AccountId from Opportunity where id in :fromOppAccts.keySet()]) {
			fromOppAccts.put(opp.Id, opp.AccountId);
		}
		
		// query partner for existing from/to combinations
		set<string> existingPartners = new set<string>();
		for(Partner p : [select AccountFromId, AccountToId from Partner where AccountFromId in :fromOppAccts.values() and AccountToId in :toAccts]) {
			existingPartners.add(p.AccountFromId + '-' + p.AccountToId);
		}
		
		// check for existing Partner record
		list<Partner> insertPartners = new list<Partner>();
		for(Opportunity_Consultant__c oc : Trigger.new) {
			// don't add duplicate Partner records
			// 9-4-2012 - also, don't add if firmid == oppty.acctid
			if(fromOppAccts.get(oc.Opportunity__c) != null
						&& fromOppAccts.get(oc.Opportunity__c) != oc.Firm_Name__c 
						&& !existingPartners.contains(fromOppAccts.get(oc.Opportunity__c) + '-' + oc.Firm_Name__c)) {
				insertPartners.add(new Partner(AccountFromId=fromOppAccts.get(oc.Opportunity__c), AccountToId=oc.Firm_Name__c, Role=ROLENAME));
				existingPartners.add(fromOppAccts.get(oc.Opportunity__c) + '-' + oc.Firm_Name__c);
			}
		}
		
		// insert new partner records
		if(insertPartners.size() > 0) {
			insert insertPartners;
		}
	}catch(Exception e){
		se.sendEmail('Exception Occurred in Opportunity Consultant Trigger : \n'+e.getMessage()+trigger.new);
	}
}