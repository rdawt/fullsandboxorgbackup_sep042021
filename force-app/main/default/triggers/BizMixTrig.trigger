trigger BizMixTrig on Business_Mix__c (after insert, after update, after delete) {
    Set<Id> contactIds = new Set<Id>();

    if (!Trigger.isDelete) {
        for (Business_Mix__c obj : Trigger.new) {
            contactIds.add(obj.Contact__c);
        }
    }

    if (!Trigger.isInsert) {
        for (Business_Mix__c obj : Trigger.oldMap.values()) {
            contactIds.add(obj.Contact__c);
        }
    }

    contactIds.remove(null);

    if (!contactIds.isEmpty()) {
        Map<Id, List<Business_Mix__c>> contactToObjMap = new Map<Id, List<Business_Mix__c>>();

        for (Id id : contactIds) {
            contactToObjMap.put(id, new List<Business_Mix__c>());
        }

        for (Business_Mix__c obj : [
            select Contact__c,
                Business_Mix__c
            from Business_Mix__c
            where Contact__c in :contactIds
        ]) {
            contactToObjMap.get(obj.Contact__c).add(obj);
        }   

        List<Contact> contactsToUpdate = new List<Contact>();
        String bizMixString='';

        for (Id id : contactIds) {
            Decimal spentSum = 0;
            Decimal balanceSum = 0;

            for (Business_Mix__c obj : contactToObjMap.get(id)) {
                if (bizMixString == null || bizMixString == '') {
                    bizMixString = obj.Business_Mix__c;
                   // spentSum += obj.Money_Spent__c;
                }

               else  {
                    bizMixString  = bizMixString  + ';' + obj.Business_Mix__c;
                }
            }

            //Decimal maxMoney = (spentSum + balanceSum) / 40;

            contactsToUpdate.add(new Contact(
                Id = id
                ,
                Business_Mix_View_Only__c = bizMixString
            ));
        }

        if (!contactsToUpdate.isEmpty()) {
            update contactsToUpdate;
        }
    }
}