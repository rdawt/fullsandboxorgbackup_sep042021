trigger LeadTrigger_Automations on Lead (before insert, after insert, before update, after update) 
{

    if(TriggerOnOff__c.getOrgDefaults() != NULL && TriggerOnOff__c.getOrgDefaults().isActive_Lead__c == true)
    {
        LeadTrigger_Handler objHandler = new LeadTrigger_Handler(Trigger.new);

        if(trigger.isUpdate && trigger.isBefore){
            objHandler.onBeforeUpdate(Trigger.new);
        }

        if(trigger.isUpdate && trigger.isAfter){
            objHandler.onAfterUpdate(Trigger.new, Trigger.oldmap);
        }
    }
}