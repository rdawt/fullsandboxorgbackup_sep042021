trigger DiscoveryTeamMemberTrigger on ASP_Team_Member__c (before insert, before update, after insert, after update) {

    if(TriggerOnOff__c.getOrgDefaults() != NULL && TriggerOnOff__c.getOrgDefaults().isActive_ASPTeamMember__c == true)
    {
        TeamMemberTriggerHandler objHandler = new TeamMemberTriggerHandler();
        
        if(trigger.isInsert && trigger.isAfter){
            objHandler.onAfterInsert(Trigger.new);
        }

        if(trigger.isUpdate && trigger.isAfter){
            objHandler.onAfterUpdate(Trigger.new, Trigger.oldmap);
        }
    }

}