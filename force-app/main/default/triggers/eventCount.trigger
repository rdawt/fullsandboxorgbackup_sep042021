trigger eventCount on Event(before insert, before update, after insert, after update, after delete,before delete) {
       
       
    if (trigger.isBefore && trigger.isInsert) {
        
         if(TriggerOnOff__c.getOrgDefaults() != NULL && TriggerOnOff__c.getOrgDefaults().isActive_Event__c == true)
         {
                updateEvent_ContactInformation(trigger.new, true);
        }
    }
    if (trigger.isBefore && trigger.isUpdate) {
         if(TriggerOnOff__c.getOrgDefaults() != NULL && TriggerOnOff__c.getOrgDefaults().isActive_Event__c == true)
         {
            updateEvent_ContactInformation(trigger.new, false);
         }
    }
    //Tharaka De Silva on 08-09-2020 ITSALES-1376
    //Added condition for safe deletion - allowed to delete Event owned by Event Owner from Australia profiles and Admins
    if (trigger.isBefore && trigger.isDelete) {
      if(TriggerOnOff__c.getOrgDefaults() != NULL && TriggerOnOff__c.getOrgDefaults().isActive_Event__c == true)
      {
          onBeforeDelete(Trigger.old);
      }
    }
    
    // This fucntion is updating the Task 4 field
    // ContactOwner__c
    // IsVEContact__c
    // IsTestContact__c
    // FirmName__c
    public void updateEvent_ContactInformation(List<Event> listNew, Boolean isInsert)
    {
        Set<Id> setWhoId = new Set<Id>();
        Map<Id, Contact> mapContact = new Map<Id, Contact>();
        
        for(Event objEvent : listNew)
        {
            if(objEvent.WhoId != NULL)
            {
                if( objEvent.WhoId.getSobjectType() == Schema.Contact.SObjectType)
                {
                    setWhoId.add(objEvent.WhoId);
                }
            }
        }
        
        if(!setWhoId.isEmpty())
        {
            for(Contact objContact : [Select Id,IS_User__c, IsTestContact__c, Account.Id, Account.Name from Contact where Id IN: setWhoId])
            {
                mapContact.put(objContact.Id, objContact);
            }
        }
        
        for(Event objEvent : listNew)
        {
            if(objEvent.WhoId != NULL)
            {
                if( objEvent.WhoId.getSobjectType() == Schema.Contact.SObjectType)
                {
                    objEvent.ContactOwner__c = objEvent.WhoId;
                    
                    if(mapContact.containskey(objEvent.WhoId))
                    {
                        objEvent.IsVEContact__c = mapContact.get(objEvent.WhoId).IS_User__c;
                        objEvent.IsTestContact__c = mapContact.get(objEvent.WhoId).IsTestContact__c;
                        
                        if(isInsert == true)
                            objEvent.FirmName__c = mapContact.get(objEvent.WhoId).Account.Name;
                    }
                }
            }
        }
    }
   
   //buggy  datetime currentDate = Date.today();
    date currentDate = Date.today();
    set<id> ids= new set<id>();
    set<id> userIds= new set<id>();
    
    List<User> addUserIds; //= new set<User>([Select id from User where id in : e.createdbyid and Year__c ]);
    
    if ((trigger.isDelete) && trigger.isAfter) {
      for(event e: trigger.old){
        //ids.addAll(trigger.newMap.accountid);
       if (((e.whoid != null) && (e.ownerid <> '005A00000032QAD')) || ((e.whoid != null) && (e.Marketing_Automation__c == false))) {
            system.debug('inside delete trigger condition - contact id'+e.whoid);
            ids.add(e.whoid);
            
            
           // addUserIds = ([Select id from User limit 10 ]);// where id in : e.createdbyid and Year__c in : String.ValueOf(currentDate.Year()) and Month__c in: String.ValueOf(currentDate.Month())
            userIds.add(e.createdbyid);
        }
     } 

    }
    else{
      if(!trigger.isDelete){
          for(event e: trigger.new){
          //ids.addAll(trigger.newMap.accountid);
          if (((e.whoid != null) && (e.ownerid <> '005A00000032QAD')) || ((e.whoid != null) && (e.Marketing_Automation__c == false)))  {
              ids.add(e.whoid);
              userIds.add(e.createdbyid);
          }

        } 
      }

    } 
    AggregateResult[] eCounts;  
    if (ids.size() != 0){
        eCounts  =[select  whoid, count(id) EventCount
                                  from Event
                                  where whoid in : ids and Marketing_Automation__c = false
                                   group by whoid];//
    
          system.debug('after agg. count- count is'+ eCounts);
                            
         list<Contact> conList= new list<Contact>();                             
         for(AggregateResult forLoopCount : eCounts){
           contact parentEventContact = new contact(Id=string.valueof(forLoopCount.get('whoId')),Event_Counter__c=integer.valueof(forLoopCount.get('eventCount')));

            conList.add(parentEventContact);
 
        }
    
    

        update conList;
    }

//this is for user object count

AggregateResult[] eventUsersCount; 
AggregateResult[] eventUsersQCount,IWS2XternalEventQCount,eventsQETFReferalCount,eventETFLeadsQCountFromETFSpls,eventETFLeadsQCountFromRIASpls    ; 

list<PayeeSummary__c> addPayeeSummaryList= new list<PayeeSummary__c>();
list<PayeeSummary__c> modPayeeSummaryList= new list<PayeeSummary__c>();

//Get Current Date


 
    if (userIds.size() != 0){
        eventUsersCount  =[select  createdbyid, sum(FA_Activity_Score__c ) FAActivityScore, sum(RIA_Activity_Score__c ) RIAActivityScore
                                  from Event
                                      where createdbyid in : userIds  and  Marketing_Automation__c = false and IsVEContact__c = false and IsTestContact__c = false and 
                                      createddate = THIS_MONTH and createddate = THIS_YEAR
                                           group by createdbyid];//
 
        
         
         eventUsersQCount  =[select  createdbyid, sum(FA_Activity_Score__c ) FAActivityQScore, sum(RIA_Activity_Score__c ) RIAActivityQScore
                                  from Event
                                      where createdbyid in : userIds  and  Marketing_Automation__c = false and IsVEContact__c = false and IsTestContact__c = false and 
                                      createddate = THIS_QUARTER and createddate = THIS_YEAR
                                           group by createdbyid];//
                                           
         //IWSEventsQCount
        
         IWS2XternalEventQCount = [SELECT count(id) IWS2XternalEventsQCount, createdbyid  FROM Event where (Subject = 'Call' or Subject = 'Conference call' or Subject = 'Conference' or Subject= 'Meeting' or subject = 'Meeting-Social')  
                                        and IsVEContact__c = false and IsTestContact__c = false
                                        and Assigned_To_User_Internal_External__c = 'External' and createddate = this_quarter and createdbyid in : userIds group by  createdbyid];
                                        //and whoid IN (Select id from Contact where IsTestContact__c =false and IS_User__c = false) // and id IN :ids
        //End IWS2XternalTaskQCount 
        
        //eventsQETFReferalCount  Count
        
         eventsQETFReferalCount =  [SELECT count(id) ETFQEventRefCount,createdbyid FROM EVENT where  createddate=  this_quarter 
                                        and IsVEContact__c = false and IsTestContact__c = false
                                      and ProfileIdCheck__c = True and (createdby.Internal_Sales_Score_Model__c != 'FA – ETF' AND createdby.Internal_Sales_Score_Model__c != 'RIA – ETF' ) and createdby.Internal_Sales_Score_Model__c != null
                                        and createdbyid in : userIds group by createdbyid];
        
        //eventsQETFReferalCount  Soql ends
        
         //ETF Leads from ETF Specialists
        
          eventETFLeadsQCountFromETFSpls = [SELECT count(id) eventETFLeadsCountByETFSpls, createdbyid FROM EVENT where OwnerInternalOrExternal__c = 'external'
                                                  and (createdby.Internal_Sales_Score_Model__c = 'FA – ETF' 
                                                        OR createdby.Internal_Sales_Score_Model__c = 'RIA – ETF') 
                                                        and IsVEContact__c = false and IsTestContact__c = false
                                                        and createddate = this_quarter 
                                                        and createdbyid in : userIds group by createdbyid];
                                                        
                                                        
        //End ETF leads from ETF Specialists  
        
        //ETF Leads from RIA Specialists
        // 
          eventETFLeadsQCountFromRIASpls = [SELECT count(id) eventETFLeadsCountByRIASpls, createdbyid FROM EVENT where (createdby.Internal_Sales_Score_Model__c = 'RIA – Active' and  OwnerModelName__c != 'RIA – Active') 
                                                 and ((OwnerModelName__c =  'FA –ETF' or OwnerModelName__c = 'RIA – ETF') Or etf_team__c != null)  
                                                 and IsVEContact__c = false and IsTestContact__c = false
                                                 and createddate = this_quarter 
                                                 and  createdbyid in : userIds group by createdbyid];
                                                        
                                                        
        //End ETF leads from RIA Specialists   
        
        //IWSEventsQCount
          
          if ( (eventUsersCount.size() == 0) && (eventUsersQCount.size() == 0) &&  (IWS2XternalEventQCount .size() == 0) &&  (eventsQETFReferalCount.size() == 0) && (eventETFLeadsQCountFromETFSpls.size() ==0) && (eventETFLeadsQCountFromRIASpls.size() ==0)  ){
             return;
         }                                 
         system.debug('**********eventUserCount!!*******************'+ eventUsersCount.size());
         system.debug('**********eventUserQCount!!*******************'+ eventUsersQCount.size());
        //system.debug('**********iWsEventQCount!!!!*******************'+ iWsEventsQCount.size());
         system.debug('**********IWS2XternalEventQCount !!!!*******************'+ IWS2XternalEventQCount.size());
         system.debug('**********EventQETFReferralCount    !!!!*******************'+ eventsQETFReferalCount.size());
         system.debug('**********EventETFLeadsQCountFromETFSpls !!!!*******************'+ eventETFLeadsQCountFromETFSpls.size());
         system.debug('**********EventETFLeadsQCountFromRIASpls !!!!*******************'+ eventETFLeadsQCountFromRIASpls.size());
         
          //for Q count of Score
         
          Map<Id, PayeeSummary__c> payeeListQScoreCountMap = new Map<Id, PayeeSummary__c>();
           
         for (AggregateResult scoreQLoopUsersCount  : eventUsersQCount ) //
         {
           
           string payeeId = string.valueof(scoreQLoopUsersCount.get('createdbyid'));
           integer scoreQFACounter = integer.valueof(scoreQLoopUsersCount.get('FAActivityQScore'));
           integer scoreQRIACounter = integer.valueof(scoreQLoopUsersCount.get('RIAActivityQScore'));
           PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId, FA_Q_Activity_Score__c = scoreQFACounter , TaskRIAQScore__c = scoreQRIACounter  );
           payeeListQScoreCountMap.put(payeeId,pSummary);
                                                     
         }
         
         
         //IWS2XternalEventQCount starts
         
          Map<Id, PayeeSummary__c> payeeListQIWS2XternalEventsCountMap = new Map<Id, PayeeSummary__c>();
           
         for (AggregateResult IWSEventQLoopUsersCount  : IWS2XternalEventQCount ) //
         {
           
           string payeeId = string.valueof(IWSEventQLoopUsersCount.get('createdbyid'));
           integer IWS2XternalQEventsCounter = integer.valueof(IWSEventQLoopUsersCount.get('IWS2XternalEventsQCount')); 
           system.debug('**********IWS2XternalEventQCount from query to variable !!!!*******************'+ IWS2XternalQEventsCounter );
           PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId, IWSToExternalEventsQCount__c= IWS2XternalQEventsCounter );
           payeeListQIWS2XternalEventsCountMap.put(payeeId,pSummary);
                                                     
         }
         
         
         //IWS2XternalEventQCount ends
         
         
         //eventsQETFReferralCount starts
         
          Map<Id, PayeeSummary__c> payeeListQETFReferralEventsCountMap = new Map<Id, PayeeSummary__c>();
           
         for (AggregateResult ETFEventReferralQLoopUsersCount  : eventsQETFReferalCount ) //
         {
           
           string payeeId = string.valueof(ETFEventReferralQLoopUsersCount.get('createdbyid'));
           integer ETFEventRefQCounter= integer.valueof(ETFEventReferralQLoopUsersCount.get('ETFQEventRefCount'));
           PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId, ETF_Event_Referral_Q_Count__c= ETFEventRefQCounter);
           payeeListQETFReferralEventsCountMap.put(payeeId,pSummary);
                                                     
         }
         
         
         //eventsQETFReferalCount ends
         
         //Start  ETF Leads to Xternals By ETF Splists
         
          Map<Id, PayeeSummary__c> payeeListQETFLeads4XEventsCountMap = new Map<Id, PayeeSummary__c>();
           
         for (AggregateResult ETFLeadsXEventQLoopUsersCount: eventETFLeadsQCountFromETFSpls ) //
         {
           
           string payeeId = string.valueof(ETFLeadsXEventQLoopUsersCount.get('createdbyid'));
           integer ETFLeads4Xter = integer.valueof(ETFLeadsXEventQLoopUsersCount.get('eventETFLeadsCountByETFSpls'));
           PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId, ETF_Ref_Q_Count_4_Xternals_Evts_ETF_Spl__c = ETFLeads4Xter );
           payeeListQETFLeads4XEventsCountMap.put(payeeId,pSummary);
                                                     
         }
          //Start  ETF Leads to Xternals By RIA Splists
         
          Map<Id, PayeeSummary__c> payeeListQRIALeads4XEventsCountMap = new Map<Id, PayeeSummary__c>();
           
         for (AggregateResult RIALeadsXEventQLoopUsersCount: eventETFLeadsQCountFromRIASpls ) //
         {
           
           string payeeId = string.valueof(RIALeadsXEventQLoopUsersCount.get('createdbyid'));
           integer RIALeads4Xter = integer.valueof(RIALeadsXEventQLoopUsersCount.get('eventETFLeadsCountByRIASpls'));
           PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId, ETF_Ref_Evts_Q_Count_4_Xternals_RIA_Spl__c = RIALeads4Xter );
           payeeListQRIALeads4XEventsCountMap.put(payeeId,pSummary);
                                                     
         }
         
         //End ETF Leads to Xternals By RIA Splists
         
        // Map<String,Integer> payeeListMap = new Map<String,Integer>();
         Map<Id, PayeeSummary__c> payeeListMap = new Map<Id, PayeeSummary__c>();
         Integer j =0;
      
         for(PayeeSummary__c payeeSummary: [Select id, user__c from PayeeSummary__c  Where user__c in : userIds and Month__c =: String.ValueOf(currentDate.Month()) and Year__c =: String.ValueOf(currentDate.Year()) ]){
            modPayeeSummaryList.add(payeeSummary);
            payeeListMap.put(payeeSummary.user__c,payeeSummary); // payeeSummary);
            system.debug('**********PayeeSummary Count!!*******************'+ payeeSummary);
            //payeeListMap.put(payeeSummary.User__c,j++);
         }
         

          //addPayeeSummaryList = [Select id, user__r.id from PayeeSummary__c  Where user__r.id in : userIds and Month__c !=: String.ValueOf(currentDate.Month()) and Year__c !=: String.ValueOf(currentDate.Year()) ];
          //modPayeeSummaryList = [Select id, user__r.id from PayeeSummary__c  Where user__r.id in : userIds and Month__c =: String.ValueOf(currentDate.Month()) and Year__c =: String.ValueOf(currentDate.Year()) ];
          
          system.debug('after agg. User-Payee FA Activity Score Sum- Payee List is'+ eventUsersCount  );
          //system.debug('after agg. User-Payee RIA Activity Score Sum- Sum is'+ RIAActivityScore);
                            
         list<PayeeSummary__c> addPayeeList = new list<PayeeSummary__c>(); 
         list<PayeeSummary__c> updatePayeeList = new list<PayeeSummary__c>(); 
         
         
                                 
         for(AggregateResult forLoopCount : eventUsersCount){
         
            system.debug('entering forloop to chase the matching payee ids'+ eventUsersCount  );

         
           if((payeeListMap != null) && payeeListMap.containsKey(string.valueof(forLoopCount.get('createdbyid'))))
           {
              integer fAScoreQCount = 0;
              integer rIAScoreQCount = 0;
            
              //integer IWSTaskQCount =0;
              integer IWS2XternalEventsQCount = 0;
             //Referrals Internal Wholesalers-https://na94.salesforce.com/00OA0000004sEPs
              integer ETFQEvent4RefIntWholeSalers = 0;
              integer ETFRef4Xter=0;
              integer RIARef4Xter=0;
              
             //get the index
              PayeeSummary__c index = payeeListMap.get(string.valueof(forLoopCount.get('createdbyid')));
              // If (index != null){
                   //do something if Payee  exists in CO for the same month and year already so use update instead of insert in the PayeeSummary CO
                   system.debug('inside else of **MODIFYING** new payee summary for the current month'+ eventUsersCount  );
                 //  payeeListMap.get(createdbyid);
                 //Q score count for events
                  if((payeeListQScoreCountMap!= null) && (  payeeListQScoreCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                       PayeeSummary__c qScoreIndex = payeeListQScoreCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                       fAScoreQCount = integer.valueOf(qScoreIndex.TaskFAQScore__c );
                       rIAScoreQCount = integer.valueOf(qScoreIndex.TaskRIAQScore__c );
              
                 }
                 //end Q score count for events
                 
                 //start IWSToExternalEventsQCount__c
                 
                 if((payeeListQIWS2XternalEventsCountMap != null) && (  payeeListQIWS2XternalEventsCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c qScoreIndex = payeeListQIWS2XternalEventsCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   IWS2XternalEventsQCount = integer.valueOf(qScoreIndex.IWSToExternalEventsQCount__c);
                   system.debug('IWSToExternalEventsQCount__c - Retrieved value from the map where we put the SOQL result is for the userid:'+ string.valueof(forLoopCount.get('createdbyid')) +  ' IWS2XternalEventsQCount : '+ IWS2XternalEventsQCount );
                   
                                
                 }
                 
                 
                 //end IWSToExternalEventsQCount__c
                  if((payeeListQETFReferralEventsCountMap != null) && (  payeeListQETFReferralEventsCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c qScoreIndex = payeeListQETFReferralEventsCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   ETFQEvent4RefIntWholeSalers  = integer.valueOf(qScoreIndex.ETF_Event_Referral_Q_Count__c);
                  
                                
                 }
                 if((payeeListQETFLeads4XEventsCountMap != null) && (  payeeListQETFLeads4XEventsCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c qScoreIndex = payeeListQETFLeads4XEventsCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   ETFRef4Xter = integer.valueOf(qScoreIndex.ETF_Ref_Q_Count_4_Xternals_Evts_ETF_Spl__c);
                                
                 }
                 if((payeeListQRIALeads4XEventsCountMap != null) && (  payeeListQRIALeads4XEventsCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c qScoreIndex = payeeListQRIALeads4XEventsCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   RIARef4Xter = integer.valueOf(qScoreIndex.ETF_Ref_Evts_Q_Count_4_Xternals_RIA_Spl__c);
                                
                 }
                 
                   PayeeSummary__c payeeSummary = new PayeeSummary__c(id=string.valueof( String.ValueOf(index.id)),Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),User__c=string.valueof(forLoopCount.get('createdbyid')),FA_Activity_Score__c =integer.valueof(forLoopCount.get('FAActivityScore')),FA_Q_Activity_Score__c = fAScoreQCount , RIA_Activity_Score__c =integer.valueof(forLoopCount.get('RIAActivityScore')),RIA_Q_Activity_Score__c = rIAScoreQCount,IWSToExternalEventsQCount__c = IWS2XternalEventsQCount, ETF_Event_Referral_Q_Count__c =  ETFQEvent4RefIntWholeSalers ,ETF_Ref_Q_Count_4_Xternals_Evts_ETF_Spl__c = ETFRef4Xter, ETF_Ref_Evts_Q_Count_4_Xternals_RIA_Spl__c = RIARef4Xter);
                   updatePayeeList.add(payeeSummary);                   
               //}
               
            }
            else{
                
                integer fAScoreQCount = 0;
                integer rIAScoreQCount = 0;
                integer IWS2XternalEventsQCount = 0;
                 //Referrals Internal Wholesalers-https://na94.salesforce.com/00OA0000004sEPs
                integer ETFQEvent4RefIntWholeSalers = 0;
                integer ETFRef4Xter=0;
                integer RIARef4Xter =0;
                
                system.debug('inside else of **ADDING** new payee summary for the current month'+ eventUsersCount  );
                
                 if((payeeListQScoreCountMap!= null) && (  payeeListQScoreCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c qScoreIndex = payeeListQScoreCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   fAScoreQCount = integer.valueOf(qScoreIndex.TaskFAQScore__c );
                   rIAScoreQCount = integer.valueOf(qScoreIndex.TaskRIAQScore__c );
              
                 }
                 if((payeeListQIWS2XternalEventsCountMap != null) && (  payeeListQIWS2XternalEventsCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c qScoreIndex = payeeListQIWS2XternalEventsCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   IWS2XternalEventsQCount = integer.valueOf(qScoreIndex.IWSToExternalEventsQCount__c);
                                
                 }
                 
                 
                 
                 if((payeeListQETFReferralEventsCountMap != null) && (  payeeListQETFReferralEventsCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c qScoreIndex = payeeListQETFReferralEventsCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   ETFQEvent4RefIntWholeSalers  = integer.valueOf(qScoreIndex.ETF_Event_Referral_Q_Count__c);
                                
                 }
                 if((payeeListQETFLeads4XEventsCountMap != null) && (  payeeListQETFLeads4XEventsCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c qScoreIndex = payeeListQETFLeads4XEventsCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   ETFRef4Xter = integer.valueOf(qScoreIndex.ETF_Ref_Q_Count_4_Xternals_Evts_ETF_Spl__c);
                                
                 }
             
                 
               if((payeeListQRIALeads4XEventsCountMap != null) && (  payeeListQRIALeads4XEventsCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                           
                   PayeeSummary__c qScoreIndex = payeeListQRIALeads4XEventsCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                   RIARef4Xter = integer.valueOf(qScoreIndex.ETF_Ref_Evts_Q_Count_4_Xternals_RIA_Spl__c);
                                
                }
                PayeeSummary__c payeeSummary = new PayeeSummary__c(Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),User__c=string.valueof(forLoopCount.get('createdbyid')),FA_Activity_Score__c =integer.valueof(forLoopCount.get('FAActivityScore')),FA_Q_Activity_Score__c = fAScoreQCount ,RIA_Activity_Score__c =integer.valueof(forLoopCount.get('RIAActivityScore')),RIA_Q_Activity_Score__c = rIAScoreQCount,IWSToExternalEventsQCount__c = IWS2XternalEventsQCount, ETF_Event_Referral_Q_Count__c =  ETFQEvent4RefIntWholeSalers ,ETF_Ref_Q_Count_4_Xternals_Evts_ETF_Spl__c = ETFRef4Xter , ETF_Ref_Evts_Q_Count_4_Xternals_RIA_Spl__c = RIARef4Xter  );
                addPayeeList.add(payeeSummary);
            
            }
         
              

                
        }
            
         
 
        
    
        if (updatePayeeList.size() != 0) {
            update updatePayeeList;
        }
        if (addPayeeList.size() != 0) {
           // insert addPayeeList;
           if(Test.isRunningTest()){
                for(PayeeSummary__c objPayeeSummary : addPayeeList)
                {
                    objPayeeSummary.Total_Days_for_this_Calendar_Month__c = 30;
                    objPayeeSummary.No_Of_Days_in_Office__c = 25;
                }
                  
              }
              insert addPayeeList;
        }
        
    }

    //Tharaka De Silva on 08-09-2020 ITSALES-1376
    //Added condition for safe deletion - allowed to delete Tasks owned by Task Owner from Australia profiles and Admins
    public void onBeforeDelete(List<Event> lstOldEvents){ 
        Activity_Validate_Settings__c settings = Activity_Validate_Settings__c.getOrgDefaults();
        if(settings.Is_Event_Delete_Logic_Active__c){
            Set<Id> setProfileIds = new Set<Id>();
            Set<Id> setUserIds = new Set<Id>();
            Id currentUserId = UserInfo.getUserId();
            Id currentUserProfileId = UserInfo.getProfileId();
            
            if(settings.Event_Delete_Profiles__c != null){
                Set<String> setProfileNames = new Set<String>();
                setProfileNames.addAll(settings.Event_Delete_Profiles__c.split(','));
                for(Profile pr : [SELECT Id FROM profile WHERE Name In :setProfileNames]){
                    setProfileIds.add(pr.Id);
                }
            }
            if(settings.Event_Delete_Users__c != null){
                Set<String> setUserNames = new Set<String>();
                setUserNames.addAll(settings.Event_Delete_Users__c.split(','));
                for(User ur : [SELECT Id FROM User WHERE Name In :setUserNames]){
                    setUserIds.add(ur.Id);
                }
            }     
            Profile pfAdmin= [SELECT Id FROM profile WHERE Name='System Administrator' LIMIT 1]; 

            for (Event oldEvent : lstOldEvents){
                //To delete the event : Current user profile should be equal to given profile from custom settings (Ex: Australia) and current user should be equal to event owner.
                //Also, allowed delete to special event owner records which are configured in Custom settings(Ex: marketo user) if only that user has enough permissions to delete that event
                if (setProfileIds.contains(currentUserProfileId) && (oldEvent.OwnerId == currentUserId || setUserIds.contains(oldEvent.OwnerId))){
                    return;
                }else if(currentUserProfileId == pfAdmin.Id){
                    return;
                }else {
                    oldEvent.addError(System.Label.Error_Message_Event_Delete_Validation);
                }
            }
        }
    }
   

}