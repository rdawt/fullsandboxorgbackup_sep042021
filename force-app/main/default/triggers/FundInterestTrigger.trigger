/*
    Author: Vidhya Krishnan
    Date: 09/19/12
    Description: Fund Interest Insert, Update, Deletion, UnDelete & Exception handled.
            Fund Interest Indicator fields in contact is updated.
            Fund Interest is made as child object of contact. So rollup summery is automated - no fund intr. count coding needed
*/

trigger FundInterestTrigger on Fund_Interest__c (after delete, after insert, after undelete, after update) {
   
    //buggy datetime currentDate = Date.today();
    date currentDate = Date.today();
    
    set<id> userIds= new set<id>();
    
    SendEmailNotification se = new SendEmailNotification('Error: Fund Interest Trigger failed');    
    try
    {
        Set<Id> lstIds =new Set<Id>(); // To store contact Ids
        Map<String,string> etfPosMap = new Map<String,string>(); // To store ETF & ETN +ve interest
        Map<String,string> etfNegMap = new Map<String,string>(); // To store ETF & ETN -ve interest
        Map<String,string> mfPosMap = new Map<String,string>(); // To store MF,GI,FI_ETF,SEP,HEDGE,WWIT and other +ve interest
        Map<String,string> mfNegMap = new Map<String,string>(); // To store MF,GI,FI_ETF,SEP,HEDGE,WWIT and other -ve interest
        List<Contact> conList = new List<Contact>();
        List<Fund_Interest__c> fndIntList = new List<Fund_Interest__c>();
        String strIndr1,strIndr2,strIndr3,strIndr4,s1,s2,s3,s4;
        Map<Id, String> mapExternalUserCheckPresent = new Map<Id, String>();
        Map<Id, String> mapExternalUser = new Map<Id, String>();
        
        Map<String, FI_Q_External_Criteria__mdt> mapFI_Q_External_Criteria = new Map<String, FI_Q_External_Criteria__mdt>();
        for(FI_Q_External_Criteria__mdt objFI_Q_External_Criteria : [Select Id, MasterLabel, Firm_Channel__c, Contact_Mailing_State__c from FI_Q_External_Criteria__mdt])
        {
            mapFI_Q_External_Criteria.put(objFI_Q_External_Criteria.MasterLabel, objFI_Q_External_Criteria);
        }
        
        for(User objUser : [Select Id, Name, User_Territory__c, User_Internal_External__c from User where User_Internal_External__c = 'External'])
        {
            if(objUser.User_Territory__c == 'ETF RIA East')
                mapExternalUserCheckPresent.put(objUser.Id, objUser.User_Territory__c);
            else if(objUser.User_Territory__c == 'ETF RIA West')
                mapExternalUserCheckPresent.put(objUser.Id, objUser.User_Territory__c);
            else if(objUser.User_Territory__c == 'ETF BD West')
                mapExternalUserCheckPresent.put(objUser.Id, objUser.User_Territory__c);
            else if(objUser.User_Territory__c == 'ETF BD East')
                mapExternalUserCheckPresent.put(objUser.Id, objUser.User_Territory__c);
            else if(objUser.User_Territory__c == 'ETF Inst')
                mapExternalUserCheckPresent.put(objUser.Id, objUser.User_Territory__c);
        }
        
        //  if Delete fired this trigger, consider the old set of records before deletion for updating the count
        if (Trigger.isDelete)
            fndIntList = Trigger.old;
        else if(Trigger.isInsert || Trigger.isUnDelete)
        {
            fndIntList = Trigger.new;
            for(Fund_Interest__c fI: trigger.new)
            {
                userIds.add(fI.LastModifiedById);
                if(!mapExternalUserCheckPresent.isEmpty())
                {
                    if(mapExternalUserCheckPresent.containskey(fI.LastModifiedById))
                        mapExternalUser.put(fI.LastModifiedById, mapExternalUserCheckPresent.get(fI.LastModifiedById));
                }
            }
        }

        if(fndIntList != null)
            for(Fund_Interest__c fndInt:fndIntList)
                    lstIds.add(fndInt.Contact__c);

        if(Trigger.isUpdate)
        {
            Set<String> c = new Set<String>();
            for(Fund_Interest__c fndInt:Trigger.new)
            {
                // consider only if there is actual update
                if (((fndInt.Level_Of_Interest__c != Trigger.oldMap.get(fndInt.Id).Level_Of_Interest__c)) || (((fndInt.Fund_Client__c))!= Trigger.oldMap.get(fndInt.Id).Fund_Client__c))
                // || ((fndInt.Fund_Client__c)!= Trigger.oldMap.get(fndInt.Id)))
                        lstIds.add(fndInt.Contact__c);
                        userIds.add(fndInt.LastModifiedById);
                
                if(!mapExternalUserCheckPresent.isEmpty())
                {
                    if(mapExternalUserCheckPresent.containskey(fndInt.LastModifiedById))
                        mapExternalUser.put(fndInt.LastModifiedById, mapExternalUserCheckPresent.get(fndInt.LastModifiedById));
                }        
            }
        }
        system.debug('List of Contact in the Fund Interest trigger.....'+lstIds);
        
        FundInterestManagement fim = new FundInterestManagement();
        string result = fim.updateContact(lstIds);
        
        if(!result.equals('true'))
        {
            se.sendEmail(result);
        }
        //STart here
                
        try
        {
            //this is for user object count
    
            AggregateResult[] fIMonthlyCount; 
            AggregateResult[] fIQCount;
            List<AggregateResult> fIQCountExternal = new List<AggregateResult>();
            
            list<PayeeSummary__c> addPayeeSummaryList= new list<PayeeSummary__c>();
            list<PayeeSummary__c> modPayeeSummaryList= new list<PayeeSummary__c>();
        
            //Get Current Date
         
            if (userIds.size() != 0)
            {
                fIMonthlyCount  =[SELECT count(id) fInterestMonthCount, LastModifiedById from fund_interest__c
                                    where LastModifiedById in : userIds and lastmodifieddate = THIS_MONTH and lastmodifieddate = THIS_YEAR 
                                    group by LastModifiedById]; 
         
                if(fIMonthlyCount.size() == 0)
                {
                      system.debug('**********Inside fIMonthlyCount size check as ZERO*******************'+ fIMonthlyCount.size());
                     //return;
                }
                
                Map<Id, PayeeSummary__c> payeeListfIQCountMap= new Map<Id, PayeeSummary__c>();
                //Internals only
                //fIQCount= [SELECT count(id) fInterestQuarterCount,LastModifiedById  from fund_interest__c
                //                where LastModifiedById  in : userIds and createddate = THIS_QUARTER and createddate = THIS_YEAR
                //                group by LastModifiedById ]; 
                 //LastModifiedById  and created date is changed to LastModifiedById and lastmodifieddate               
                 fIQCount= [SELECT count(id) fInterestQuarterCount,LastModifiedById from fund_interest__c
                                where LastModifiedById in : userIds and LastModifiedDate = THIS_QUARTER and LastModifiedDate = THIS_YEAR
                                group by LastModifiedById];
                
                for (AggregateResult fInterestQuarter2Count : fIQCount) 
                {
                    //string payeeId = string.valueof(fInterestQuarter2Count.get('LastModifiedById '));
                    string payeeId = string.valueof(fInterestQuarter2Count.get('LastModifiedById'));
                    integer fIQCounter = integer.valueof(fInterestQuarter2Count.get('fInterestQuarterCount'));
                    PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId,ThisQuarterFICount__c = fIQCounter);
                    payeeListfIQCountMap.put(payeeId,pSummary);
                }
         
                if(fIQCount.size() == 0)
                {
                    system.debug('**********Inside fIQCountsize check as ZERO*******************'+ fIQCount.size());
                    //return;
                }
                
                Map<Id, PayeeSummary__c> payeeListfIQCountExternalMap= new Map<Id, PayeeSummary__c>();
                
                // FI Q Count for External
                if(!mapExternalUser.isEmpty())
                {
                    fIQCountExternal =  getAggregateResultForUser(mapExternalUser,mapFI_Q_External_Criteria);
                        
                    for (AggregateResult fInterestQuarter2CountExternal : fIQCountExternal) 
                    {
                        system.debug('>>>>>>>>>>>>>>>>fInterestQuarter2CountExternal>>>>>>>>>>>>>>>' + fInterestQuarter2CountExternal);
                        string payeeId = string.valueof(fInterestQuarter2CountExternal.get('LastModifiedById'));
                        integer fIQCounterExternal = integer.valueof(fInterestQuarter2CountExternal.get('ThisQFICountXternal'));
                        PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId,ThisQuarterFICount_External__c = fIQCounterExternal);
                        payeeListfIQCountExternalMap.put(payeeId,pSummary);
                    }
                }
                
                /*
                
                */
         
                if(fIQCountExternal.size() == 0)
                {
                    system.debug('**********Inside fIQCountsize check as ZERO*******************'+ fIQCountExternal.size());
                    //return;
                }
                 
                 system.debug('**********fIQCountsize!!*******************'+ fIQCount.size());
                // Map<String,Integer> payeeListMap = new Map<String,Integer>();
                Map<Id, PayeeSummary__c> payeeListMap = new Map<Id, PayeeSummary__c>();
                Integer j =0;
                for(PayeeSummary__c payeeSummary: [Select id, user__c,EmailSubscription__c, LastModifiedById 
                                                    from PayeeSummary__c  
                                                    Where user__c in : userIds and Month__c =: String.ValueOf(currentDate.Month()) 
                                                        and Year__c =: String.ValueOf(currentDate.Year()) ])
                {
                    modPayeeSummaryList.add(payeeSummary);
                    //added user who are already present in payeesummary object for current month
                    payeeListMap.put(payeeSummary.user__c,payeeSummary); // payeeSummary);
                    system.debug('**********PayeeSummary Count!!*******************'+ payeeSummary);
                    //payeeListMap.put(payeeSummary.User__c,j++);
                }
                 
                //addPayeeSummaryList = [Select id, user__r.id from PayeeSummary__c  Where user__r.id in : userIds and Month__c !=: String.ValueOf(currentDate.Month()) and Year__c !=: String.ValueOf(currentDate.Year()) ];
                //modPayeeSummaryList = [Select id, user__r.id from PayeeSummary__c  Where user__r.id in : userIds and Month__c =: String.ValueOf(currentDate.Month()) and Year__c =: String.ValueOf(currentDate.Year()) ];
                  
                system.debug('after agg. User-Payee fIMonthlyCount  - Payee List is'+ fIMonthlyCount );
                                    
                list<PayeeSummary__c> addPayeeList = new list<PayeeSummary__c>(); 
                list<PayeeSummary__c> updatePayeeList = new list<PayeeSummary__c>(); 
                 
                for(AggregateResult forLoopCount : fIMonthlyCount  )
                {
                    system.debug('entering forloop to chase the matching payee ids'+ fIMonthlyCount );
                    
                    if((payeeListMap != null) && (  payeeListMap.containsKey(string.valueof(forLoopCount.get('LastModifiedById')))) ) //  ||  payeeListMap.containsKey(string.valueof(forLoopCount.get('LastModifiedById'))))  
                    {
                        //get the index
                        integer qFICount=0;
                        integer qFICountExternal=0;
                         
                        PayeeSummary__c index = payeeListMap.get(string.valueof(forLoopCount.get('LastModifiedById')));
                        If (index != null)
                        {
                            //do something if Payee  exists in CO for the same month and year already so use update instead of insert in the PayeeSummary CO
                            system.debug('inside else of **MODIFYING-index-for LastModifiedById ** new payee summary for the current month'+ fIMonthlyCount );
                            // payeeListMap.get(LastModifiedById );
                            if((payeeListfIQCountMap!= null) && (  payeeListfIQCountMap.containsKey(string.valueof(forLoopCount.get('LastModifiedById')))) )
                            {
                                PayeeSummary__c qIndex = payeeListfIQCountMap.get(string.valueof(forLoopCount.get('LastModifiedById')));
                                qFICount = integer.valueOf(qIndex.ThisQuarterFICount__c );
                            
                            }

                            if((payeeListfIQCountExternalMap!= null) && (  payeeListfIQCountExternalMap.containsKey(string.valueof(forLoopCount.get('LastModifiedById')))) )
                            {
                                PayeeSummary__c qIndex = payeeListfIQCountExternalMap.get(string.valueof(forLoopCount.get('LastModifiedById')));
                                qFICountExternal = integer.valueOf(qIndex.ThisQuarterFICount_External__c );
                            
                            }
                                                        
                            PayeeSummary__c payeeSummary = new PayeeSummary__c(id=string.valueof( String.ValueOf(index.id)),
                                                                                            Year__c=String.ValueOf(currentDate.Year()),
                                                                                            Month__c=String.ValueOf(currentDate.Month()),
                                                                                            User__c=string.valueof(forLoopCount.get('LastModifiedById')),
                                                                                            ThisMonthFICount__c =integer.valueof(forLoopCount.get('fInterestMonthCount')), 
                                                                                            ThisQuarterFICount__c = qFICount,
                                                                                            ThisQuarterFICount_External__c = qFICountExternal  );
                            updatePayeeList.add(payeeSummary); 
                        }
                    }
                    else
                    {
                        integer qFICount=0;
                        system.debug('inside else of **ADDING** new payee summary for the current month'+ fIMonthlyCount );
                        if((payeeListfIQCountMap!= null) && (  payeeListfIQCountMap.containsKey(string.valueof(forLoopCount.get('LastModifiedById')))) )
                        {
                               PayeeSummary__c qIndex = payeeListfIQCountMap.get(string.valueof(forLoopCount.get('LastModifiedById')));
                               qFICount = integer.valueOf(qIndex.ThisQuarterFICount__c );
                        }
                        
                        PayeeSummary__c payeeSummary = new PayeeSummary__c(Year__c=String.ValueOf(currentDate.Year()),
                                                                            Month__c=String.ValueOf(currentDate.Month()),
                                                                            User__c=string.valueof(forLoopCount.get('LastModifiedById')), 
                                                                            ThisMonthFICount__c =integer.valueof(forLoopCount.get('fInterestMonthCount')), 
                                                                            ThisQuarterFICount__c = qFICount  );
                        addPayeeList.add(payeeSummary);
                    }
                }
        
                if (updatePayeeList.size() != 0) 
                {
                    update updatePayeeList;
                }
                
                if (addPayeeList.size() != 0) {
                    insert addPayeeList;
                }
            }
        }
        catch(Exception e)
        {
            String strErrorBody = '';
            strErrorBody = 'Exception Line Number : ' + e.getLineNumber() + ' \n ' + 
                            'Exception Type Name : ' + e.getTypeName() +  ' \n ' + 
                            'Exception Stack Tracing : ' + e.getStackTraceString() + ' \n ' + 
                            'Exception Message : ' + e.getMessage();
                              
            se.sendEmailMoreDetails(strErrorBody);
        }
        //End here
    }
    catch(Exception e)
    {
        se.sendEmail('Exception Occurred \n'+e.getMessage()+trigger.new);
    }
    
    public List<AggregateResult> getAggregateResultForUser(Map<Id, String> mapExternalUser, map<String, FI_Q_External_Criteria__mdt> mapFI_Q_External_Criteria)
    {
        List<AggregateResult> fInterestQuarter2CountExternal = new List<AggregateResult>();

        //Get Current Date
        Integer intMonth = Integer.valueOf(PayeeSummaryBatch_Criteria__c.getOrgDefaults().Month__c);
        Integer intYear = Integer.valueOf(PayeeSummaryBatch_Criteria__c.getOrgDefaults().Year__c);
        Integer intQuarter = Integer.valueOf(PayeeSummaryBatch_Criteria__c.getOrgDefaults().Quarter__c);
        
        for(Id objUserId : mapExternalUser.keyset())
        {
            String strUserTerritory = mapExternalUser.get(objUserId);
            
            // Patrick Schramm,RIA ETF SPECIALIST EAST, Firm Channel: RIA or AM and the mailing states listed above
            if(strUserTerritory == 'ETF RIA East')
            {
                List<String> lstFirmChannel = new List<String>();
                List<String> lstMailingState  = new List<String>();
                lstFirmChannel.addAll(mapFI_Q_External_Criteria.get('ETF RIA East').Firm_Channel__c.split(','));
                lstMailingState.addAll(mapFI_Q_External_Criteria.get('ETF RIA East').Contact_Mailing_State__c.split(','));
                
                system.debug('>>>>>>>>>>>>>>>I am in ETF RIA East>>>>>>>>>>>>>>>>>>>');
                fInterestQuarter2CountExternal.addAll([SELECT count(id) ThisQFICountXternal, LastModifiedById  
                                                    FROM Fund_Interest__c 
                                                    Where LastModifiedById =:objUserId and LastmodifiedBy.User_Territory__c ='ETF RIA East' 
                                                    and CALENDAR_QUARTER(lastmodifiedDate ) =: intQuarter and CALENDAR_YEAR(lastmodifiedDate) =: intYear 
                                                    and contact__c <> null  and contact__r.Account.id <> null 
                                                    and contact__r.IsTestContact__c = false and contact__r.IS_User__c =false
                                                    and firm_channel__c in: lstFirmChannel
                                                    and contact_mailing_state__c in: lstMailingState
                                                    group by LastModifiedById]);
                system.debug('>>>>>>>>>>>>>>>I am in ETF RIA East>>>>>>>>>>>>>>>>>>>' + fInterestQuarter2CountExternal);                                    
            }
            // RIA ETF SPECIALIST WEST, Firm channel AM or RIA - Kol Estreicher
            else if(strUserTerritory == 'ETF RIA West')
            {
                List<String> lstFirmChannel = new List<String>();
                List<String> lstMailingState  = new List<String>();
                lstFirmChannel.addAll(mapFI_Q_External_Criteria.get('ETF RIA West').Firm_Channel__c.split(','));
                lstMailingState.addAll(mapFI_Q_External_Criteria.get('ETF RIA West').Contact_Mailing_State__c.split(','));
                
                system.debug('>>>>>>>>>>>>>>>I am in ETF RIA West>>>>>>>>>>>>>>>>>>>');
                fInterestQuarter2CountExternal.addAll([SELECT count(id) ThisQFICountXternal, LastModifiedById  
                                                    FROM Fund_Interest__c 
                                                    Where LastModifiedById =:objUserId and LastmodifiedBy.User_Territory__c ='ETF RIA West' 
                                                    and CALENDAR_QUARTER(lastmodifiedDate) =: intQuarter and CALENDAR_YEAR(lastmodifiedDate) =: intYear 
                                                    and contact__c <> null  and contact__r.Account.id <> null 
                                                    and contact__r.IsTestContact__c = false and contact__r.IS_User__c =false
                                                    and firm_channel__c in: lstFirmChannel
                                                    and contact_mailing_state__c in: lstMailingState
                                                    group by LastModifiedById]);
                system.debug('>>>>>>>>>>>>>>>I am in ETF RIA West>>>>>>>>>>>>>>>>>>>' + fInterestQuarter2CountExternal);                                                    
            }
            // Michael Gandy ETF BD West; Firm channel – Financial Advisor  
            else if(strUserTerritory == 'ETF BD West')
            {
                List<String> lstFirmChannel = new List<String>();
                List<String> lstMailingState  = new List<String>();
                lstFirmChannel.addAll(mapFI_Q_External_Criteria.get('ETF BD West').Firm_Channel__c.split(','));
                lstMailingState.addAll(mapFI_Q_External_Criteria.get('ETF BD West').Contact_Mailing_State__c.split(','));
                
                system.debug('>>>>>>>>>>>>>>>I am in ETF BD West>>>>>>>>>>>>>>>>>>>');
                fInterestQuarter2CountExternal.addAll([SELECT count(id) ThisQFICountXternal, LastModifiedById   
                                                    FROM Fund_Interest__c 
                                                    Where LastModifiedById =:objUserId and LastmodifiedBy.User_Territory__c like 'ETF BD West' 
                                                    and CALENDAR_QUARTER(lastmodifiedDate) =: intQuarter and CALENDAR_YEAR(lastmodifiedDate) =: intYear 
                                                    and contact__c <> null  and contact__r.Account.id <> null 
                                                    and contact__r.IsTestContact__c = false and contact__r.IS_User__c =false
                                                    and firm_channel__c in: lstFirmChannel
                                                    and contact_mailing_state__c in: lstMailingState
                                                    group by LastModifiedById]);
                system.debug('>>>>>>>>>>>>>>>I am in ETF BD West>>>>>>>>>>>>>>>>>>>' + fInterestQuarter2CountExternal);                                       
            } 
            // Philip McKeon ETF BD East??= user territory field in User record for Phil is null or has no value as of 10/25/2019 1:54 pm ET
            else if(strUserTerritory == 'ETF BD East')
            {
                List<String> lstFirmChannel = new List<String>();
                List<String> lstMailingState  = new List<String>();
                lstFirmChannel.addAll(mapFI_Q_External_Criteria.get('ETF BD East').Firm_Channel__c.split(','));
                lstMailingState.addAll(mapFI_Q_External_Criteria.get('ETF BD East').Contact_Mailing_State__c.split(','));
                
                system.debug('>>>>>>>>>>>>>>>I am in ETF BD East>>>>>>>>>>>>>>>>>>>');
                fInterestQuarter2CountExternal.addAll([SELECT count(id) ThisQFICountXternal, LastModifiedById  
                                                    FROM Fund_Interest__c 
                                                    Where LastModifiedById =:objUserId and LastmodifiedBy.User_Territory__c like 'ETF BD East' 
                                                    and CALENDAR_QUARTER(lastmodifiedDate) =: intQuarter and CALENDAR_YEAR(lastmodifiedDate) =: intYear 
                                                    and contact__c <> null  and contact__r.Account.id <> null 
                                                    and contact__r.IsTestContact__c = false and contact__r.IS_User__c =false
                                                    and firm_channel__c in: lstFirmChannel
                                                    and contact_mailing_state__c in: lstMailingState
                                                    group by LastModifiedById]);
                system.debug('>>>>>>>>>>>>>>>I am in ETF BD East>>>>>>>>>>>>>>>>>>>' + fInterestQuarter2CountExternal);                                      
            } 
            // Patrick Finn User Territory- ETF Inst- Firm channel : Institutional
            else if(strUserTerritory == 'ETF Inst')
            {
                List<String> lstFirmChannel = new List<String>();
                lstFirmChannel.addAll(mapFI_Q_External_Criteria.get('ETF Inst').Firm_Channel__c.split(','));

                
                system.debug('>>>>>>>>>>>>>>>I am in ETF Inst>>>>>>>>>>>>>>>>>>>');
                fInterestQuarter2CountExternal.addAll([SELECT count(id) ThisQFICountXternal, LastModifiedById  
                                                    FROM Fund_Interest__c 
                                                    Where LastModifiedById =:objUserId and LastmodifiedBy.User_Territory__c like 'ETF Inst' 
                                                    and CALENDAR_QUARTER(lastmodifiedDate) =: intQuarter and CALENDAR_YEAR(lastmodifiedDate) =: intYear 
                                                    and contact__c <> null  and contact__r.Account.id <> null 
                                                    and contact__r.IsTestContact__c = false and contact__r.IS_User__c =false 
                                                    and firm_channel__c in: lstFirmChannel
                                                    group by LastModifiedById]);
                system.debug('>>>>>>>>>>>>>>>I am in ETF Inst>>>>>>>>>>>>>>>>>>>' + fInterestQuarter2CountExternal);                                        
            }
        }
        
        return fInterestQuarter2CountExternal;      
        
    }
}