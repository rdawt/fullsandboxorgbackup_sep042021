/**
      Author: Vidhya Krishnan
      Date Created: 04/04/2012
      Description: Community Registration Trigger that 
            1. updates contact id in the corresponding community member record
            2. updates hyperlink field in contact with the corresponding registrant id
            3. delete related community member when registrant is deleted 
*/
trigger RegistrantTrigger on Community_Registrant__c (before insert,before update,after insert,after update, before delete) {

	SendEmailNotification se = new SendEmailNotification('Error: Registrant Trigger failed');	
    if(trigger.isBefore && trigger.isDelete){
        
        /**** if Contact's PP Awk date = Registrant Awk Date, then delete the PP Awk date value of Contact too   */
        
        List<Id> regIds = new List<Id>();
        List<Community_Member__c> cmList = new List<Community_Member__c>();
        for(Community_Registrant__c cr : Trigger.old)
            regIds.add(cr.Id);
        cmList.addall([select Id from Community_Member__c where Community_Registrant__c IN: regIds]);
        try{
            delete cmList;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred during deleting Community Member Record(s) : \n'+e.getMessage()+trigger.new);
        }
    }
    if(trigger.isAfter && trigger.isUpdate){
        List<Id> regIds = new List<Id>();
        List<Community_Member__c> cmList = new List<Community_Member__c>();
        List<Community_Member__c> cmUpdList = new List<Community_Member__c>();
        List<Contact> conUpdList = new List<Contact>();
        Map<string,string> regConMap = new Map<string,string>();
        Map<string,string> regConDelMap = new Map<string,string>();
        Contact c = new Contact();
    
        for(Community_Registrant__c cr : Trigger.new)
            // update only if the contact mapping is changed
            if(cr.Contact__c!= null && cr.Contact__c != Trigger.oldMap.get(cr.Id).Contact__c){ 
                regConMap.put(cr.Id, cr.Contact__c);
                regIds.add(cr.Id);
            }
            else if(cr.Contact__c == null && cr.Contact__c != Trigger.oldMap.get(cr.Id).Contact__c){
                regConDelMap.put(cr.Id, Trigger.oldMap.get(cr.Id).Contact__c);
            }
        if(!regConDelMap.isEmpty()){
            cmList = new List<Community_Member__c>();
            cmList.addall([select id, Community_Registrant__c from Community_Member__c where Community_Registrant__c IN: regConDelMap.keySet()]);
            for(String s : regConDelMap.keySet()){
                c = new Contact(id = regConDelMap.get(s));
                c.Community_Registrant__c = null;
                c.Privacy_Policy_Web_Ack_Date__c = null;
                conUpdList.add(c);         
                for(Community_Member__c cm : cmList){
                    cm.Contact__c = null;
                    cmUpdList.add(cm);
                }
            }
        }
        if(!regIds.isEmpty()){
            cmList = new List<Community_Member__c>();
            cmList.addall([select id, Community_Registrant__c from Community_Member__c where Community_Registrant__c IN: regIds]);
            for(Id rid : regIds){
                for(Community_Member__c cm : cmList){
                    if(rid == cm.Community_Registrant__c){
                        string sConId = regConMap.get(rid);
                        if(sConId != null){
                            Id cid = (Id)regConMap.get(rid);
                            cm.Contact__c = cid;
                            cmUpdList.add(cm);
                        }
                        else{
                            Id cid = (Id)regConMap.get(rid);
                            cm.Contact__c = null;
                            cmUpdList.add(cm);
                        }
                    }
                }
                if(regConMap.get(rid) != null){
                    c = [select id, MailingCountry from Contact where id =: regConMap.get(rid)];
                    c.Community_Registrant__c = rid;
                    c.Privacy_Policy_Web_Ack_Date__c = Trigger.newMap.get(rid).Privacy_Policy_Web_Ack_Date__c;
                    if(c.MailingCountry != null)
                        conUpdList.add(c);
                }
                if(Trigger.oldMap.get(rid).Contact__c != null){
                    c = [select id, MailingCountry from Contact where id =: Trigger.oldMap.get(rid).Contact__c];
                    c.Community_Registrant__c = null;
                    c.Privacy_Policy_Web_Ack_Date__c = null;
                    if(c.MailingCountry != null)
                        conUpdList.add(c);
                }
            } // end of selected reg ids loop
        }
        try{
            if(cmList != null) update cmList;
            if(conUpdList != null) update conUpdList;
        }catch(Exception e){
        	se.sendEmail('Exception Occurred during updating Community Member or Contact Records(s): \n'+e.getMessage()+trigger.new);
        }
    }// end of update

    //Tharaka 2021-07-26
    if(trigger.isBefore && (trigger.isUpdate || trigger.isInsert)){
        Map<String, String> mapSubscriptions = new Map<String, String>();
        List<String> lstSubIds = new List<String>();
        //Set<String> setSubIds ;
        for(Community_Registrant__c cr : Trigger.new){
            if(cr.Source__c == 'GDPR Opted-In' || cr.Source__c == 'GDPR'){
                if(cr.SFDC_Subscription_Ids_CSV__c != null){
                    lstSubIds = cr.SFDC_Subscription_Ids_CSV__c.split(',');
                    //setSubIds = new Set<String>(lstSubIds);
                }
            }
        }
        System.debug('lstSubIds : '+lstSubIds);
        if(!lstSubIds.isEmpty()){
            for(Subscription__c sub: [SELECT Id,Subscription_Indicator_Marker__c FROM Subscription__c WHERE Id IN :lstSubIds]){
                if(sub.Subscription_Indicator_Marker__c != null){
                    mapSubscriptions.put(sub.Id,sub.Subscription_Indicator_Marker__c);
                }
            }
            System.debug('mapSubscriptions : '+mapSubscriptions);
            
            for(Community_Registrant__c cr : Trigger.new){
                if(cr.Source__c == 'GDPR Opted-In' || cr.Source__c == 'GDPR'){
                    if(cr.SFDC_Subscription_Ids_CSV__c != null){
                        String strIndicatorMarkers = '';
                        List<String> lstSubscriptionIds = cr.SFDC_Subscription_Ids_CSV__c.split(',');
                        for(String strSubId: lstSubscriptionIds){
                            System.debug('strSubId : '+strSubId);
                            if(mapSubscriptions.containsKey(strSubId)){
                                System.debug('strIndicatorMarkers : '+strIndicatorMarkers);
                                strIndicatorMarkers += (String.isBlank(strIndicatorMarkers) ? '' : ',')+mapSubscriptions.get(strSubId);
                                System.debug('Done strIndicatorMarkers : '+strIndicatorMarkers);
                            }
                        }
                        cr.Subscription_Options__c = strIndicatorMarkers;
                    }
                }
            }
        }
    }
    //Tharaka 2021-07-26 end

    //Tharaka 2021-04-15
    // if(trigger.isAfter && trigger.isInsert){
    //     if(system.isFuture()) return;
    //     CommunityRegistrantTriggerHandler handler = new CommunityRegistrantTriggerHandler();
    //     handler.onAfterInsert(Trigger.new, Trigger.oldmap);
    // }
}