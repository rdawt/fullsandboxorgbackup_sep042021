trigger AccountTerritoryTrigger on Account (after insert,after update) {

    if(TriggerOnOff__c.getOrgDefaults() != NULL && TriggerOnOff__c.getOrgDefaults().isActive_AccountTerritory__c == true)
    {
        if(AccountTriggerHandler.isFirstTime){
            AccountTriggerHandler.isFirstTime = false;
            if(trigger.isInsert && trigger.isAfter){
               
                    AccountTriggerHandler.onAfterInsert(Trigger.new);
            }

            if(trigger.isUpdate && trigger.isAfter){
                    AccountTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldmap);
            }
        }
    }
}