({
    afterScriptsLoaded : function(component, event, helper) 
    {
        helper.getOnLoadRecords(component,event,helper);
    },

    getOwnerRecordsCntrl : function(component, event, helper) 
    {
        /**
        helper.loadCharts(component,event,helper);
        helper.loadOpportunityChart(component,event,helper);
        helper.loadChartsForSelectedUser(component,event,helper);
        helper.loadChartsForSelectedUserScore(component,event,helper);
        helper.loadChartsForAllSalesUserScore(component,event,helper);
        **/
        
        var selectedVal = event.getSource().get('v.name');
        var isSelected = event.getSource().get('v.checked'); 
        
        // if checkbox is selected
        if(isSelected){
            
            // if all is selected
            if(selectedVal == 'ALL'){
                component.set("v.SelectedOwnersOption",helper.selectAll(component, event, helper, component.get("v.OwnersOptions")));
                
                // select all checkbox                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );
                
            }
            
            // if any other value is selected
            else{
                component.set("v.SelectedOwnersOption",helper.addNewSelectedValue(component, event, helper, component.get("v.SelectedOwnersOption"), selectedVal));
            }
            
        }      
        
        // if check box is not checked
        else{
            if(selectedVal == 'ALL'){
                component.set("v.SelectedOwnersOption",[]);
                
                // select all checkbox                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",false);     
                } );
            }
            else{
                component.set("v.SelectedOwnersOption",helper.removeSelectedValue(component, event, helper, component.get("v.SelectedOwnersOption"), selectedVal));                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    if(itemcmp.get("v.label") == 'ALL')
                        itemcmp.set("v.checked",false);     
                } );
            }
        }
        
    },

    activityTypeChange : function(component, event, helper) 
    {   
        
        var selectedVal = event.getSource().get('v.name');
        var isSelected = event.getSource().get('v.checked'); 
        
        // if checkbox is selected
        if(isSelected){
            
            // if all is selected
            if(selectedVal == 'ALL'){
                component.set("v.selectedActivityTypeOption",helper.selectAll(component, event, helper, component.get("v.activityTypeOptions")));
                
                // select all checkbox                
                component.find("activityCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );
                
            }
            
            // if any other value is selected
            else{
                component.set("v.selectedActivityTypeOption",helper.addNewSelectedValue(component, event, helper, component.get("v.selectedActivityTypeOption"), selectedVal));
            }
            
        }      
        
        // if check box is not checked
        else{
            if(selectedVal == 'ALL'){
                component.set("v.selectedActivityTypeOption",[]);
                // select all checkbox                
                component.find("activityCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",false);     
                } );
            }
            else{
                component.set("v.selectedActivityTypeOption",helper.removeSelectedValue(component, event, helper, component.get("v.selectedActivityTypeOption"), selectedVal));                
                component.find("activityCheckbox").forEach( function(itemcmp) {
                    if(itemcmp.get("v.label") == 'ALL')
                        itemcmp.set("v.checked",false);     
                } );
            }
        }
    },

    handlePayeeUserChange : function(component, event, helper) {
        var selectedUserId = component.get('v.strSelectedUserOption');
        if($A.util.isEmpty(selectedUserId)){
            return;
        }else{
            helper.getDrillDownActivities(component, event, helper, selectedUserId);
        }
    },

    setCompareCheckbox : function(component, event, helper) 
    {
        var isSelected = event.getSource().get('v.checked'); 
        
        if(isSelected)
        {
            component.set("v.isCompareChecked",true);
        }
        else
        {
            component.set("v.isCompareChecked", false);
        }      
        console.log('??????????Check Compare Value in ???setCompareCheckbox????????', component.get("v.isCompareChecked"));
    },
    
    getActivityForEachContactCntrl : function(component, event, helper) 
    {        
        var selectedUserId = component.get('v.strSelectedUserOption');
        var compareChecked = component.get('v.isCompareChecked');

        if($A.util.isEmpty(selectedUserId)){
            return;
        }else{
            helper.getDrillDownActivities(component, event, helper, selectedUserId);
        }

        if(component.get('v.isCompareChecked') == false)
        {
            component.set('v.isActivitiesByTypeAndScorePY', false);
            component.set('v.isActivityScoreByUserPY', false);
            component.set('v.isActivityCountbyUserPY', false);
            component.set('v.isActivityScorebyUserStackedPY', false);
            component.set('v.isOpportunitiesCreatedPY', false);

            component.set('v.isActivitiesByTypeAndScoreCY', true);
            component.set('v.isActivityScoreByUserCY', true);
            component.set('v.isActivityCountbyUserCY', true);
            component.set('v.isActivityScorebyUserStackedCY', true);
            component.set('v.isOpportunitiesCreatedCY', true);

            helper.loadCharts(component,event,helper);
            helper.loadChartsForSelectedUser(component,event,helper);
            helper.loadChartsForSelectedUserScore(component,event,helper);
            helper.loadChartsForAllSalesUserScore(component,event,helper);
            helper.loadOpportunityChart(component,event,helper);
        }
        else if(component.get('v.isCompareChecked') == true)
        {
            component.set('v.isActivitiesByTypeAndScorePY', false);
            component.set('v.isActivityScoreByUserPY', false);
            component.set('v.isActivityCountbyUserPY', false);
            component.set('v.isActivityScorebyUserStackedPY', false);
            component.set('v.isOpportunitiesCreatedPY', false);
            component.set('v.isActivitiesByTypeAndScoreCY', false);
            component.set('v.isActivityScoreByUserCY', false);
            component.set('v.isActivityCountbyUserCY', false);
            component.set('v.isActivityScorebyUserStackedCY', false);
            component.set('v.isOpportunitiesCreatedCY', false);

            if(component.get('v.compareChartSelected') == 'Activities by Type and Score')
            {
                component.set('v.isActivitiesByTypeAndScoreCY', true);
                component.set('v.isActivitiesByTypeAndScorePY', true);
            }
            if(component.get('v.compareChartSelected') == 'Activity Score by User')  
            { 
                component.set('v.isActivityScoreByUserCY', true);
                component.set('v.isActivityScoreByUserPY', true);
            }
            if(component.get('v.compareChartSelected') == 'Activity Count by User')
            {
                component.set('v.isActivityCountbyUserCY', true);
                component.set('v.isActivityCountbyUserPY', true);
            }
            if(component.get('v.compareChartSelected') == 'Activity Score by User (Stacked)')
            {
                component.set('v.isActivityScorebyUserStackedCY', true);
                component.set('v.isActivityScorebyUserStackedPY', true);
            }
            if(component.get('v.compareChartSelected') == 'Opportunities Created')  
            { 
                component.set('v.isOpportunitiesCreatedCY', true);
                component.set('v.isOpportunitiesCreatedPY', true);
            }

            if(component.get('v.isActivitiesByTypeAndScorePY') == true)
            {
                helper.loadCharts(component, event, helper);
                helper.loadChartsPY(component, event, helper);
            }
            if(component.get('v.isActivityScoreByUserPY') == true)
            {
                helper.loadChartsForAllSalesUserScore(component, event, helper);
                helper.loadChartsForAllSalesUserScorePY(component, event, helper);
            }
            if(component.get('v.isActivityCountbyUserPY') == true)
            {
                helper.loadChartsForSelectedUser(component, event, helper);
                helper.loadChartsForSelectedUserPY(component, event, helper);
            }
            if(component.get('v.isActivityScorebyUserStackedPY') == true)
            {
                helper.loadChartsForSelectedUserScore(component, event, helper);
                helper.loadChartsForSelectedUserScorePY(component, event, helper);
            }


            //helper.loadChartsForSelectedUser(component,event,helper);
            //helper.loadChartsForSelectedUserScore(component,event,helper);
        }
        
    },

    handleSaveButtonClick: function(component, event, helper) 
    {
        helper.saveUserScoreWeightage(component,event,helper);
        helper.loadCharts(component,event,helper);
        helper.loadChartsForSelectedUser(component,event,helper);
        helper.loadChartsForSelectedUserScore(component,event,helper);
        helper.loadChartsForAllSalesUserScore(component,event,helper);
        //helper.loadOpportunityChart(component,event,helper);
    }
})