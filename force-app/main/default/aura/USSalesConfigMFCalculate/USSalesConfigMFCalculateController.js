({
    handleInit : function(component, event, helper) {
        component.set("v.showSpinner",true);
        var today = new Date();
        component.set('v.dateFrom', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
        helper.getFunds(component, event, helper);
    },
    handleFundChange : function(component, event, helper) {
        component.set("v.showSpinner",true);
        helper.getChannels(component, event, helper);
    },
    handleChannelChange : function(component, event, helper) {
        component.set("v.showSpinner",true);
        helper.getTerritory(component, event, helper);
    },
    handleCalculate : function(component, event, helper) {
        component.set("v.showSpinner",true);
        helper.calculateNetSales(component, event, helper); 
    },

    handleDataSourceChange : function(component, event, helper) {
        console.log(event.getParam('value'));
    }
})