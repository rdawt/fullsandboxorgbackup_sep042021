({
    handleInit : function(component,event,handler) {
        if(component.get('v.contentType') == 'sub'){
            component.set('v.yearDisabled', true);
        }
        component.set('v.loadedYear',component.get('v.selectedYear'));
        handler.setTableColumns(component,event,handler);
        handler.getChartData(component,event,handler);
    },

    handleChartDrillDown : function(component,event,handler) {
        var chartId = event.getParam("chartId");
        handler.handleShowModal(component,event,handler,chartId);
    },

    handleContentChange : function(component,event,handler) {
        var selectedButtonLabel = event.getSource().get("v.label");
        component.set('v.selectedContent', selectedButtonLabel);
        handler.setVisualType(component,event,handler,selectedButtonLabel);
    },

    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'drill_down':
                helper.handleShowModal(component,event,helper,row.chartId);
                break;
            default:
                helper.handleShowModal(component,event,helper,row.chartId);
                break;
        }
    },

    handleYearChange : function(component,event,handler) {   
        component.set('v.loadedYear',component.get('v.selectedYear'));     
        component.set("v.showSpinner",true);
        handler.getChartData(component,event,handler);
    },

    handleTabChange : function(component,event,handler) {
        if(component.get('v.loadedYear') != component.get('v.selectedYear')){
            component.set('v.loadedYear',component.get('v.selectedYear'));    
            component.set("v.showSpinner",true);
            handler.getChartData(component,event,handler);
        }
    }
})