({ 
    handleInit : function(component, event, helper) {
        // alert('New Cmp loaded');
        helper.getVisualType(component,event,helper);
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        //Below users have permissions to use Gauges/Grid feature(Buttons available)
        //0052K00000ARbyrQAD - Tharaka
        //005A0000005DhiSIAS - Shankar
        //005A0000000PDBIIA4 - Diana Dileo
        //0052K000009aOFlQAM - Jonathan Wang
        //005A0000000OqpmIAC - Brian Donnelly
        //005A0000003Gnv5IAC - Richard Potocki
        //005A0000008X4AbIAK - Michael Gandy 
        if(userId == '0052K00000ARbyrQAD' || userId == '005A0000005DhiSIAS' || userId == '005A0000000PDBIIA4' || userId == '0052K000009aOFlQAM' || userId == '005A0000000OqpmIAC' || userId == '005A0000003Gnv5IAC' || userId == '005A0000008X4AbIAK' || userId == '00519000003WzqeAAC' ){
            component.set('v.showButtons',true);
        }
        var userProfile = component.get('v.currentUser.Profile.Name');
        var yearOptions = [];
        //Below users have permissions to view 2020 data
        //0052K00000ARbyrQAD - Tharaka
        //005A0000005DhiSIAS - Shankar
        //005A0000000PDBIIA4 - Diana Dileo
        //005A0000003Gnv5IAC - Richard Potocki
        if(userId == '0052K00000ARbyrQAD' || userId == '005A0000005DhiSIAS' || userId == '005A0000000PDBIIA4' || userId == '005A0000003Gnv5IAC'  || userId == '00519000003WzqeAAC'){
            yearOptions.push({'label': '2020', 'value': '2020'});
        }
        yearOptions.push({'label': '2021', 'value': '2021'});
        component.set('v.year', yearOptions);
        
        var selectedYear = component.get('v.selectedYear');
        if( selectedYear == '2020'){
            component.set('v.selectedTabId','EME-IIG');
        }else if(selectedYear == '2021'){
            component.set('v.selectedTabId','EME');
        }
        //component.set('v.showButtons',true);//only for full box
    },

    handleVisualTypeChangeEvent :  function(component, event, helper) {
        var visualType = event.getParam("visualType"); 
        component.set("v.visualType", visualType);  
    },

    handleTabSelect : function(component, event, helper) {
        var selectedId = event.getParam('id');
        component.set('v.selectedTabId',selectedId);
        //Log Usage Statistics
        var page = (component.get('v.chartType') == 'fund' ? 'US Sales Goal Tracker By Fund' : 'US Sales Goal Tracker By Channel');
        var service = component.find('usageService');
        service.logUsageStatistics('US Sales Dashboard',page,component.get('v.selectedTabId'),'Tab');  
        //Log Usage Statistics end 
        var dataView = component.find(selectedId);
        dataView.tabChange();
    },

    handleContentChange : function(component,event,handler) {
        var selectedButtonLabel = event.getSource().get("v.label");
        component.set('v.visualType', selectedButtonLabel);
        handler.setVisualType(component,event,handler,selectedButtonLabel);
    },

    handleYearChange : function(component,event,handler) {
        //debugger;
        if(component.get('v.chartType') == 'fund'){
            var selectedYear = component.get('v.selectedYear');
            var tab = component.get('v.selectedTabId');
            if(selectedYear == '2021' && tab == 'EME-IIG'){
                component.find("fundTabs").set("v.selectedTabId",'EME');
                component.set('v.selectedTabId','EME');
            }else if(selectedYear == '2020' && (tab == 'EME' || tab == 'IIG')){
                component.find("fundTabs").set("v.selectedTabId",'EME-IIG');
                component.set('v.selectedTabId','EME-IIG');
            }
        }
        // setTimeout(function () {
        //     var dataView = component.find(tab);
        // dataView.tabChange();
        // }, 1000);
        
        // setTimeout(alert, 1000); 
        
    }
    
})