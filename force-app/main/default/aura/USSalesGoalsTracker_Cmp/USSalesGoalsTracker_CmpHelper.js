({
    getVisualType : function(component,event,helper) {
        var action = component.get('c.getDataVisualType');
        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                if($A.util.isEmpty(response.getReturnValue())){
                    component.set("v.visualType", "Gauge Chart");
                }else{
                    component.set("v.visualType",response.getReturnValue());
                }
            } 
        });
        $A.enqueueAction(action);
    },
    setVisualType : function(component,event,helper,type) {
        
        var action = component.get('c.setDataVisualType');
        action.setParams({
            type : type
		  });

        action.setCallback(this, function(response) {
            if (response.getState() === 'SUCCESS') {
                var appEvent = $A.get("e.c:DBVisualTypeChangeEvent"); 
                appEvent.setParams({"visualType" : type}); 
                appEvent.fire(); 
            } 
        });
        $A.enqueueAction(action);
    }
})