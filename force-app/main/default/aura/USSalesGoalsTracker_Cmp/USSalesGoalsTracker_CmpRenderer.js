({
    afterRender: function (component, helper) {
        this.superAfterRender();
        //Log Usage Statistics
        var page = (component.get('v.chartType') == 'fund' ? 'US Sales Goal Tracker By Fund' : 'US Sales Goal Tracker By Channel');
        var cmp = (component.get('v.chartType') == 'fund' ? component.get('v.selectedTabId') : 'Firm');
        var service = component.find('usageService');
        service.logUsageStatistics('US Sales Dashboard',page,cmp,'Page');    
    },

})