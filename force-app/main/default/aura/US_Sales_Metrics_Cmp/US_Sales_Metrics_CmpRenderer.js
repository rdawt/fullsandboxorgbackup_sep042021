({
    afterRender: function (component, helper) {
        this.superAfterRender();
        //Log Usage Statistics
        var service = component.find('usageService');
        service.logUsageStatistics('US Sales Dashboard','US Sales Data','US Sales - Active Funds','Page');    
    },

})