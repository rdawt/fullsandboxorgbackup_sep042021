({
	doInit : function(component, event, helper) {
		helper.doInit(component, event, helper);
	},
	
	handleSaveClick : function(component, event, helper) {
		helper.saveFundsRecords(component,event,helper);
	},
	
	handleCancelClick : function(component, event, helper) {
		helper.callCancelButton(component, event, helper);
	},
	
	handleOtherSelectAll : function(component, event, helper) {
		helper.handleOtherSelectAll(component, event, helper);
	},
	
	handleOtherSingleSelect : function(component, event, helper) {
		helper.handleOtherSingleSelect(component, event, helper);
	},
	
	handleOtherSearch : function(component, event, helper) {
		helper.getOtherSearchRecords(component, event, helper);
	},
	
	handleDefaultSingleSelect : function(component, event, helper) {
		helper.handleDefaultSingleSelect(component, event, helper);
	},
	
	// Main Function from Desktop to do search
	handleSearch : function(component, event, helper) {
		helper.getFocusSearchRecords(component, event, helper);
	},
	
	handleDefaultSearch : function(component, event, helper) {
		helper.getDefaultSearchRecords(component, event, helper);
	},
	
	handleFocusSingleSelect : function(component, event, helper) {
		helper.handleFocusSingleSelect(component, event, helper);
	},
	
	handleFocusSearch : function(component, event, helper) {
		helper.getDefaultSearchRecords(component, event, helper);
	},
	
	// Main Function from Mobile to do search
	handleMobileSearch : function(component, event, helper) {
		helper.getMobileSearchRecords(component, event, helper);
	},
	
    // toggle remaining rows onclick of Fund Name for iterated items
    toggleRows : function(component, event, helper) {
		// first get the div element. by using aura:id
		var target = event.currentTarget;
        var panel1 = target.nextElementSibling;
        var panel2 = panel1.nextElementSibling;
        var panel3 = panel2.nextElementSibling;
        $A.util.toggleClass(panel1, "hideCell");
        $A.util.toggleClass(panel2, "hideCell");
        $A.util.toggleClass(panel3, "hideCell");
        
        
      }
	
})