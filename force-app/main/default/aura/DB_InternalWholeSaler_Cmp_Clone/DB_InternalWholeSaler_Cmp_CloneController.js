({
    afterScriptsLoaded : function(component, event, helper) {
        var intCurrentMonth = new Date().getMonth();
        intCurrentMonth += 1;
        var intCurrentYear = new Date().getFullYear();
        console.log('????????intCurrentMonth????', intCurrentMonth);
        console.log('????????intCurrentYear????', intCurrentYear);
        /*
        lstDefaultUsers.add('005A0000008kKiAIAU');  // Ian Amberson
        lstDefaultUsers.add('005A0000008X7phIAC');  // Madison Morelli
        lstDefaultUsers.add('005A0000004im7zIAA');  // Michael Hofbauer
        lstDefaultUsers.add('0052K000008ncSIQAY');  // Ryan Henshaw
        */
        var defaultUsers = ["005A0000008kKiAIAU", "005A0000008X7phIAC", "005A0000004im7zIAA", "0052K000008ncSIQAY"];
        helper.getOnLoadRecords(component, event, helper, intCurrentMonth, intCurrentYear, defaultUsers);
    },

    resetFilters : function(component, event, helper) {
        console.log('????????Year on Reset Button????', event.getParam('id'));
        var intString = event.getParam('id');
        console.log('????????Year on Reset Button????', intString);
        var intMonth = intString.substring(0, 2);
        var intYear = intString.substring(3, 7);
        helper.getOnLoadRecords(component, event, helper, intMonth, intYear);
    },

    applyFilters : function(component, event, helper) {
        
        var currentSelectUserStates = component.get("v.SelectedUsersOption");
        console.log('>>>>>>>>>>currentSelectUserStates>>>>>>>>>' , currentSelectUserStates);
        helper.saveActivityTypeAndUserDays(component, event, helper, 
                component.get("v.intSelectedMonthFromCntrl"), component.get("v.intSelectedYearFromCntrl"), currentSelectUserStates);
    },

    getUserRecordsCntrl : function(component, event, helper) 
    {
        var selectedVal = event.getSource().get('v.name');
		var isSelected = event.getSource().get('v.checked'); 
        
        // if checkbox is selected
        if(isSelected){
            // if all is selected
            if(selectedVal == 'ALL'){
                component.set("v.SelectedUsersOption",helper.selectAll(component, event, helper, component.get("v.UsersOptions")));
                
                // select all checkbox                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );
            }
            // if any other value is selected
            else{
                component.set("v.SelectedUsersOption",helper.addNewSelectedValue(component, event, helper, component.get("v.SelectedUsersOption"), selectedVal));
            }
        }      
        
        // if check box is not checked
        else{
            if(selectedVal == 'ALL'){
                component.set("v.SelectedUsersOption",[]);
                // select all checkbox                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",false);     
                } );
            }
            else{
                component.set("v.SelectedUsersOption",helper.removeSelectedValue(component, event, helper, component.get("v.SelectedUsersOption"), selectedVal));                
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    if(itemcmp.get("v.label") == 'ALL')
                        itemcmp.set("v.checked",false);     
                } );
            }
        }    
    },

    //Tharaka 2020-12-15
    handlePayeeUserChange : function(component, event, helper) {
        var selectedUserId = component.get('v.strSelectedUserOption');
        if($A.util.isEmpty(selectedUserId)){
            return;
        }else{
            helper.getDrillDownActivities(component, event, helper, selectedUserId);
        }
    },

    handleYearChange : function(component, event, helper) {
        //component.set('v.lstUserDrillDownActivity',null);
        //component.set('v.selectedYear',event.getParam('id'));
        console.log('????????Year Button????', event.getParam('id'));
        var intMonth = '';
        var intYear = '';
        var intString = event.getParam('id');
        console.log('????????Year on Reset Button????', intString);
        if(intString.length > 4)
        {
            intMonth = intString.substring(0, 2);
            intYear = intString.substring(3, 7);
        }
        else
        {
            intMonth = 1;
            intYear = intString;
        }
        console.log('????????Year on Reset Button intMonth????', intMonth);
        console.log('????????Year on Reset Button intYear????', intYear);
        helper.getOnLoadRecords(component, event, helper, intMonth, intYear);
    }

})