({
    //calling method getSubscriptionDetails from apex and assigning returned wrapperlist to attributes
	doInitialize : function(component,event) {
        component.set("v.showSpinner",true);
		var recordId = component.get("v.recordId");
        
        var action = component.get("c.getSubscriptionDetails");
        action.setParams({
            "recordId" : recordId
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                var conObj  = response.getReturnValue().con;
                
                var currentRecordName = '';
                if ('FirstName' in conObj) {
                    currentRecordName = currentRecordName+ conObj.FirstName+' ';
                }
                currentRecordName = currentRecordName + conObj.LastName;
                component.set("v.currentRecordName",currentRecordName);
                 
                var isEmailBlank = false;
                var isOptdEmail = false;
                var isEmailBounced = false;
                
                if(!conObj.Email)
                {
                    isEmailBlank = true;
                    component.set("v.noRecordsFound",true);
                    component.set("v.errorMessage",'Contact has no Email address specified.');
                }
                
                if(conObj.HasOptedOutOfEmail == true)
                {
                    isOptdEmail = true;
                    component.set("v.isEmailOpted",true);
                    component.set("v.errorMessageForEmailOptd",'Contact has Opted out of Emails.');
                }
                
                if(conObj.Bounced_Email__c == true)
                {
                    isEmailBounced = true;
                    component.set("v.isEmailBounced",true);
                    component.set("v.errorMessageForEmailBncd",'Contact has bounced email. Please provide valid Email address.');
                }
                
                if(isEmailBlank != true && isOptdEmail != true && isEmailBounced != true){
                    component.set("v.showSubmit",true);
                    component.set("v.subscriptionWrapper",JSON.parse(JSON.stringify(response.getReturnValue().wrapperLst)));
                	component.set("v.subscriptionWrapperTemp",JSON.parse(JSON.stringify(response.getReturnValue().wrapperLst)));
                    component.set("v.subscriptionWrapperOld",JSON.parse(JSON.stringify(response.getReturnValue().wrapperLst)));
                }
                
                component.set("v.showSpinner",false);
            }
        });
        $A.enqueueAction(action);
	},
    
    //filtering records on search by using input fields
    onSearchOrecords : function(component,event){
        var searchedTxt = event.getSource().get("v.value");
        var wrapper = component.get("v.subscriptionWrapper");
        var filterType = event.getSource().get("v.name");
        
        var filteredWrapper = wrapper.filter(wrapperObj =>
        {
            if(wrapperObj[filterType])
            {
            	return wrapperObj[filterType].toLowerCase().includes(searchedTxt.toLowerCase());
        	}
        });
        
        if(filteredWrapper.length > 0){
            component.set("v.noRecordsFound",false);
        }
        else{
            component.set("v.errorMessage",'No Records Found.');
            component.set("v.noRecordsFound",true);
        }
        
        component.set("v.subscriptionWrapperTemp",filteredWrapper);
    },
    
    //updating wrapper list by making subscribed to false when unsusbscribed
    unsubscribeFromWrapper : function(component,event){
        //debugger;
        var recordId = component.get("v.recordId");
        var currentSubId = event.currentTarget.id;
        var currentSubMemId = event.currentTarget.title;;
        var wrapperLstTemp = component.get("v.subscriptionWrapperTemp");
        
        wrapperLstTemp.forEach(wrapperObjTemp => {
            if(wrapperObjTemp['SubId'] == currentSubId)
            {
            	wrapperObjTemp['Subscribed'] = false;
        	}
        });
        component.set("v.subscriptionWrapperTemp",wrapperLstTemp);
        
        
        var wrapperLst = component.get("v.subscriptionWrapper");
        wrapperLst.forEach(wrapperObj => {
            if(wrapperObj['SubId'] == currentSubId)
            {
            	wrapperObj['Subscribed'] = false;
        	}
        });
        
        component.set("v.subscriptionWrapper",wrapperLst);
        
    },
    
    //calling method setSubscription from apex and passing old and updated wrapper lists
    handleSubmitClick : function(component,event){
        component.set("v.showSpinner",true);
        var recordId = component.get("v.recordId");
        var wrapperToUpdateorInsert = component.get("v.subscriptionWrapper");
        var oldWrapper = component.get("v.subscriptionWrapperOld");
        
        var action = component.get("c.setSubscription");
        
        console.log('SFDC recordId1 : '+recordId);	 
        console.log('SFDC oldWrapper : '+JSON.stringify(oldWrapper));
        console.log('SFDC wrapperToUpdateorInsert : '+JSON.stringify(wrapperToUpdateorInsert));
        
        action.setParams({
            "recordId" : recordId,
            "lstOldRSet"	: oldWrapper,
            "lstRSetChanged"  : wrapperToUpdateorInsert
            
        });
        
        action.setCallback(this,function(respose){
            var state = respose.getState();
            if (state === "SUCCESS"){
                console.log('Response from Apex: '+respose.getReturnValue());
                component.set("v.showSpinner",false);
                component.set("v.isSuccess",true);
                this.cancelOut(component,event);
            }
        });
        $A.enqueueAction(action);
    },
    
    //go back to the contact record
    cancelOut : function(component,event){
        var recordId = component.get("v.recordId");
        
        if((typeof sforce != 'undefined') && sforce && (!!sforce.one))
        {
            var result = '/lightning/r/Contact/'+recordId+'/view';
            sforce.one.navigateToURL(result);
        }
        else{
            var result = '/'+recordId;
            window.open(result,'_top');
        }
    },
    
    handleSingleCheckboxClick : function(component,event){
        //debugger;
        
        var currentSubId = event.getSource().get("v.id");;
        var wrapperLst = component.get("v.subscriptionWrapper");
        var wrapperLstTemp = component.get("v.subscriptionWrapperTemp");
        console.log('>>>>>>>>>>Check Clicked Value>>>>>>>>>>>' , event.getSource().get("v.checked"));
        var currentSubCheckboxChecked = event.getSource().get("v.checked");
        
        wrapperLst.forEach(wrapperObj => {
            if(wrapperObj['SubId'] == currentSubId)
            {
            	wrapperObj['Subscribed'] = currentSubCheckboxChecked;
        	}
        });
        
        wrapperLstTemp.forEach(wrapperObjTemp => {
            if(wrapperObjTemp['SubId'] == currentSubId)
            {
            	wrapperObjTemp['Subscribed'] = currentSubCheckboxChecked;
        	}
        });
        
        component.set("v.subscriptionWrapper",wrapperLst);
        component.set("v.subscriptionWrapperTemp",wrapperLstTemp);
    }
})