({
    handleInit : function(component, event, helper) {
        component.set('v.showSpinner',true);
        component.set('v.subHeader','Calculation History :');
        var today = new Date();
        if(today.getMonth() == 0){
            component.set('v.dateFrom', (today.getFullYear() - 1) + "-" + (12) + "-" + today.getDate());
        }else{
            component.set('v.dateFrom', today.getFullYear() + "-" + (today.getMonth() - 1) + "-" + today.getDate());
        }
        component.set('v.dateTo', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
        helper.getHistoryAndAssetDate(component, event, helper);
    },
    handleCalculate : function(component, event, helper) {
        component.set('v.showSpinner',true);
        helper.calculateOrgTypeNetFlow(component, event, helper);
    },

    fundChange : function(component, event, helper) {
        component.set('v.showSpinner',true);
        if(component.get('v.selectedFund') == 'Mutual Fund'){
            component.set('v.channelLabel','Channel Name');
            component.set('v.channelOptions', [
                {'label': 'Institutional', 'value': 'Institutional'}]);
        }else if(component.get('v.selectedFund') == 'ETF'){
            component.set('v.channelLabel','Broadridge Channel Name');
            component.set('v.channelOptions', [
                {'label': 'INSTITUTIONAL', 'value': 'INSTITUTIONAL'},
                {'label': 'FINANCIAL ADVISOR', 'value': 'FINANCIAL ADVISOR'},
                {'label': 'RIA', 'value': 'RIA'},
                {'label': 'ASSET MANAGER', 'value': 'ASSET MANAGER'}]);
        }
        helper.getHistoryAndAssetDate(component, event, helper);
    },

    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'show_details':
                helper.showRowDetails(component,row);
                break;
            default:
                helper.showRowDetails(component,row);
                break;
        }
    },

    handleDetailRowAction: function (component, event, helper) {
        component.set('v.showSpinner',true);
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'firm_drill_down':
                helper.showDrillDownDetails(component,row,helper,'Firm');
                break;
            case 'territory_drill_down':
                helper.showDrillDownDetails(component,row,helper,'Territory');
                break;
            default:
                helper.showDrillDownDetails(component,row,helper,'Firm');
                break;
        }
    },

    handleBackToHistroy :  function (component, event, helper) {
        component.set('v.disableInputs', false);
        component.set('v.dataTableType','Summary');   
        component.set('v.subHeader','Calculation History :');  
        component.set('v.selectedChannel', component.get('v.selectedChannelBackUp'));
        component.set('v.assetDate', component.get('v.assetDateBackUp'));
        component.set('v.dateFrom', component.get('v.dateFromBackUp'));
        component.set('v.dateTo', component.get('v.dateToBackUp'));
    },

    handleSort: function(cmp, event, helper) {
        helper.handleSort(cmp, event);
    },

    channelChange : function (component, event, helper) {
        component.set('v.showSpinner',true);
        helper.getFirmName(component, event, helper);
    },

    handleDateChange : function (component, event, helper) {
        if(component.get('v.selectedChannel') == 'Financial Advisor' && component.get('v.selectedFund') == 'Mutual Fund'){
            component.set('v.showSpinner',true);
            helper.getFirmName(component, event, helper);
        }
    },
    

})