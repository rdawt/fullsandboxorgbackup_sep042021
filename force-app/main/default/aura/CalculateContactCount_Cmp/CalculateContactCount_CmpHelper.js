({
    calculateCount: function(component, event) 
    {       
        var oppId = component.get("v.recordId");    
        var action = component.get("c.countContactsForEachAccount");        
            action.setParams({recordId : oppId});         
        
            action.setCallback(this, function(response)
        {            
            var state = response.getState();                       
            if (state === 'SUCCESS') 
            {               
                var strMessage = response.getReturnValue();               
                if(strMessage == 'Failed')      {                   
                    $A.get('e.force:closeQuickAction').fire();               
                    var toastEvent = $A.get('e.force:showToast');               
                    toastEvent.setParams({
                                            'title': 'Failed',
                                            'type' : 'Error',
                                            'message': 'There was some issue while calculating Count, please contact your administrator for support'
                                        }); 
                    toastEvent.fire();               
                    $A.get('e.force:refreshView').fire();                
                } 
                else if(strMessage == 'Success')     
                {                   
                    $A.get('e.force:closeQuickAction').fire();               
                    var toastEvent = $A.get('e.force:showToast');               
                    toastEvent.setParams({
                                            'title': 'Success',
                                            'type' : 'Success',
                                            'message': 'Calculating Contact Count was Successfully Completed!!'
                                        }); 
                    toastEvent.fire();               
                    $A.get('e.force:refreshView').fire();               
                }
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do nothing
            }           
            else if (state === 'ERROR') 
            { 
                    var errors = response.getError();               
                    if (errors) 
                    {                   
                        if (errors[0] && errors[0].message) 
                        {                        
                            console.log('Error message: ' + errors[0].message);
                        }               
                    }
                    else 
                    {
                        console.log('Unknown error');                
                    }           
            }       
        });
        $A.enqueueAction(action);
    }
})