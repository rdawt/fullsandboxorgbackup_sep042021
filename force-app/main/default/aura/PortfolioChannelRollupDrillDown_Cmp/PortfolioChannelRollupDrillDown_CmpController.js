({
    handleInit : function(component, event, helper) {
        var fundType = component.get('v.selectedFundType');
        if(fundType == 'ETF'){
            if(component.get('v.mode') == 'Firm'){
                component.set('v.columns', [
                    {label: 'Broadridge Firm Name', fieldName: 'accountNameBR', type: 'text'},
                    {label: 'SFDC Firm Name', fieldName: 'accountLink', type: 'url', 
                    typeAttributes: {label: { fieldName: 'accountName' }, target: '_blank'} , sortable: true },
                    {label: 'AUM', fieldName: 'aum', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
                    {label: 'Net Sales', fieldName: 'netSales', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
                    {label: 'Net Sales Last Month', fieldName: 'netSalesLastMonth', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}}
                ]);  
            }else if(component.get('v.mode') == 'Territory'){
                component.set('v.columns', [
                    {label: 'Fund Name', fieldName: 'fundLink', type: 'url', 
                    typeAttributes: {label: { fieldName: 'fundName' }, target: '_blank'} },
                    {label: 'Channel Name', fieldName: 'channelLink', type: 'url', 
                    typeAttributes: {label: { fieldName: 'channelName' }, target: '_blank'} },
                    {label: 'Territory', fieldName: 'territoryLink', type: 'url', 
                    typeAttributes: {label: { fieldName: 'territory' }, target: '_blank'} , sortable: true },
                    {label: 'AUM', fieldName: 'aum', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
                    {label: 'Net Sales', fieldName: 'netSales', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}},
                    {label: 'Net Sales Last Month', fieldName: 'netSalesLastMonth', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}}
                ]);  
            }
        }else if(fundType == 'Mutual Fund'){
            if(component.get('v.mode') == 'Firm'){
                component.set('v.columns', [
                    {label: 'Fund', fieldName: 'fundLink', type: 'url', 
                    typeAttributes: {label: { fieldName: 'fundName' }, target: '_blank'}  },
                    {label: 'Channel Name', fieldName: 'channelLink', type: 'url', 
                    typeAttributes: {label: { fieldName: 'channelName' }, target: '_blank'} , sortable: true },
                    {label: 'Firm Name', fieldName: 'accountLink', type: 'url', 
                    typeAttributes: {label: { fieldName: 'accountName' }, target: '_blank'} , sortable: true },
                    {label: 'Total Assets', fieldName: 'currentAssets', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}}
                ]);  
            }else if(component.get('v.mode') == 'Territory'){
                component.set('v.columns', [
                    {label: 'Fund', fieldName: 'fundLink', type: 'url', 
                    typeAttributes: {label: { fieldName: 'fundName' }, target: '_blank'}  },
                    {label: 'Channel Name', fieldName: 'channelLink', type: 'url', 
                    typeAttributes: {label: { fieldName: 'channelName' }, target: '_blank'} , sortable: true },
                    {label: 'Territory', fieldName: 'territoryLink', type: 'url', 
                    typeAttributes: {label: { fieldName: 'territory' }, target: '_blank'} , sortable: true },
                    {label: 'Total Assets', fieldName: 'currentAssets', type: 'currency' , sortable: true , typeAttributes: { currencyCode: 'USD'}}
                ]);  
            }
            
        }
    },

    handleSortSubData: function(cmp, event,helper) {
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        var data = cmp.get('v.data');
        var cloneData = data.slice(0);
        cloneData.sort((helper.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1)));
        
        cmp.set('v.data', cloneData);
        cmp.set('v.sortDirection', sortDirection);
        cmp.set('v.sortedBySub', sortedBy);
    }
})