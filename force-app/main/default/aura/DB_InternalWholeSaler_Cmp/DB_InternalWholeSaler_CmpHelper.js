({
    getOnLoadRecords: function (component, event, helper, selectedMonth, selectedYear, defaultUsers) {

        console.log('?????????selectedMonth??????????' , selectedMonth);
        console.log('?????????selectedYear??????????' , selectedYear);
        component.set("v.showSpinner", true);
        var action = component.get("c.getOnLoadRecords"); 
        action.setParams({
            "intSelectedMonth" : selectedMonth,
            "intSelectedYear" : selectedYear,
            "strUserId" : '',
            "lstSetDefaultUsers" : defaultUsers
        });         
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();                     
            if (state === 'SUCCESS') 
            {   
                console.log('??????????rtnVal.NOTCurrentMonthYearSelected?????????', rtnVal.NOTCurrentMonthYearSelected);
                component.set("v.isMattLogin", rtnVal.isMattLogin);
                component.set("v.isNOTCurrentMonthYearSelected", rtnVal.NOTCurrentMonthYearSelected);
                component.set("v.intSelectedMonthFromCntrl", rtnVal.SelectedMonth);
                component.set("v.intSelectedYearFromCntrl", rtnVal.SelectedYear);
                component.set("v.UsersOptions", rtnVal.UsersOptions);
                component.set("v.lstUserOptions", rtnVal.DrillDownUsersOptions);
                component.set("v.SelectedUsersOption", rtnVal.SelectedUsersOption);
                component.set("v.lstAttendanceDate", rtnVal.AttendanceDates);
                component.set("v.objUsers", rtnVal.ObjectUserDays);
                component.set("v.objActivityType", rtnVal.ObjectActivityTypes);
                console.log(rtnVal.DrillDownUsersOptions);
                console.log('??????????rtnVal.DrillDownUsersRecords?????????', rtnVal.DrillDownUsersRecords);
                component.set("v.lstUserDrillDownActivity", rtnVal.DrillDownUsersRecords);

                var payeeAttendance = [];
                var currentLstPayeeAttendance = rtnVal.UserAttendanceInformation;
                for ( var key in currentLstPayeeAttendance ) {
                    payeeAttendance.push({value: (currentLstPayeeAttendance[key]), key:key});
                }
                component.set("v.lstPayeeAttendance", payeeAttendance);

                console.log('>>>>>>>>>>rtnVal.SelectedUsersOption>>>>>>>>>' , rtnVal.SelectedUsersOption);
                
                component.set("v.SelectedUsersOption", helper.defaultSelectedValue(component, event, helper, component.get("v.UsersOptions"), rtnVal.SelectedUsersOption));
                var defaultOwners = component.get("v.SelectedUsersOption");
                try {
                    component.find("ownerCheckbox").forEach( function(itemcmp) {
                        console.log('>>>>>>>>>>defaultOwners>>>>>>>>>' , defaultOwners);
                        console.log('>>>>>>>>>>itemcmp.get("v.name")>>>>>>>>>' , itemcmp.get("v.name"));
                        if(defaultOwners.includes(itemcmp.get("v.name")))
                        {
                            console.log('>>>>>>>>>>Yes it contains>>>>>>>>>');
                            itemcmp.set("v.checked",true); 
                            console.log('>>>>>>>>>>itemcmp>>>>>>>>>' , itemcmp);
                        }
                    } );
                }
                catch(err) {//Tharaka 2021-01-25 stop the process if user left the browser window.
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'Information',
                        message: 'Page load interrupted, please reload the page again.',
                        type : 'warning',
                        mode: 'sticky'
                    });
                    toastEvent.fire();
                    component.set("v.showSpinner", false); 
                    return;
                }
                console.log('>>>>>>>>>>component.get("v.SelectedUsersOption">>>>>>>>>' , component.get("v.SelectedUsersOption"));

                // setting the owner check box to true                
                // component.set("v.SelectedUsersOption",helper.selectAll(component, event, helper, component.get("v.UsersOptions")));
                
                // select all checkbox                
                /*
                component.find("ownerCheckbox").forEach( function(itemcmp) {
                    itemcmp.set("v.checked",true);     
                } );
                */
                
                helper.loadChartsUserWiseChartCurrentMonth(component, event, helper);
                helper.loadChartsUserWiseChartQuarterly(component, event, helper);
                helper.loadChartsUserWiseChartMeetingAndZoomMetrics(component, event, helper);
                helper.loadChartsUserWiseChartProfileMetrics(component, event, helper);
                helper.loadChartsUserWiseChartETFLeadsMetrics(component, event, helper);
                
                
                console.log('???????????All Loading of Chart is complete??????????');
                component.set("v.showSpinner", false); 

                
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }       
        });
        $A.enqueueAction(action);
    },

    
    callGetCurrentMonthPayeeAttendanceRecordsDynamic: function (component, event, helper, intSelectedMonth, intSelectedYear) {
        //component.set("v.showSpinner", true);

        var action = component.get("c.getCurrentMonthPayeeAttendanceRecordsDynamic"); 
        action.setParams({
            "intSelectedMonth" : intSelectedMonth,
            "intSelectedYear" : intSelectedYear
        });         
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();                     
            if (state === 'SUCCESS') 
            {   
                // 
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }       
        });
        $A.enqueueAction(action);
    },

    saveActivityTypeAndUserDays: function (component, event, helper, intSelectedMonth, intSelectedYear, currentSelectUserStates) {
        //component.set("v.showSpinner", true);
        var activityTypeObject = component.get("v.objActivityType");
        var userDaysObject = component.get("v.objUsers");
        var action = component.get("c.saveActivityTypeAndUserDays"); 
        action.setParams({
            "activityTypeParam" : activityTypeObject,
            "userDaysParam" : userDaysObject
        });         
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
            var rtnVal = response.getReturnValue();                     
            if (state === 'SUCCESS') 
            {   
                helper.getOnLoadRecords(component, event, helper, intSelectedMonth, intSelectedYear, currentSelectUserStates);
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            }       
        });
        $A.enqueueAction(action);
    },

    //Chart No 1 : User Wise Chart - Current Month - Start 
    loadChartsUserWiseChartCurrentMonth : function(component, event, helper) 
    {
        //component.set("v.showSpinner", true);
        var strSelectedUsers = component.get("v.SelectedUsersOption");
        var objUserDaysLocal = component.get("v.objUsers");
        var objActivityTypeValuesLocal = component.get("v.objActivityType");
        var intSelectedMonthLocal = component.get("v.intSelectedMonthFromCntrl");
        var intSelectedYearLocal = component.get("v.intSelectedYearFromCntrl");

        console.log('>>>>>>>>>>Chart 1 strSelectedUsers>>>>>>>>>>>>', strSelectedUsers); 
        console.log('>>>>>>>>>>objUserDays>>>>>>>>>>>>', component.get("v.objUsers"));
        console.log('>>>>>>>>>>objUserDaysLocal>>>>>>>>>>>>', objUserDaysLocal);

        var action = component.get("c.getUserWiseChartCurrentMonth"); 
        action.setParams({
            "lstUserId" : strSelectedUsers,
            "objUserDays" : objUserDaysLocal,
            "objActivityTypeValues" : objActivityTypeValuesLocal,
            "intSelectedMonth" : intSelectedMonthLocal,
            "intSelectedYear" : intSelectedYearLocal

        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                console.log('>>>>>>>>>>>>>Monthly Chart Data>>>>>>>>>>>>>', response.getReturnValue());
                var dataObj= response.getReturnValue(); 
                component.set("v.dataSalesByUserCurrentMonth",dataObj);
                helper.userWiseChartCurrentMonth(component,event,helper, intSelectedMonthLocal, intSelectedYearLocal);
                component.set("v.showSpinner", false);
                component.set("v.SelectedUsersOption", strSelectedUsers);
            }
        });
        $A.enqueueAction(action);
        
    },

    userWiseChartCurrentMonth : function(component,event,helper, intSelectedMonth, intSelectedYear) {
        
        var jsonDataChannel = component.get("v.dataSalesByUserCurrentMonth");
        var userWiseChartData = JSON.parse(jsonDataChannel);

        if(window.myBarChannel && window.myBarChannel !== null){
            window.myBarChannel.destroy();
        }
        
        var ctxChannel = document.getElementById('displayUserWiseChartCurrentMonth'+ intSelectedMonth + '_' + intSelectedYear).getContext('2d');
        window.myBarChannel = new Chart(ctxChannel, {
            type: 'bar',
            data: userWiseChartData,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        display: true,
						offset: true,
                    }]
                    /*
                    yAxes: [{
                        stacked: true
                    }]*/
                },
                plugins: {
                    datalabels: {
                        display: false,
                    },
                }
            }
        });
    },
    //Chart No 1 : User Wise Chart - Current Month - End

    //Chart No 2 : User Wise Chart - Quarterly - Start 
    loadChartsUserWiseChartQuarterly : function(component, event, helper) 
    {
        //component.set("v.showSpinner", true);
        var strSelectedUsers = component.get("v.SelectedUsersOption");
        var objUserDaysLocal = component.get("v.objUsers");
        var objActivityTypeValuesLocal = component.get("v.objActivityType");
        var intSelectedMonthLocal = component.get("v.intSelectedMonthFromCntrl");
        var intSelectedYearLocal = component.get("v.intSelectedYearFromCntrl");

        var action = component.get("c.getUserWiseChartQuarterly"); 
        action.setParams({
            "lstUserId" : strSelectedUsers,
            "objUserDays" : objUserDaysLocal,
            "objActivityTypeValues" : objActivityTypeValuesLocal,
            "intSelectedMonth" : intSelectedMonthLocal,
            "intSelectedYear" : intSelectedYearLocal
            
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                component.set("v.dataSalesByUserQuarterly",dataObj);
                helper.userWiseChartQuarterly(component,event,helper, intSelectedMonthLocal, intSelectedYearLocal);
                component.set("v.showSpinner", false);
                component.set("v.SelectedUsersOption", strSelectedUsers);
            }
        });
        $A.enqueueAction(action);
        
    },

    userWiseChartQuarterly : function(component,event,helper, intSelectedMonth, intSelectedYear) {
        
        var jsonDataChannel_Q = component.get("v.dataSalesByUserQuarterly");
        var userWiseChartData_Q = JSON.parse(jsonDataChannel_Q);

        if(window.myBarChannel_Q && window.myBarChannel_Q !== null){
            window.myBarChannel_Q.destroy();
        }
        
        var ctxChannel_Q = document.getElementById('displayUserWiseChartQuarterly' + intSelectedMonth + '_' + intSelectedYear).getContext('2d');
        window.myBarChannel_Q = new Chart(ctxChannel_Q, {
            type: 'bar',
            data: userWiseChartData_Q,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        display: true,
						offset: true,
                    }]
                    /*
                    yAxes: [{
                        stacked: true
                    }]*/
                },
                plugins: {
                    datalabels: {
                        display: false,
                    },
                }
            }
        });
    },
    //Chart No 2 : User Wise Chart - Quarterly - End
    //Chart No 3 : User Wise Chart - Meeting and Zoom Metrics - Start 
    loadChartsUserWiseChartMeetingAndZoomMetrics : function(component, event, helper) 
    {
        //component.set("v.showSpinner", true);
        var strSelectedUsers = component.get("v.SelectedUsersOption");
        var objUserDaysLocal = component.get("v.objUsers");
        var objActivityTypeValuesLocal = component.get("v.objActivityType");
        var intSelectedMonthLocal = component.get("v.intSelectedMonthFromCntrl");
        var intSelectedYearLocal = component.get("v.intSelectedYearFromCntrl");
        
        var action = component.get("c.getUserWiseChartMeetingAndZoomMetrics"); 
        action.setParams({
            "lstUserId" : strSelectedUsers,
            "objUserDays" : objUserDaysLocal,
            "objActivityTypeValues" : objActivityTypeValuesLocal,
            "intSelectedMonth" : intSelectedMonthLocal,
            "intSelectedYear" : intSelectedYearLocal
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                component.set("v.dataSalesByUserMeetingMetrics",dataObj);
                helper.userWiseChartMeetingAndZoom(component,event,helper, intSelectedMonthLocal, intSelectedYearLocal);
                component.set("v.showSpinner", false);
                component.set("v.SelectedUsersOption", strSelectedUsers);
            }
        });
        $A.enqueueAction(action);
        
    },

    userWiseChartMeetingAndZoom : function(component,event,helper, intSelectedMonth, intSelectedYear) {
        
        var jsonDataChannel_MZ = component.get("v.dataSalesByUserMeetingMetrics");
        var userWiseChartData_MZ = JSON.parse(jsonDataChannel_MZ);

        if(window.myBarChannel_MZ && window.myBarChannel_MZ !== null){
            window.myBarChannel_MZ.destroy();
        }
        
        var ctxChannel_MZ = document.getElementById('displayUserWiseChartMeetingMetrics' + intSelectedMonth + '_' + intSelectedYear).getContext('2d');
        window.myBarChannel_MZ = new Chart(ctxChannel_MZ, {
            type: 'bar',
            data: userWiseChartData_MZ,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                plugins: {
                    datalabels: {
                        display: false,
                    },
                }
            }
        });
    },
    //Chart No 3 : User Wise Chart - Meeting and Zoom Metrics - End

    //Chart No 4 : User Wise Chart - Profile Metrics - Start 
    loadChartsUserWiseChartProfileMetrics : function(component, event, helper) 
    {
        //component.set("v.showSpinner", true);
        var strSelectedUsers = component.get("v.SelectedUsersOption");
        var objUserDaysLocal = component.get("v.objUsers");
        var objActivityTypeValuesLocal = component.get("v.objActivityType");
        var intSelectedMonthLocal = component.get("v.intSelectedMonthFromCntrl");
        var intSelectedYearLocal = component.get("v.intSelectedYearFromCntrl");

        var action = component.get("c.getUserWiseChartProfileMetrics"); 
        action.setParams({
            "lstUserId" : strSelectedUsers,
            "objUserDays" : objUserDaysLocal,
            "objActivityTypeValues" : objActivityTypeValuesLocal,
            "intSelectedMonth" : intSelectedMonthLocal,
            "intSelectedYear" : intSelectedYearLocal
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                component.set("v.dataSalesByUserProfileMetrics",dataObj);
                helper.userWiseChartProfile(component,event,helper, intSelectedMonthLocal, intSelectedYearLocal);
                component.set("v.showSpinner", false);
                component.set("v.SelectedUsersOption", strSelectedUsers);
            }
        });
        $A.enqueueAction(action);
        
    },

    userWiseChartProfile : function(component,event,helper, intSelectedMonth, intSelectedYear) {
        
        var jsonDataChannel_P = component.get("v.dataSalesByUserProfileMetrics");
        var userWiseChartData_P = JSON.parse(jsonDataChannel_P);

        if(window.myBarChannel_P && window.myBarChannel_P !== null){
            window.myBarChannel_P.destroy();
        }
        
        var ctxChannel_P = document.getElementById('displayUserWiseChartProfileMetrics' + intSelectedMonth + '_' + intSelectedYear).getContext('2d');
        window.myBarChannel_P = new Chart(ctxChannel_P, {
            type: 'bar',
            data: userWiseChartData_P,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                plugins: {
                    datalabels: {
                        display: false,
                    },
                }
            }
        });
    },
    //Chart No 4 : User Wise Chart - Profile Metrics - End

    //Chart No 5 : User Wise Chart - ETF Leads Metrics - Start 
    loadChartsUserWiseChartETFLeadsMetrics : function(component, event, helper) 
    {
        //component.set("v.showSpinner", true);
        var strSelectedUsers = component.get("v.SelectedUsersOption");
        var objUserDaysLocal = component.get("v.objUsers");
        var objActivityTypeValuesLocal = component.get("v.objActivityType");
        var intSelectedMonthLocal = component.get("v.intSelectedMonthFromCntrl");
        var intSelectedYearLocal = component.get("v.intSelectedYearFromCntrl");

        var action = component.get("c.getUserWiseChartETFLeadsMetrics"); 
        action.setParams({
            "lstUserId" : strSelectedUsers,
            "objUserDays" : objUserDaysLocal,
            "objActivityTypeValues" : objActivityTypeValuesLocal,
            "intSelectedMonth" : intSelectedMonthLocal,
            "intSelectedYear" : intSelectedYearLocal
        });    

        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            if (state === "SUCCESS") { 
                var dataObj= response.getReturnValue(); 
                console.log('??????????? getUserWiseChartETFLeadsMetrics ??????????', dataObj);
                component.set("v.dataSalesByUserETFLeadsMetrics",dataObj);
                helper.userWiseChartETFLeads(component,event,helper,intSelectedMonthLocal, intSelectedYearLocal);
                component.set("v.showSpinner", false);
                component.set("v.SelectedUsersOption", strSelectedUsers);
            }
        });
        console.log('???????????All Loading of Chart is complete last ??????????');
        $A.enqueueAction(action);
        
    },

    userWiseChartETFLeads : function(component,event,helper,intSelectedMonth, intSelectedYear) {
        
        var jsonDataChannel_ETL = component.get("v.dataSalesByUserETFLeadsMetrics");
        var userWiseChartData_ETL = JSON.parse(jsonDataChannel_ETL);
        if(window.myBarChannel_ETL != undefined && window.myBarChannel_ETL !== null){
            window.myBarChannel_ETL.destroy();
        }
        
        var myBarChannel_ETL = document.getElementById('displayUserWiseChartETFLeadsMetrics' + intSelectedMonth + '_' + intSelectedYear).getContext('2d');
        window.myBarChannel_ETL = new Chart(myBarChannel_ETL, {
            type: 'bar',
            data: userWiseChartData_ETL,
            options: {
                title: {
                    display: true,
                    text: ''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                plugins: {
                    datalabels: {
                        display: false,
                    },
                }
            }
        });
    },
    //Chart No 5 : User Wise Chart - ETF Leads Metrics - End

    defaultSelectedValue : function(component, event, helper, allOptions, defaultOptions) {            
			
        // reset the selected options
        var selectedOptions = [];
        
        // for each option set
        for(var ele in allOptions){
            if(defaultOptions.includes(allOptions[ele].value)){
                selectedOptions.push(allOptions[ele].value);
            }
        }

        console.log('>>>>>>>>>>>>>selectedOptions>' , selectedOptions);
        
        return selectedOptions ;   
        
    },  

 	selectAll : function(component, event, helper, allOptions) {            
			
        // reset the selected options
        var selectedOptions = [];
        
        // for each option set
        for(var ele in allOptions){
            if(allOptions[ele].value){
                selectedOptions.push(allOptions[ele].value);
            }
        }
        
        return selectedOptions ;   
        
    },  
    
    addNewSelectedValue : function(component, event, helper, selectecoptions, newSelection){ 
        
        // if the value is not already present
        if(!selectecoptions.includes(newSelection)){
            selectecoptions.push(newSelection);
        }          
        return selectecoptions;
    },
    
    removeSelectedValue : function(component, event, helper, selectedOpts, removeEle){         
        // this will save the options already Selected
        var newSelectedOpts = [];
        
        // for each selected option set
        for(var ele in selectedOpts){
            
            if (selectedOpts[ele] != removeEle
               	&& selectedOpts[ele] != 'ALL'){                
                newSelectedOpts.push(selectedOpts[ele]);
            }
        }
		return newSelectedOpts;
    },

    //Tharaka 2020-12-15
    getDrillDownActivities: function (component, event, helper, selectedUser) {
        component.set("v.showSpinner", true);   
        var selectedMonth = component.get('v.intSelectedMonthFromCntrl');
        var selectedYear = component.get('v.intSelectedYearFromCntrl');
        var action = component.get("c.getDrillDownUserRecords"); 
        action.setParams({
            "strUserId" : selectedUser,
            "intSelectedMonth" : selectedMonth,
            "intSelectedYear" : selectedYear
        });         
        action.setCallback(this, function(response)
        {            
            var state = response.getState();  
                          
            if (state === 'SUCCESS') 
            {   
                var rtnVal = response.getReturnValue();       
                component.set("v.lstUserDrillDownActivity", rtnVal);
                
            }           
            else if (state === 'INCOMPLETE') 
            {                            
                //Do when Incomplete
            }           
            else if (state === 'ERROR') 
            { 
               // Do When Error              
            } 
            component.set("v.showSpinner", false);      
        });
        $A.enqueueAction(action);
    },
})