({
    doInit : function(component, event, helper) {
        var currentAccountId = component.get("v.recordId");
        var params = {'strAccountId' : currentAccountId};
        helper.callToServer(
            component,
            "c.findAccountsForMap",
            function(response)
            { 
                console.log('apex response :'+JSON.stringify(response));
                var markers = [];
                for(var i=0; i < response.length; i++) {
                    var accountInfo = response[i];
                    var completeAddress=[];
                    if(accountInfo.BillingCity){
                        completeAddress.push(accountInfo.BillingCity);
                    }if(accountInfo.BillingState){
                       completeAddress.push(accountInfo.BillingState);
                    }if(accountInfo.BillingPostalCode){
                        completeAddress.push(accountInfo.BillingPostalCode);
                    }if(accountInfo.BillingCountry){
                        completeAddress.push(accountInfo.BillingCountry);
                    }
                    
                   markers.push({
                        'location': {
                            'City': accountInfo.BillingCity,
                            'Country': accountInfo.BillingCountry,
                            'PostalCode': accountInfo.BillingPostalCode,
                            'State': accountInfo.BillingState,
                            'Street': accountInfo.Street
                        },
                        'icon': 'standard:account',
                        'title' : accountInfo.Name,
                        'description' : completeAddress.join()
                    });
                    
                }
                console.log('****markers data-'+JSON.stringify(markers));
                component.set('v.markersTitle', 'All Account Locations');
                component.set('v.mapMarkers', markers);
            }, 
            params
        );	
    },

    onMarkerSelection :  function(component, event, helper) {
        
        /*
        console.log('???????event???????', event);
        console.log('???????event???????', event.getSource());
        
        var target = event.getSource();
        console.log('???????target???????', target);
        console.log("current text: " + target.get("v.markersTitle"));
        console.log("current text: " + target.get("v.mapMarkers"));
        console.log("current text: " + target.get("v.name"));
        console.log("current text: " + target.get("v.value"));
        */
        /*
        var recordId = component.get('v.recordId');
        var navEvt;
        if(recordId) {
            navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParam("recordId", recordId);
        }
        */
        
    }
})