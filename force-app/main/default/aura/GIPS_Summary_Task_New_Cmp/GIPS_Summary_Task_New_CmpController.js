({
    //Initialize
    doInIt : function(component, event, helper) {
        helper.doInitialize(component, event);
	},

    //called on cancel click
    handleCancelClick : function(component, event, helper){
        helper.cancelOut(component,event);
    },

    //called on cancel click
    handleSaveClick : function(component, event, helper){
        helper.saveGIPSSummary(component,event);
    },

    handleAddGIPS: function(component, event, helper){
        helper.addGIPSSummaryRecord(component,event);
    },

    onChangeDate : function(component, event, helper){
        helper.callGIPSOnDateChange(component,event);
    }
    
})