({
    //calling method getSubscriptionDetails from apex and assigning returned wrapperlist to attributes
	doInitialize : function(component,event) {
        component.set("v.showSpinner",true);
		var recordId = component.get("v.recordId");
        
        var action = component.get("c.getOnLoadRecords_Account");
        action.setParams({
            "strAccountId" : recordId
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            var rtnVal = response.getReturnValue();
            if (state === "SUCCESS")
            {
                component.set("v.objAccount", rtnVal.Account);
                component.set("v.lstGIPSSummary", rtnVal.GIPSSummary)
                component.set("v.lstOLDGIPSSummary", rtnVal.OLD_GIPSSummary)
                component.set("v.showSpinner",false);
            }
        });
        $A.enqueueAction(action);
	},

    addGIPSSummaryRecord : function(component,event) {
        component.set("v.showSpinner",true);
        var recordId = component.get("v.recordId");
        var gipsSummary = component.get("v.lstGIPSSummary");
        
        var action = component.get("c.addGIPSSummary");
        action.setParams({
            "objectName" : 'Account',
            "strRecordId" : recordId,
            "lstGIPS" : gipsSummary
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            var rtnVal = response.getReturnValue();
            if (state === "SUCCESS")
            {
                component.set("v.lstGIPSSummary", rtnVal)
                component.set("v.showSpinner",false);
            }
        });
        $A.enqueueAction(action);
	},
    

    //go back to the contact record
    cancelOut : function(component,event){
        var recordId = component.get("v.recordId");
        
        if((typeof sforce != 'undefined') && sforce && (!!sforce.one))
        {
            var result = '/lightning/r/Account/'+recordId+'/view';
            sforce.one.navigateToURL(result);
        }
        else{
            var result = '/'+recordId;
            window.open(result,'_top');
        }
    },

    showSuccessToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: 'The record has been updated successfully.',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    showErrorToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error',
            message:'Please fill all required fields',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    
    
    //save GIPS Summary and go back to the contact record
    saveGIPSSummary : function(component,event,helper){
        var recordId = component.get("v.recordId");
        var varlstGIPS = component.get("v.lstGIPSSummary");
        var action = component.get("c.saveAccountGIPSRecord");
        action.setParams({
            "lstGIPS" : varlstGIPS,
            "strAccountId" : recordId
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            var rtnVal = response.getReturnValue();
            if (state === "SUCCESS")
            {
                console.log('>>>>>>>>>>>rtnVal>>>>>>>>>>>>' + rtnVal);
                if(rtnVal == 'Success')
                {
                    if((typeof sforce != 'undefined') && sforce && (!!sforce.one))
                    {
                        var result = '/lightning/r/Account/'+recordId+'/view';
                        sforce.one.navigateToURL(result);
                    }
                    else{
                        var result = '/'+recordId;
                        window.open(result,'_top');
                    }
                }
                else 
                {
                    console.log('>>>>>>>>>>>rtnVal 2>>>>>>>>>>>>' + rtnVal);
                    //this.showErrorToast(component, event, helper);
                    if(rtnVal == 'Empty')
                        component.set('v.strMessage', '** Please fill all required fields.');
                    else
                        component.set('v.strMessage', rtnVal);
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    

    callGIPSOnDateChange : function(component,event) {
        component.set("v.showSpinner",true);
        var objAccount = component.get("v.objAccount");
        var lstGIPSSummary = component.get("v.lstGIPSSummary");

        var accountId = objAccount.Id;
        
        var action = component.get("c.getAccountGIPSOnDateChange");
        action.setParams({
            "strContactId" : null,
            "strAccountId" :  accountId,
            "lstGIPSParam" : lstGIPSSummary
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            var rtnVal = response.getReturnValue();
            if (state === "SUCCESS")
            {
                component.set("v.lstGIPSSummary", rtnVal)
                component.set("v.showSpinner",false);
            }
        });
        $A.enqueueAction(action);
	},
})