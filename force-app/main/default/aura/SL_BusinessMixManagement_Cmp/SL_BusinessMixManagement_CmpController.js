({
	//Initialize
    doInIt : function(component, event, helper) {
        helper.doInitialize(component, event);
	},
    
    handleSubmitClick : function(component, event, helper){
        let oldWrapperLst = JSON.parse(JSON.stringify(component.get("v.busMixWrapperLst")));
        let newWrapperLst = JSON.parse(JSON.stringify(component.get("v.busMixWrapperLstTemp")));
        
        var sameWrapper = true;
        for(var i=0; i < oldWrapperLst.length ; i++){
            if((oldWrapperLst[i]['checked'] != newWrapperLst[i]['checked'])
              || ((oldWrapperLst[i]['comment'] != newWrapperLst[i]['comment'])))
            {
                sameWrapper = false;
                break;
            }
        }
        
        if(sameWrapper)
        {
            window.alert('Please select any Business Mix or add any comment to continue..');
        }
        else{
            helper.handleSubmitClick(component, event);
        }
    },
    
    //called on cancel click
    handleCancelClick : function(component, event, helper){
        helper.cancelOut(component,event);
    },
})