({
    //calling method getSubscriptionDetails from apex and assigning returned wrapperlist to attributes
	doInitialize : function(component,event) {
        component.set("v.showSpinner",true);
		var recordId = component.get("v.recordId");
        
        var action = component.get("c.getOnLoadRecords_Task");
        action.setParams({
            "strTaskId" : recordId
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            var rtnVal = response.getReturnValue();
            if (state === "SUCCESS")
            {
                component.set("v.objTask", rtnVal.Task);
                component.set("v.objContact", rtnVal.Contact);
                component.set("v.lstGIPSSummary", rtnVal.GIPSSummary)
                component.set("v.showSpinner",false);
            }
        });
        $A.enqueueAction(action);
	},

    addGIPSSummaryRecord : function(component,event) {
        component.set("v.showSpinner",true);
        var recordId = component.get("v.recordId");
        var gipsSummary = component.get("v.lstGIPSSummary");
        
        var action = component.get("c.addGIPSSummary");
        action.setParams({
            "objectName" : 'Task',
            "strRecordId" : recordId,
            "lstGIPS" : gipsSummary
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            var rtnVal = response.getReturnValue();
            if (state === "SUCCESS")
            {
                component.set("v.lstGIPSSummary", rtnVal)
                component.set("v.showSpinner",false);
            }
        });
        $A.enqueueAction(action);
	},

    //go back to the contact record
    cancelOut : function(component,event){
        var recordId = component.get("v.recordId");
        
        if((typeof sforce != 'undefined') && sforce && (!!sforce.one))
        {
            var result = '/lightning/r/Contact/'+recordId+'/view';
            sforce.one.navigateToURL(result);
        }
        else{
            var result = '/'+recordId;
            window.open(result,'_top');
        }
    },

    showSuccessToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: 'The record has been updated successfully.',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    showErrorToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error',
            message:'Please fill all required fields',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    
    //save GIPS Summary and go back to the contact record
    saveGIPSSummary : function(component,event,helper){
        var recordId = component.get("v.recordId");
        var varlstGIPS = component.get("v.lstGIPSSummary");
        var action = component.get("c.saveGIPSRecord");
        action.setParams({
            "lstGIPS" : varlstGIPS
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            var rtnVal = response.getReturnValue();
            if (state === "SUCCESS")
            {
                console.log('>>>>>>>>>>>rtnVal>>>>>>>>>>>>' + rtnVal);
                if(rtnVal == 'Success')
                {
                    //component.set('v.strMessage', 'The record has been updated successfully.');

                    if((typeof sforce != 'undefined') && sforce && (!!sforce.one))
                    {
                        var result = '/lightning/r/Contact/'+recordId+'/view';
                        sforce.one.navigateToURL(result);
                    }
                    else{
                        var result = '/'+recordId;
                        window.open(result,'_top');
                    }
                }
                else 
                {
                    console.log('>>>>>>>>>>>rtnVal 2>>>>>>>>>>>>' + rtnVal);
                    //this.showErrorToast(component, event, helper);

                    component.set('v.strMessage', '** Please fill all required fields.');
                }
            }
        });
        $A.enqueueAction(action);
        
    },

    callGIPSOnDateChange : function(component,event) {
        component.set("v.showSpinner",true);
        var objContact = component.get("v.objContact");
        var lstGIPSSummary = component.get("v.lstGIPSSummary");

		var contactId = objContact.Id;
        var accountId = objContact.AccountId;
        
        var action = component.get("c.getGIPSRecordsRelatedToContact");
        action.setParams({
            "strContactId" : contactId,
            "strAccountId" :  accountId,
            "lstGIPSParam" : lstGIPSSummary
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            var rtnVal = response.getReturnValue();
            if (state === "SUCCESS")
            {
                component.set("v.lstGIPSSummary", rtnVal)
                component.set("v.showSpinner",false);
            }
        });
        $A.enqueueAction(action);
	},
})