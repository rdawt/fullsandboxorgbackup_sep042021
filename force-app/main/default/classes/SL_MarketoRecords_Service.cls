public class SL_MarketoRecords_Service 
{
	public static boolean isProcssingFromJSON = false;
	
	@TestVisible 
	public static void doCallOut()
	{
		
	}
	
	@TestVisible 
	// This function will create task records from JSON File
	public static void createTaskRecords()
	{
		Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
        SendEmailNotification se = new SendEmailNotification('Error: Marketo JSON Service Processing Exception');
        
		try
		{
			List<SL_MarketoRecords_Wrapper> lstSL_MarketoRecords_Wrapper = new List<SL_MarketoRecords_Wrapper>();
			Map<Integer, String> mapActivityType = new Map<Integer, String>();
			mapActivityType = getActivityType();
			
			List<Marketo_JSON__c> lstMarketo_JSON = new List<Marketo_JSON__c>();
			List<Marketo_JSON__c> lstMarketo_JSON_Update = new List<Marketo_JSON__c>();
			//lstMarketo_JSON = [Select Id, Name__c, isActive__c, JSON__c, Activity_Date__c, Processed_Datetime__c from Marketo_JSON__c where isActive__c = true];
			
			/* this code is used to process data directly from custom object.
			for(Marketo_JSON__c objMarketo_JSON : lstMarketo_JSON)
			{
				if(objMarketo_JSON.JSON__c != NULL && objMarketo_JSON.JSON__c != '')
				{
					lstSL_MarketoRecords_Wrapper.addAll(SL_MarketoRecords_Wrapper.parse(objMarketo_JSON.JSON__c));
					Marketo_JSON__c objMarketo_JSON_Update = new Marketo_JSON__c(Id = objMarketo_JSON.Id);
					objMarketo_JSON_Update.Activity_Date__c = Date.today();
					objMarketo_JSON_Update.Processed_Datetime__c = Datetime.now();
					objMarketo_JSON_Update.isActive__c = false;
					lstMarketo_JSON_Update.add(objMarketo_JSON_Update);
				}
			}
			*/
			
			// This code is to fetch json from static resource
			StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'MarketoJSONData' LIMIT 1];
			String body = '';
			body = sr.Body.toString();
			
			if(!Test.isRunningTest())
				lstSL_MarketoRecords_Wrapper.addAll(SL_MarketoRecords_Wrapper.parse(body));
			else
				lstSL_MarketoRecords_Wrapper.addAll(SL_MarketoRecords_Wrapper.parse(SL_MarketoRecords_Service_Test.json));	
			
			Set<String> setContactIds = new Set<String>();
			Set<String> setAccountIds = new Set<String>();
			Set<String> setOwnerIds = new Set<String>();
			
			for(SL_MarketoRecords_Wrapper objRecord : lstSL_MarketoRecords_Wrapper)
			{
				system.debug('>>>>>>>>>>>>>>objRecord.sfdcAccountId>>>>>>>>>>>>>>>' + objRecord.sfdcAccountId);
				system.debug('>>>>>>>>>>>>>>objRecord.sfdcContactId>>>>>>>>>>>>>>>' + objRecord.sfdcContactId);
				system.debug('>>>>>>>>>>>>>>objRecord.sfdcLeadOwnerId>>>>>>>>>>>>>>>' + objRecord.sfdcLeadOwnerId);
				
				if(objRecord.sfdcAccountId != NULL && objRecord.sfdcAccountId != '')
					setAccountIds.add(objRecord.sfdcAccountId);
				
				if(objRecord.sfdcContactId != NULL && objRecord.sfdcContactId != '')
					setContactIds.add(objRecord.sfdcContactId);
				
				if(objRecord.sfdcLeadOwnerId != NULL && objRecord.sfdcLeadOwnerId != '')
					setOwnerIds.add(objRecord.sfdcLeadOwnerId);		
			}
			
			Map<String, Id> mapAccount = new Map<String, Id>();
			Map<String, Id> mapContact = new Map<String, Id>();
			Map<Id, Boolean> mapUser = new Map<Id, Boolean>();
			
			if(!setAccountIds.isEmpty())
			{
				for(Account objAccount : [Select Id from Account where Id IN: setAccountIds])
				{
					mapAccount.put(objAccount.Id, objAccount.Id);
				}
			}
			
			if(!setContactIds.isEmpty())
			{
				for(Contact objContact : [Select Id from Contact where Id IN: setContactIds])
				{
					mapContact.put(objContact.Id, objContact.Id);
				}
			}
			
			if(!setOwnerIds.isEmpty())
			{
				for(User objUser : [Select Id , isActive from User where Id IN: setOwnerIds and isActive = true])
				{
					mapUser.put(objUser.Id, objUser.isActive);
				}
			}
				
			Set<String> setContactIdsNotPresent = new Set<String>();
			Set<String> setAccountIdsNotPresent = new Set<String>();
			Set<String> setOwnerIdsNotPresent = new Set<String>();	
			
			List<Task> lstTask = new List<Task>();
			for(SL_MarketoRecords_Wrapper objRecord : lstSL_MarketoRecords_Wrapper)
			{
			    if(objRecord != NULL)
			    {
			        Task objTask = new Task();
			        objTask.Email__c = objRecord.email;
			        if(objRecord.activityDate != NULL && objRecord.activityDate != '')
			        	objTask.ActivityDate = Date.valueOf(objRecord.activityDate);
			        else
			        	objTask.ActivityDate = Date.today();
			        
			        if(mapContact.containskey(objRecord.sfdcContactId))		
			        	objTask.WhoId = objRecord.sfdcContactId;
			        else
			        	setContactIdsNotPresent.add(objRecord.sfdcContactId);	
			        
			        if(mapAccount.containskey(objRecord.sfdcAccountId))
			        	objTask.WhatId = objRecord.sfdcAccountId;
			        else
			        	setAccountIdsNotPresent.add(objRecord.sfdcAccountId);	
			        
			        if(mapUser.containskey(objRecord.sfdcLeadOwnerId))
			        	objTask.OwnerId = objRecord.sfdcLeadOwnerId;
			        else
			        {
			        	setOwnerIdsNotPresent.add(objRecord.sfdcLeadOwnerId);
			        	objTask.OwnerId = system.UserInfo.getUserId();	
			        }
			        if(mapActivityType.containskey(objRecord.activityTypeId))
			        {
			        	objTask.Subject = mapActivityType.get(objRecord.activityTypeId) + '' +objRecord.primaryAttributeValue;
			        	objTask.Summary_Recap__c = mapActivityType.get(objRecord.activityTypeId) + '' +objRecord.primaryAttributeValue;
			        }
			        else
			        {
			        	objTask.Subject = objRecord.primaryAttributeValue;
			        	objTask.Summary_Recap__c = objRecord.primaryAttributeValue;
			        }
			        
			        objTask.Processing_Source__c = 'Marketo JSON';
			        	
			        lstTask.add(objTask);
			    }
			}
			
			Marketo_JSON__c objMarketo_JSON_Insert = new Marketo_JSON__c();
			objMarketo_JSON_Insert = createMarketoJSONRecord(setAccountIdsNotPresent, setContactIdsNotPresent, setOwnerIdsNotPresent, 
																lstSL_MarketoRecords_Wrapper.size() , lstTask.size(), sr);
			
			system.debug('>>>>>>>>>>>lstTask Size>>>>>>>' + lstTask.size());
			system.debug('>>>>>>>>>>>lstTask>>>>>>>' + lstTask);
			
			if(!lstTask.isEmpty())
			{
				insertTaskRecords(lstTask, objMarketo_JSON_Insert);	
			}
		}
		catch(Exception e)
		{
			String strErrorBody = '';
            strErrorBody = 'Exception Line Number : ' + e.getLineNumber() + ' \n ' + 
                            'Exception Type Name : ' + e.getTypeName() +  ' \n ' + 
                            'Exception Stack Tracing : ' + e.getStackTraceString() + ' \n ' + 
                            'Exception Message : ' + e.getMessage();
                              
            se.sendEmailMoreDetails(strErrorBody);
		}
	}
	
	@TestVisible 
	// This function will create Records in Market JSON Activity Object
	public static void createMarketoJSONActivityRecords()
	{
		Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
        SendEmailNotification se = new SendEmailNotification('Error: Marketo JSON Service Processing Exception');
        
		try
		{
			transient List<SL_MarketoRecords_Wrapper> lstSL_MarketoRecords_Wrapper = new List<SL_MarketoRecords_Wrapper>();
			transient Map<Integer, String> mapActivityType = new Map<Integer, String>();
			mapActivityType = getActivityType();
			
			transient List<Marketo_JSON__c> lstMarketo_JSON = new List<Marketo_JSON__c>();
			transient List<Marketo_JSON__c> lstMarketo_JSON_Update = new List<Marketo_JSON__c>();
			
			// This code is to fetch json from static resource
			transient StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'MarketoJSONData' LIMIT 1];
			
			if(!Test.isRunningTest())
				lstSL_MarketoRecords_Wrapper.addAll(SL_MarketoRecords_Wrapper.parse(sr.Body.toString()));
			else
				lstSL_MarketoRecords_Wrapper.addAll(SL_MarketoRecords_Wrapper.parse(SL_MarketoRecords_Service_Test.json));	
			
			transient Set<String> setContactIdsNotPresent = new Set<String>();
			transient Set<String> setAccountIdsNotPresent = new Set<String>();
			transient Set<String> setOwnerIdsNotPresent = new Set<String>();	
			
			
			transient List<Marketo_JSON_Activity__c> lstMarketo_JSON_Activity = new List<Marketo_JSON_Activity__c>();
			for(SL_MarketoRecords_Wrapper objRecord : lstSL_MarketoRecords_Wrapper)
			{
			    if(objRecord != NULL)
			    {
			        transient Marketo_JSON_Activity__c objMarketo_JSON_Activity = new Marketo_JSON_Activity__c();
			        objMarketo_JSON_Activity.Email__c = objRecord.email;
			        if(objRecord.activityDate != NULL && objRecord.activityDate != '')
			        	objMarketo_JSON_Activity.Activity_Date__c = Date.valueOf(objRecord.activityDate);
			        else
			        	objMarketo_JSON_Activity.Activity_Date__c = Date.today();
			        
		        	objMarketo_JSON_Activity.WhoId__c = objRecord.sfdcContactId;
		        	objMarketo_JSON_Activity.WhatId__c = objRecord.sfdcAccountId;
		        	objMarketo_JSON_Activity.Activity_Owner__c = objRecord.sfdcLeadOwnerId;
			        
			        if(mapActivityType.containskey(objRecord.activityTypeId))
			        {
			        	objMarketo_JSON_Activity.Subject__c = mapActivityType.get(objRecord.activityTypeId) + '' +objRecord.primaryAttributeValue;
			        	objMarketo_JSON_Activity.Summary_Recap__c = mapActivityType.get(objRecord.activityTypeId) + '' +objRecord.primaryAttributeValue;
			        }
			        else
			        {
			        	objMarketo_JSON_Activity.Subject__c = objRecord.primaryAttributeValue;
			        	objMarketo_JSON_Activity.Summary_Recap__c = objRecord.primaryAttributeValue;
			        }
			        
			        objMarketo_JSON_Activity.Processing_Source__c = 'Marketo JSON';
			        	
			        lstMarketo_JSON_Activity.add(objMarketo_JSON_Activity);
			    }
			}
			
			transient Marketo_JSON__c objMarketo_JSON_Insert = new Marketo_JSON__c();
			objMarketo_JSON_Insert = createMarketoJSONRecord(setAccountIdsNotPresent, setContactIdsNotPresent, setOwnerIdsNotPresent, 
																lstSL_MarketoRecords_Wrapper.size() , lstMarketo_JSON_Activity.size(), sr);
			if(!lstMarketo_JSON_Activity.isEmpty())
			{
				insert lstMarketo_JSON_Activity;
				insert objMarketo_JSON_Insert;	
			}
		}
		catch(Exception e)
		{
			String strErrorBody = '';
            strErrorBody = 'Exception Line Number : ' + e.getLineNumber() + ' \n ' + 
                            'Exception Type Name : ' + e.getTypeName() +  ' \n ' + 
                            'Exception Stack Tracing : ' + e.getStackTraceString() + ' \n ' + 
                            'Exception Message : ' + e.getMessage();
                              
            se.sendEmailMoreDetails(strErrorBody);
		}
	}
	
	public static void insertTaskRecords(List<Task> lstTask, Marketo_JSON__c objMarketo_JSON_Insert)
	{
		SL_MarketoRecords_Service.isProcssingFromJSON = true;
		insert lstTask;
		
		insert objMarketo_JSON_Insert;
	} 
	
	public static Marketo_JSON__c createMarketoJSONRecord(Set<String> setAccountIdsNotPresent, Set<String> setContactIdsNotPresent, 
															Set<String> setOwnerIdsNotPresent, 
															Integer intTotalFoundCount, Integer intTotalProcessedCount,
															staticresource sr)
	{
		List<String> lstContactIdsNotPresent = new List<String>();
		List<String> lstAccountIdsNotPresent = new List<String>();
		List<String> lstOwnerIdsNotPresent = new List<String>();
		
		if(!setAccountIdsNotPresent.isEMpty())
			lstAccountIdsNotPresent.addAll(setAccountIdsNotPresent);
			
		if(!setContactIdsNotPresent.isEMpty())
			lstContactIdsNotPresent.addAll(setContactIdsNotPresent);
		
		if(!setOwnerIdsNotPresent.isEMpty())
			lstOwnerIdsNotPresent.addAll(setOwnerIdsNotPresent);		
		
		Marketo_JSON__c objMarketo_JSON_Insert = new Marketo_JSON__c();
		objMarketo_JSON_Insert.Activity_Date__c = Date.today();
		objMarketo_JSON_Insert.Processed_Datetime__c = Datetime.now();
		objMarketo_JSON_Insert.isActive__c = false;
		objMarketo_JSON_Insert.Ids_Not_Processed_Account__c = String.join(lstAccountIdsNotPresent, ', ');
		objMarketo_JSON_Insert.Ids_Not_Processed_Contact__c = String.join(lstContactIdsNotPresent, ', ');
		objMarketo_JSON_Insert.Ids_Not_Processed_InActive_Users__c = String.join(lstOwnerIdsNotPresent, ', ');
		objMarketo_JSON_Insert.Total_Count_of_Records_Found__c = intTotalFoundCount;
		objMarketo_JSON_Insert.Total_Count_of_Records_Processed__c = intTotalProcessedCount;
		if(sr.body != NULL)
		{
			//objMarketo_JSON_Insert.JSON__c = sr.body.toString().substring(0, 50000);
		}
		
		return objMarketo_JSON_Insert;
		
	}
	
	public static Map<Integer, String> getActivityType()
	{
		Map<Integer, String> mapActivityType = new Map<Integer, String>();
		mapActivityType.put(10, 'Opened Email:');
		mapActivityType.put(11 , 'Clicked Link in Email:');
		
		return mapActivityType;
	}   
}