/********************************************************************************************
* @Author: Tharaka De Silva
* @Date : 2021-06-28

* Batch class that upsert Contact Sales by Fund data in to Contact Sales by Fund History Monthly object
* With this batch, allows to upsert all the records in SalesConnect__Contact_Portfolio_Breakdown__c object 
* in to History_Con_Portfolio_Breakdown_Monthly__c object
* *******************************************************************************************/
public class BatchConSalesByFundMonthlyUpsert implements Database.Batchable<sObject>, Database.Stateful{
    
    List<String> lstErrorMsgs = new List<String>();
    Map<String, Id> mapDSTFundMapping = new Map<String, Id>();
    String strYear;
    String strMonth;

    /************************************************************************
    * Constructor for BatchConSalesByFundMonthlyUpsert.  
    * Get the Year and month
    * Keep fund mapping information
    ************************************************************************/
    public BatchConSalesByFundMonthlyUpsert(){
        strYear = String.valueof(Date.Today().Year());
        strMonth = String.valueof(Date.Today().Month());

        List<Fund_Mapping__c> lstFundMapping = [SELECT Name,SFDC_Fund_Lookup__c FROM Fund_Mapping__c WHERE Status__c = 'Active'];
        if(lstFundMapping.size() > 0){
            for(Fund_Mapping__c mapping: lstFundMapping){
                mapDSTFundMapping.put(mapping.Name,mapping.SFDC_Fund_Lookup__c);
            }
        }
    }

    /************************************************************************
    * Starts up the batch using the query specified in strQuery string
    *
    * @param bc					Context of the batch
    * @return     				The query locator
    ************************************************************************/
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String strQuery = 'SELECT Id,Of_Trades_Placed_Redemption_YTD__c,Of_Trades_Placed_Sales_YTD__c,DDS_Last_Month_Sales_with_Partnerships__c,'
            +'SalesConnect__Contact_ID__c,SalesConnect__External_ID__c,Fund_Goal__c,Firm_Branch_Lookup__c,'
            +'Name,SalesConnect__Current_Assets__c,SalesConnect__Contact__c,SalesConnect__Last_Year_Redemptions__c,'
            +'SalesConnect__Last_Year_Sales__c,SalesConnect__MTD_Redemptions__c,SalesConnect__MTD_Sales__c,SalesConnect__Yesterday_Redemptions__c,'
            +'SalesConnect__Yesterday_Sales__c,SalesConnect__YTD_Redemptions__c,SalesConnect__YTD_Sales__c,SFDC_Channel_Id__c,SFDC_Fund_Id__c,SFDC_Fund_Ticker__c,'
            +'LastModifiedDate,SalesConnect__Contact__r.AccountId,Fund_Vehicle_Type__c FROM SalesConnect__Contact_Portfolio_Breakdown__c '; 
        
            return Database.getQueryLocator(strQuery); 
    } 

    /************************************************************************
    * Executes the batch and upsert the records in to History_Con_Portfolio_Breakdown_Monthly__c object
    *
    * @param bc					Context of the batch
    * @param records			The list of objects (SalesConnect__Contact_Portfolio_Breakdown__c)
    ************************************************************************/
    public void execute(Database.BatchableContext bc, List<SalesConnect__Contact_Portfolio_Breakdown__c> records) {
        try{
            List<History_Con_Portfolio_Breakdown_Monthly__c> lstHistory = new List<History_Con_Portfolio_Breakdown_Monthly__c>();
            History_Con_Portfolio_Breakdown_Monthly__c history;
            
            if(strYear == null || strMonth == null){
                System.debug('Eror : No Year/Month information found');
                lstErrorMsgs.add('No Year/Month information found \n');
            }else{
                for(SalesConnect__Contact_Portfolio_Breakdown__c record: records){
                    history = new History_Con_Portfolio_Breakdown_Monthly__c();
                    history.SFDC_External_Id__c = record.SalesConnect__External_ID__c + '-' + strYear + '-' +strMonth;
                    history.Contact_Sales_by_Fund_Id__c = record.Id;
                    history.Of_Trades_Placed_Redemption_YTD__c = record.Of_Trades_Placed_Redemption_YTD__c;
                    history.Of_Trades_Placed_Sales_YTD__c = record.Of_Trades_Placed_Sales_YTD__c;
                    history.DDS_Last_Month_Sales_with_Partnerships__c = record.DDS_Last_Month_Sales_with_Partnerships__c;
                    history.SalesConnect_Contact_ID__c = record.SalesConnect__Contact_ID__c;
                    history.SalesConnect_External_ID__c = record.SalesConnect__External_ID__c;
                    history.Firm_Branch_Lookup__c = record.SalesConnect__Contact__r.AccountId;
                    history.Fund_Goal__c = record.Fund_Goal__c;
                    history.Name = record.Name;
                    history.SalesConnect_Current_Assets__c = record.SalesConnect__Current_Assets__c;
                    history.SalesConnect_Contact__c = record.SalesConnect__Contact__c;
                    history.SalesConnect_Last_Year_Redemptions__c = record.SalesConnect__Last_Year_Redemptions__c;
                    history.SalesConnect_Last_Year_Sales__c = record.SalesConnect__Last_Year_Sales__c;
                    history.SalesConnect_MTD_Redemptions__c = record.SalesConnect__MTD_Redemptions__c;
                    history.SalesConnect_MTD_Sales__c = record.SalesConnect__MTD_Sales__c;
                    history.Asset_Date__c = record.LastModifiedDate.date();
                    history.SalesConnect_Yesterday_Redemptions__c = record.SalesConnect__Yesterday_Redemptions__c;
                    history.SalesConnect_Yesterday_Sales__c = record.SalesConnect__Yesterday_Sales__c;
                    history.SalesConnect_YTD_Redemptions__c = record.SalesConnect__YTD_Redemptions__c;
                    history.SalesConnect_YTD_Sales__c = record.SalesConnect__YTD_Sales__c;
                    history.Channel_Lookup__c = record.SFDC_Channel_Id__c;
                    
                    if(record.Fund_Vehicle_Type__c == 'Mutual Fund'){
                        history.Fund_Lookup__c = record.SFDC_Fund_Id__c;
                    }else{
                        if(mapDSTFundMapping.containsKey(record.Name)){
                            history.Fund_Lookup__c = mapDSTFundMapping.get(record.Name);
                        }
                    }

                    history.SFDC_Fund_Ticker__c = record.SFDC_Fund_Ticker__c;
                    history.Year__c = strYear;
                    history.Month__c = strMonth;
                    history.Fund_Vehicle_Type__c = record.Fund_Vehicle_Type__c; 
                    lstHistory.add(history);
                }

                Database.UpsertResult[] results = Database.upsert(lstHistory, History_Con_Portfolio_Breakdown_Monthly__c.SFDC_External_Id__c,false);
            
                //Error log --> send to email
                for (Database.UpsertResult ur : results) {
                    if (!ur.isSuccess()) {
                        for(Database.Error objErr : ur.getErrors()) {
                            System.debug('Upsert error : '+objErr.getMessage());
                            lstErrorMsgs.add(objErr.getStatusCode() + ': ' + objErr.getMessage()+ '\n');
                        }
                    } 
                }
            }
        }catch (Exception ex){ 
            System.debug('Exception : '+ex.getStackTraceString());
            lstErrorMsgs.add(ex.getStackTraceString() + '\n');
        }
    }

    /************************************************************************
    * Send the email notification if any errors has occured
    *
    * @param bc					Context of the batch
    ************************************************************************/
    public void finish(Database.BatchableContext bc) {
        if(lstErrorMsgs.size() > 0){
            String allString = String.join(lstErrorMsgs,',');
            SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Batch Update Of Contact Sales By Fund');
            se.toAddresses.add('tdesilva.consultant@vaneck.com');
            se.sendEmail(allString);
        }
    }

}