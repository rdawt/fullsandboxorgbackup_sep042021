/**
  Author: Vidhya Krishnan
  Date Created: 08/13/2012
  Description: Test Class for Create Contact From Registrant Class - used in Community Registration
 */
@isTest
private class CreateConFromRegTest {
    static testMethod void myUnitTest() {
        list<Id> conIds = new list<Id>();
        Account acc = new Account(Name = 'dfasf', Channel__c = 'RIA', BillingCountry='United States');
        insert acc;
        Test.startTest();
        Contact con = new Contact(LastName='Test', AccountId=acc.Id, Email='test@abc.com', MailingCountry='United States', DB_Source__c = 'MVIS Registrant');
        insert con;
        
        Contact con1 = new Contact(LastName='Test', AccountId=acc.Id, Email='test23@abc.com', MailingCountry='United States', DB_Source__c = '');
        insert con1;
        
        
        Contact con2 = new Contact(LastName='Test', AccountId=acc.Id, Email='test23@abc.com', MailingCountry='United States', DB_Source__c = 'MVIS Registrant');
        insert con2;
        
        Campaign camp1 = new Campaign(Name = 'Test Campaign1');
        insert camp1;
        
        Campaign camp2 = new Campaign(Name = 'Test Campaign2');
        insert camp2;
        
        Campaign camp3 = new Campaign(Name = 'Test Campaign3');
        insert camp3;
        
        Community__c c = new Community__c(Name = 'Test Community', Is_Active__c = true );
        insert c;
        
        Subscription_Group__c sg1 = new Subscription_Group__c(Name = 'Test Subscription1');
        insert sg1;
        
        Subscription_Group__c sg2 = new Subscription_Group__c(Name = 'Test Subscription2');
        insert sg2;
        
        Subscription_Group__c sg3 = new Subscription_Group__c(Name = 'Test Subscription3');
        insert sg3;
        
        Community_Registrant__c cr1 = new Community_Registrant__c();
        cr1.User_Name__c = 'test1'; cr1.Email__c = 'test1@test.com'; cr1.First_Name__c = 'test1';
        cr1.Last_Name__c = 'test1'; cr1.Company__c = 'test1';cr1.Address1__c = 'test1'; cr1.City__c = 'test';
        cr1.State__c = 'test'; cr1.Country__c = 'test'; cr1.Zip_Code__c = 'test'; cr1.Password__c = 'test';cr1.CampaignId__c = camp1.Id;
        cr1.Title__c ='Mr.';cr1.Phone__c ='212-293-2198';cr1.Address2__c ='111 wall st';cr1.Contact__c = con.Id;
        cr1.Subscription_Group__c =sg1.Id;cr1.MunchkinTokenListJSON__c='none';cr1.Set_Up_Call__c='Yes';cr1.Set_Up_Call_Request_Reason__c='abc';
        insert cr1;
        
        Community_Registrant__c cr2 = new Community_Registrant__c(); 
        cr2.User_Name__c = 'test@abc.com'; cr2.Email__c = 'test@abc.com'; cr2.First_Name__c = 'test1';
        cr2.Last_Name__c = 'test1'; cr2.Company__c = 'test1';cr2.Address1__c = 'test1'; cr2.City__c = 'test';
        cr2.State__c = 'test'; cr2.Country__c = 'test'; cr2.Zip_Code__c = 'test'; cr2.Password__c = 'test';cr1.CampaignId__c = camp2.Id;
        cr2.Title__c ='Mrr.';cr2.Phone__c ='212-293-9198';cr2.Address2__c ='111 wall ts';cr1.Contact__c = con1.Id;
        cr2.Subscription_Group__c =sg2.Id;cr2.MunchkinTokenListJSON__c='noe';cr2.Set_Up_Call__c='Yes';cr2.Set_Up_Call_Request_Reason__c='def';
        insert cr2;
        
        Community_Registrant__c cr3 = new Community_Registrant__c();
        cr3.User_Name__c = 'test23@abc.com'; cr3.Email__c = 'test23@abc.com'; cr3.First_Name__c = 'test1';
        cr3.Last_Name__c = 'test1'; cr3.Company__c = 'test1';cr3.Address1__c = 'test1'; cr3.City__c = 'test';
        cr3.State__c = 'test'; cr3.Country__c = 'test'; cr3.Zip_Code__c = 'test'; cr3.Password__c = 'test';cr3.CampaignId__c = camp3.Id;
        cr3.Title__c ='Mrs.';cr3.Phone__c ='212-293-4198';cr3.Address2__c ='111 wll st';cr3.Contact__c = con2.Id;
        cr3.Subscription_Group__c =sg3.Id;cr3.MunchkinTokenListJSON__c='non';cr3.Set_Up_Call__c='Yes';cr3.Set_Up_Call_Request_Reason__c='ghi';
        insert cr3;
        conIds.add(con.Id);conIds.add(con1.Id);conIds.add(con2.Id);
        
        CreateConFromReg ccfr = new CreateConFromReg(cr1, c.Id);
        ccfr.checkExistingContact();
        ccfr.addContact();
        ccfr.CreateTask(conIds, cr1);
        CreateConFromReg ccfr1 = new CreateConFromReg(cr2, c.Id);
        ccfr1.checkExistingContact();
        ccfr1.addContact();
        ccfr1.CreateTask(conIds, cr1);
        CreateConFromReg ccfr2 = new CreateConFromReg(cr3, c.Id);
        ccfr2.checkExistingContact();
        ccfr2.addContact();
        
        SendEmailNotification se = new SendEmailNotification('Error: Create Contact From Registrant Class Failed');
        try{
              if (!Test.isRunningTest()) {
                  //ccfr.CreateTask(conIds, cr1);
                  //ccfr1.CreateTask(conIds, cr2);
                 // ccfr2.CreateTask(conIds, cr3);
              }
              
            }catch(Exception e){
            se.sendEmail('Exception Occurred in CreateTask method for CTA Lead Collection: \n'+e.getMessage()+ 'Campaign Id: ' + 'Contact Id: ');
            return ; }
         try{
              if (!Test.isRunningTest()) {
                  //ccfr.CreateCampMemForLeadCollection(con.Id, cr1);
              }
              //ccfr1.CreateCampMemForLeadCollection(con1.Id, cr2);
              //ccfr2.CreateCampMemForLeadCollection(con2.Id, cr3);
              
            }catch(Exception e){
            se.sendEmail('Exception Occurred in CreateTask method for CTA Lead Collection: \n'+e.getMessage()+ 'Campaign Id: ' + 'Contact Id: ');
            return ; }
        try{
             
              
             if (c.Id == 'a0q3B000000Co2YQAS' || c.Id =='a0qA000000412iEIAQ'|| c.Id == 'a0q3B000000Co2Y' || c.Id =='a0qA000000412iE') {
              //boolean b1 = ccfr2.CreateTaskForLeadCollection(c.Id, cr1);
              //boolean b2 = ccfr2.CreateTaskForLeadCollection(c.Id, cr2);
              //boolean b3 = ccfr2.CreateTaskForLeadCollection(c.Id, cr3);
        }
            se.sendEmail('Exception Occurred in CreateConFromReg Constructor: \n'+ccfr1);
            se.sendEmail('Exception Occurred in CreateConFromReg Constructor: \n'+ccfr2);
        }catch(Exception e){
            se.sendEmail('Exception Occurred in CreateTask method for CTA Lead Collection: \n'+e.getMessage()+ 'Campaign Id: ' + 'Contact Id: ');
            return ; 
       
        }
       Test.stopTest();
    }
    
    
       static testmethod void sfdctest(){
           
        Account acc = new Account(Name = 'dfasf', Channel__c = 'RIA', BillingCountry='United States');
        insert acc;
        
        Contact con4 = new Contact(LastName='Test4', AccountId=acc.Id, Email='test4@abc.com', MailingCountry='United States', DB_Source__c = 'MVIS Registrant');
        insert con4;
        
        Contact con5 = new Contact(LastName='Test5', AccountId=acc.Id, Email='test5@abc.com', MailingCountry='United States', DB_Source__c = 'MVIS Registrant');
        insert con5;
           
        Campaign camp4 = new Campaign(Name = 'Test Campaign');
        insert camp4;
    
        Community__c c = new Community__c(Name = 'Test Community', Is_Active__c = true);
        insert c;
        
        Community_Registrant__c cr1 = new Community_Registrant__c();
        
        Subscription_Group__c sg4 = new Subscription_Group__c(Name = 'Test Subscription3');
        insert sg4;
           
        cr1.User_Name__c = 'test1'; cr1.Email__c = 'test1@test.com'; cr1.First_Name__c = 'test1';
        cr1.Last_Name__c = 'test1'; cr1.Company__c = 'test1';cr1.Address1__c = 'test1'; cr1.City__c = 'test';
        cr1.State__c = 'test'; cr1.Country__c = 'test'; cr1.Zip_Code__c = 'test'; cr1.Password__c = 'test';cr1.CampaignId__c = camp4.Id;
        cr1.Title__c ='Mr.';cr1.Phone__c ='212-293-2198';cr1.Address2__c ='111 wall st';cr1.Contact__c = con4.Id;
        cr1.Subscription_Group__c = sg4.Id;cr1.MunchkinTokenListJSON__c='none';cr1.Set_Up_Call__c='y';cr1.Set_Up_Call_Request_Reason__c='abc';cr1.CampaignId__c='7013B0000004ov3';
        insert cr1;
        
        cr1.Set_Up_Call__c='Yes';
        CreateConFromReg obj=new CreateConFromReg(cr1,c.Id);
        obj.CreateTaskForLeadCollection(con4.Id, cr1);
          System.debug('@@@test ');
           
            Community_Registrant__c cr2 = new Community_Registrant__c();
           
        cr2.User_Name__c = 'test2'; cr2.Email__c = 'test2@test.com'; cr2.First_Name__c = 'test2';
        cr2.Last_Name__c = 'test2'; cr2.Company__c = 'test2';cr2.Address1__c = 'test1'; cr2.City__c = 'test';
        cr2.State__c = 'test'; cr2.Country__c = 'test'; cr2.Zip_Code__c = 'test'; cr2.Password__c = 'test'; cr2.Set_Up_Call__c='Yes';
        cr2.Title__c ='Mr.';cr2.Phone__c ='212-293-2198';cr2.Address2__c ='111 wall st';cr2.Contact__c = con5.Id;
      
        insert cr2;
        CreateConFromReg obj2=new CreateConFromReg(cr2,c.Id);
        obj2.CreateTaskForLeadCollection(con5.Id, cr2);
           
        //CreateConFromReg obj3=new CreateConFromReg(cr2,c.Id);
        //obj3.conId='003A00000150h71IAA';
        
       }
    
}