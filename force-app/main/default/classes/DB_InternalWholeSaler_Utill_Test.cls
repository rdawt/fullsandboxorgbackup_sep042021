@isTest
public with sharing class DB_InternalWholeSaler_Utill_Test {
    
    @isTest
    public static void  testCalculateWorkingDays() {

        Test.startTest();
        Integer iFullmonth = DB_InternalWholeSaler_Utill.calculateWorkingDays(System.Date.today(),'Fullmonth');
        Integer iRemain = DB_InternalWholeSaler_Utill.calculateWorkingDays(System.Date.today(),'Remain');
        Integer iElapsed = DB_InternalWholeSaler_Utill.calculateWorkingDays(System.Date.today(),'Elapsed');
        Test.stopTest();

        System.assert(iFullmonth != null);
        System.assert(iRemain != null);
        System.assert(iElapsed != null);
    }

    @isTest
    public static void  testInsertActivityTypeScoreWeightage() {

        Test.startTest();
        DB_InternalWholeSaler_Utill.insertActivityTypeScoreWeightage(30,5,10,5,2,12,25,10,5,5);
        //Id result02 = DB_InternalWholeSaler_Utill.insertActivityTypeScoreWeightageWithIdCheck(result01,30,5,10,5,2,12,25,10,5,true);
        //Id result03 = DB_InternalWholeSaler_Utill.insertActivityTypeScoreWeightageWithIdCheck(result01,30,5,20,5,2,12,25,10,5,true);
        DB_InternalWholeSaler_Utill.insertActivityTypeScoreWeightage(12,15,15,5,5,10,30,15,2,5);
        Test.stopTest();

        //List<InternalDBActivityTypeScoreWeightage__c> lstActiveRecords = [SELECT Id,Status__c FROM InternalDBActivityTypeScoreWeightage__c];
        //List<InternalDBActivityTypeScoreWeightage__c> lstInactiveRecords = [SELECT Id,Status__c FROM InternalDBActivityTypeScoreWeightage__c];

        //System.assertEquals(result01 , result02);
        //System.assertNotEquals(result01 , result03);
        //System.assertEquals(lstActiveRecords.size() , 1);
        //System.assertEquals(lstInactiveRecords.size() , 2); 
    }
}