@isTest 
public with sharing class SchedulePortfolioChannelRollupQuarteTest {
    
    @isTest static void testSchedulePortfolioChannelRollupQuarte() { 
        
        Test.startTest();

        SchedulePortfolioChannelRollupQuarterly scheduledBatchable = new SchedulePortfolioChannelRollupQuarterly();
        String chron = '0 0 2 * * ?';
        String jobid = system.schedule('Test API Version Check', chron, scheduledBatchable);

        // Get the information from the CronTrigger API object
        CronTrigger ct = [Select Id, CronExpression, TimesTriggered, State, NextFireTime from CronTrigger where id = :jobId];
        // Verify the expressions are the same
        System.assertEquals('0 0 2 * * ?', ct.CronExpression, 'The expected CronExpression is 0 0 2 * * ? but is actually ' + ct.CronExpression);
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered, 'The job should not be runned.');
        // Verify job is waiting
        System.assertEquals('WAITING', ct.State, 'The expected State is WAITING but is ' + ct.State);
        
        Test.stopTest();
    }
    
}