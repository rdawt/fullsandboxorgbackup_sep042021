/**
    Author: Vidhya Krishnan
    Created Date: 4/25/2012
    Description: Class to handle Dynamic Subscription Concept
        This method is called from Dynamic Eligibility Class from Contact Trigger
        For every single Subscription Member Record of each contact do the following
        1. Check for the Indicator of each subscription - blank or not
        2. For blank indicator - no variation of subscription available
            So either delete only or no change in sub_mem record
        3. For indicator value - Check the new eligibility & their sub_grp_sub list
            a. If sub is not eligible in new group - delete it
            b. If variation of it is eligible - delete old & add new variation
            c. If sub is eligible in new group - no change is required
        Also Create Task for the Contact Owner with appropirate values of old subscription details
*/
public class DynamicSubscription {

    public string src;
    public list<Contact> con = new list<Contact>();
    public list<Contact> cons = new list<Contact>();
    public Map<Id,Subscription__c> subIndicatorMap; // each sub id holds vlaue of its indicator - subscriptions with null indicator are also stored
    public Map<Id, list<Subscription_Group_Subscription__c>> elgSGSMap; //1 sub_grp id key holds list of sub_grp_sub obj
    public Map<Id, list<Subscription_Member__c>> conSubMemMap = new Map<Id, list<Subscription_Member__c>>(); //1 contact id key holds list of sub_mem ids
    public SendEmailNotification se = new SendEmailNotification('Error: Dynamic Subscription Class failed');

    public DynamicSubscription(list<Contact> con, String src){
        try{
            this.con = con;
            this.src = src;
            list<Subscription_Group_Subscription__c> newSGSlist;
            list<Subscription_Member__c> smlist = new list<Subscription_Member__c>();
            list<Subscription_Member__c> smlistSV = new list<Subscription_Member__c>();
            list<Subscription_Group_Subscription__c> sgslist = new list<Subscription_Group_Subscription__c>(); 

            subIndicatorMap = new Map<Id, Subscription__c>([Select Id, Subscription_Indicator_Marker__c from Subscription__c where IsActive__c =: true]);
            system.debug('SubIndicatorMap.....'+subIndicatorMap);
            //inner join soql
            list<Subscription_Group__c> sglist = [select id,(select Id,Subscription__c from Subscription_Group_Subscriptions__r) from Subscription_Group__c where isActive__c =: true];
            elgSGSMap = new Map<Id, list<Subscription_Group_Subscription__c>>();
            for(Subscription_Group__c sg : sglist){
                newSGSlist = new list<Subscription_Group_Subscription__c>();
                for(Subscription_Group_Subscription__c sgs : sg.Subscription_Group_Subscriptions__r)
                    newSGSlist.add(sgs);
                if(newSGSlist != null)
                    elgSGSMap.put(sg.Id,newSGSlist);
            }
            
            cons = [select Id, Auto_Subscription_Group__c, LastName, FirstName, AccountId, Subscription_Group__c, HasOptedOutOfEmail, Email from Contact where Id in: con]; 
                                    
           
                smlistSV = [select Id, Subscription__c, Name, subscribed__c, Subscription_Unsubscribed_Date__c from Subscription_Member__c 
                                            where Contact__c =:Trigger.newMap.keySet()];
             for(Subscription_Member__c s: smListSV){
                list<Subscription_Member__c> newSMlist = new list<Subscription_Member__c>();
                if(smlistSV != null)
                    conSubMemMap.put( smlistSV[0].Id, smlistSV);
            } 
            
        }catch(Exception e){
            se.sendEmail('Exception Occurred in DynamicSubscription Constructor : \n'+e.getMessage()+e.getStackTraceString()+con);
        }
    }

    public void handleSubscription(){
        string s = '';
        list<Subscription_Group_Subscription__c> sgsList; // to store all SGS of new eligibility
        list<Subscription_Member__c> subMemInsList = new list<Subscription_Member__c>();
        try{
        for(Contact c : con){
            sgsList = new list<Subscription_Group_Subscription__c>();
            set<String> stList = new set<String>();
            if(elgSGSMap.containsKey(c.Auto_Subscription_Group__c)) // check to avoid null pointer exception
                sgsList = elgSGSMap.get(c.Auto_Subscription_Group__c);
            system.debug('Contact@@@'+c);
            // If the contact has any Subscription Member record
            if(conSubMemMap.containsKey(c.Id)){
                system.debug('containsKey###'+ conSubMemMap.containsKey(c.Id));
                list<Subscription_Member__c> subMemlist = new list<Subscription_Member__c>(); subMemlist = conSubMemMap.get(c.Id);
                
                // for every contact with subsription member
                for(Subscription_Member__c subMem : subMemlist){
                    s = '';
                    Subscription_Member__c smem = new Subscription_Member__c(); // to store new subscription member if required
                    
                    //smem.source__c = 'Dynamic Subscription';
                    Subscription__c sub = subIndicatorMap.get(subMem.Subscription__c);
                    /*  Cleaning up unsubscribed records    */
                    if(subMem.Subscription_Unsubscribed_Date__c != null && subMem.Subscribed__c == false)
                        s = s +'delete-'+subMem.Subscription__c+',';
                    
                    /*  Subscription without market indicator value.    */
                    else if(sub != null && sub.Subscription_Indicator_Marker__c == null){
                        for(Subscription_Group_Subscription__c sgs : sgsList){
                            // if this subscription is not eligible in new subscription group
                            if(sgs.Subscription__c == sub.Id) s = s +'no change-'+sgs.Subscription__c+',';
                            else s = s +'delete-'+sgs.Subscription__c+',';
                        }
                    }// end of marker = null

                    /*  Subscriptions with marker indicator value
                            1. No Change - if current subscription is eligible in new subscription group
                            2. Only Delete - if current subscription is not eligible in new SG
                            3. Delete old subscription member & add new subscription member - if another verity of it is eligible in new SG
                    */
                    else{
                        for(Subscription_Group_Subscription__c sgs : sgsList){
                            if(sgs.Subscription__c == sub.Id){s = s +'no change-'+sgs.Subscription__c+',';
                                stList.add((subIndicatorMap.get(sgs.Subscription__c)).Id);
                            }
                            else{
                                s = s +'delete-'+sgs.Subscription__c+',';
                                Subscription__c sub1 = subIndicatorMap.get(sgs.Subscription__c);
                                if((sub1 != null) &&(sub.Subscription_Indicator_Marker__c == sub1.Subscription_Indicator_Marker__c)){
                                    // create new sub_mem object to insert
                                    smem = new Subscription_Member__c(); smem.Contact__c = c.Id;smem.Subscription__c = sub1.Id;
                                    smem.Subscribed__c = true; s = s +'new-'+smem.Subscription__c+',';
                                }
                            }
                        } // end of SGS loop

                    } // end of marker != null
                    if(s.contains('no change-')){} else{  if(s.contains('new-')){ subMemInsList.add(smem);
                        }
                        //subMemDelList.add(subMem); // add this subscription to delete list
                    }
                } // end of subscription member's Subscription Id loop
            }
            /* chk for unique subid_conid condition */  
            set<id> st = new set<id>();
            // get non dupe sub ids
            for(Subscription_Member__c sm : subMemInsList) st.add(sm.Subscription__c);
            // clear all the insert list
            subMemInsList = new List<Subscription_Member__c>();
            Subscription_Member__c smem;
            // with non dupe set of sub ids, add new set of insert list
            for(id i : st){
                smem = new Subscription_Member__c(); smem.Contact__c = c.Id;smem.Subscription__c = i; smem.Subscribed__c = true;
                //smem.source__c = 'Dynamic Subscription';  // This is to avoid recursive trigger call when Sub_Mem is not a child of Contact object
                if(!stList.contains(smem.Subscription__c))  subMemInsList.add(smem);
            }
                        
        } // end of contact loop
        
        //system.debug('Subscription Member Delete List....'+subMemDelList);
        system.debug('Subscription Member Insert List....'+subMemInsList);
        if(!subMemInsList.isEmpty()) insert subMemInsList;
        
        }catch(Exception e){ se.sendEmail('Exception Occurred in handleSubscription method : \n'+e.getMessage()+ e.getStackTraceString()+con);
        }
    }
    
    /*  For every contact - Create New Task with the details of contacts previous subscriptions & delete those subscripion record in sub_mem object.    */
    public void createTask(Map<Id, List<Subscription_Member__c>> conSubMemMap)
    {
        /*  Create the Concadinated String for Description  */
        List<Task> newTaskList = new List<Task>();
        List<Subscription_Member__c> subMemList = new List<Subscription_Member__c>();
        String isSubscribe, Subscription, unSubscribeDate, Comments, sub;
        
        // loop through every contact in case of bulk updates
        for(Contact c: cons)
        {
            sub = 'System Alert: Email Subscription Eligibility Change';
            if(c.HasOptedOutOfEmail) sub = sub + ' - MAILING OPTED OUT';
            else if(c.Email == '' || c.Email == null) sub = sub + ' - MISSING EMAIL';
            
            Comments = 'Some values of the Contact '+ c.FirstName + ' '+ c.LastName+' has been changed.';
            Comments = Comments+'\nThis changes the Email Subscription Eligibility.\n';
            Comments = Comments+'\nEmail Subscription Eligibility has been changed automatically according to the current values of contact ';
            Comments = Comments+'and the privous subscriptions that are not eligible anymore were deleted.';
            Comments = Comments+'\nFollowing are the details of the contact previous subscriptions:\n\n';
            
            if(conSubMemMap.containsKey(c.Id)){
                subMemList = conSubMemMap.get(c.Id);
            
                // Loop through every Sub_Mem record for particular contact
                for(Subscription_Member__c sm : subMemList)
                {
                    Subscription = ''; isSubscribe = ''; unSubscribeDate = '';
                    Subscription = sm.Subscription__r.Name;
                    String s = String.valueOf(sm.Subscribed__c);
                    if(s == 'true')
                        isSubscribe = 'Yes'; else  isSubscribe = 'No';
                    if(sm.Subscription_Unsubscribed_Date__c != null){
                        unSubscribeDate = String.valueOf(sm.Subscription_Unsubscribed_Date__c);
                        Comments = Comments+'Name : '+ Subscription + ' ;\t\t  Subscribed : ' + isSubscribe + ' ;\t\t  Unsubscribe Date : ' + unSubscribeDate + '\n';
                    }
                    else
                        Comments = Comments+'Name : '+ Subscription + ' ;\t\t  Subscribed : ' + isSubscribe + '\n';
                }
                if(c.HasOptedOutOfEmail) Comments = Comments+'\nContact has OPTED OUT of Mailing. So contact can not receive any emails until he is opted back in.';
                else if(c.Email == '' || c.Email == null) Comments = Comments+'\nContact has BLANK EMAIL Id. So contact can not receive any mails until email id is filled. Please take the necessary action. Thanks!';
                else
                    Comments = Comments+'\nNecessary action has been taken.';
                
                /*  Create a new Task for each Contact Id   */
                if(subMemList.size()>0)
                    newTaskList.add(new Task(Description = Comments, Priority = 'Low', Status = 'Closed', Subject = sub, IsReminderSet = true, Summary_Recap__c = sub,
                             ActivityDate = System.today(), ReminderDateTime = System.now(), WhoId = c.Id, WhatId = c.AccountId));
            }
        }
        try{
            if(newTaskList.size() > 0)
                insert newTaskList;
        }catch(Exception e){  se.sendEmail('Exception Occurred in createTask method : \n'+e.getMessage()+ e.getStackTraceString()+conSubMemMap);
        }
    } // end of createTask()
}