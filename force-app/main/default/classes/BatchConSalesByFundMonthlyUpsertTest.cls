/********************************************************************************************
* @Author: Tharaka De Silva
* @Date  : 2021-06-28
*
* Test class for BatchConSalesByFundMonthlyUpsert class
*
* *******************************************************************************************/
@IsTest 
public with sharing class BatchConSalesByFundMonthlyUpsertTest {
   
    @TestSetup static void setup()
    {
        Account objAcc01 = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 1', Channel__c = 'Financial Advisor', 
                            BillingCountry='United States'), false);
        insert objAcc01;

        Contact conObj01 = (Contact)SL_TestDataFactory.createSObject(new Contact(LastName='Test Contact 01',
                            AccountId=objAcc01.Id,MailingCountry='United States', Email='testUser01@gmail.com'),false);
        insert conObj01;

        Contact conObj02 = (Contact)SL_TestDataFactory.createSObject(new Contact(LastName='Test Contact 02',
                            AccountId=objAcc01.Id,MailingCountry='United States', Email='testUser02@gmail.com'),false);
        insert conObj02;

        Fund__c fnd1=new Fund__c(Name='Biotech ETF (BBH)',Fund_Type__c='ETF',Fund_Ticker_Symbol__c='BBH');
        insert fnd1;
        Fund__c fnd2=new Fund__c(Name='Africa Index ETF (AFK)',Fund_Type__c='ETF',Fund_Ticker_Symbol__c='AFK');
        insert fnd2;

        List<SalesConnect__Contact_Portfolio_Breakdown__c> lstContactPorBreakdown = new List<SalesConnect__Contact_Portfolio_Breakdown__c>();
        
        lstContactPorBreakdown.add((SalesConnect__Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Contact_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000001',Name= 'VanEck Emerging Markets Fund' ,SalesConnect__Contact__c = conObj01.Id,  Firm_Branch_Lookup__c = objAcc01.Id, SalesConnect__Current_Assets__c = 1258000 , SalesConnect__YTD_Sales__c = 2585000), false));
        lstContactPorBreakdown.add((SalesConnect__Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Contact_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000002',Name= 'VanEck Cm Commodity Index Fund' ,SalesConnect__Contact__c = conObj01.Id , Firm_Branch_Lookup__c = objAcc01.Id, SalesConnect__Current_Assets__c = 4255000 , SalesConnect__YTD_Sales__c = 8285000), false));
        lstContactPorBreakdown.add((SalesConnect__Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Contact_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000003',Name= 'VanEck International Investors Gold' ,SalesConnect__Contact__c = conObj01.Id , Firm_Branch_Lookup__c = objAcc01.Id, SalesConnect__Current_Assets__c = 8748000 , SalesConnect__YTD_Sales__c = 3565000), false));
        lstContactPorBreakdown.add((SalesConnect__Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Contact_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000004',Name= 'VanEck Emerging Markets Fund',SalesConnect__Contact__c = conObj02.Id , Firm_Branch_Lookup__c = objAcc01.Id, SalesConnect__Current_Assets__c = 69858000 , SalesConnect__YTD_Sales__c = 1585000), false));
        lstContactPorBreakdown.add((SalesConnect__Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Contact_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000005',Name= 'VanEck Global Hard Assets Fund',SalesConnect__Contact__c = conObj02.Id , Firm_Branch_Lookup__c = objAcc01.Id, SalesConnect__Current_Assets__c = 7558000 , SalesConnect__YTD_Sales__c = 355000), false));
        lstContactPorBreakdown.add((SalesConnect__Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Contact_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000006',Name= 'VanEck Vectors Africa Index ETF',SalesConnect__Contact__c = conObj01.Id , Firm_Branch_Lookup__c = objAcc01.Id, SalesConnect__Current_Assets__c = 69858000 , SalesConnect__YTD_Sales__c = 1585000), false));
        lstContactPorBreakdown.add((SalesConnect__Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Contact_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000007',Name= 'VanEck Vectors Biotech ETF',SalesConnect__Contact__c = conObj02.Id , Firm_Branch_Lookup__c = objAcc01.Id, SalesConnect__Current_Assets__c = 7558000 , SalesConnect__YTD_Sales__c = 355000), false));

        insert lstContactPorBreakdown;

        List<Fund_Mapping__c> lstFundMapping = new List<Fund_Mapping__c>();
        
        lstFundMapping.add((Fund_Mapping__c)SL_TestDataFactory.createSObject(
            new Fund_Mapping__c(Name= 'VanEck Vectors Africa Index ETF', SFDC_Fund_Lookup__c = fnd1.Id), false));
        lstFundMapping.add((Fund_Mapping__c)SL_TestDataFactory.createSObject(
            new Fund_Mapping__c(Name= 'VanEck Vectors Biotech ETF', SFDC_Fund_Lookup__c = fnd2.Id), false));

        insert lstFundMapping;
    }

    /********************************************************************************************
    * Tests the scenario where BatchConSalesByFundMonthlyUpsert is working as expected.
    * Read the all records from SalesConnect__Contact_Portfolio_Breakdown__c and do the upsert in to History_Con_Portfolio_Breakdown_Monthly__c 
    * object.
    * *******************************************************************************************/
    @IsTest static void testContactSalesByFundMonthlyUpsert() {

        Test.startTest();
        BatchConSalesByFundMonthlyUpsert batch = new BatchConSalesByFundMonthlyUpsert(); 
        database.executebatch(batch); 
        Test.stopTest();

        List<History_Con_Portfolio_Breakdown_Monthly__c> lstHistory = [SELECT Id,Name FROM History_Con_Portfolio_Breakdown_Monthly__c];
        
        System.assert(lstHistory != null);
        System.assertEquals(lstHistory.size() , 7);      
    }
}