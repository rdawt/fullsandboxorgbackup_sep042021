@IsTest
public with sharing class BatchPortfolioChannelRollupMonthTest {
 
    @TestSetup static void setup()
    {
        List<Fund__c> lstFunds = new List<Fund__c>();
        
        for(Integer i=0 ; i <= 5 ; i++){
            lstFunds.add((Fund__c)SL_TestDataFactory.createSObject(
                new Fund__c(Name='Test Fund MF '+i,Fund_Type__c='Mutual Fund',Fund_Vehicle_Type__c = 'Mutual Fund', Fund_Ticker_Symbol__c='MF'+i,  BU_Region__c = 'USA'), false));
        }
        
        insert lstFunds;

        List<Channel__c> lstChannels = new List<Channel__c>();
        Channel__c financialAdvisorSFDC = new Channel__c(Channel_Name__c='VE - Financial Advisor', Channel_Description__c='Financial Advisor', Status__c = 'Active',Channel_Type__c = 'SFDC',Allow_Quarterly_Analysis__c = true);
        lstChannels.add(financialAdvisorSFDC);
        insert lstChannels;

    }


   @IsTest static void testPortfolioChannelRollupMonthly01() {

        Test.startTest();
        BatchPortfolioChannelRollupMonthly batch = new BatchPortfolioChannelRollupMonthly(true,new List<Id>(),'Mutual Fund');
        database.executebatch(batch,1); 
        Test.stopTest();
    }

    @IsTest static void testPortfolioChannelRollupMonthly02() {

        Test.startTest();

        List<Id> lstFundIds = new List<Id>();
        for(Fund__c fund : [SELECT Id FROM Fund__c LIMIT 3]){
            lstFundIds.add(fund.Id);
        }

        BatchPortfolioChannelRollupMonthly batch = new BatchPortfolioChannelRollupMonthly(false,lstFundIds,'Mutual Fund');
        database.executebatch(batch,1); 
        Test.stopTest();
    }
    
}