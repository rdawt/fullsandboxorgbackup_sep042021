@isTest
public with sharing class Batch_DeleteLead_DoubleOptIn_Test 
{
    @TestSetup
    static void makeData()
    {
        User sysAdminUserObj = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'AdminUser',
            Email = 'AdminUser@admin.com',
            Username = 'SalesforceUser@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        
        System.runAs(sysAdminUserObj)
        {
            TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
            settings.isActive_Lead__c = TRUE;
            upsert settings custSettings__c.Id;

            Subscription__c subObj = (Subscription__c)SL_TestDataFactory.createSObject(
                                                                                        new Subscription__c(Name='Test Subscription',
                                                                                                            IsActive__c=true,
                                                                                                            Subscription_Display_Name__c='Test Display Name'    
                                                                                      ),true);
            Subscription__c subObj1 = (Subscription__c)SL_TestDataFactory.createSObject(
                                                                                        new Subscription__c(Name='Test Subscription1',
                                                                                                            IsActive__c=true,
                                                                                                            Subscription_Display_Name__c='Test Display Name1'   
                                                                                      ),true);
            
           Subscription_Group__c subGroupObj = (Subscription_Group__c)SL_TestDataFactory.createSObject(
                                                                                        new Subscription_Group__c(Name='Unassigned Eligibility',
                                                                                                                  IsActive__c=true
                                                                                       ),true);
            
            Community__c c = new Community__c(Name = 'Test Community', Is_Active__c = true, Default_Subscription_Group__c = subGroupObj.Id);
            insert c;

            Community_Registrant__c cr1 = new Community_Registrant__c();
            cr1.User_Name__c = 'test1'; cr1.Email__c = 'test1@test.com'; cr1.First_Name__c = 'test1';
            cr1.Last_Name__c = 'test1'; cr1.Company__c = 'test1';cr1.Address1__c = 'test1'; cr1.City__c = 'test';
            cr1.State__c = 'test'; cr1.Country__c = 'test'; cr1.Zip_Code__c = 'test'; cr1.Password__c = 'test';
            cr1.SFDC_Subscription_Ids_CSV__c = subObj.Id + ',' + subObj1.Id;
            cr1.Subscription_Group__c = subGroupObj.Id;
            insert cr1;

            Community_Member__c cm1 = new Community_Member__c(Community__c = c.Id,Community_Registrant__c = cr1.Id);
            insert cm1;
            System.debug('Community_Registrant Id :'+cr1.Id);
            Lead objLead = new Lead( FirstName = 'Test', LastName = 'Sample', Status = 'Open',
                                        Country = 'US', Channel__c = 'Bank', Organization_Type__c = 'Bank BDs',
                                        Company = 'Testing Sample Co', Community_Registrant__c=cr1.Id,
                                        Activation_Approval_Detail__c = 'Test',
                                        LeadSource = 'Web', Deactivation_Requested__c = Datetime.now().adddays(-2),
                                        Activation_Approved__c = Datetime.now(),
                                        Activation_Requested__c = Datetime.now() );  
            insert objLead;  

        }    
    }   
    
    @isTest
    public static void testBatch_DeleteLead_DoubleOptIn()
    {
        Database.executeBatch( new Batch_DeleteLead_DoubleOptIn(), 200);
    }

    @isTest static void testBatch_SCH_DeleteLead_DoubleOptIn() {
        
        Test.startTest();

        Batch_Sch_DeleteLead_DoubleOptIn scheduledBatchable = new Batch_Sch_DeleteLead_DoubleOptIn();
        String chron = '0 0 2 * * ?';
        String jobid = system.schedule('Test API Version Check', chron, scheduledBatchable);

        // Get the information from the CronTrigger API object
        CronTrigger ct = [Select Id, CronExpression, TimesTriggered, State, NextFireTime from CronTrigger where id = :jobId];
        // Verify the expressions are the same
        System.assertEquals('0 0 2 * * ?', ct.CronExpression, 'The expected CronExpression is 0 0 2 * * ? but is actually ' + ct.CronExpression);
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered, 'The job should not be runned.');
        // Verify job is waiting
        System.assertEquals('WAITING', ct.State, 'The expected State is WAITING but is ' + ct.State);
        
        Test.stopTest();
    }
}