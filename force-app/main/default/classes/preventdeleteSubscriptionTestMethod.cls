@isTest
private class preventdeleteSubscriptionTestMethod{
static testMethod void preventdeleteSubscriptionTestMethod() 
{ 
   Subscription__c sub = new Subscription__c(Name='abcd',Subscription_Display_Name__c='ABC');
   Insert sub;
   Account acc = new Account(Name = 'ektron', Channel__c = 'RIA', BillingCountry='US');
   insert acc;
   Contact con = new contact(AccountId=acc.Id,LastName='xyz',FirstName='test',Email='abc@gmail.com', MailingCountry='US');
   Insert con;
   Subscription_Member__c submem = new subscription_member__c(Subscription__c =sub.id,Contact__c =con.Id,Email__c ='abc@gmail.com');
   Insert submem;
   Subscription_Group__c subGrp = new Subscription_Group__c(Name='test');
   Insert subGrp; 
   Subscription_Group_Subscription__c SGS = new Subscription_Group_Subscription__c(Subscription_Group__c =subGrp .Id,Subscription__c =sub.Id );
   insert  SGS ;
   try {
       delete sub;
   } catch(Exception e) {
        System.debug('Unable to delete it.');
   }
   }
}