/********************************************************************************************
* @Author: Tharaka De Silva
* @Date  : 2021-06-23
*
* Test class for BatchAccSalesByFundMonthlyUpsert class
*
* *******************************************************************************************/
@IsTest
public with sharing class BatchAccSalesByFundMonthlyUpsertTest {

    @TestSetup static void setup()
    {
        Account objAcc01 = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 1', Channel__c = 'Financial Advisor', 
        BillingCountry='United States'), false);
        insert objAcc01;

        Account objAcc02 = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 2', Channel__c = 'Financial Advisor', 
        BillingCountry='United States'), false);
        insert objAcc02;

        Fund__c fnd1=new Fund__c(Name='Biotech ETF (BBH)',Fund_Type__c='ETF',Fund_Ticker_Symbol__c='BBH');
        insert fnd1;
        Fund__c fnd2=new Fund__c(Name='Africa Index ETF (AFK)',Fund_Type__c='ETF',Fund_Ticker_Symbol__c='AFK');
        insert fnd2;

        List<SalesConnect__Firm_Portfolio_Breakdown__c> lstFirmPorBreakdown = new List<SalesConnect__Firm_Portfolio_Breakdown__c>();
        
        lstFirmPorBreakdown.add((SalesConnect__Firm_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Firm_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000001',Name= 'VanEck Emerging Markets Fund', SalesConnect__Account__c = objAcc01.Id, SalesConnect__Current_Assets__c = 1258000 , SalesConnect__YTD_Sales__c = 2585000), false));
        lstFirmPorBreakdown.add((SalesConnect__Firm_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Firm_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000002',Name= 'VanEck Cm Commodity Index Fund', SalesConnect__Account__c = objAcc01.Id, SalesConnect__Current_Assets__c = 4255000 , SalesConnect__YTD_Sales__c = 8285000), false));
        lstFirmPorBreakdown.add((SalesConnect__Firm_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Firm_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000003',Name= 'VanEck International Investors Gold', SalesConnect__Account__c = objAcc01.Id, SalesConnect__Current_Assets__c = 8748000 , SalesConnect__YTD_Sales__c = 3565000), false));
        lstFirmPorBreakdown.add((SalesConnect__Firm_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Firm_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000004',Name= 'VanEck Emerging Markets Fund', SalesConnect__Account__c = objAcc02.Id, SalesConnect__Current_Assets__c = 69858000 , SalesConnect__YTD_Sales__c = 1585000), false));
        lstFirmPorBreakdown.add((SalesConnect__Firm_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Firm_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000005',Name= 'VanEck Global Hard Assets Fund', SalesConnect__Account__c = objAcc02.Id, SalesConnect__Current_Assets__c = 7558000 , SalesConnect__YTD_Sales__c = 355000), false));
        lstFirmPorBreakdown.add((SalesConnect__Firm_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Firm_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000006',Name= 'VanEck Vectors Africa Index ETF', SalesConnect__Account__c = objAcc01.Id, SalesConnect__Current_Assets__c = 98000 , SalesConnect__YTD_Sales__c = 345000), false));
        lstFirmPorBreakdown.add((SalesConnect__Firm_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
            new SalesConnect__Firm_Portfolio_Breakdown__c(SalesConnect__External_ID__c = 'Test0000007',Name= 'VanEck Vectors Biotech ETF', SalesConnect__Account__c = objAcc02.Id, SalesConnect__Current_Assets__c = 3658000 , SalesConnect__YTD_Sales__c = 3455000), false));
        insert lstFirmPorBreakdown;

        List<Fund_Mapping__c> lstFundMapping = new List<Fund_Mapping__c>();
        
        lstFundMapping.add((Fund_Mapping__c)SL_TestDataFactory.createSObject(
            new Fund_Mapping__c(Name= 'VanEck Vectors Africa Index ETF', SFDC_Fund_Lookup__c = fnd1.Id), false));
        lstFundMapping.add((Fund_Mapping__c)SL_TestDataFactory.createSObject(
            new Fund_Mapping__c(Name= 'VanEck Vectors Biotech ETF', SFDC_Fund_Lookup__c = fnd2.Id), false));

        insert lstFundMapping;
    }


    /********************************************************************************************
    * Tests the scenario where BatchAccSalesByFundMonthlyUpsert is working as expected.
    * Read the all records from SalesConnect__Firm_Portfolio_Breakdown__c and do the upsert in to SalesConnect__Firm_Portfolio_Breakdown__c 
    * object.
    * *******************************************************************************************/
    @IsTest static void testAccSalesByFundUpsertBatch() {

        Test.startTest();
        BatchAccSalesByFundMonthlyUpsert batch = new BatchAccSalesByFundMonthlyUpsert(); 
        database.executebatch(batch);
        Test.stopTest();

        List<History_Firm_Portfolio_Breakdown_Monthly__c> lstHistory = [SELECT Id,Name FROM History_Firm_Portfolio_Breakdown_Monthly__c];
        
        System.assert(lstHistory != null);
        System.assertEquals(lstHistory.size() , 7);      
    }
}