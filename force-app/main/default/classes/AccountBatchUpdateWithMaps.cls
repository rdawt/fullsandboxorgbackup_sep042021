global class AccountBatchUpdateWithMaps implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'Select Id from Account where First_Human_Contact_Date_DB__c = NULL ';
        system.debug('**start query**  '+ query );
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List <Account> scope)
    {
        List<AggregateResult> lstContact = [SELECT AccountId, min(First_Human_Contact_Date_DB__c) 
                                            FROM Contact 
                                            where  AccountId in: scope and  
                                                First_Human_Contact_Date_DB__c <> null and 
                                                    (NOT FirstHumanContactActivityType__c like 'Processed But there was no Activity Records%') 
                                            group by AccountId];

        List<Account> lstAccountUpdate = new List<Account>();
        for(AggregateResult objAR : lstContact)
        {
            Account objAccount = new Account();
            objAccount.Id = (Id)(objAR.get('AccountId'));
            objAccount.First_Human_Contact_Date_DB__c = Date.valueOf(objAR.get('expr0'));
            lstAccountUpdate.add(objAccount);
        }

        if(!lstAccountUpdate.isEmpty())
        {
            update lstAccountUpdate;
        }
    }       

    global void finish(Database.BatchableContext BC) 
    {

    }
}