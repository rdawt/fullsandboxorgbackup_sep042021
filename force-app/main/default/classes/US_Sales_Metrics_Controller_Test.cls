/**
* @File Name          : US_Sales_Metrics_Controller_Test.cls
* @Description        : Test Class for US_Sales_Metrics_Controller
* @Author             : 
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author                    Modification
*==============================================================================
* 1.0        03/09/2020               
**/
@isTest
public class US_Sales_Metrics_Controller_Test 
{
    public static integer recordCount = 5;
    public static User objUser1 = [Select Id, EMEADBAccess__c, Division from User where isActive=true and Profile.Name = 'System Administrator' limit 1];
    public static User objUser2 = [Select Id from User where Id !=:objUser1.Id and isActive=true limit 1];

    @TestSetup
    public static void createTestData()
    {
        User objCurrentuser = [Select Id from User where Id=: UserInfo.getUserId() limit 1];
        System.runAs(objUser1)
        {
            BroadridgeAssetDate__c setting = new BroadridgeAssetDate__c();
            Date today = date.today();
            setting.AssetDate__c = today;
            setting.Error_Message__c = 'Test error message';
            setting.Previous_Asset_Date__c = today;
            setting.Previous_Previous_Asset_Date__c = today.addMonths(-1);
            insert setting;

            List<Fund__c> lstFund = (List<Fund__c>)SL_TestDataFactory.createSObjectList(
                                        new Fund__c(Name='Vaneck Fund Test', Fund_Ticker_Symbol__c='VFT', 
                                        Sales_Connect_Portfolio_Name__c = 'Vaneck Fund Test'),
                                            1, false);
            if(!lstFund.isEmpty())
                insert lstFund;

            List<YTDSalesPortfolioChannelRollUp__c> lstYTDSalesPortfolioChannelRollUp = (List<YTDSalesPortfolioChannelRollUp__c>)SL_TestDataFactory.createSObjectList(
                                                    new YTDSalesPortfolioChannelRollUp__c(Portfolio__c='Vaneck Fund Test', NetSalesCalculated__c=1000, 
                                                    Gross_Sales__c= 1000, Redemption__c=1000,
                                                    ProcessedDate__c = date.today()),
                                                        1, false);
            if(!lstYTDSalesPortfolioChannelRollUp.isEmpty())
                insert lstYTDSalesPortfolioChannelRollUp;

            SL_TestDataFactory.createTestDataForContact();
            Account objAccount = [Select Id from Account limit 1];

            List<ETF_Assets_By_Firm__c> lstETF_Assets_By_Firm = (List<ETF_Assets_By_Firm__c>)SL_TestDataFactory.createSObjectList(
                                                    new ETF_Assets_By_Firm__c(Name='VFT', Account__c=objAccount.Id, 
                                                                                Fund_YTM_NNA__c=1000, allocation_etf__C = true,
                                                                                asset_date__c = date.today()),//,SFDCFundID__c = lstFund[0].Id
                                                        1, false);
            if(!lstETF_Assets_By_Firm.isEmpty())
                insert lstETF_Assets_By_Firm;
        }    

    }   

    //US Sales Active Funds
    @isTest
    public static void testGetOnLoadRecords(){
        US_Sales_Metrics_Controller.getOnLoadRecords();
    }

    @isTest
    public static void testgetSalesByFundChart(){
        List<String> lstSelectedFund = new List<String>{'ALL', 'VFT'};
        Date startDate = date.newinstance(Date.today().year(), 1, 1);
        Date endDate = date.newinstance(Date.today().year(), 12, 31);

        US_Sales_Metrics_Controller.getSalesByFundChart(lstSelectedFund, startDate, endDate);
    }


    //US Sales ETF Funds
    @isTest
    public static void testGetOnLoadRecords_ETF(){
        US_Sales_Metrics_Controller.getOnLoadRecordsETF();
    }

    @isTest
    public static void testgetSalesByFundChart_ETF(){
        List<String> lstSelectedFund = new List<String>{'VFT'};
        Date startDate = date.newinstance(Date.today().year(), 1, 1);
        Date endDate = date.newinstance(Date.today().year(), 12, 31);

        US_Sales_Metrics_Controller.getSalesByFundChart_ETF(lstSelectedFund, startDate, endDate);
    }

    @isTest
    public static void testgetPlainReportforActiveFunds(){
        US_Sales_Metrics_Controller.getPlainReportforActiveFunds();
    }
    
}