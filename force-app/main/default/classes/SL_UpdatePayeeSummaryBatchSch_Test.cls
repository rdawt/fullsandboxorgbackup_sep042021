@isTest
private class SL_UpdatePayeeSummaryBatchSch_Test {

    @isTest
    static void testSL_UpdatePayeeSummaryBatch_Schedulable()
    {
        Test.StartTest();
   		SL_TestDataFactory.createTestDataForCustomSetting();
   		
        String sch = '0 0 0 15 3 ? *'; 
        system.schedule('Test SL_UpdatePayeeSummaryBatch', sch, new SL_UpdatePayeeSummaryBatch_Schedulable()); 
            
        Test.stopTest(); 
    }
}