/**
    Author: Vidhya Krishnan
    Date Created: 5/9/2012
    Description: Class to test Dynamic Subscription Concept
    Changes for Lead Collection : SV commented some of the SOQL join to make this class work in Sep. 2015
 */
@isTest
    private class DynamicSubscriptionTest {
        
        @isTest
        static void test() {
       
        String src='test';
       
        List<Contact> con1=new List<Contact>();
        
        List<Subscription_Member__c> sml1=new List<Subscription_Member__c>();
       
        Map<Id, list<Subscription_Member__c>> conSubMemMap = new Map<Id, list<Subscription_Member__c>>(); 
       
        List<Subscription_Member__c> smlistSV = new list<Subscription_Member__c>();
        
        
        Test.startTest();
        
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
        
        Account acc = new Account(Name = 'ektron', Channel__c = 'RIA', BillingCountry='US');
        sObject accInserted = SL_TestDataFactory.createSObject(acc,true);
  
        Contact con = new Contact(LastName='Test',SalesConnect__Firm_Name__c='[select name from contact]', AccountId=acc.Id, Email='test@abc.com',DB_Source__c = 'ABC',Service_Level__c='Super Senior', MailingCountry='US');
        sObject conInserted = SL_TestDataFactory.createSObject(con,true); 
  
        Subscription__c sub = new Subscription__c(Name='Test Sub', Subscription_Display_Name__c='Test Sub',IsActive__c=true);
        sObject subInserted = SL_TestDataFactory.createSObject(sub,true);
  
        Subscription__c sub1 = new Subscription__c(Name='Test Sub1', Subscription_Display_Name__c='Test Sub1',IsActive__c=true,Subscription_Indicator_Marker__c='test');
        sObject sub1Inserted = SL_TestDataFactory.createSObject(sub1,true);
  
        Subscription__c sub2 = new Subscription__c(Name='Test Sub2', Subscription_Display_Name__c='Test Sub2',IsActive__c=true,Subscription_Indicator_Marker__c='test');
        sObject sub2Inserted = SL_TestDataFactory.createSObject(sub2,true);
  
        Subscription_Group__c subGrp = new Subscription_Group__c(Name='test',IsActive__c=true,DB_Source_Exc_Filter__c='MVIS',KASL_Filter__c='Super Senior');
        sObject subGrpInserted = SL_TestDataFactory.createSObject(subGrp,true);
  
        Subscription_Group__c subGrp1 = new Subscription_Group__c(Name='test',IsActive__c=true,DB_Source_Exc_Filter__c='MVIS',Channel_Filter__c='RIA');
        sObject subGrp1Inserted = SL_TestDataFactory.createSObject(subGrp1,true);
    
        Subscription_Group_Subscription__c SGS = new Subscription_Group_Subscription__c(Subscription_Group__c =subGrp.Id,Subscription__c =sub.Id );
        sObject SGSInserted = SL_TestDataFactory.createSObject(SGS,true);
    
        Subscription_Group_Subscription__c SGS1 = new Subscription_Group_Subscription__c(Subscription_Group__c =subGrp.Id,Subscription__c =sub1.Id );
        sObject SGS1Inserted = SL_TestDataFactory.createSObject(SGS1,true);
    
        Subscription_Group_Subscription__c SGS2 = new Subscription_Group_Subscription__c(Subscription_Group__c =subGrp1.Id,Subscription__c =sub1.Id );
        sObject SGS2Inserted = SL_TestDataFactory.createSObject(SGS2,true);
    
        Subscription_Group_Subscription__c SGS3 = new Subscription_Group_Subscription__c(Subscription_Group__c =subGrp1.Id,Subscription__c =sub2.Id );
        sObject SGS3Inserted = SL_TestDataFactory.createSObject(SGS3,true);
    
        Subscription_Member__c sm1 = new Subscription_Member__c(Subscription__c=sub.Id, Contact__c=con.Id, Email__c='test@abc.com', Subscribed__c=true);
        sObject sm1Inserted = SL_TestDataFactory.createSObject(sm1,true);
    
        Subscription_Member__c sm2 = new Subscription_Member__c(Subscription__c=sub1.Id, Contact__c=con.Id, Email__c='test1@abc.com', Subscribed__c=true);
        sObject sm2Inserted = SL_TestDataFactory.createSObject(sm2,true);
    
        con.Service_Level__c=null;
    
        update con;
        
        con1.add(con);
    
        DynamicSubscription ds1=new DynamicSubscription(con1,src);        
        
        sml1.add(sm1);
      
        conSubMemMap.put(con.id,sml1);
              
        ds1.handleSubscription();
        
        ds1.createTask(conSubMemMap);
        
        Test.stopTest(); 
     }
}