public class AccountTriggerHandler {

    public static Boolean isFirstTime = true;
    static List<Account> records = new List<Account>();
    static Map<Id, Account> oldMap = new Map<Id, Account>();

    public static void onAfterInsert(List<Account> lstNewAccounts){
        records = lstNewAccounts;
        Set<Id> setAccountIds = new Set<Id>();
        Set<Id> setAccountRemoveIds = new Set<Id>();
        for(Account acct : records){
            if(acct.Apply_Territory_Rules__c){
                setAccountIds.add(acct.Id);
            }else{
                setAccountRemoveIds.add(acct.Id);
            }
        }
        if(!setAccountIds.isEmpty()){
            updateTerritoryDataOnAccounts(setAccountIds);
        }
        if(!setAccountRemoveIds.isEmpty()){
            removeTerritoryDataOnAccounts(setAccountRemoveIds);
        }
    }

    public static void onAfterUpdate(List<Account> lstNewAccounts,Map<Id, Account> mapOldAccounts){
        records = lstNewAccounts;
        oldMap = mapOldAccounts;
        Set<Id> setAccountIds = new Set<Id>();
        Set<Id> setAccountRemoveIds = new Set<Id>();
        for(Account acct : records){
            if(acct.Apply_Territory_Rules__c){
                setAccountIds.add(acct.Id);
            }else{
                setAccountRemoveIds.add(acct.Id);
            }
        }
        if(!setAccountIds.isEmpty()){
            updateTerritoryDataOnAccounts(setAccountIds);
        }
        if(!setAccountRemoveIds.isEmpty()){
            removeTerritoryDataOnAccounts(setAccountRemoveIds);
        }   
    }

    public static void updateTerritoryDataOnAccounts(Set<Id> setAccountIds){
        Map<Id, Set<Id>> mapAccIdUserOrGroupIds = new Map<Id, Set<Id>>();
        Map<Id, Id> mapGroupIdRelatedId = new Map<Id, Id>();
        Map<Id, String> mapRelatedIdTerritoryName = new Map<Id, String>();
        Map<Id, String> mapGrpIdTerritoryName = new Map<Id, String>();
        Set<Id> setUserGroupIds = new Set<Id>();
        String strChannel;
        String strTerritory;
        String strRegion;

        for(AccountShare acctShare : [SELECT Id,RowCause,UserOrGroupId,AccountId FROM AccountShare WHERE RowCause = 'Territory' AND AccountId IN :setAccountIds]){
            if(mapAccIdUserOrGroupIds.containsKey(acctShare.AccountId)){
                Set<Id> tempUserOrGroupIds = mapAccIdUserOrGroupIds.get(acctShare.AccountId);
                tempUserOrGroupIds.add(acctShare.UserOrGroupId);
                mapAccIdUserOrGroupIds.put(acctShare.AccountId,tempUserOrGroupIds);
            }else{
                Set<Id> tempUserOrGroupIds = new Set<Id>();
                tempUserOrGroupIds.add(acctShare.UserOrGroupId);
                mapAccIdUserOrGroupIds.put(acctShare.AccountId,tempUserOrGroupIds);
            }
            setUserGroupIds.add(acctShare.UserOrGroupId);
        }

        for(Group grp : [Select Id, RelatedId from Group where Type='Territory' and Id IN : setUserGroupIds]){
            mapGroupIdRelatedId.put(grp.Id,grp.RelatedId);
        }
        
        for(Territory2 territory: [select Id, Name from Territory2 where Id IN :mapGroupIdRelatedId.Values()] ){
            mapRelatedIdTerritoryName.put(territory.Id, territory.Name);
        }

        for (Id grpId : mapGroupIdRelatedId.keySet()) {
            if(mapRelatedIdTerritoryName.containsKey(mapGroupIdRelatedId.get(grpId))){
                mapGrpIdTerritoryName.put(grpId , mapRelatedIdTerritoryName.get(mapGroupIdRelatedId.get(grpId)));
            }
        }

        List<Account> lstAccount = [Select Id,Name,Channel_New__c,Territory_New__c from Account where Id IN : setAccountIds];
        for(Account acc : lstAccount){
            strChannel = '';
            strTerritory = '';
            strRegion = '';
            if(mapAccIdUserOrGroupIds.containsKey(acc.Id)){
                Set<Id> setGroupIds = mapAccIdUserOrGroupIds.get(acc.Id);
                for(Id grpId : setGroupIds){
                    if(mapGrpIdTerritoryName.containsKey(grpId)){
                        Integer endIndex = mapGrpIdTerritoryName.get(grpId).indexOf('-');
                        if(endIndex != -1){
                            if(mapGrpIdTerritoryName.get(grpId).containsIgnoreCase('Channel')){
                                String tempChannel = mapGrpIdTerritoryName.get(grpId);
                                strChannel = tempChannel.substring(0,endIndex);
                            }else if(mapGrpIdTerritoryName.get(grpId).containsIgnoreCase('Region')){
                                String tempRegion = mapGrpIdTerritoryName.get(grpId);
                                strRegion = tempRegion.substring(0,endIndex);
                            }else if(mapGrpIdTerritoryName.get(grpId).containsIgnoreCase('Territory')){
                                String tempTerritory = mapGrpIdTerritoryName.get(grpId);
                                strTerritory = tempTerritory.substring(0,endIndex);
                            }
                        }
                    }
                }
            }
            acc.Channel_New__c = strChannel.trim();
            acc.Region_New__c = strRegion.trim();
            acc.Territory_New__c = strTerritory.trim();
        }
        update lstAccount;
       
    }

    public static void removeTerritoryDataOnAccounts(Set<Id> setAccountIds){
        List<Account> lstAccount = [Select Id,Name,Channel_New__c,Territory_New__c from Account where Id IN : setAccountIds];
        for(Account acc : lstAccount){
            acc.Channel_New__c = '';
            acc.Region_New__c = '';
            acc.Territory_New__c = '';
        }
        update lstAccount;
    }
        
}