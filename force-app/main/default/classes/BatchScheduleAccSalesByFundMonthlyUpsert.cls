/**************************************************************************************
* @Author: Tharaka De Silva
*
* This class implement the Schedulable interface in the purpose of calling BatchAccSalesByFundMonthlyUpsert.
* This schedulable will upsert the Accounts Sales by Fund data in to backup object monthly.
***************************************************************************************/
public class BatchScheduleAccSalesByFundMonthlyUpsert implements Schedulable{
    public void execute(SchedulableContext sc)
    { 
        BatchAccSalesByFundMonthlyUpsert batch = new BatchAccSalesByFundMonthlyUpsert(); 
        database.executebatch(batch,200);
    }
}