public class TeamMemberTriggerHandler {

    List<ASP_Team_Member__c> records = new List<ASP_Team_Member__c>();
    Map<Id, ASP_Team_Member__c> oldMap = new Map<Id, ASP_Team_Member__c>();

    public void onAfterInsert(List<ASP_Team_Member__c> lstNewTeamMembers){
        records = lstNewTeamMembers;

        Set<Id> setTeamsIds = new Set<Id>();
        for(ASP_Team_Member__c member : records){
            if(member.Sales_Priority_Level_Derived__c  != null){
                setTeamsIds.add(member.Teams__c);
            }
        }
        if(setTeamsIds.size() > 0){
            updatePriorityMembers(setTeamsIds);
        }
    }


    public void onAfterUpdate(List<ASP_Team_Member__c> lstNewTeamMembers,Map<Id, ASP_Team_Member__c> mapOldTeamMembers){
        records = lstNewTeamMembers;
        oldMap = mapOldTeamMembers;

        Set<Id> setTeamsIds = new Set<Id>();
        for(ASP_Team_Member__c member : records){
            ASP_Team_Member__c oldMember = oldMap.get(member.Id);
            if(member.Sales_Priority_Level_Derived__c != oldMember.Sales_Priority_Level_Derived__c){
                setTeamsIds.add(member.Teams__c);
            }
        }
        if(setTeamsIds.size() > 0){
            updatePriorityMembers(setTeamsIds);
        }
    }

    private void updatePriorityMembers(Set<Id> setTeamsIds){
        Map<Id, List<ASP_Team_Member__c>> mapTeams = new Map<Id, List<ASP_Team_Member__c>>();
        List<ASP_Team_Member__c> lstTeamMembers = [SELECT Id,Sales_Priority_Level_Derived__c,Teams__c,ASP_Team_Member__r.Name
                                                    FROM ASP_Team_Member__c
                                                    WHERE Teams__c IN : setTeamsIds];

        if(!lstTeamMembers.isEmpty()){
            for(ASP_Team_Member__c member: lstTeamMembers){
                if(!mapTeams.containsKey(member.Teams__c)){
                    mapTeams.put(member.Teams__c,new List<ASP_Team_Member__c>{member});
                }else{
                    List<ASP_Team_Member__c> tempList = (List<ASP_Team_Member__c>)mapTeams.get(member.Teams__c);
                    tempList.add(member);
                    mapTeams.put(member.Teams__c,tempList);
                }
            }
        }

        List<ASP_Team__c> lstTeams = [SELECT Id,Priority_Members_A__c,Priority_Members_B__c,Priority_Members_C__c,Priority_Members_P__c 
                                                    FROM ASP_Team__c
                                                    WHERE Id  IN : setTeamsIds];
        for(ASP_Team__c team: lstTeams){
            String priorityMembersA = '';
            String priorityMembersB = '';
            String priorityMembersC = '';
            String priorityMembersP = '';
            Decimal priorityMembersACount = 0;
            Decimal priorityMembersBCount = 0;
            Decimal priorityMembersCCount = 0;
            Decimal priorityMembersPCount = 0;
            if(mapTeams.containsKey(team.Id)){
                
                for(ASP_Team_Member__c atm: mapTeams.get(team.Id)){
                    if(atm.Sales_Priority_Level_Derived__c == 'A'){
                        priorityMembersACount++;
                        priorityMembersA += (!String.isBlank(priorityMembersA)  ? ' , '+ atm.ASP_Team_Member__r.Name : atm.ASP_Team_Member__r.Name);
                    }else if(atm.Sales_Priority_Level_Derived__c == 'B'){
                        priorityMembersBCount++;
                        priorityMembersB += (!String.isBlank(priorityMembersB) ? ' , '+ atm.ASP_Team_Member__r.Name : atm.ASP_Team_Member__r.Name);
                    }else if(atm.Sales_Priority_Level_Derived__c == 'C'){
                        priorityMembersCCount++;
                        priorityMembersC += (!String.isBlank(priorityMembersC) ? ' , '+ atm.ASP_Team_Member__r.Name : atm.ASP_Team_Member__r.Name);
                    }else if(atm.Sales_Priority_Level_Derived__c == 'P'){
                        priorityMembersPCount++;
                        priorityMembersP += (!String.isBlank(priorityMembersP) ? ' , '+ atm.ASP_Team_Member__r.Name : atm.ASP_Team_Member__r.Name);
                    }
                }
            }
            team.Priority_Members_A__c = priorityMembersA;
            team.Priority_Members_B__c = priorityMembersB;
            team.Priority_Members_C__c = priorityMembersC;
            team.Priority_Members_P__c = priorityMembersP;
            team.Priority_Members_A_Count__c = priorityMembersACount;
            team.Priority_Members_B_Count__c = priorityMembersBCount;
            team.Priority_Members_C_Count__c = priorityMembersCCount;
            team.Priority_Members_P_Count__c = priorityMembersPCount;
        }
        update lstTeams;
    }
}