public with sharing class SL_TaskTriggerHandler {
    
    /*
    public void onAfterInsert(List<Task> lstNewTask){
    	
    	//Commented out whole function as data sending to other function is commented out and no need of this processing right now
        Id idMarketoUserId = [SELECT Id, Name FROM User WHERE FirstName ='Marketo' AND LastName = 'API' LIMIT 1].Id;
        Set<Id> setWhoIds = new Set<Id>();
        Set<Id> setCreatedByIds = new Set<Id>();
        for(Task objNewTask: lstNewTask){
            if (((objNewTask.whoid != null) && objNewTask.Assigned_To_is_active__c == TRUE &&(objNewTask.OwnerId <> idMarketoUserId)) ) { //|| ((objNewTask.whoid != null) && (objNewTask.Marketing_Automation__c == false))
                setWhoIds.add(objNewTask.whoid);
                // Commented out the code so that update Payee Summary record is query only for Owner and not who create the Task Record
                //setCreatedByIds.add(objNewTask.createdbyid);
                // below is updated code (4 lines)
                if(objNewTask.ContactOwnerID__c != NULL && objNewTask.Assigned_To_is_active__c == TRUE && objNewTask.OwnerId <> idMarketoUserId)
                    setCreatedByIds.add(objNewTask.ContactOwnerID__c);
            }
        }
        
        system.debug('>>>>>>>>>>>>>>>>>>>>>setWhoIds>>>>>>>>>>>>>>>>>>>>' + setWhoIds.isEmpty());
        system.debug('>>>>>>>>>>>>>>>>>>>>>setWhoIds>>>>>>>>>>>>>>>>>>>>' + setWhoIds);
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>>setCreatedByIds>>>>>>>>>>>>>>>>>' + setCreatedByIds.isEmpty());     
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>>setCreatedByIds>>>>>>>>>>>>>>>>>' + setCreatedByIds); 
           
        
        // Commented out the code as logic moved to batch
        //if(!setWhoIds.isEmpty())
            //updateParentTaskContact(setWhoIds);
        //else if(!setCreatedByIds.isEmpty() && !setWhoIds.isEmpty())
            //updatePayeeList(setCreatedByIds,setWhoIds);
            
        
    }
    */
    
    public void onAfterUpdate(List<Task> lstNewTask){
        
        /* Commented out whole function as data sending to other function is commented out and no need of this processing right now
        Id idMarketoUserId = [SELECT Id, Name FROM User WHERE FirstName ='Marketo' AND LastName = 'API' LIMIT 1].Id;
        Set<Id> setWhoIds = new Set<Id>();
        Set<Id> setCreatedByIds = new Set<Id>();
        for(Task objNewTask: lstNewTask){
            if (((objNewTask.whoid != null) && (objNewTask.ownerid <> idMarketoUserId)) ) { //|| ((objNewTask.whoid != null) && (objNewTask.Marketing_Automation__c == false))
                setWhoIds.add(objNewTask.whoid);
                // Commented out the code so that update Payee Summary record is query only for Owner and not who create the Task Record
                //setCreatedByIds.add(objNewTask.createdbyid);
                // below is updated code (4 lines)
                if(objNewTask.ContactOwnerID__c != NULL)
                    setCreatedByIds.add(objNewTask.ContactOwnerID__c);
                else
                    setCreatedByIds.add(objNewTask.ownerid);
            }
        } 
        // Commented out the code as logic moved to batch
        //if(setWhoIds.size() != 0)
        //    updateParentTaskContact(setWhoIds);
        //if(setCreatedByIds.size() != 0)
        //    updatePayeeList(setCreatedByIds,setWhoIds);
        */
        
    }
    public void onAfterDelete(List<Task> lstOldTask){
        
        /* Commented out whole function as data sending to other function is commented out and no need of this processing right now
        Id idMarketoUserId = [SELECT Id, Name FROM User WHERE FirstName ='Marketo' AND LastName = 'API' LIMIT 1].Id;
        Set<Id> setWhoIds = new Set<Id>();
        Set<Id> setCreatedByIds = new Set<Id>();
        for(Task objOldTask: lstOldTask){
            //exclude mkto api and nulls
            if (((objOldTask.whoid != null) && (objOldTask.ownerid <> idMarketoUserId)) ){ //|| ((objOldTask.whoid != null) && (objOldTask.Marketing_Automation__c == false))
                system.debug('inside delete trigger condition - contact id'+objOldTask.whoid);
                setWhoIds.add(objOldTask.whoid);
                // Commented out the code so that update Payee Summary record is query only for Owner and not who create the Task Record
                //setCreatedByIds.add(objOldTask.createdbyid);
                // below is updated code (4 lines)
                if(objOldTask.ContactOwnerID__c != NULL)
                    setCreatedByIds.add(objOldTask.ContactOwnerID__c);
                else
                    setCreatedByIds.add(objOldTask.ownerid);
            }
        } 
        // Commented out the code as logic moved to batch
        //if(setWhoIds.size() != 0)
        //    updateParentTaskContact(setWhoIds);
        //if(setCreatedByIds.size() != 0)
        //    updatePayeeList(setCreatedByIds,setWhoIds );
        */
    }
    
    /*
    //Commented this code as logic is moved to SL_UpdateContactFromTaskBatch.cls
    private void updateParentTaskContact(Set<Id> setWhoIds){
    	
    	system.debug('>>>>>>>>>>>>>>>>>>I am in update parent Task Contact>>>>>>>>>>>>>>>>>>>>');
        AggregateResult[] tCounts=[select  whoid, count(id) taskCount
                                   from task
                                   where whoid in : setWhoIds and Marketing_Automation__c = false 
                                   group by whoid];//and CALENDAR_MONTH(CreatedDate) = 1 //ActivityDate
        system.debug('after agg. count- count is'+ tCounts);
        list<Contact> conList= new list<Contact>();                             
        for(AggregateResult forLoopCount : tCounts){
            contact parentTaskContact = new contact(Id=string.valueof(forLoopCount.get('whoId')),TaskCounter__c =  integer.valueof(forLoopCount.get('taskCount')));
            conList.add(parentTaskContact);
        }
        update conList;
        
    }
    */ 
    
    /*    
	// Commented out this code as logic is moved to SL_UpdatePayeeSummaryBatch.cls
    private void updatePayeeList(Set<Id> setCreatedByIDs, Set<Id> setWhoIds){
    	
    	system.debug('>>>>>>>>>>>>>>>>>>I am in update Payee List>>>>>>>>>>>>>>>>>>>>');
        Date currentDate = system.today();
        AggregateResult[] taskUsersCount; 
        AggregateResult[] taskUsersQCount; 
        AggregateResult[] timeTradeUsersCount;
        AggregateResult[] glanceUsersCount;
        
        list<PayeeSummary__c> addPayeeSummaryList= new list<PayeeSummary__c>();
        list<PayeeSummary__c> modPayeeSummaryList= new list<PayeeSummary__c>();
        taskUsersCount  =[select  createdbyid, sum(FA_Activity_Score__c ) FAActivityScore, sum(RIA_Activity_Score__c ) RIAActivityScore
                          from Task
                          where createdbyid in : setCreatedByIDs  and  Marketing_Automation__c = false and createddate = THIS_MONTH and createddate = THIS_YEAR
                          group by createdbyid];//
        
        taskUsersQCount =[select  createdbyid, sum(FA_Activity_Score__c ) FAActivityQScore, sum(RIA_Activity_Score__c ) RIAActivityQScore
                          from Task
                          where createdbyid in : setCreatedByIDs  and  Marketing_Automation__c = false and createddate = THIS_QUARTER and createddate = THIS_YEAR
                          group by createdbyid];//
        
        //Timetrade count query
        timeTradeUsersCount = [SELECT count(OwnerId) TimeTradeCount,createdbyid FROM Task where (NOT (Subject like '%Email: Van Eck Global’s Online Privacy Policy')) 
                               and whoid IN  :setWhoIds
                               and timetrade__c = true and (status = 'Call Completed' or status = 'Call Attempted' or status = 'call left voicemail') 
                               and activitydate = this_quarter and ownerid in : setCreatedByIDs group by createdbyid];
        
        //Glace count query
        glanceUsersCount = [SELECT count(OwnerId) GlanceCount,createdbyid FROM Task where  (NOT (Subject like '%Email: Van Eck Global’s Online Privacy Policy')) and 
                            whoid IN : setWhoIds
                            and (status = 'Call Completed' or status = 'Call Attempted' or status ='call left voicemail' or status like '%call%')  and glance__c = true 
                            and activitydate = this_quarter and ownerid in : setCreatedByIDs group by  createdbyid];
        
        if ((timeTradeUsersCount.size() == 0) && (glanceUsersCount.size() == 0) && (taskUsersCount.size() == 0) && (taskUsersQCount.size() == 0) ){
            return;
        }                               
        
        system.debug('**********TASKUserCount!!*******************'+ taskUsersCount.size());
        system.debug('**********TASKUserQCount!!*******************'+ taskUsersQCount.size());
        system.debug('**********timeTradeUsersCount!!*******************'+ timeTradeUsersCount.size());
        system.debug('**********timeTradeUsersCount!!*******************'+ glanceUsersCount.size());
        
        
        //for Q count of Score
        
        Map<Id, PayeeSummary__c> payeeListQScoreCountMap = new Map<Id, PayeeSummary__c>();
        
        for (AggregateResult scoreQLoopUsersCount  : taskUsersQCount ) //
        {
            
            string payeeId = string.valueof(scoreQLoopUsersCount.get('createdbyid'));
            integer scoreQFACounter = integer.valueof(scoreQLoopUsersCount.get('FAActivityQScore'));
            integer scoreQRIACounter = integer.valueof(scoreQLoopUsersCount.get('RIAActivityQScore'));
            PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId,TaskFAQScore__c= scoreQFACounter , TaskRIAQScore__c = scoreQRIACounter  );
            payeeListQScoreCountMap.put(payeeId,pSummary);
            
        }
        
        //end Q score 
        Map<Id, PayeeSummary__c> payeeListQTimeTradeCountMap = new Map<Id, PayeeSummary__c>();
        
        for (AggregateResult timeTradeLoopUsersCount  : timeTradeUsersCount ) //
        {
            
            string payeeId = string.valueof(timeTradeLoopUsersCount.get('createdbyid'));
            integer timeTradeQCounter = integer.valueof(timeTradeLoopUsersCount.get('TimeTradeCount'));
            PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId,timeTradeQCount__c = timeTradeQCounter );
            payeeListQTimeTradeCountMap.put(payeeId,pSummary);
            
        }
        
        Map<Id, PayeeSummary__c> payeeListQGlanceCountMap = new Map<Id, PayeeSummary__c>();
        
        for (AggregateResult glanceLoopUsersCount  : glanceUsersCount ) //
        {
            
            string payeeId = string.valueof(glanceLoopUsersCount.get('createdbyid'));
            integer glanceQCounter = integer.valueof(glanceLoopUsersCount.get('GlanceCount'));
            PayeeSummary__c pSummary = new PayeeSummary__c(user__c=payeeId,GlaceQCount__c= glanceQCounter );
            payeeListQGlanceCountMap.put(payeeId,pSummary);
            
        }
        
        // Map<String,Integer> payeeListMap = new Map<String,Integer>();
        Map<Id, PayeeSummary__c> payeeListMap = new Map<Id, PayeeSummary__c>();
        Integer j =0;
        
        system.debug('what is !!Today!! '+ currentDate );
        system.debug('what is this ***month*** '+ currentDate.Month());
        system.debug('what is this ***calendar month*** '+ (system.today().month()));
        system.debug('what is this ***year***'+ currentDate.Year());
        system.debug('current month after using string of  :'+String.ValueOf(currentDate.Month()));
        system.debug('current year after using string of  :'+String.ValueOf(currentDate.Year()));
        
        system.debug('?????????????setCreatedByIDs????????????????' + setCreatedByIDs);
        
        for(PayeeSummary__c payeeSummary: [Select id, user__c from PayeeSummary__c  Where user__c in : setCreatedByIDs and Month__c = :String.ValueOf(currentDate.Month()) and Year__c =: String.ValueOf(currentDate.Year()) FOR UPDATE]){
            modPayeeSummaryList.add(payeeSummary);
            payeeListMap.put(payeeSummary.user__c,payeeSummary); // payeeSummary);
            system.debug('**********PayeeSummary Count!!*******************'+ payeeSummary);
        }
        
        system.debug('?????????????payeeListMap????????????????' + payeeListMap);
        
        system.debug('after agg. User-Payee FA Activity Score Sum- Payee List is'+ taskUsersCount  );
        list<PayeeSummary__c> addPayeeList = new list<PayeeSummary__c>(); 
        list<PayeeSummary__c> updatePayeeList = new list<PayeeSummary__c>(); 
        
        for(AggregateResult forLoopCount : taskUsersCount){
            system.debug('entering forloop to chase the matching payee ids'+ taskUsersCount  );
            if((payeeListMap != null) && payeeListMap.containsKey(string.valueof(forLoopCount.get('createdbyid'))))
            {
                //get the index
                integer timeTradeCount = 0;
                integer glanceCount = 0;
                integer fAScoreQCount = 0;
                integer rIAScoreQCount = 0;
                PayeeSummary__c index = payeeListMap.get(string.valueof(forLoopCount.get('createdbyid')));
                if((payeeListQTimeTradeCountMap!= null) && (  payeeListQTimeTradeCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                    
                    PayeeSummary__c timeTradeIndex = payeeListQTimeTradeCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                    timeTradeCount = integer.valueOf(timeTradeIndex.timeTradeQCount__c );
                }
                if((payeeListQGlanceCountMap!= null) && (  payeeListQGlanceCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                    
                    PayeeSummary__c glanceIndex = payeeListQGlanceCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                    glanceCount = integer.valueOf(glanceIndex.GlaceQCount__c );
                    
                }
                if((payeeListQScoreCountMap!= null) && (  payeeListQScoreCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                    
                    PayeeSummary__c qScoreIndex = payeeListQScoreCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                    fAScoreQCount = integer.valueOf(qScoreIndex.TaskFAQScore__c );
                    rIAScoreQCount = integer.valueOf(qScoreIndex.TaskRIAQScore__c );
                    
                }
                PayeeSummary__c payeeSummary = new PayeeSummary__c(id=string.valueof( String.ValueOf(index.id)),Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),User__c=string.valueof(forLoopCount.get('createdbyid')), TaskFAScore__c =integer.valueof(forLoopCount.get('FAActivityScore')),TaskFAQScore__c = fAScoreQCount  ,TaskRIAScore__c =integer.valueof(forLoopCount.get('RIAActivityScore')),TaskRIAQScore__c = rIAScoreQCount , timeTradeQCount__c = timeTradeCount, GlaceQCount__c =  glanceCount );
                updatePayeeList.add(payeeSummary);                   
            }
            else{
                
                integer timeTradeCount = 0;
                integer glanceCount = 0;
                integer fAScoreQCount = 0;
                integer rIAScoreQCount = 0;
                
                system.debug('inside else of **ADDING** new payee summary for the current month'+ taskUsersCount  );
                
                if((payeeListQTimeTradeCountMap!= null) && (  payeeListQTimeTradeCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                    
                    PayeeSummary__c timeTradeIndex = payeeListQTimeTradeCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                    timeTradeCount = integer.valueOf(timeTradeIndex.timeTradeQCount__c );
                    
                }
                if((payeeListQGlanceCountMap!= null) && (  payeeListQGlanceCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                    
                    PayeeSummary__c glanceIndex = payeeListQGlanceCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                    glanceCount = integer.valueOf(glanceIndex.GlaceQCount__c );
                    
                }
                if((payeeListQScoreCountMap!= null) && (  payeeListQScoreCountMap.containsKey(string.valueof(forLoopCount.get('createdbyid')))) ){
                    
                    PayeeSummary__c qScoreIndex = payeeListQScoreCountMap.get(string.valueof(forLoopCount.get('createdbyid')));
                    fAScoreQCount = integer.valueOf(qScoreIndex.TaskFAQScore__c );
                    rIAScoreQCount = integer.valueOf(qScoreIndex.TaskRIAQScore__c );
                    
                }
                
                PayeeSummary__c payeeSummary = new PayeeSummary__c(Year__c=String.ValueOf(currentDate.Year()),Month__c=String.ValueOf(currentDate.Month()),User__c=string.valueof(forLoopCount.get('createdbyid')),TaskFAScore__c =integer.valueof(forLoopCount.get('FAActivityScore')),TaskFAQScore__c =fAScoreQCount ,TaskRIAScore__c =integer.valueof(forLoopCount.get('RIAActivityScore')),TaskRIAQScore__c = rIAScoreQCount , timeTradeQCount__c = timeTradeCount,GlaceQCount__c =  glanceCount);
                addPayeeList.add(payeeSummary);
                
            }
        }
        if (updatePayeeList.size() != 0) {
            update updatePayeeList;
        }
        if (addPayeeList.size() != 0) {
            if(Test.isRunningTest()){
                addPayeeList[0].Total_Days_for_this_Calendar_Month__c = 30;
                addPayeeList[0].No_Of_Days_in_Office__c = 25;
                
            }
            insert addPayeeList;
        }
        
    }
    */
   
}