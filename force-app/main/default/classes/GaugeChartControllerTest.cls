/********************************************************************************************
* @Author       : Tharaka De Silva
* @Date Created : 06-08-2020
* @Description  : Test class for the GaugeChartController.
* *******************************************************************************************/
@isTest
public with sharing class GaugeChartControllerTest {

    @TestSetup
    public static void createTestData()
    {
        Integer iYear = Date.Today().Year();
        String year = String.valueOf(iYear);
        DB_US_Goal_Year_Config__c setting = new DB_US_Goal_Year_Config__c();
        setting.Name = year;
        setting.MF_Year_Type__c = 'THIS_YEAR';
        setting.ETF_Base_Object__c = 'Net_Sales_Data'; 
        insert setting;

        //Insert Fund records
        Fund__c fundEme = new Fund__c(Name='Emerging Markets Fund (EME)',Fund_Type__c='MF',Fund_Ticker_Symbol__c='EME');
        insert fundEme; 
        Fund__c fundAngl = new Fund__c(Name='Fallen Angel High Yield Bond ETF (ANGL)',Fund_Type__c='FI-ETF',Fund_Ticker_Symbol__c='ANGL');
        insert fundAngl; 
        Fund__c fundMix = new Fund__c(Name='RAAX-MAAX-LFEQ Combination',Fund_Type__c='ETF', Fund_Ticker_Symbol__c='RAAX-MAAX-LFEQ');
        insert fundMix; 
        Fund__c fundMFMix = new Fund__c(Name='EME-IIG Combination',Fund_Type__c='MF', Fund_Ticker_Symbol__c='EME-IIG');
        insert fundMFMix; 
           
        //Insert US_Sales_Goals_Tracker records
        List<US_Sales_Goals_Tracker__c> lstConfigs = new List<US_Sales_Goals_Tracker__c>();
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='EME-Firm', Title__c='EME Chart', Portfolio_Name__c = fundEme.Id, Channel__c = 'Firm',Status__c = 'Active', Goal_Amount__c = 680000000, Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='EME-Institutional', Title__c='EME Institutional Chart', Portfolio_Name__c = fundEme.Id , Channel__c = 'Institutional', Status__c = 'Active' ,Goal_Amount__c = 680000000, Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='EME-Insurance', Title__c='EME Insurance Chart', Portfolio_Name__c = fundEme.Id , Channel__c = 'Insurance', Status__c = 'Active', Goal_Amount__c = 680000000, Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='ANGL-All', Title__c='ANGL Chart', Portfolio_Name__c = fundAngl.Id ,Channel__c = 'Firm', Status__c = 'Active', Goal_Amount__c = 680000000 , Display_ETF_Source_Data__c = true
            ,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000,ETF_Channel__c='Firm', Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='ANGL-Financial Advisor', Title__c='ANGL Financial Advisor Chart', Portfolio_Name__c = fundAngl.Id , Channel__c = 'Financial Advisor',Status__c = 'Active', Goal_Amount__c = 680000000 ,Display_ETF_Source_Data__c = true
            ,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000,ETF_Channel__c='Financial Advisor', Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='RAAX-MAAX-LFEQ-All', Title__c='RAAX-MAAX-LFEQ Firm Chart', Portfolio_Name__c = fundMix.Id , Channel__c = 'Firm', Status__c = 'Active', Goal_Amount__c = 680000000 ,Display_ETF_Source_Data__c = true
            ,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000,ETF_Channel__c='Firm', Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='RAAX-MAAX-LFEQ-Financial Advisor', Title__c='RAAX-MAAX-LFEQ Financial Advisor Chart', Portfolio_Name__c = fundMix.Id , Channel__c = 'Financial Advisor',Status__c = 'Active', Goal_Amount__c = 680000000
            ,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000,ETF_Channel__c='ASSET MANAGER', Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='ANGL-Financial Advisor', Title__c='ANGL Financial Advisor Org Type Chart', Portfolio_Name__c = fundAngl.Id , Channel__c = 'Financial Advisor',Status__c = 'Active', Is_Organization_Type__c=true, Organization_Type__c = 'Unknown' 
            , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000, Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='ANGL-Financial Advisor-Territory', Title__c='ANGL Financial Advisor Territory Chart', Portfolio_Name__c = fundAngl.Id , Channel__c = 'Financial Advisor',Status__c = 'Active', Is_Organization_Type__c=false, IS_Territory__c = true 
            , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000,ETF_Channel__c='RIA', Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='RAAX-MAAX-LFEQ-Territory', Title__c='RAAX-MAAX-LFEQ Territory Chart', Portfolio_Name__c = fundMix.Id, Channel__c = 'Financial Advisor',Status__c = 'Active', Is_Organization_Type__c=false, IS_Territory__c = true 
            , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000, Goal_Year__c = year), false));

            lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='RAAX-MAAX-LFEQ-Territory', Title__c='RAAX-MAAX-LFEQ Territory Chart', Portfolio_Name__c = fundMix.Id, Channel__c = 'Financial Advisor',Status__c = 'Active', Is_Organization_Type__c=false, IS_Territory__c = true 
            , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000, Goal_Year__c = year), false));



        
            //
            lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
                new US_Sales_Goals_Tracker__c(Name='ANGL-Financial Advisor', Title__c='ANGL Financial Advisor Chart', Portfolio_Name__c = fundAngl.Id , Channel__c = 'Financial Advisor',Status__c = 'Active', Goal_Amount__c = 680000000
                ,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000,ETF_Channel__c='INSTITUTIONAL', Goal_Year__c = year), false));
                lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='ANGL-Financial Advisor', Title__c='ANGL Financial Advisor Chart', Portfolio_Name__c = fundAngl.Id , Channel__c = 'Financial Advisor',Status__c = 'Active', Goal_Amount__c = 680000000
            ,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000,ETF_Channel__c='ASSET MANAGER', Goal_Year__c = year), false));
            lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='ANGL-Financial Advisor', Title__c='ANGL Financial Advisor Chart', Portfolio_Name__c = fundAngl.Id , Channel__c = 'Financial Advisor',Status__c = 'Active', Goal_Amount__c = 680000000
            ,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000,ETF_Channel__c='RIA', Goal_Year__c = year), false));


        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='EME-IIG-UnAssigned', Title__c='EME | IIG-UnAssigned', Portfolio_Name__c = fundMFMix.Id, Channel__c = 'Unassigned',Status__c = 'Active', Is_Organization_Type__c=false, IS_Territory__c = false
            , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000, Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='EME-IIG - Financial Advisor', Title__c='EME-IIG - Financial Advisor', Portfolio_Name__c = fundMFMix.Id, Channel__c = 'Financial Advisor',Status__c = 'Active', Is_Organization_Type__c=false, IS_Territory__c = false
            , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000, Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='EME-IIG - RIA', Title__c='EME-IIG - RIA', Portfolio_Name__c = fundMFMix.Id, Channel__c = 'RIA',Status__c = 'Active', Is_Organization_Type__c=false, IS_Territory__c = false
            , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000, Goal_Year__c = year), false));
            lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
                new US_Sales_Goals_Tracker__c(Name='EME-IIG - Insurance', Title__c='EME-IIG - Insurance', Portfolio_Name__c = fundMFMix.Id, Channel__c = 'Insurance',Status__c = 'Active', Is_Organization_Type__c=false, IS_Territory__c = false
                , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000, Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='EME-IIG - Firm', Title__c='EME-IIG - Firm', Portfolio_Name__c = fundMFMix.Id, Channel__c = 'Firm',Status__c = 'Active', Is_Organization_Type__c=false, IS_Territory__c = false
            , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000, Goal_Year__c = year), false));
        //--
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='EME-IIG-FA-FL', Title__c='EME-IIG-FA-FL', Portfolio_Name__c = fundMFMix.Id, Channel__c = 'Financial Advisor',Status__c = 'Active', Is_Organization_Type__c=false, IS_Territory__c = true
            , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000,FF_Territory__c = 'FL', Goal_Year__c = year), false));

        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='EME-IIG-FA-FL', Title__c='EME-IIG-FA-FL', Portfolio_Name__c = fundMFMix.Id, Channel__c = 'FA',Status__c = 'Active', Is_Organization_Type__c=false, IS_Territory__c = true
            , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000,FF_Territory__c = 'Unassigned', Goal_Year__c = year), false));
        lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='EME-IIG-RIA-FL', Title__c='EME-IIG-RIA-FL', Portfolio_Name__c = fundMFMix.Id, Channel__c = 'FA',Status__c = 'Active', Is_Organization_Type__c=false, IS_Territory__c = true
            , Goal_Amount__c = 680000000,Red_Range__c=227000000,Amber_Range__c=226000000,Green_Range__c=227000000,FF_Territory__c = 'FL', Goal_Year__c = year), false));

            lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='ETF', Fund_Vehicle_Type__c='ETF', Portfolio_Name__c = fundMFMix.Id, Channel__c = 'FA',Status__c = 'Active', Is_Organization_Type__c=true, IS_Territory__c = false
            , INSTSaleByOrgType__c = true, Goal_Year__c = year), false));
            lstConfigs.add((US_Sales_Goals_Tracker__c)SL_TestDataFactory.createSObject(
            new US_Sales_Goals_Tracker__c(Name='Mutual Fund', Fund_Vehicle_Type__c='Mutual Fund', Portfolio_Name__c = fundEme.Id, Channel__c = 'FA',Status__c = 'Active', Is_Organization_Type__c=true, IS_Territory__c = false 
            , INSTSaleByOrgType__c = true, Goal_Year__c = year), false));
            //
        if(!lstConfigs.isEmpty()){  
            insert lstConfigs;
        }

        //Create Account records
        Account objAcc = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 1', Channel__c = 'Financial Advisor', 
            																			// RecordTypeId = SalesConnectFirm,
                                                                                        BillingCountry='United States'), false);
        insert objAcc;
        
        //Insert ETF_Assets_By_Firm records
        List<ETF_Assets_By_Firm__c> lstETLAssets = new List<ETF_Assets_By_Firm__c>();     
        lstETLAssets.add((ETF_Assets_By_Firm__c)SL_TestDataFactory.createSObject(
            new ETF_Assets_By_Firm__c(Name='ANGL', Account__c=objAcc.Id,  Fund_YTM_NNA__c = 3500000,SFDCFundID__c=fundEme.Id,Asset_Date__c=Date.newInstance(2020, 01, 15)), false));
        lstETLAssets.add((ETF_Assets_By_Firm__c)SL_TestDataFactory.createSObject(
            new ETF_Assets_By_Firm__c(Name='EMLC', Account__c=objAcc.Id,  Fund_YTM_NNA__c = 4000000,SFDCFundID__c=fundEme.Id,Asset_Date__c=Date.newInstance(2020, 01, 15)), false));
        lstETLAssets.add((ETF_Assets_By_Firm__c)SL_TestDataFactory.createSObject(
            new ETF_Assets_By_Firm__c(Name='RAAX', Account__c=objAcc.Id,  Fund_YTM_NNA__c = 6000000,SFDCFundID__c=fundEme.Id,Asset_Date__c=Date.newInstance(2020, 01, 15)), false));
        lstETLAssets.add((ETF_Assets_By_Firm__c)SL_TestDataFactory.createSObject(
            new ETF_Assets_By_Firm__c(Name='MAAX', Account__c=objAcc.Id,  Fund_YTM_NNA__c = 6000000,SFDCFundID__c=fundEme.Id,Asset_Date__c=Date.newInstance(2020, 01, 15)), false));
        lstETLAssets.add((ETF_Assets_By_Firm__c)SL_TestDataFactory.createSObject(
            new ETF_Assets_By_Firm__c(Name='LFEQ', Account__c=objAcc.Id,  Fund_YTM_NNA__c = 6000000,SFDCFundID__c=fundEme.Id,Asset_Date__c=Date.newInstance(2020, 01, 15)), false));

        if(!lstETLAssets.isEmpty()){
            insert lstETLAssets;
        }

        //Insert YTDSalesPortfolioChannelRollUp records
        List<YTDSalesPortfolioChannelRollUp__c> lstChannelRollUps = new List<YTDSalesPortfolioChannelRollUp__c>();
        lstChannelRollUps.add((YTDSalesPortfolioChannelRollUp__c)SL_TestDataFactory.createSObject(
            new YTDSalesPortfolioChannelRollUp__c(Name='VanEck Vip Emerging Markets',Portfolio__c='VanEck Vip Emerging Markets',Channel__c='Insurance',ProcessedDate__c=Datetime.now()  ,Gross_Sales__c=350000,NetSalesCalculated__c=-520000), false));
        lstChannelRollUps.add((YTDSalesPortfolioChannelRollUp__c)SL_TestDataFactory.createSObject(
            new YTDSalesPortfolioChannelRollUp__c(Name='VanEck VIP Emerging Markets Fund',Portfolio__c='VanEck VIP Emerging Markets Fund',Channel__c='Insurance',ProcessedDate__c=Datetime.now(),Gross_Sales__c=1100000,NetSalesCalculated__c=-450000), false));    
        lstChannelRollUps.add((YTDSalesPortfolioChannelRollUp__c)SL_TestDataFactory.createSObject(
            new YTDSalesPortfolioChannelRollUp__c(Name='VanEck Vip Global Gold Fund',Portfolio__c='VanEck Vip Global Gold Fund',Channel__c='Insurance',ProcessedDate__c=Datetime.now()  ,Gross_Sales__c=1500000,NetSalesCalculated__c=760000), false));
        // lstChannelRollUps.add((YTDSalesPortfolioChannelRollUp__c)SL_TestDataFactory.createSObject(
            // new YTDSalesPortfolioChannelRollUp__c(Name='VanEck Emerging Markets Fund-Financial Advisor',Portfolio__c='VanEck Emerging Markets Fund',Channel__c='Financial Advisor',ProcessedDate__c=Datetime.now(),Gross_Sales__c=3400000,NetSalesCalculated__c=640000), false));     
           
            //
        List<YTDFocusFundsPortChannelRollup__c> lstFFPortChannelRollup = new List<YTDFocusFundsPortChannelRollup__c>();
        lstFFPortChannelRollup.add((YTDFocusFundsPortChannelRollup__c)SL_TestDataFactory.createSObject(
                new YTDFocusFundsPortChannelRollup__c(Name='a392K00000B1xwT',Portfolio__c='VanEck Emerging Markets Fund',Channel__c='RIA',ProcessedDate__c=Datetime.now()  ,Gross_Sales__c=350000,NetSalesCalculated__c=520000), false));
        lstFFPortChannelRollup.add((YTDFocusFundsPortChannelRollup__c)SL_TestDataFactory.createSObject(
                new YTDFocusFundsPortChannelRollup__c(Name='a392K00000B1xwU',Portfolio__c='VanEck Emerging Markets Fund',Channel__c='FA',ProcessedDate__c=Datetime.now()  ,Gross_Sales__c=550000,NetSalesCalculated__c=-580000), false));
        lstFFPortChannelRollup.add((YTDFocusFundsPortChannelRollup__c)SL_TestDataFactory.createSObject(
                new YTDFocusFundsPortChannelRollup__c(Name='a392K00000B1xwV',Portfolio__c='VanEck International Investors Gold',Channel__c='FA',ProcessedDate__c=Datetime.now()  ,Gross_Sales__c=1350000,NetSalesCalculated__c=9820000), false));

        if(!lstChannelRollUps.isEmpty()){
            insert lstChannelRollUps;
        }

        if(!lstFFPortChannelRollup.isEmpty()){
            insert lstFFPortChannelRollup; 
        }



        List<YTDDailySalesData__c> lstYTDDailySalesData = new List<YTDDailySalesData__c>();
        lstYTDDailySalesData.add((YTDDailySalesData__c)SL_TestDataFactory.createSObject(
                new YTDDailySalesData__c(AccountLookup__c=objAcc.Id,SFDC_FundID__c=fundEme.Id,Gross_Amount__c=1100100,PostingDate__c=Date.newInstance(2020, 01, 15)  ,Channel__c='Institutional',Name='Sale',Transaction_DST__c='a1u19000000WeLqAAK'), false));
        
        insert lstYTDDailySalesData;
    }

    /************************************************************************
    * Tests the scenario where gauge chart details return for given portfolio
    * 
    ************************************************************************/
    @isTest
    public static void  testGetUSSalesGoalsByPortfolio() {
        Integer iYear = Date.Today().Year();
        String year = String.valueOf(iYear);
        Test.startTest();
        // List<GaugeChartController.picklistOptions> resultMutualFunds = GaugeChartController.getFundNames(true);
        // List<GaugeChartController.picklistOptions> resultFunds = GaugeChartController.getFundNames(false);
        // List<GaugeChartController.picklistOptions> resultChannels = GaugeChartController.getChannelNames('EME-IIG');
        // List<GaugeChartController.picklistOptions> resultTerritory = GaugeChartController.getTerritoryNames('EME-IIG', 'Financial Advisor');
        // Date startDate = Date.parse('01/01/19');
        // Date endDate = Date.parse('05/30/19');
        // Map<String, US_Sales_Goals_Tracker__c>  resultFA = GaugeChartController.getCalculatedMFNetSales('EME-IIG', 'Financial Advisor','FL', '2019-01-01' ,'2019-05-01',true,true);
        // Map<String, US_Sales_Goals_Tracker__c>  resultRIA = GaugeChartController.getCalculatedMFNetSales('EME-IIG', 'RIA','RIA West', '2019-01-01' ,null,true,true);
        List<GaugeChartController.GaugeChartWrapper> result01 = GaugeChartController.getUSSalesGoalsByPortfolio('fund','EME-IIG','',false,false,false,false, year); 
        List<GaugeChartController.GaugeChartWrapper> result02 = GaugeChartController.getUSSalesGoalsByPortfolio('fund','ANGL','',false,false,false,false,year);
        List<GaugeChartController.GaugeChartWrapper> result03 = GaugeChartController.getUSSalesGoalsByPortfolio('fund','RAAX-MAAX-LFEQ','',false,false,false,false,year);
        List<GaugeChartController.GaugeChartWrapper> result04 = GaugeChartController.getUSSalesGoalsByPortfolio('fund','ANGL','Financial Advisor',true,false,false,false,year);
        List<GaugeChartController.GaugeChartWrapper> result05 = GaugeChartController.getUSSalesGoalsByPortfolio('fund','ANGL','Financial Advisor',false,true,false,false,year);
        List<GaugeChartController.GaugeChartWrapper> result06 = GaugeChartController.getUSSalesGoalsByPortfolio('fund','RAAX-MAAX-LFEQ','Financial Advisor',false,true,false,false,year);
        List<GaugeChartController.GaugeChartWrapper> result07 = GaugeChartController.getUSSalesGoalsByPortfolio('fund','EME-IIG','Financial Advisor',false,true,false,false,year);
        List<GaugeChartController.GaugeChartWrapper> result08 = GaugeChartController.getUSSalesGoalsByPortfolio('channel','','Financial Advisor',false,true,false,false,year);

        Map<String, US_Sales_Goals_Tracker__c> mapGoalsTrackers = new Map<String, US_Sales_Goals_Tracker__c>();
        List<US_Sales_Goals_Tracker__c> objs = [Select Id,Current_Net_Sales_for_ETF_Source__c from US_Sales_Goals_Tracker__c];
        mapGoalsTrackers.put('INSTITUTIONAL',objs[0]);
        mapGoalsTrackers.put('ASSET MANAGER',objs[1]);
        mapGoalsTrackers.put('RIA',objs[2]);
        mapGoalsTrackers.put('FINANCIAL ADVISOR',objs[3]);
        mapGoalsTrackers.put('PRIVATE BANK',objs[4]);
        mapGoalsTrackers.put('Firm',objs[5]);
        GaugeChartController.calculateETFChannelWise('RAAX-MAAX-LFEQ','ChanelWise',mapGoalsTrackers,false,false,null,Date.newInstance(2020, 01, 15) , false ,'Firm_Focus_Fund');
        // List<GaugeChartController.GaugeChartWrapper> result10 = GaugeChartController.getUSSalesGoalsByPortfolio('fund','ANGL','PRIVATE BANK',false,true,false,false,'2021');
        // List<GaugeChartController.GaugeChartWrapper> result11 = GaugeChartController.getUSSalesGoalsByPortfolio('fund','RAAX-MAAX-LFEQ','Financial Advisor',false,true,false,false,'2021');
        
        // Boolean result09 = GaugeChartController.calculateOrgTypeNetFlowFF('Mutual Fund','Institutional', '2020-01-01' , '2020-01-31',null,null);
        // Boolean result10 = GaugeChartController.calculateOrgTypeNetFlowFF('ETF','Institutional', '2020-01-01' , null,null,null);

        // Map<String,Object> result11 = GaugeChartController.getOrgTypeConfig('ETF');
        // Map<String,Object> result12 = GaugeChartController.getOrgTypeConfig('Mutual Fund');
        

        // List<US_Sales_Goals_Tracker__c> result13 = GaugeChartController.getConfigRecordForDrillDown('Mutual Fund','Financial Advisor' ,'2020-01-30','2020-01-01','2020-01-31');
        
        US_Sales_Goals_Tracker__c tracker = [SELECT Id FROM US_Sales_Goals_Tracker__c limit 1];
        // List<Organization_Type_Firm_Drill_Down__c> result14 = GaugeChartController.getDrillDownRecords(tracker.Id);
        // Map<String,List<GaugeChartController.picklistOptions>> result15 = GaugeChartController.getFirmNames('Institutional', '2020-01-01','2020-05-01');
        
        Boolean result16 = GaugeChartController.setDataVisualType('Table');
        String result17 = GaugeChartController.getDataVisualType();
        Test.stopTest();

        AggregateResult[] aggResultEMEAll = [SELECT SUM(NetSalesCalculated__c) FROM YTDSalesPortfolioChannelRollUp__c WHERE portfolio__c='VanEck Emerging Markets Fund' GROUP BY Portfolio__c];
        decimal totalEMEFirm = 0;
        for (AggregateResult ar : aggResultEMEAll)  { 
            totalEMEFirm = (decimal)ar.get('expr0');
        }

        AggregateResult[] aggResultEMEInst = [SELECT SUM(NetSalesCalculated__c) FROM YTDSalesPortfolioChannelRollUp__c WHERE portfolio__c='VanEck Emerging Markets Fund' AND channel__c = 'Institutional' GROUP BY Portfolio__c];
        decimal totalEMEInst = 0;
        for (AggregateResult ar : aggResultEMEInst)  {
            totalEMEInst = (decimal)ar.get('expr0');
        }

        AggregateResult[] aggResultEMECount = [SELECT COUNT(Id) FROM US_Sales_Goals_Tracker__c WHERE Fund_Ticker_Symbol__c='EME' AND Status__c = 'Active' AND Is_Organization_Type__c=false AND IS_Territory__c=false];
        decimal countEME = 0;
        for (AggregateResult ar : aggResultEMECount)  {
            countEME = (decimal)ar.get('expr0');
        }

        System.assert(result01 != null);
        System.assert(result02 != null);
        System.assert(result03 != null);
        System.assert(result04 != null);
        System.assert(result05 != null);
        System.assert(result06 != null);

        // system.assertEquals(result01.size(),countEME);
        // for(GaugeChartController.GaugeChartWrapper wrapper : result01){
        //     if(wrapper.channel == 'Firm'){
        //         system.assertEquals(wrapper.netSalesCalculated,totalEMEFirm);
        //     }else if(wrapper.channel == 'Institutional'){
        //         system.assertEquals(wrapper.netSalesCalculated,totalEMEInst);
        //     } 
        // }
    }

}