/**
    Author: Vidhya Krishnan
    Date Created: 1/26/2012
    Description: Class to test Dynamic Eligibility Concept
 */
@isTest
private class DynamicEligibilityTest {
    
   
    @isTest
    static void myUnitTest()
    {   
        Test.startTest();
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
        
        DynamicEligibility de = new DynamicEligibility();
        
        Account acc = new Account(Name = 'kj b gfg', Channel__c = 'Insurance', BillingCountry = 'US');
        sObject accInserted = SL_TestDataFactory.createSObject(acc,true);
        
        Account acc1 = new Account(Name = 'lklk6 jhjhj8', Channel__c = '401 K', BillingCountry = 'US');
        sObject acc1Inserted = SL_TestDataFactory.createSObject(acc1,true);        
        Account acc2 = new Account(Name = 'mhgdjhjhjhj987', Channel__c = 'Financial Advisor', BillingCountry = 'US');
        sObject acc2Inserted = SL_TestDataFactory.createSObject(acc2,true);
        
        Community_Registrant__c cr1 = new Community_Registrant__c();
        cr1.User_Name__c = 'test1'; cr1.Email__c = 'test1@test.com'; cr1.First_Name__c = 'test1';
        cr1.Last_Name__c = 'test1'; cr1.Company__c = 'test1';cr1.Address1__c = 'test1'; cr1.City__c = 'test';
        cr1.State__c = 'test'; cr1.Country__c = 'test'; cr1.Zip_Code__c = 'test'; cr1.Password__c = 'test';
        sObject cr1Inserted = SL_TestDataFactory.createSObject(cr1,true);
        
        Contact con = new Contact(LastName='Test', AccountId=acc.Id, Email='test@abc.com',DB_Source__c = 'MVIS', MailingCountry='USA');
        Contact con1 = new Contact(LastName='Test1', AccountId=acc.Id, Email='test1@abc.com',Service_Level__c='Super Senior', MailingCountry='USA');
        Contact con2 = new Contact(LastName='Test2', AccountId=acc1.Id, Email='test2@abc.com',DB_Source__c = 'ABC', MailingCountry='USA');
        Contact con2a = new Contact(LastName='Test2', AccountId=acc1.Id, Email='test2@abc.com', MailingCountry='USA');
        Contact con2b = new Contact(LastName='Test2', AccountId=acc2.Id, Email='test2@abc.com', MailingCountry='USA');
        Contact con3 = new Contact(LastName='Test1', AccountId=acc.Id, Email='test3@abc.com', MailingCountry='Norway');
        Contact con3a = new Contact(LastName='Test1', AccountId=acc.Id, Email='test3@abc.com', MailingCountry='USA');
        Contact con4 = new Contact(LastName='Test1', AccountId=acc1.Id, Email='test4@abc.com',DB_Source__c = 'ABC', Service_Level__c='ETF Trader', MailingCountry='US');      
        List<Contact> conlist = new List<Contact>();
        conlist.add(con);
        conList.add(con1);
        conList.add(con2);
        conList.add(con2a);
        conList.add(con2b);
        conList.add(con3);
        conList.add(con3a);
        conList.add(con4);
        insert conList;
        
        Subscription__c sub = new Subscription__c(Name='Test Sub', Subscription_Display_Name__c='Test Sub');
        sObject subInserted = SL_TestDataFactory.createSObject(sub,true);
        
        Subscription__c sub1 = new Subscription__c(Name='Test Sub1', Subscription_Display_Name__c='Test Sub1');
        sObject sub1Inserted = SL_TestDataFactory.createSObject(sub1,true);
        
        Subscription_Member__c sm1 = new Subscription_Member__c(Subscription__c=sub.Id, Contact__c=conList[0].Id, Email__c='test@abc.com', Subscribed__c=true);
        sObject sm1Inserted = SL_TestDataFactory.createSObject(sm1,true);
        
        Subscription_Member__c sm2 = new Subscription_Member__c(Subscription__c=sub1.Id, Contact__c=conList[0].Id, Email__c='test1@abc.com', Subscribed__c=true);
        sObject sm2Inserted = SL_TestDataFactory.createSObject(sm2,true);
        
        sm2.Subscription_Unsubscribed_Date__c = system.today(); sm2.Subscribed__c=false;
        update sm2;
        
        DynamicEligibility.handleEligibility(conlist,'insert');
        
        con2a.AccountId = acc2.Id;
        con2a.Community_Registrant__c = cr1.Id;
        update con2a;        
        
        conList.add(con2a);
        //conList.add(con2b);        
        DynamicEligibility.handleEligibility(conlist, 'update');
        acc.Channel__c = 'RIA';
        update acc;        
        
        DynamicEligibility.handleEligibility(conlist, 'account');
        
        DynamicEligibility.getEligibility(conList);
        
        Test.stopTest();
    }
    
    @isTest
    static void sfdcTest()
    {
    	Test.startTest();
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
            sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
        DynamicEligibility de = new DynamicEligibility();
        Account acc = new Account(Name = 'kjfbnccggfg', Channel__c = 'Insurance', BillingCountry = 'US');
        sObject accInserted = SL_TestDataFactory.createSObject(acc,true);
        

        Contact con = new Contact(LastName='Test12', AccountId=acc.Id, Email='test12@abc.com', MailingCountry='USA');
        List<Contact> conlist = new List<Contact>();
        conlist.add(con);
        insert conList;        
        
        Subscription__c sub = new Subscription__c(Name='Test Sub', Subscription_Display_Name__c='Test Sub');
        sObject subInserted = SL_TestDataFactory.createSObject(sub,true);
        
        Subscription_Member__c sm1 = new Subscription_Member__c(Subscription__c=sub.Id, Contact__c=conList[0].Id, Email__c='test@abc.com', Subscribed__c=true);
        sObject sm1Inserted = SL_TestDataFactory.createSObject(sm1,true);
        
        Map<Id,Id> conSubGrpMap=new Map<Id,Id>();
        conSubGrpMap.put(acc.Id,con.id);
        DynamicEligibility.updateEligibility(conSubGrpMap, 'abcd');
        
        Test.stopTest();
    }
    
    
    
    
    
    @isTest
    static void scenario1()
    {
      List<Contact> conList = new List<Contact>();
    	Test.startTest();
    	
    	User objUser1 = [Select Id from User where Profile.NAme = 'ETF-US Sales' and isActive=true limit 1];
    	User objUser2 = [Select Id from User where Profile.NAme = 'Institutional US User' and isActive=true limit 1];
    	
    	Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', 
    																Country_Filter__c = 'IN',Channel_Filter__c = 'test1', 
    																DB_Source_Inc_Filter__c = 'MVIS1,MVIS2',Profile_Channel_Filter__c='testchannel',
    																IsActive__c= true);
      sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
      
      
      Account acc3 = new Account(Name = 'firm 1', Channel__c = 'Insurance', BillingCountry = 'US');
      sObject accInserted3 = SL_TestDataFactory.createSObject(acc3,true);
      
      Contact con3 = new Contact(LastName='Test', AccountId=acc3.Id, Email='test@abc.com',DB_Source__c = 'MVIS', Service_Level__c= 'Test', MailingCountry='US', OwnerId=objUser2.Id);
      sObject conInserted3 = SL_TestDataFactory.createSObject(con3,true); 
        
     	DynamicEligibility de = new DynamicEligibility();
     	
      Subscription_Group__c sg2 =new Subscription_Group__c();
      sg2.Name='test';
      sg2.Channel_Filter__c='filter';
      sg2.Firm_Name_Filter__c='firm';
      sg2.KASL_Filter__c='Test';
      sg2.DB_Source_Inc_Filter__c='';
      sg2.DB_Source_Exc_Filter__c='MVIS,testingfilter';
      sg2.Owner_Profile_Filter__c='testprofile';
      sg2.Profile_Channel_Filter__c='testchannel';
      sg2.Country_Filter__c='US,IN';
      //sg.Eligibility_Sort_Order__c=3;
      sg2.IsActive__c=true;
        
     	List<Subscription_Group__c> scg=new List<Subscription_Group__c>();
        
      scg.add(sg2);
      
      insert scg;
      
      conList.add(con3); 
      
      DynamicEligibility.getEligibility(conList);
        
      Test.stopTest();
    
    }
    
    
    @isTest
    static void scenario2()
    {
      List<Contact> conList = new List<Contact>();
    	
    	Test.startTest();
    	
    	User objUser1 = [Select Id from User where Profile.NAme = 'ETF-US Sales' and isActive=true limit 1];
    	User objUser2 = [Select Id from User where Profile.NAme = 'Institutional US User' and isActive=true limit 1];
    	
    	Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', 
    																Country_Filter__c = 'IN',Channel_Filter__c = 'test1', 
    																DB_Source_Inc_Filter__c = 'MVIS1,MVIS2',Profile_Channel_Filter__c='testchannel',
    																IsActive__c= true);
      sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
        
        
      Account acc3 = new Account(Name = 'firm 1', Channel__c = 'Insurance', BillingCountry = 'US');
      sObject accInserted3 = SL_TestDataFactory.createSObject(acc3,true);
        
      Contact con3 = new Contact(LastName='Test', AccountId=acc3.Id, Email='test@abc.com',DB_Source__c = 'MVIS', Service_Level__c= 'Test', MailingCountry='US', OwnerId=objUser2.Id);
      sObject conInserted3 = SL_TestDataFactory.createSObject(con3,true); 
        
      DynamicEligibility de = new DynamicEligibility();
     	
      Subscription_Group__c sg3 =new Subscription_Group__c();
      sg3.Name='test';
      sg3.Channel_Filter__c='filter';
      sg3.Firm_Name_Filter__c='firm';
      sg3.KASL_Filter__c='Test1';
      sg3.DB_Source_Inc_Filter__c='';
      sg3.DB_Source_Exc_Filter__c='MVIS,testingfilter';
      sg3.Owner_Profile_Filter__c='testprofile';
      sg3.Profile_Channel_Filter__c='testchannel';
      sg3.Country_Filter__c='US,IN';
      //sg.Eligibility_Sort_Order__c=3;
      sg3.IsActive__c=true;
    
     	List<Subscription_Group__c> scg=new List<Subscription_Group__c>();
        
      //scg.add(sg);
      scg.add(sg3);
      
      insert scg;
      //system.debug('test for status' + sg.IsActive__c);
        
      conList.add(con3);
        
      DynamicEligibility.getEligibility(conList);
        
      Test.stopTest();
    
    }
    
    
    @isTest
    static void scenario3()
    {
      List<Contact> conList = new List<Contact>();
    	Test.startTest();
    	
    	User objUser1 = [Select Id from User where Profile.NAme = 'ETF-US Sales' and isActive=true limit 1];
    	User objUser2 = [Select Id from User where Profile.NAme = 'Institutional US User' and isActive=true limit 1];
    	
    	Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', 
    																Country_Filter__c = 'IN',Channel_Filter__c = 'test1', 
    																DB_Source_Inc_Filter__c = '',Profile_Channel_Filter__c='testchannel',
    																IsActive__c= true);
      sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
      
      
      Account acc2 = new Account(Name = 'firm 1', Channel__c = 'Insurance', BillingCountry = 'US');
      sObject accInserted2 = SL_TestDataFactory.createSObject(acc2,true);
      
      Contact con2 = new Contact(LastName='Test', AccountId=acc2.Id, Email='test@abc.com',DB_Source__c = 'MVIS', MailingCountry='US', OwnerId=objUser2.Id);
      sObject conInserted2 = SL_TestDataFactory.createSObject(con2,true); 
        
     	DynamicEligibility de = new DynamicEligibility();
     	
      Subscription_Group__c sg=new Subscription_Group__c();
      sg.Name='test';
      sg.Channel_Filter__c='filter';
      sg.Firm_Name_Filter__c='firm';
      sg.KASL_Filter__c='testKasl';
      sg.DB_Source_Inc_Filter__c='';
      sg.DB_Source_Exc_Filter__c='MVIS,testingfilter';
      sg.Owner_Profile_Filter__c='testprofile';
      sg.Profile_Channel_Filter__c='testchannel';
      sg.Country_Filter__c='US,IN';
      sg.Eligibility_Sort_Order__c=1;
      sg.IsActive__c=true;
        
     	List<Subscription_Group__c> scg=new List<Subscription_Group__c>();
        
      scg.add(sg);
      
      insert scg;
      
      conList.add(con2);
        
      DynamicEligibility.getEligibility(conList);
      
      Test.stopTest();
    
    }
    
    @isTest
    static void scenario4()
    {
    	List<Contact> conList = new List<Contact>();
    	Test.startTest();
    	
    	User objUser1 = [Select Id from User where Profile.NAme = 'ETF-US Sales' and isActive=true limit 1];
    	User objUser2 = [Select Id from User where Profile.NAme = 'Institutional US User' and isActive=true limit 1];
    	
    	Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', 
    																Country_Filter__c = 'IN',Channel_Filter__c = 'test1', 
    																DB_Source_Inc_Filter__c = 'MVIS1,MVIS2',Profile_Channel_Filter__c='testchannel',
    																IsActive__c= true);
      sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
        
        
     	Account acc = new Account(Name = 'firm', Channel__c = 'Insurance', BillingCountry = 'US');
      sObject accInserted = SL_TestDataFactory.createSObject(acc,true);
      
      Contact con = new Contact(LastName='Test', AccountId=acc.Id, Email='test@abc.com',DB_Source__c = 'MVIS', MailingCountry='US', OwnerId=objUser1.Id);
      sObject conInserted = SL_TestDataFactory.createSObject(con,true); 
      
      Account acc2 = new Account(Name = 'firm 1', Channel__c = 'Insurance', BillingCountry = 'US');
      sObject accInserted2 = SL_TestDataFactory.createSObject(acc2,true);
      
      Contact con2 = new Contact(LastName='Test', AccountId=acc2.Id, Email='test@abc.com',DB_Source__c = 'MVIS', MailingCountry='US', OwnerId=objUser2.Id);
      sObject conInserted2 = SL_TestDataFactory.createSObject(con2,true); 
        
     	DynamicEligibility de = new DynamicEligibility();
     	
      Subscription_Group__c sg=new Subscription_Group__c();
      sg.Name='test';
      sg.Channel_Filter__c='filter';
      sg.Firm_Name_Filter__c='firm';
      sg.KASL_Filter__c='testKasl';
      sg.DB_Source_Inc_Filter__c='MVIS';
      sg.DB_Source_Exc_Filter__c='MVIS,testingfilter';
      sg.Owner_Profile_Filter__c='System Admin';
      sg.Profile_Channel_Filter__c='ETF-US Sales, Institutional US User, System Admin';
      sg.Country_Filter__c='US,IN';
      sg.Eligibility_Sort_Order__c=1;
      sg.IsActive__c=true;
      
      Subscription_Group__c sg1=new Subscription_Group__c();
      sg1.Name='test';
      sg1.Channel_Filter__c='Insurance';
      sg1.Firm_Name_Filter__c='';
      sg1.KASL_Filter__c='';
      sg1.DB_Source_Inc_Filter__c='';
      sg1.DB_Source_Exc_Filter__c='MVIS,testingfilter';
      sg1.Owner_Profile_Filter__c='System Admin';
      sg1.Profile_Channel_Filter__c='ETF-US Sales, Institutional US User, System Admin';
      sg1.Country_Filter__c='US,IN';
      sg.Eligibility_Sort_Order__c=2;
      sg1.IsActive__c=true;
        
     	List<Subscription_Group__c> scg=new List<Subscription_Group__c>();
        
      scg.add(sg);
      scg.add(sg1);
      
      insert scg;
        
      conList.add(con);
      conList.add(con2);
        
      DynamicEligibility.getEligibility(conList);
        
      Test.stopTest();
    }
    

    @isTest
    static void scenario()
    {
    	
    	Test.startTest();
    	
    	List<Contact> conList = new List<Contact>();
    	
    	User objUser1 = [Select Id, ProfileId from User where Profile.NAme = 'ETF-US Sales' and isActive=true limit 1];
    	User objUser2 = [Select Id, ProfileId from User where Profile.NAme = 'Institutional US User' and isActive=true limit 1];
    	
    	Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', 
    																Country_Filter__c = 'IN',Channel_Filter__c = 'test1', 
    																DB_Source_Inc_Filter__c = 'MVIS1,MVIS2',Profile_Channel_Filter__c='testchannel',
    																IsActive__c= true);
      sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
      
      
      Community_Registrant__c cr1 = new Community_Registrant__c();
      cr1.User_Name__c = 'test1'; cr1.Email__c = 'test1@test.com'; cr1.First_Name__c = 'test1';
      cr1.Last_Name__c = 'test1'; cr1.Company__c = 'test1';cr1.Address1__c = 'test1'; cr1.City__c = 'test';
      cr1.State__c = 'test'; cr1.Country__c = 'test'; cr1.Zip_Code__c = 'test'; cr1.Password__c = 'test';
      sObject cr1Inserted = SL_TestDataFactory.createSObject(cr1,true);
        
     	Account acc = new Account(Name = 'firm', Channel__c = 'Insurance', BillingCountry = 'US');
      sObject accInserted = SL_TestDataFactory.createSObject(acc,true);
      
      Account acc2 = new Account(Name = 'firm 1', Channel__c = 'Insurance', BillingCountry = 'US');
      sObject accInserted2 = SL_TestDataFactory.createSObject(acc2,true);

      Contact con = new Contact(LastName='Test', AccountId=acc.Id, Email='test@abc.com',DB_Source__c = 'MVIS', Community_Registrant__c = cr1.Id, MailingCountry='US', OwnerId=objUser1.Id);
      sObject conInserted = SL_TestDataFactory.createSObject(con,true); 
      
      Contact con2 = new Contact(LastName='Test', AccountId=acc2.Id, Email='test@abc.com',DB_Source__c = 'MVIS', Community_Registrant__c = cr1.Id, MailingCountry='US', OwnerId=objUser2.Id);
      sObject conInserted2 = SL_TestDataFactory.createSObject(con2,true); 
        
     	DynamicEligibility de = new DynamicEligibility();
     	
      Subscription_Group__c sg=new Subscription_Group__c();
      sg.Name='test';
      sg.Channel_Filter__c='Insurance';
      sg.Firm_Name_Filter__c='firm';
      sg.KASL_Filter__c='testKasl';
      sg.DB_Source_Inc_Filter__c='MVIS';
      sg.DB_Source_Exc_Filter__c='MVIS,testingfilter';
      sg.Owner_Profile_Filter__c='System Admin';
      sg.Profile_Channel_Filter__c= objUser1.ProfileId + ',' + objUser2.ProfileId+ ',' + UserInfo.getUserId();
      sg.Country_Filter__c='US,IN';
      sg.Eligibility_Sort_Order__c=1;
      sg.IsActive__c=true;
        
      Subscription_Group__c sg1=new Subscription_Group__c();
      sg1.Name='test';
      sg1.Channel_Filter__c='Insurance';
      sg1.Firm_Name_Filter__c='';
      sg1.KASL_Filter__c='';
      sg1.DB_Source_Inc_Filter__c='';
      sg1.DB_Source_Exc_Filter__c='MVIS,testingfilter';
      sg1.Owner_Profile_Filter__c='System Admin';
      sg1.Profile_Channel_Filter__c=objUser1.ProfileId + ',' + objUser2.ProfileId+ ',' + UserInfo.getUserId();
      sg1.Country_Filter__c='US,IN';
      sg.Eligibility_Sort_Order__c=2;
      sg1.IsActive__c=true;
        
     	List<Subscription_Group__c> scg=new List<Subscription_Group__c>();
        
      scg.add(sg);
      scg.add(sg1);
      
      insert scg;
      conList.addAll([Select Id, LastName, AccountId, Channel_New__c, 
                        Email,DB_Source__c, Community_Registrant__c, MailingCountry, 
                        OwnerId , Service_Level__c
                        from Contact where Id=: con.Id OR Id =:con2.Id]);
      
      DynamicEligibility.getEligibility(conList);
               
      //DynamicEligibility.handleEligibility(conlist, 'update');
      
      Test.stopTest();
    }
    
    
}