public class BatchAccountTerritoryValueGen implements Database.Batchable<sObject>, Database.Stateful{
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String strQuery = 'SELECT AccountId FROM AccountShare WHERE LastModifiedDate = today AND RowCause = \'Territory\' order by AccountId';
        return Database.getQueryLocator(strQuery);
    } 

    public void execute(Database.BatchableContext bc, List<AccountShare> records) {
        Set<Id> setAllAccountIds = new Set<Id>();
        for(AccountShare accShare: records){
            setAllAccountIds.add(accShare.AccountId);
        }
        List<Account> lstAccount = [SELECT Id FROM Account WHERE Id IN :setAllAccountIds AND Apply_Territory_Rules__c = true ];
        if(!lstAccount.isEmpty()){
            Set<Id> setSelectedAccIds = (new Map<Id,SObject>(lstAccount)).keySet();
            System.debug('No of accounts going to update : '+setSelectedAccIds.size());
            AccountTriggerHandler.updateTerritoryDataOnAccounts(setSelectedAccIds);
        }
        List<Account> lstAccountsRemove = [SELECT Id FROM Account WHERE Id IN :setAllAccountIds AND Apply_Territory_Rules__c = false ];
        if(!lstAccountsRemove.isEmpty()){
            Set<Id> setSelectedAccIds = (new Map<Id,SObject>(lstAccountsRemove)).keySet();
            System.debug('No of accounts going to remove territory info : '+setSelectedAccIds.size());
            AccountTriggerHandler.removeTerritoryDataOnAccounts(setSelectedAccIds);
        }
    }

    public void finish(Database.BatchableContext bc) {
        System.debug('******** BatchAccountTerritoryValueGen Completed *********');
    }
}