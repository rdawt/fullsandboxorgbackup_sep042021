/**************************************************************************************
* @Author: Tharaka De Silva
* @Date: 2021-06-30
*
* This class implement the Schedulable interface in the purpose of calling BatchContactTerritoryRuleGen.
***************************************************************************************/
public with sharing class BatchScheduleContactTerritoryRuleGen implements Schedulable {
  
    public void execute(SchedulableContext sc)
    {
        BatchContactTerritoryRuleGen batch = new BatchContactTerritoryRuleGen(); 
        database.executebatch(batch,200);
    }
}