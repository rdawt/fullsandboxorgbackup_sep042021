global class BatchContactUpdateForActivityReminder implements Database.Batchable < sObject >{

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //Nicholas Frasse - 005A0000007t1KOIAY
        //Maryrose Burns - 005A0000006RYNcIAO 
        String query = 'Select Id,name,email from Contact Where Contact_Status__c != \'DELETE\' AND MailingCountry = \'United States\' and SalesConnect__Rep_Type__c != \'partnership\' and DB_Source__c != \'MVIS Registrant\' and IsTestContact__c != true and Account.ID_Status2__c !=\'DELETE\' AND  OwnerId In (\'005A0000007t1KOIAY\',\'005A0000006RYNcIAO\') ' ;
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List <contact> scope)
    {
        //Create a new list of Contacts for updating;
        list<Contact> conList= new list<Contact>();
        string loopContactId;
        try{
            Map<Id, EventWhoRelation> eventWhoRelationMap = new Map<Id, EventWhoRelation>();
              
            for(EventWhoRelation ewrForMap : [Select Id, Event.ID,Event.ActivityDate,Event.whoid,relationid,eventid,event.subject,Event.Description From EventWhoRelation Where Event.whoid != null and relationid IN :scope ORDER BY event.activitydate desc])
            {   
                if(!eventWhoRelationMap.containsKey(ewrForMap.relationid)){
                    eventWhoRelationMap.put(ewrForMap.relationid, ewrForMap);
                }
                else{
                    // system.debug('**outside-ewrforMap------Activity Date' +ewrForMap.event.activitydate+' '+'Reminder : Contact Id with max event date '+ ewrForMap.relationid+' @@ '+contact.email+'Event Id-->>'+ewrForMap.Event.ID);
                }
            }
            Map<Id, TaskWhoRelation> taskWhoRelationMap = new Map<Id, TaskWhoRelation>();
              
            for(TaskWhoRelation twrforMap : [Select Id, Task.ID,Task.ActivityDate,Task.whoid,relationid,taskid, task.type,Task.Subject,Task.Description From taskWhoRelation Where   (task.subject like '%social%'  Or task.subject like '%Webinar%'  Or task.subject like '%conference%'  Or task.subject ='Call' or task.subject like 'email%') and task.whoid != null and relationid = :scope ORDER BY task.activitydate desc])
            {
                if(!taskWhoRelationMap.containsKey(twrforMap.relationid)){
                    taskWhoRelationMap.put(twrforMap.relationid, twrforMap);
                    // system.debug('**Inside-twrforMap------Activity Date' +twrForMap.task.activitydate+' '+'Reminder : Contact Id with max task date '+twrForMap.relationid+' @@ '+contact.email+'Task Id-->>'+twrForMap.Task.ID);
                }
                else{
                    // system.debug('**Outside-twrforMap------Activity Date' +twrForMap.task.activitydate+' '+'Reminder : Contact Id with max task date '+twrForMap.relationid+' @@ '+contact.email+'Task Id-->>'+twrForMap.Task.ID);
                }
            }
           
            Date today = System.today();
            Date dateMargin = today.addDays(-90);
            try{
                for(Contact contact: scope)
                {
                    System.debug(contact);
                    date loopLastHumanTouchDateEvent;
                    date loopLastHumanTouchDateTask;
                    string loopWhoId;
                
                    Contact activityContactToBeUpdated;
                    loopContactId =  string.valueof(contact.get('Id'));
                    List<EventWhoRelation> ContIdWithEventMAxDateList;
                    List<TaskWhoRelation> ContIdWithTaskMAxDateList;
                    string eventActivityType;
                    string taskActivityType;
                    string eventDescription;
                    string taskDescription;
                    //map check for test
                    if (eventWhoRelationMap.size() > 0 && eventWhoRelationMap.containsKey(contact.id)){
                        EventWhoRelation ewr = eventWhoRelationMap.get(contact.id);
                        loopLastHumanTouchDateEvent =  ewr.Event.activitydate;
                        eventActivityType = 'Reminder : ' + ewr.event.subject+ ' EventID-->> ' + ewr.event.id;
                        eventDescription = ewr.Event.Description;
                    }
                    else{
                        // system.debug('**In Else where there is no matching Event for-Contact in For Loop for Event------'+' '+loopContactId+' @@ '+contact.id);
                    }
                    
                    if (taskWhoRelationMap.size() > 0 && taskWhoRelationMap.containsKey(contact.id)){
                        taskWhoRelation twr = taskWhoRelationMap.get(contact.id);
                        loopLastHumanTouchDateTask =  twr.Task.activitydate;
                        taskActivityType = 'Reminder : ' +  twr.task.subject + ' TaskID-->> ' + twr.task.id;
                        taskDescription = twr.Task.Description;
                    }
                    if ((loopLastHumanTouchDateEvent != null) && (loopLastHumanTouchDateTask != null)){
                        if(loopLastHumanTouchDateEvent > dateMargin || loopLastHumanTouchDateTask > dateMargin){
                        
                        }//Event Activity Date is greater than Task Activity Date
                        else if (loopLastHumanTouchDateEvent >= loopLastHumanTouchDateTask)  {
                            if(eventDescription != null && eventDescription.length() > 50){
                                activityContactToBeUpdated = new contact(Id = loopContactId, CSMDate__c = loopLastHumanTouchDateEvent, CSMActivityType__c=  eventActivityType); //Reached_Date__c, Reached_Activity_Detail__c
                            }
                        }
                        else if (loopLastHumanTouchDateTask >= loopLastHumanTouchDateEvent){
                            if(taskDescription != null && taskDescription.length() > 50){
                                activityContactToBeUpdated = new contact(Id = loopContactId, CSMDate__c = loopLastHumanTouchDateTask, CSMActivityType__c=  taskActivityType); 
                            }
                        }
                    }

                    else if ((loopLastHumanTouchDateEvent != null) && (loopLastHumanTouchDateTask == null)) {
                        if(loopLastHumanTouchDateEvent < dateMargin){
                            if(eventDescription != null && eventDescription.length() > 50){
                                activityContactToBeUpdated = new contact(Id = loopContactId, CSMDate__c = loopLastHumanTouchDateEvent, CSMActivityType__c=  eventActivityType); //Reached_Date__c, Reached_Activity_Detail__c
                            }
                        }
                    }
                    //make it task date as the last successful human touch date
                    else if ((loopLastHumanTouchDateEvent == null) && (loopLastHumanTouchDateTask != null)) {
                    if(loopLastHumanTouchDateTask < dateMargin){   
                        if(taskDescription != null && taskDescription.length() > 50){
                                activityContactToBeUpdated = new contact(Id = loopContactId, CSMDate__c = loopLastHumanTouchDateTask, CSMActivityType__c=  taskActivityType); 
                            }
                        }
                    }
                    //both Event and Task Dates are null so never bother to insert that contact with any Last Successful Human Touch Date
                    else if ((loopLastHumanTouchDateEvent == null) && (loopLastHumanTouchDateTask == null)) {
                        //no contact is added for update to the conList
                    }
                    if (activityContactToBeUpdated != null){
                        conList.add(activityContactToBeUpdated);
                        activityContactToBeUpdated = null;
                    } 
                } //end of for loop for 200 records
                //update contact outside the for loop
                try{
                    if (conList != null){
                        system.debug('*After For Loop Execute Scope inside conlist update**   --> : '+ conList);
                        update conList;
                    }
                    else{
                        system.debug('*Empty ConList for update in this batch!!***->>After For Loop Execute Scope inside conlist update**   --> : '+ conList);
                    }
                    }catch (Exception e)
                        {
                            // string error = 'Exception occured in  Batch Apex failed while updating the conList with the contact to update the date..... '+e.getMessage()+conList;
                            system.debug('error catch at the exeption level-error catch bloc at the update contact statement ------Exception occured in  Batch Apex failed while updating the conList with the contact to update the date..... '+e.getMessage()+conList);
                            SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Batch Update Of Last Human Interation Date processing module');
                            // String    msg = 'Exception Occurred in Apex while writing to LastHumanInteraction Date update(updateconList) **in UPDATE Statment** in Contact \n '+e.getMessage() + ' ' +conList;
                            se.sendEmail('Exception Occurred in Apex while writing to LastHumanInteraction Date update(updateconList) **in UPDATE Statment** in Contact \n '+e.getMessage() + ' ' +conList);
                        }
                } catch (Exception e) {
                    // string error = 'Exception occured in  Batch Apex failed while processing the loop in Events or sObjects..... '+e.getMessage()+conList;
                    system.debug('error catch at the exeption level-error catch bloc ------Exception occured in  Batch Apex failed while processing the loop in Events or sObjects..... '+e.getMessage()+conList);
                    SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Batch Update Of Last Human Interation Date processing module');
                    // String    msg = 'Exception Occurred in Apex while writing to LastHumanInteraction Date update in Contact-in For LOOOP level \n '+e.getMessage() + ' ' +loopContactId;
                    se.sendEmail( 'Exception Occurred in Apex while writing to LastHumanInteraction Date update in Contact-in For LOOOP level \n '+e.getMessage() + ' ' +loopContactId);
                    }
        }catch (Exception e) {
        //    string error = 'Exception occured in  Batch Apex failed while processing the try catch section of Map SOQL..... '+e.getMessage()+scope;
           system.debug('error catch at the exeption level-error catch bloc of the map load from original call in the beginning of Execution ------Exception occured in  Batch Apex failed while processing the try catch section of Map SOQL..... '+e.getMessage()+scope);
           SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Batch Update Of Last Contact activity using MAPS processing module');
        //    String    msg = 'Exception Occurred in Apex while writing to Last Contact Date update in Contact- outside of LOOP level in class using MAPS  \n '+e.getMessage() + ' ' +loopContactId;
           se.sendEmail('Exception Occurred in Apex while writing to Last Contact Date update in Contact- outside of LOOP level in class using MAPS  \n '+e.getMessage() + ' ' +loopContactId); 
        }   
     }       
          
    global void finish(Database.BatchableContext BC) {

    }
}