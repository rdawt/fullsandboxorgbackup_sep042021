/*
    Class Name  :   SL_BusinessMixManagement_Test
    JIRA Ticket :   VANECK-37
    Description :   Test class for SL_BusinessMixManagement_Controller
*/
@isTest
public class SL_BusinessMixManagement_Test {
    /*
        Method Name : setUp()
        Description : testsetup method to create test data
    */
    @TestSetup
    private static void setUp(){
        User sysAdminUserObj = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'AdminUser',
            Email = 'AdminUser@admin.com',
            Username = 'SalesforceUser11@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        
        System.runAs(sysAdminUserObj){
            SL_TestDataFactory.createTestDataForContact(); 
            Contact conObj = [SELECT Id FROM Contact WHERE LastName='Contact'];
            Business_Mix__c busMixObj = (Business_Mix__c)SL_TestDataFactory.createSObject(new Business_Mix__c(Contact__c=conObj.Id,
                                                                                                      Business_Mix__c='MF',
                                                                                                      Comments__c='Testing'),true);
        } 
    }
    
    /*
        Testing method getBusinessMixDetails
    */
    @isTest
    private static void testGetBusinessMixDetails(){
        Contact conObj = [SELECT Id FROM Contact WHERE LastName='Contact' LIMIT 1];
        User adminUserObj = [SELECT Id FROM User WHERE UserName='SalesforceUser11@amamama.com'];
        System.runAs(adminUserObj){
            Test.startTest();
                SL_BusinessMixManagement_Controller.conAndBusMixDetailWrapper returnResult = SL_BusinessMixManagement_Controller.getBusinessMixDetails(conObj.Id);
            Test.stopTest(); 
            System.assertEquals(10,returnResult.busMixWrapperLst.size());
        }
    }
    
    /*
        Testing method insertOrUpdateBusMix
    */
    @isTest
    private static void testInsertOrUpdateBusMix(){
        Contact conObj = [SELECT Id FROM Contact WHERE LastName='Contact' LIMIT 1]; 
        User adminUserObj = [SELECT Id FROM User WHERE UserName='SalesforceUser11@amamama.com'];
        System.runAs(adminUserObj){
            Test.startTest();
                SL_BusinessMixManagement_Controller.insertOrUpdateBusMix(conObj.Id,'[{"busMixId":"","busMixPL":"ETF","comment":"test","checked":true}]','[{"busMixId":"","busMixPL":"ETF","comment":"test1","checked":true}]');
            Test.stopTest(); 
            System.assertEquals(1,[SELECT Id,Comments__c FROM Business_Mix__c WHERE Contact__c=:conObj.Id AND Comments__c='test'].size());
        }
    }
    
    /*
        Testing getBaseUrl method
    */
    @isTest
    private static void testGetBaseUrl(){
        User adminUserObj = [SELECT Id FROM User WHERE UserName='SalesforceUser11@amamama.com'];
        System.runAs(adminUserObj){
            Test.startTest();
                String urlStr = SL_BusinessMixManagement_Controller.getBaseUrl();
            Test.stopTest(); 
            System.assertEquals(true,urlStr != NULL);
        }
    }
}