/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-02-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-02-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/

@isTest
private class Batch_Create_Payee_Atd_Recs_Sch_Test 
{
    //Testing Batch_Create_Payee_Atd_Recs_Schedule
    @isTest 
    static void testScheduleClass() {
        
        SL_TestDataFactory.createTestDataForCustomSetting();
        
        String CRON_EXP = '0 0 0 15 3 ? *';
        
        Test.startTest();
        
           String jobId = System.schedule('SCH_Batch_Create_Payee_Attendance_Records',  CRON_EXP, new Batch_Create_Payee_Atd_Recs_Schedule());
           
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();
        
    }
    
    /*
    @isTest 
    static void testBatchClass() 
    {
        SL_TestDataFactory.createTestDataForCustomSetting();
        Test.startTest();
        String jobId = Database.executeBatch(new Batch_Create_Payee_Attendance_Records(), 1);
        Test.stopTest();
    }
    */
}