public class BatchScheduleUpdateWithMaps4FHCD  implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled for calculating FHD for Contacts with Tasks and Events
        contactFHCDateUpdateWithMaps b = new contactFHCDateUpdateWithMaps ();  
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,200);
    }
   
}