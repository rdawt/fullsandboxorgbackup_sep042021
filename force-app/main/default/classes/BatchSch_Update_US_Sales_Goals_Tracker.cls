global class BatchSch_Update_US_Sales_Goals_Tracker implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        // We now call the batch class to be scheduled
        Batch_Update_US_Sales_Goals_Tracker_CS objclass = new Batch_Update_US_Sales_Goals_Tracker_CS ();
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(objclass, 1);
    }
   
}