public class populateCampaignfromCSG {

	public list<SelectOption> subscriptionOptionList {get; set;}
	public list<SelectOption> campaingnOptionList {get; set;}
	public list<Campaign> campaingnList=new List<Campaign>();
	public list<Id> lstSubId=new List<Id>();
	public string defaultCSGId {get; set;}
	public string selectedSubscription {get; set;}
	public string selectedCampaign {get; set;}
	public string strSubscription {get; set;}
	public string cmpCount {get; set;}
	public integer subsMemCount {get; set;}
	public integer campMemCount {get; set;}
	public integer campSubGrpCount {get; set;}

	public populateCampaignfromCSG ()
	{
		defaultCSGId = ApexPages.currentPage().getParameters().get('cId');
		subscriptionOptionList = subscriptionOptions();
		campaingnOptionList = campaignOptions();
		refreshSubsMemCount();
		refreshCampMemCount();
	}
	
	public List<SelectOption> GetSubscriptions()
	{
		list<SelectOption> lstSGS = new List<SelectOption>();
		list<Subscription__c> subList = new list<Subscription__c>();
		subList = [SELECT Name,IsActive__c from Subscription__c Where IsActive__c =: true];
		for(Subscription__c sub : subList)
			lstSGS.add(new SelectOption(sub.Name, sub.Name));
		return lstSGS;
	}

	public List<SelectOption> subscriptionOptions()
	{
		List<SelectOption> subsOptionsList = new List<SelectOption>();
		List<Campaign_Subscription_Group__c> subscriptionList = [Select Id, Name from Campaign_Subscription_Group__c order by Name ASC ];
		for(Campaign_Subscription_Group__c subs: subscriptionList) {
			subsOptionsList.add(new SelectOption(subs.Id, subs.Name));
			// This will make default radio button selected as the CSG from which they came initiated the page
			if(subs.Id == defaultCSGId)
				selectedSubscription = subs.Id;
		}
		return subsOptionsList;   
	}

	public List<SelectOption> campaignOptions()
	{
		List<SelectOption> campaignOptionsList = new List<SelectOption>();
		campaingnList = [Select Id, Name from Campaign where IsActive =: true order by Name ASC];   
		for(Campaign camp : campaingnList) {
			campaignOptionsList.add(new SelectOption(camp.Id, camp.Name));
			selectedCampaign =  campaingnList[0].Id;     
		}
		return campaignOptionsList;   
	}
    
	public PageReference refreshSubsMemCount()
	{
		try {
			Set<Id> contactIdList = new Set<Id>();
			strSubscription = null;
			campSubGrpCount = 0;
			subsMemCount = 0;
			lstSubId = new List<Id>();
			list<Campaign_Subscription_Group_Member__c> subsList = new list<Campaign_Subscription_Group_Member__c>();
			subsList = [SELECT s.Id, s.Name,s.Campaign_Subscription_Group__c,s.Subscription__c,s.Subscription__r.Name 
						from Campaign_Subscription_Group_Member__c s 
						Where s.Campaign_Subscription_Group__c =: selectedSubscription 
						limit:Limits.getLimitQueryRows()];
			for(Campaign_Subscription_Group_Member__c subs : subsList)
			{
				lstSubId.add(subs.Subscription__c);
				if(strSubscription == null)
					strSubscription = subs.Subscription__r.Name + ', ';
				else
					strSubscription = strSubscription + subs.Subscription__r.Name + '.';
			}
			
			campSubGrpCount = lstSubId.size();
			list<Subscription_Member__c> subList = new list<Subscription_Member__c>();
			subList = [SELECT s.Id, s.Name,s.Subscription__c,s.Subscribed__c,s.Contact__c,s.Subscription_Unsubscribed_Date__c 
						from Subscription_Member__c s 
						Where s.Subscribed__c=:true and s.Contact__r.HasOptedOutOfEmail =: false and s.Subscription__c IN:lstSubId 
						limit:Limits.getLimitQueryRows()];
			for(Subscription_Member__c subs : subList)
				contactIdList.add(subs.Contact__c);
			subsMemCount = contactIdList.size();
		}catch(Exception e) {
			System.debug('There is no valid subscription members exists for the given details.');
			subsMemCount = 0;
		}
		return null;
	}
    
	public PageReference refreshCampMemCount()
	{
		try {
			cmpCount = ApexPages.currentpage().getparameters().get('val1');
			System.Debug('cmpCount'+cmpCount);
			Set<Id> lstCmpId = new Set<Id>();
			Campaign cmp = [Select Id,Total_Members__c from Campaign Where Id =: selectedCampaign Limit 1];
			campMemCount = Integer.valueOf(cmp.Total_Members__c);
		}catch(Exception e) {
			System.debug('There is no valid campaign members exists for the given details.');
			campMemCount = 0;
		}
		return null;
	}

	public PageReference populateSelected()
	{
		if(selectedSubscription == null)
			selectedSubscription = defaultCSGId;
		AddMembersToCampaign addCmp = new AddMembersToCampaign(selectedSubscription,selectedCampaign);
		Database.executeBatch(addCmp);
		Campaign cmp = new Campaign(Id = selectedCampaign);
		cmp.Campaign_Subscription_Group__c = selectedSubscription ;
		upsert cmp;
		PageReference mysecondPage=new PageReference('/'+selectedCampaign);
		mysecondPage.setRedirect(true);
		return mysecondPage;    
	}

	public PageReference cancel()
	{
		if(selectedSubscription == null)
			selectedSubscription = defaultCSGId;
		return new PageReference('/'+selectedSubscription);
	}
}