public class CommunityRegistrantTriggerHandler {

    List<Community_Registrant__c> records = new List<Community_Registrant__c>();
    Map<Id, Community_Registrant__c> oldMap = new Map<Id, Community_Registrant__c>();

    public void onAfterInsert(List<Community_Registrant__c> lstNewRecords,Map<Id, Community_Registrant__c> mapOldOpportunity){
        records = lstNewRecords;
        oldMap = mapOldOpportunity;
        Set<Id> setCRIds = new Set<Id>();
        for(Community_Registrant__c record: records){
            if(record.Email__c != null){
                setCRIds.add(record.Id);
            }
        }
        if(!setCRIds.isEmpty()){
            validateEmailIds(setCRIds);
        }       
    }

    @future (callout=true)
    private static void validateEmailIds(Set<Id> setCRIds) {
        try{
            EmailValidationAPISettings__c settings = EmailValidationAPISettings__c.getOrgDefaults();
            List<Community_Registrant__c> lstUpdateRecords = new List<Community_Registrant__c>();
            List<Community_Registrant__c> records = [SELECT Id,Email__c,Email_Status__c FROM Community_Registrant__c WHERE Id IN : setCRIds];
            for(Community_Registrant__c record: records){
                HttpRequest request = new HttpRequest();
                HttpResponse response = new HttpResponse();
                Http http = new Http();
                request.setEndpoint(settings.End_point__c+'?email='+ record.Email__c +'&apikey='+settings.API_Key__c);
                
                request.setMethod('GET');
                response = http.send(request);
                System.debug('-1-'+response.getStatusCode()); 
                System.debug('-2-'+response.getBody());

                if(response.getStatusCode() == 200){
                    String responseBody = response.getBody();
                    EmailValidationJsonParser validationResponse  = (EmailValidationJsonParser)System.JSON.deserialize(responseBody, EmailValidationJsonParser.class);
                    if(validationResponse.result == 'Valid' ){
                        if(validationResponse.free == 'true'){
                            record.Email_Status__c = 'Personal';
                        }else{
                            record.Email_Status__c = 'Business';
                        }
                    }else{
                        record.Email_Status__c = 'Invalid';
                    }
                    record.Email_Validation_Result__c = validationResponse.reason;
                    lstUpdateRecords.add(record);
                }

                // If(response.getStatusCode() == 301 || response.getStatusCode() == 302)
                // {    
                //     request.setEndpoint(settings.End_point__c+''+response.getHeader('Location'));
                //     response = new Http().send(request);
                //     if(response.getStatusCode() == 200){
                //         String responseBody = response.getBody();
                //         EmailValidationJsonParser validationResponse  = (EmailValidationJsonParser)System.JSON.deserialize(responseBody, EmailValidationJsonParser.class);
                //         if(validationResponse.deliverability == 'DELIVERABLE' && validationResponse.is_valid_format.value == true){
                //             if(validationResponse.is_free_email.value == true){
                //                 record.Email_Status__c = 'Personal';
                //             }else{
                //                 record.Email_Status__c = 'Business';
                //             }
                //         }else{
                //             record.Email_Status__c = 'Invalid';
                //         }
                //         lstUpdateRecords.add(record);
                //     }
                // }
            }
            if(!lstUpdateRecords.isEmpty()){
                update records;
            }
            
        }
        catch(Exception e){
            System.debug('Error-' + e.getMessage());   
        }
    }
}