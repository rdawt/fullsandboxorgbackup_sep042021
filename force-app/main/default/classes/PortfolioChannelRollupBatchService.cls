public class PortfolioChannelRollupBatchService {
    
    public Map<String, Id> getChannelMap(String strVehicleType){
        Map<String, Id> mapChannels = new Map<String, Id>();
        Set<String> setChannelTypes = new Set<String>();
        setChannelTypes.add('SFDC');
        System.debug('strVehicleType --> '+strVehicleType);
        if(strVehicleType == 'ETF'){
            setChannelTypes.add('Broadridge');
        }
        System.debug('setChannelTypes --> '+setChannelTypes);
        for(Channel__c channel: [SELECT Id,Channel_Description__c FROM Channel__c WHERE Status__c = 'Active' AND Channel_Description__c != null AND Channel_Type__c IN :setChannelTypes AND Allow_Quarterly_Analysis__c = true ORDER BY Channel_Type__c])
        {
            mapChannels.put(channel.Channel_Description__c,channel.Id);
        }
        return mapChannels;
    }

    public void calculateMFAllChannels(Map<String,sObject> mapChannelRollUp,Map<String,sObject> mapTerritoryRollUp,List<Id> lstPortfolio,Set<String> setFunds,Date dStartDate,Date dEndDate,String strChannel,Integer iQuarter,String strVehicleType,String strYear,String strType){
        try{
            AggregateResult[] resultSfdc;
            AggregateResult[] resultTerritory;
            Integer iYear = Integer.valueof(strYear);
            if(strType == 'Quarter'){
                resultSfdc = [SELECT SFDC_Fund_Ticker__c fund,sum(SalesConnect_Current_Assets__c) currentAssets FROM History_Firm_Portfolio_Breakdown__c WHERE Fund_Vehicle_Type__c = :strVehicleType AND Asset_Date__c >= :dStartDate AND Asset_Date__c <= :dEndDate AND SFDC_Channel_Derived__c = :strChannel GROUP BY SFDC_Fund_Ticker__c];
                resultTerritory = [SELECT SFDC_Fund_Ticker__c fund,sum(SalesConnect_Current_Assets__c) currentAssets,SalesConnect_Account__r.Owner.User_Territory__c territory FROM History_Firm_Portfolio_Breakdown__c WHERE Fund_Vehicle_Type__c = :strVehicleType AND Asset_Date__c >= :dStartDate AND Asset_Date__c <= :dEndDate AND SFDC_Channel_Derived__c = :strChannel GROUP BY SFDC_Fund_Ticker__c,SalesConnect_Account__r.Owner.User_Territory__c];
            }else if(strType == 'Month'){
                resultSfdc = [SELECT SFDC_Fund_Ticker__c fund,sum(SalesConnect_Current_Assets__c) currentAssets FROM History_Firm_Portfolio_Breakdown_Monthly__c WHERE Fund_Vehicle_Type__c = :strVehicleType AND Asset_Date__c >= :dStartDate AND Asset_Date__c <= :dEndDate AND SFDC_Channel_Derived__c = :strChannel GROUP BY SFDC_Fund_Ticker__c];
                resultTerritory = [SELECT SFDC_Fund_Ticker__c fund,sum(SalesConnect_Current_Assets__c) currentAssets,SalesConnect_Account__r.Owner.User_Territory__c territory FROM History_Firm_Portfolio_Breakdown_Monthly__c WHERE Fund_Vehicle_Type__c = :strVehicleType AND Asset_Date__c >= :dStartDate AND Asset_Date__c <= :dEndDate AND SFDC_Channel_Derived__c = :strChannel GROUP BY SFDC_Fund_Ticker__c,SalesConnect_Account__r.Owner.User_Territory__c];
            }            
            //mapChannelRollUp = fillAggregateResultsMF(resultSfdc,mapChannelRollUp,iQuarter,strType);
            //mapTerritoryRollUp = fillAggregateResultsMFTerritory(resultTerritory,mapTerritoryRollUp,iQuarter,strType);

            upsert mapChannelRollUp.values();
            upsert mapTerritoryRollUp.values();
        }catch(Exception e){
            System.debug('Erroe calculateMFAllChannels '+e.getMessage());
            throw e;
        }
    }


}