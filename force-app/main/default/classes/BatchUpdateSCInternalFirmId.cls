public class BatchUpdateSCInternalFirmId implements Database.Batchable<sObject>, Database.Stateful{
    
    Map<String, String> mapInternalFirmIds = new Map<String, String>();

    public BatchUpdateSCInternalFirmId(){
        mapInternalFirmIds.put('18353','00000000000000001202');
        mapInternalFirmIds.put('8174','00000000000000001036');
        mapInternalFirmIds.put('361','00000000000000001002');
        mapInternalFirmIds.put('15296','00011063586254622456');
        mapInternalFirmIds.put('7569','00000000000000001085' );
        mapInternalFirmIds.put('40258','00000000000000003803');
        mapInternalFirmIds.put('6413','00000000000000001018' );
        mapInternalFirmIds.put('705','00000000000000001120');
        mapInternalFirmIds.put('519','00000000000000001025' );
        mapInternalFirmIds.put('10299','00000000000000002037' );
        mapInternalFirmIds.put('6627','00000000000000001889');
        mapInternalFirmIds.put('265','00000000000000001883' );
        mapInternalFirmIds.put('11759','99999113643226915270' );
        mapInternalFirmIds.put('127549','00000000000000004309');
        mapInternalFirmIds.put('13572','00000000000000002344');
        mapInternalFirmIds.put('7059','00000000000000003446');
        mapInternalFirmIds.put('106108','00011081753129424560');
        mapInternalFirmIds.put('10205','00000000000000001934');
        mapInternalFirmIds.put('25803','00000000000000001973');
        mapInternalFirmIds.put('149777','00000000000000001005');
        mapInternalFirmIds.put('42057','00000000000000003853');
        mapInternalFirmIds.put('17344','00011063586248722364');
        mapInternalFirmIds.put('19616','00000000000000001360');
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        set<string> setCRDs = new set<string>();
        setCRDs.addAll(mapInternalFirmIds.keyset());
        String strQuery = 'Select Id,SalesConnect__Internal_Firm_ID__c,Firm_CRD__c,Internal_Firm_ID__c from Account where Firm_CRD__c IN :setCRDs';
        System.debug(strQuery);
        return Database.getQueryLocator(strQuery); 
    }

    public void execute(Database.BatchableContext bc, List<Account> records) {
        List<Account> lstUpdate = new List<Account>();
        for(Account acc: records){
            System.debug('Acc --> '+acc);
            if(acc.SalesConnect__Internal_Firm_ID__c == null){
                acc.Internal_Firm_ID__c = mapInternalFirmIds.get(acc.Firm_CRD__c);
                lstUpdate.add(acc);
            }
        }
        System.debug('lstUpdate --> '+lstUpdate);
        if(!lstUpdate.isEmpty()){
            update lstUpdate;
            System.debug(lstUpdate.size() +' Accounts have been updated.');
        }
    }

    public void finish(Database.BatchableContext bc) {
        System.debug('Batch Completed');
    }

}