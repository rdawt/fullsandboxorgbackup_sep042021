/*
    Author  :       Vidhya Krishnan
    Date Created:   11/6/12
    Description:    This class is used in all the other classes to send email notification to CRM Team members
*/
public class SendEmailNotification {

    public Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
 //   public String[] toAddresses = new String[] {'FeedAlerts.Sales@vaneck.com'};
 //   public String[] toAddresses = new String[] {'svenkateswaran@vaneck.com','ddossantos@vaneck.com'};
      public List<String> toAddresses = new List<String>();
    
    public SendEmailNotification(string subject){
    	for(SendEmailNotifications__c objSendEmailNotifications : [Select Id, Email__c from SendEmailNotifications__c])
	    {
	      	toAddresses.add(objSendEmailNotifications.Email__c);
	    }
	    if(toAddresses.isEmpty())
	    	toAddresses.add('svenkateswaran@vaneck.com');
        mail.setToAddresses(toAddresses);
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setSaveAsActivity(false);
        mail.setSenderDisplayName('Salesforce Support');
        mail.setSubject(subject);
        mail.setBccSender(false); mail.setUseSignature(false);      
    }
    
    public void sendEmail(string message){
        mail.setPlainTextBody(message); mail.setHtmlBody(message);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    public void sendEmailMoreDetails(String strBody){
        mail.setPlainTextBody(strBody); mail.setHtmlBody(strBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    static testMethod void testContactTrigger_sendEmail() {
        SendEmailNotification se = new SendEmailNotification('Test');
        se.sendEmail('Test Message');
    }
    
    static testMethod void testContactTrigger_sendEmailMoreDetails() {
        SendEmailNotification se = new SendEmailNotification('Test');
        se.sendEmailMoreDetails('Test Message');
    }
}