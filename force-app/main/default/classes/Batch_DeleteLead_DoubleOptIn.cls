global class Batch_DeleteLead_DoubleOptIn implements Database.Batchable<sObject> 
{
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        String query = 'SELECT Id, Deactivation_Requested__c, Status, Community_Registrant__c ' + 
                        'From Lead ' + 
                        'where isConverted = false and Deactivation_Requested__c <= TODAY';
        system.debug('>>>>>>>>>>query>>>>>>>>>>>' + query);
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<Lead> lstLeads) 
    {
         try 
         {
            if(!lstLeads.isEmpty())
            {
                List<Lead> lstLead_Delete = new List<Lead>();
                List<Community_Member__c> lstCommunity_Member_Delete = new List<Community_Member__c>();
                List<Community_Registrant__c> lstCommunity_Registrant_Delete = new List<Community_Registrant__c>();

                Set<Id> setCRIds = new Set<Id>();

                for(Lead objLead : lstLeads)
                {
                    if(objLead.Deactivation_Requested__c <= Date.today())
                    {
                        if(objLead.Community_Registrant__c != NULL)
                        {
                            setCRIds.add(objLead.Community_Registrant__c);
                        }

                        lstLead_Delete.add(objLead);
                    }
                }

                if(!setCRIds.isEmpty())
                {
                    for(Community_Registrant__c objCR : [SELECT Id, Email__c , Subscription_Options__c, Contact__c,
                                                            SFDC_Subscription_Ids_CSV__c, Subscription_Group__c
                                                            FROM Community_Registrant__c 
                                                            WHERE ID IN: setCRIds])
                    {
                        lstCommunity_Registrant_Delete.add(objCR);
                    }

                    for(Community_Member__c objCM : [SELECT Id, Community_Registrant__c, Contact__c, Community__c 
                                    FROM Community_Member__c 
                                    WHERE Community_Registrant__c IN: setCRIds])
                    {
                        lstCommunity_Member_Delete.add(objCM);
                    }
                }

                // Deleting this in this sequence because Community member is having Community Registrant
                // Then Deletng Community Registrant
                // Then Lead record
                if(!lstCommunity_Member_Delete.isEmpty())
                {
                    delete lstCommunity_Member_Delete;
                }

                if(!lstCommunity_Registrant_Delete.isEmpty())
                {
                    delete lstCommunity_Registrant_Delete;
                }
                
                if(!lstLead_Delete.isEmpty())
                {
                    delete lstLead_Delete;
                }
            }
            
         } 
         catch (Exception e) 
         {
             
         }
    }   
     
    global void finish(Database.BatchableContext BC) 
    {
        // execute any post-processing operations like sending email
    }
}