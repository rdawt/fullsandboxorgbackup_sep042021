/********************************************************************************************
* @Author: Tharaka De Silva
* @Date  : 2021-06-23
*
* Test class for BatchScheduleAccSalesByFundMonthlyUpsert class
*
* *******************************************************************************************/
@IsTest
public with sharing class BatchSchAccSalesByFundMonthlyUpsertTest {
    /********************************************************************************************
    * Tests the scenario where BatchScheduleAccSalesByFundMonthlyUpsert is working as expected.
    * *******************************************************************************************/
    @isTest static void testBatchAccSalesByFundUpsertMonthlySchedule() {
        
        Test.startTest();

        BatchScheduleAccSalesByFundMonthlyUpsert scheduledBatchable = new BatchScheduleAccSalesByFundMonthlyUpsert();
        String chron = '0 0 2 * * ?';
        String jobid = system.schedule('Test API Version Check', chron, scheduledBatchable);

        // Get the information from the CronTrigger API object
        CronTrigger ct = [Select Id, CronExpression, TimesTriggered, State, NextFireTime from CronTrigger where id = :jobId];
        // Verify the expressions are the same
        System.assertEquals('0 0 2 * * ?', ct.CronExpression, 'The expected CronExpression is 0 0 2 * * ? but is actually ' + ct.CronExpression);
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered, 'The job should not be runned.');
        // Verify job is waiting
        System.assertEquals('WAITING', ct.State, 'The expected State is WAITING but is ' + ct.State);
        
        Test.stopTest();
    }
}