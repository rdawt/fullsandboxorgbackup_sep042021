@isTest
global class contactSalesPortfolioScheduledTest
{
    @isTest
    public static void testschedule()
    {
    	
    	Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
		sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
    	
        Account testAccount = new Account();
        testAccount.name = 'My Test Account';
        testAccount.channel__c = 'Institutional';
        testAccount.BillingCountry = 'USA';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.firstname = 'test';
        testContact.lastname = 'dude';
        testContact.territory__c = 'AA';
        testContact.accountId = testAccount.id;
        testContact.MailingCountry = 'USA';
        insert testContact;
        
        
        Fund_Goal__c thisGoal = new Fund_Goal__c();
        thisGoal.name = 'Test Goal-'+testContact.territory__c+'-'+date.today().Year();
        thisGoal.Year_End__c = date.today();
        thisGoal.Fund__c = 'Test Goal';
        thisGoal.Territory__c = 'AA';
        insert thisGoal;
        
        system.debug('\n\n\n\n\n---------------------------- Fund goal name is: '+thisGoal.name);
        
        SalesConnect__Contact_Portfolio_Breakdown__c thisSCPB = new SalesConnect__Contact_Portfolio_Breakdown__c();
        thisSCPB.name = 'Test Goal';
        thisSCPB.SalesConnect__Contact__c = testContact.id;

        insert thisSCPB;
        Test.startTest();       
            contactSalesPortfolioScheduled s8 = new contactSalesPortfolioScheduled();
            string sch = '0 0 * * 1-12 ? *';
            system.schedule('Process Trans 1', sch, s8);
            
            try{
                contactSalesPortfolioBatchProcess b = new contactSalesPortfolioBatchProcess();
                system.debug('\n\n\n-------------------------------Running query: ' + b.query);
                ID myBatchJobID = database.executebatch(b);
            }
            catch(exception ex){
                system.debug('\n\n\n\n------------------ ERROR: ' + ex.getMessage());
            }
        Test.stopTest();
               
        //make sure the linking happened.
        thisSCPB = [select id, name, fund_goal__c from SalesConnect__Contact_Portfolio_Breakdown__c];
        system.assertEquals(thisGoal.id,thisSCPB.fund_goal__c);
    }
}