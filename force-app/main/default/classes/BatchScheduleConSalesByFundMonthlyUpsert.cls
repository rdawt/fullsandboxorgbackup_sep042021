/**************************************************************************************
* @Author: Tharaka De Silva
* @Date: 2021-06-28
*
* This class implement the Schedulable interface in the purpose of calling BatchConSalesByFundMonthlyUpsert.
* This schedulable will upsert all the records in to History_Con_Portfolio_Breakdown_Monthly__c object from 
* SalesConnect__Contact_Portfolio_Breakdown__c object
***************************************************************************************/
public class BatchScheduleConSalesByFundMonthlyUpsert implements Schedulable{
    
    public void execute(SchedulableContext sc)
    {
        BatchConSalesByFundMonthlyUpsert batch = new BatchConSalesByFundMonthlyUpsert(); 
        database.executebatch(batch,200); 
    }

}