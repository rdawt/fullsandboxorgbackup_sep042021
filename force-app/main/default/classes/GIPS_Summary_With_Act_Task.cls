public class GIPS_Summary_With_Act_Task 
{
    public List<Contact> lstContacts {get;set;}
    public List<Contact> lstContact {get;set;}
    public List<Account> lstAccount {get;set;}
    public List<AttendeeRelationObject> lstAttendeeRelationObject {get;set;}
    public List<GIPS__c> lstGIPSSummary {get;set;}
    public Event objEvent {get;set;}
    public Event objTask {get;set;}
    public Contact objContact {get;set;}
    public List<Contact> lstFundLookUp {get;set;}
    public String toFundsJSON{get;set;}
    public String selectedFunds{get;set;}

    public GIPS_Summary_With_Act_Task(Apexpages.StandardController std) 
    {
        initialise();
        objEvent.Ownerid = UserInfo.getUserId();
        objEvent.StartDateTime = Datetime.now();
        objEvent.EndDateTime = Datetime.now().addMinutes(30);
        objEvent.Ownerid = UserInfo.getUserId();
        
        objContact = [SELECT Id, Name, AccountId, Account.Name from Contact where Id =: std.getId() limit 1];
        addDefaultAttendee(objContact);
        addDefaultFunds();
        
        toFundsJSON = getAllFundsRecords();
        
    }

    public String getAllFundsRecords()
    {
        String strFundsJSON = '';

        Set<String> autocompleteFundsSet = new Set<String>();
        for(Fund__c objFund : [Select Id , Name from Fund__c where BU_Region__c = 'USA' and Fund_Status__c != 'Closed'])
        {
            autocompleteFundsSet.add(objFund.Name);
        }

        strFundsJSON = JSON.serialize(autocompleteFundsSet);

        return strFundsJSON;
    }

    public void addSelectedFundToList()
    {
        System.debug('????????????selectedFunds?????????????' + selectedFunds);
        Fund__c objSelectedFund = new Fund__c();
        for(Fund__c objFund : [Select Id from Fund__c where Name =: selectedFunds])
        {
            System.debug('????????????objFund?????????????' + objFund);
            objSelectedFund = objFund;
        }

        System.debug('????????????objSelectedFund?????????????' + objSelectedFund);
        System.debug('????????????lstFundLookUp?????????????' + lstFundLookUp);

        for(Contact objContact : lstFundLookUp)
        {
            System.debug('????????????objFund?????????????' + objContact.Fund__c);

            if(objContact.Fund__c == NULL)
            {
                objContact.Fund__c = objSelectedFund.Id;
                break;
            }
        }

        selectedFunds = '';
        toFundsJSON = getAllFundsRecords();

        System.debug('????????????selectedFunds?????????????' + lstFundLookUp);
    }

    public void initialise()
    {
        toFundsJSON = '';
        selectedFunds = '';
        objContact = new Contact();
        objEvent = new Event();
        lstAccount = new List<Account>();
        lstContact = new List<Contact>();
        lstGIPSSummary = new List<GIPS__c>();
        lstContacts = new List<Contact>();
        lstAttendeeRelationObject = new List<AttendeeRelationObject>();
        lstFundLookUp = new List<Contact>();
    }

    public void addDefaultAttendee(Contact objContact)
    {
        Event paramEvent = new Event();
        Contact paramContact = new Contact();
        Account paramAccount = new Account();
        AttendeeRelationObject objAttendeeRelationObject1 = new AttendeeRelationObject(new Event(ContactOwner__c = objContact.Id), objContact);
        AttendeeRelationObject objAttendeeRelationObject2 = new AttendeeRelationObject(paramEvent, paramContact);
        AttendeeRelationObject objAttendeeRelationObject3 = new AttendeeRelationObject(paramEvent, paramContact);
        lstAttendeeRelationObject.add(objAttendeeRelationObject1);
        lstAttendeeRelationObject.add(objAttendeeRelationObject2);
        lstAttendeeRelationObject.add(objAttendeeRelationObject3);
    }

    public void addDefaultFunds()
    {
        Contact objFundLookUp1 = new Contact();
        Contact objFundLookUp2 = new Contact();
        Contact objFundLookUp3 = new Contact();
        Contact objFundLookUp4 = new Contact();
        Contact objFundLookUp5 = new Contact();
        Contact objFundLookUp6 = new Contact();

        lstFundLookUp.add(objFundLookUp1);
        lstFundLookUp.add(objFundLookUp2);
        lstFundLookUp.add(objFundLookUp3);
        lstFundLookUp.add(objFundLookUp4);
        lstFundLookUp.add(objFundLookUp5);
        lstFundLookUp.add(objFundLookUp6);
    }
    

    public void handleAddGIPS()
    {
        GIPS__c objGIPS = new GIPS__c();
        if(objContact != NULL)
        {
            objGIPS.Contact__c = objContact.Id;
            objGIPS.Account__c = objContact.AccountId;
            objGIPS.Account_Name__c = objContact.Account.Name;
        }
        objGIPS.GIPS_Mail_Out_By__c = UserInfo.getUserId();
        objGIPS.Recorded_By__c = UserInfo.getUserId();
        objGIPS.Recorded_By_Name__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
        objGIPS.GIPS_YesNo__c = 'No';
        objGIPS.GIPS_SentDate__c = Date.today();
        objGIPS.GIPS_OptOut_Discontinue_Date__c = getEndDate(objGIPS.GIPS_SentDate__c);
        objGIPS.GIPS_MailOut_Date__c = Date.today();
        objGIPS.GIPS_RenewalDate__c = getEndDate(objGIPS.GIPS_MailOut_Date__c);
        lstGIPSSummary.add(objGIPS);
    }

    public static Date getEndDate(Date currentDate)
    {
        Date dtReturnDate;
        Date dtAddYear = currentDate.addYears(1);
        Date dtStartOfMonth = dtAddYear.toStartOfMonth();
        dtReturnDate = dtStartOfMonth.addDays(-1);

        return dtReturnDate;
    }

    public void getGIPSRecordsRelatedToContact()
    {
        for(GIPS__c objGIPS : lstGIPSSummary)
        {
            if(objGIPS.GIPS_OptOut_Discontinue_Date__c == NULL || objGIPS.GIPS_SentDate__c != NULL)
                objGIPS.GIPS_OptOut_Discontinue_Date__c = getEndDate(objGIPS.GIPS_SentDate__c);

            if(objGIPS.GIPS_MailOut_Date__c == NULL)
            {
                objGIPS.GIPS_MailOut_Date__c = Date.today();
            }
            
            objGIPS.GIPS_RenewalDate__c = getEndDate(objGIPS.GIPS_MailOut_Date__c);
            

            if(objGIPS.GIPS_Mail_Out_By__c == NULL)
                objGIPS.GIPS_Mail_Out_By__c = objGIPS.Recorded_By__c;
        }
        
    }

    public Pagereference handleSaveClick()
    {
        try {
            
        
            if(!lstAttendeeRelationObject.isEmpty())
            {
                for(AttendeeRelationObject objAttendeeRelationObject : lstAttendeeRelationObject)
                {
                    if(objAttendeeRelationObject.objEvent.ContactOwner__c != NULL && objAttendeeRelationObject.objContact.AccountId != NULL)
                    {
                        objEvent.WhoId = objAttendeeRelationObject.objEvent.ContactOwner__c;
                        objEvent.WhatId = objAttendeeRelationObject.objContact.AccountId;
                        break;
                    }
                }
            }

            if(!lstFundLookUp.isEmpty())
            {
                Set<Id> setFundIs = new Set<Id>();
                Map<Id, Fund__c> mapFunds = new Map<Id, Fund__c>();
                List<String> lstFunds = new List<String>();
                for(Contact objContact : lstFundLookUp)
                {
                    if(objContact.Fund__c != NULL)
                    {
                        setFundIs.add(objContact.Fund__c);
                    }
                }

                if(!setFundIs.isEmpty())
                {
                    for(Fund__c objFund : [Select Id, Name from Fund__c where Id IN: setFundIs])
                    {
                        mapFunds.put(objFund.Id, objFund);
                    }
                }

                for(Contact objContact : lstFundLookUp)
                {
                    if(objContact.Fund__c != NULL && mapFunds.containskey(objContact.Fund__c) )
                    {
                        lstFunds.add(mapFunds.get(objContact.Fund__c).Name);
                    }
                }

                if(!lstFunds.isEmpty())
                {
                    objEvent.Call_Topic__c = String.join(lstFunds, ';');
                }
            }

            objEvent.GIPS__c = true;
            insert objEvent;

            List<EventRelation> lstEventRelation = new List<EventRelation>();

            if(!lstAttendeeRelationObject.isEmpty())
            {
                for(AttendeeRelationObject objAttendeeRelationObject : lstAttendeeRelationObject)
                {
                    if(objAttendeeRelationObject.objEvent.ContactOwner__c != NULL && objAttendeeRelationObject.objContact.AccountId != NULL)
                    {
                        if(objEvent.WhoId != objAttendeeRelationObject.objEvent.ContactOwner__c && objEvent.WhatId != objAttendeeRelationObject.objContact.AccountId)
                        {
                            EventRelation objEventRelation = new EventRelation();
                            objEventRelation.EventId = objEvent.Id;
                            objEventRelation.IsParent = true;
                            objEventRelation.isWhat = false;
                            //objEventRelation.AccountId = objAttendeeRelationObject.objContact.AccountId;
                            objEventRelation.RelationId = objAttendeeRelationObject.objEvent.ContactOwner__c;
                            lstEventRelation.add(objEventRelation);
                        }
                    }
                }
            }

            if(!lstEventRelation.isEmpty())
                insert lstEventRelation;
            
            if(!lstGIPSSummary.isEmpty())
            {
                for(GIPS__c objGIPS :  lstGIPSSummary)
                {

                }

                insert lstGIPSSummary;
            }

            Pagereference pr = new Pagereference('/lightning/r/Contact/' + objContact.Id + '/view?0.source=alohaHeader');
            pr.setRedirect(true);
            return pr;
        } 
        catch (Exception ex) 
        {
            Apexpages.addMessages(ex);
            return null;
        }

    }

    public Pagereference launchGIPSSummaryEvent()
    {
        String strBaseUrl = '';
        Pagereference pr = new Pagereference( '/apex/GIPS_Summary_With_Event?id=' + objContact.Id);
        //Pagereference pr = new Pagereference('https://vaneck--full--c.visualforce.com/one/one.app#/alohaRedirect/apex/GIPS_Summary_With_Event?id=003A000001OujakIAB&sfdcIFrameOrigin=https%3A%2F%2Fvaneck--full.lightning.force.com');
        pr.setredirect(true);
        return pr;
    }

    public Pagereference launchGIPSSummaryTask()
    {
        Pagereference pr = new Pagereference('/apex/GIPS_Summary_With_Event?id=' + objContact.Id);
        pr.setredirect(true);
        return pr;
    }

    public Pagereference handleCancelClick()
    {
        Pagereference pr = new Pagereference('/lightning/r/Contact/' + objContact.Id + '/view?0.source=alohaHeader');
        pr.setredirect(true);
        return pr;
    }

    public void handleAddFund()
    {
        Contact paramContact1 = new Contact();
        Contact paramContact2 = new Contact();
        lstFundLookUp.add(paramContact1);
        lstFundLookUp.add(paramContact2);
    }

    public void handleAddAttendee()
    {
        Event paramEvent = new Event();
        Contact paramContact = new Contact();
        // Account paramAccount = new Account();
        AttendeeRelationObject objAttendeeRelationObject = new AttendeeRelationObject(paramEvent, paramContact);
        lstAttendeeRelationObject.add(objAttendeeRelationObject);
    }

    public class AttendeeRelationObject
    {
        public Event objEvent {get;set;}
        public Contact objContact {get;set;}
        public Account objAccount {get;set;}

        public AttendeeRelationObject(Event paramEvent, Contact paramContact ) //Account paramAccount
        {
            // objEvent = new Event();
            objEvent = paramEvent;

            // objContact = new Contact();
            objContact = paramContact;

            // objAccount = new Account();
            //objAccount = paramAccount;
        }
    }
    
}