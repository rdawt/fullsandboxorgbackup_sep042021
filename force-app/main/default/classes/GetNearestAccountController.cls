public with sharing class GetNearestAccountController 
{
    @AuraEnabled
    public static Account getCurrentAccount(String strAccountId){
        Account objCurrentAccount = new Account();
        objCurrentAccount = [Select Id, BillingLatitude, BillingLongitude from Account where Id =: strAccountId limit 1];
        return objCurrentAccount;
    }

    @AuraEnabled
    public static List<Account> findAccountsForMap(String strAccountId)
    {
        Account objCurrentAccount = new Account();
        objCurrentAccount = [SELECT Id, billingCity,billingStreet,billingCountry,billingPostalCode,
                                BillingState,  Branch_StreetAddress__c,
                                BillingLatitude, BillingLongitude 
                                From Account where Id =: strAccountId limit 1];
        
        List<Account> lstAccount = new List<Account>();

        if(objCurrentAccount.Branch_StreetAddress__c != NULL)
        {
            lstAccount = [select id,name,billingCity,billingStreet,billingCountry,billingPostalCode,Firm_URL__c,
                            billingState from Account 
                            where
                            ( ID !=: objCurrentAccount.Id) and  
                            ( Name NOT IN ('Default Registrant Firm', 'Individual Investor - Retail')) and 
                            (   (NOT DB_Source__c like '%MVIS%') and 
                                billingCountry = 'United States' and RecordType.Name NOT IN ('Br Office', 'Br Account', 'Account', 'Office') ) 
                                and 
                            ( Branch_StreetAddress__c =: objCurrentAccount.Branch_StreetAddress__c OR 
                                billingCity =: objCurrentAccount.billingCity OR
                                billingPostalCode =: objCurrentAccount.billingPostalCode )
                            limit 10];
        }
        else 
        {
            lstAccount = [select id,name,billingCity,billingStreet,billingCountry,billingPostalCode,Firm_URL__c,
                            billingState from Account 
                            where 
                            ( ID !=: objCurrentAccount.Id) and 
                            ( Name NOT IN ('Default Registrant Firm', 'Individual Investor - Retail')) and 
                            (   (NOT DB_Source__c like '%MVIS%') and 
                                billingCountry = 'United States' and RecordType.Name NOT IN ('Br Office', 'Br Account', 'Account', 'Office') ) 
                                and 
                            ( billingStreet =: objCurrentAccount.billingStreet OR 
                                billingCity =: objCurrentAccount.billingCity OR
                                billingPostalCode =: objCurrentAccount.billingPostalCode )
                            limit 10];
        }
        return lstAccount;
    }

    @AuraEnabled
    public static NearestAccountWrapper getNearestAccount(String strAccountId){
        List<Account> accList = new List<Account>();
        Account objCurrentAccount = new Account();
        objCurrentAccount = [Select Id, BillingLatitude, BillingLongitude from Account where Id =: strAccountId limit 1];
        
        accList = [Select id , name, BillingLatitude, BillingLongitude 
                   From Account 
                   Where BillingLatitude != null AND BillingLongitude != null Limit 50000];
        
        Map<Id, Double> mapOfAccountIdToDistance = new Map<Id, Double>();
        Double val0 = 0.0;
        Double val1 = 0.0;
        Double val2 = 0.0;
        Double val3 = 0.0;
        Double p = 0.017453292519943295;
        Double a = 0.00;
        Double b = 0.00;
        Double c = 0.00;
        Double d = 0.00;
        
        for(Account accObj : accList){
            if(accObj.BillingLatitude != null &&
               accObj.BillingLongitude != null
              ){
                  
                  a = 0.5 - System.Math.cos((accObj.BillingLatitude - Double.valueOf(objCurrentAccount.BillingLatitude)) * p)/2 + 
                      System.Math.cos(Double.valueOf(objCurrentAccount.BillingLatitude) * p) * System.Math.cos(accObj.BillingLatitude * p) *
                      (1 - System.Math.cos((Double.valueOf(objCurrentAccount.BillingLongitude) - accObj.BillingLongitude)* p ))/2;
                  b = System.Math.sqrt(a);
                  c = System.Math.asin(b);
                  d = 12742 * c;
                  
                  mapOfAccountIdToDistance.put(accObj.Id,d);
              }
        }
        system.debug('map print*$ '+ mapOfAccountIdToDistance);
        
        List<Account> accListBilling = new List<Account>();
        accListBilling = [Select id,name,BillingGeocodeAccuracy,BillingLatitude, BillingLongitude, BillingStreet,BillingCity,BillingState,BillingCountry from Account where id in: mapOfAccountIdToDistance.keySet()];
        Decimal minValue = 100000000.0;
        Id idOfNearestAccount = null;
        Account accObjNearest = new Account();
        for(Account accObj : accListBilling){
            if((System.Math.sqrt(mapOfAccountIdToDistance.get(accObj.Id)) < minValue) && (mapOfAccountIdToDistance.get(accObj.Id) != null) ){
                minValue = mapOfAccountIdToDistance.get(accObj.Id);
                idOfNearestAccount = accObj.Id;
                accObjNearest = accObj;
            }
        }
        
        NearestAccountWrapper wrapObj = new NearestAccountWrapper(accObjNearest,minValue);
        if(accObjNearest != null){
            return wrapObj;
        }
        else {
            return null;
        }
    }

    public class NearestAccountWrapper {
        @AuraEnabled public Account accObj;
        @AuraEnabled public Decimal distance;
        public NearestAccountWrapper(Account accObj , Decimal distance){
            this.accObj = accObj;
            this.distance = distance;
        } 
    }
}