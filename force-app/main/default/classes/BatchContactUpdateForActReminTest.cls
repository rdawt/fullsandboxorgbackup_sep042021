@isTest
public class BatchContactUpdateForActReminTest { 
  
    @isTest  static void testBatchContactUpdateForActivityReminder() {
     
        EventTriggerHandler eth = new EventTriggerHandler();
        DMLException e = null;
        Set<ID> accIds = new Set<ID>();
         
        List<Contact> conlist = new List<Contact>();
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
       
        Account acc = new Account(Name = 'TEST-WFA-Unbranched--', BillingCountry='United States',Channel__c='Insurance');
        insert acc;
        
        Contact con = new Contact(LastName='Aalund****',FirstName='Gail-coninscheduleclass',email='aalundgailAscon@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000007t1KOIAY');
        Contact con2 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAscon2@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000007t1KOIAY');
        Contact con3 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAscon3@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000007t1KOIAY');
        Contact con4 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAscon4@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000006RYNcIAO');
        Contact con5 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAscon5@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000006RYNcIAO');
        
        conlist.add(con);
        conList.add(con2);
        conList.add(con3);
        conList.add(con4);
        conList.add(con5);
        insert conList;     
           
        string Comments = 'This is sample activity description for test data.This is sample activity description for test data.';
        Event e1 = new Event(Activity_Type__c='Meeting', Description = 'This is sample activity description for test data.This is sample activity description for test data.', Result__c = 'Held',  Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-92, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=2,ActivityDateTime=System.today());
        Task t1 = new Task(status='email sent',Activity_Type__c='Call', Description = 'This is sample activity description for test data.This is sample activity description for test data.', Result__c = 'Held',  Subject = 'email', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-104, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId);
        Task t7 = new Task(status='call completed',Activity_Type__c='Call', Description = 'This is sample activity description for test data.This is sample activity description for test data.', Result__c = 'Held',  Subject = 'call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-100, ReminderDateTime = System.now()+1, WhoId = con4.Id, WhatId = con4.AccountId, OwnerId = con4.OwnerId);
        Task t8 = new Task(status='call completed',Activity_Type__c='Call', Description = 'This is sample activity description for test data.This is sample activity description for test data.', Result__c = 'Held',  Subject = 'call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-114, ReminderDateTime = System.now()+1, WhoId = con5.Id, WhatId = con5.AccountId, OwnerId = con5.OwnerId);
        Task t6 = new Task(Activity_Type__c='Call',Description = 'This is sample activity description for test data.This is sample activity description for test data.', Result__c = 'Held',  Subject = 'call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-120, ReminderDateTime = System.now()-2, WhoId = con2.Id, WhatId = con.AccountId, OwnerId = con2.OwnerId);
        Task t5 = new Task(Activity_Type__c='Call',Description = Comments, Priority = 'Normal', Status = 'call completed', Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-110, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId);
                                        Comments = Comments+'2nd task for con with who id in task object';
        event e5 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                                        ActivityDate = System.today()-111, ReminderDateTime = System.now()+7, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con2.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()+7);
        event e6 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                                        ActivityDate = System.today()-105, ReminderDateTime = System.now()+7, WhoId = con2.Id, WhatId = con2.AccountId, OwnerId = con2.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()+7);
        event e7 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                                        ActivityDate = System.today()-105, ReminderDateTime = System.now()+7, WhoId = con3.Id, WhatId = con3.AccountId, OwnerId = con3.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()+7);
        event e8 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                                        ActivityDate = System.today()-115, ReminderDateTime = System.now(), WhoId = con4.Id, WhatId = con4.AccountId, OwnerId = con4.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today());                                        
            
        List<Task> lstTask = new List<Task>();
        List<Event> lstEvent = new List<Event>();
        
        lstTask.add(t1);
        lstTask.add(t5);
        lstTask.add(t6);
        lstTask.add(t7);
        lstTask.add(t8);
        
        insert lstTask;
        
        lstEvent.add(e1);
        lstEvent.add(e5);
        lstEvent.add(e6);
        lstEvent.add(e7);
        lstEvent.add(e8);
            
        insert lstEvent;

        try{
            Test.startTest();
            BatchContactUpdateForActivityReminder batch = new BatchContactUpdateForActivityReminder ();
            dataBase.executeBatch(batch ,200 );
            Test.stopTest();
        }catch(Exception except1){
           System.assert(except1!=null);
        }
        System.assert(e==null);
    }
}