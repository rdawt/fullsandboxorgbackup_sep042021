/*    
    Author  :       Vidhya Krishnan
    Date Created:   1/9/12
    Description:    Community Registration Management API Class
*/
global class CommunityManagementAPI //extends DataStructures-
{
    global static CommunityManagement cm {get;set;}
    
    WebService static list<string> getCommunityIds(string userName)
    {
        if(cm == null) cm = new CommunityManagement();
        list<string> s = cm.getCommunityIds(userName);
        return s;
    }

    // This method is used in login authentication
    WebService static string getCMemberId(string communityId, string userName)
    {
        if(cm == null) cm = new CommunityManagement();
        String s = cm.getCMemberId(communityId,userName);
        return s;
    }
     // This method is used in login authentication for CTA from Ecktron
    WebService static CommunityManagement.MemberRecord getCMemberV2(string userName)
    {
        if(cm == null) cm = new CommunityManagement();
        CommunityManagement.MemberRecord memRec = new CommunityManagement.MemberRecord();
        memRec = cm.getCMemberRecordV2(userName);
        system.debug('Get CMember V2 returning Member Record-at API level...'+memRec);
        return memRec;
    }
    
    // This method is used to authenticate the email
    WebService static string isEmailTaken(string communityId, string emailAddress)
    {
        if(cm == null) cm = new CommunityManagement();
        string s = cm.isEmailTaken(communityId, emailAddress);
        system.debug('Is Email Taken......'+s);
        return s;
    }
    // returns if the member is contact or lead or just registrant
    WebService static string getCMemberType(string cMemberId)
    {
        if(cm == null) cm = new CommunityManagement();
        string s = cm.getCMemberType(cMemberId); // returns null if the given Id is wrong or if it is deleted Id
        return s;
    }
    // This method is used in login authentication
    WebService static string getPassword(string cMemberId)
    {
        if(cm == null) cm = new CommunityManagement();
        string s = cm.getPassword(cMemberId); // returns encryped password
        return s;
    }
    // This method resets user encrypted password
    WebService static boolean setPassword(string cMemberId, string strPassword)
    {
        if(cm == null) cm = new CommunityManagement();
        boolean b = cm.setPassword(cMemberId, strPassword);
        return b;
    }
    // This method checks if the Registrant is Activated (ready to use the account) or not
    WebService static boolean isActive(string cMemberId){
        if(cm == null) cm = new CommunityManagement();
        boolean b = cm.isActive(cMemberId);
        return b;
    }
    // This method is called from the activation email where cMemberId or CommunityId is not available to check the active flag
    WebService static string getActive(string emailAddress){
        if(cm == null) cm = new CommunityManagement();
        string s = cm.getActive(emailAddress);
        return s;
    }    
    // This method sets a Registrant Active once they click the activate link 
    WebService static boolean setActive(string cMemberId, boolean active){
        if(cm == null) cm = new CommunityManagement();
        boolean b = cm.setActive(cMemberId, active);
        return b;
    }
    // This method fetch all details of a community - its name, description, domain & is_active
    WebService static  Community__c getCommunityDetails(string communityId){
        if(cm == null) cm = new CommunityManagement();
        Community__c c = new Community__c();
        c = cm.getCommunityDetails(communityId);
        return c;
    }
    // This method is used to create new Reigstrant & Community Member Record
    WebService static boolean addCMember(CommunityManagement.MemberRecord memRec)
    {
        if(cm == null) cm = new CommunityManagement();
        boolean b = false; 
        b = cm.addMemberRecord(memRec);
        return b;
    }
    // Double Opt In  : This method is used to create new Registrant
    WebService static String addCRegistrant (CommunityManagement.MemberRecord memRec, String strSource)
    {
        if(cm == null) cm = new CommunityManagement();
        String strCRegId = '';
        strCRegId = cm.addRegistrantRecord(memRec, strSource);
        return strCRegId;
    }
    
    // This method is used to update the profile edit values
    WebService static boolean setCMember(String cMemberId, CommunityManagement.MemberRecord memRec)
    {
        if(cm == null) cm = new CommunityManagement();
        boolean b = cm.setMemberRecord(cMemberId,memRec);
        return b;
    }
    // This method is used to fetch details in profile edit page
    WebService static CommunityManagement.MemberRecord getCMember(string cMemberId)
    {
        if(cm == null) cm = new CommunityManagement();
        CommunityManagement.MemberRecord memRec = new CommunityManagement.MemberRecord();
        memRec = cm.getMemberRecord(cMemberId);
        system.debug('Get Member Record...'+memRec);
        return memRec;
    }

    // Added on 11 June 2021 for Double Opt In
    // This method is used to fetch details in profile edit page
    WebService static CommunityManagement.MemberRecord getCRegistrant(string cRegistrantId)
    {
        if(cm == null) cm = new CommunityManagement();
        CommunityManagement.MemberRecord memRec = new CommunityManagement.MemberRecord();
        memRec = cm.getRegistrantRecord(cRegistrantId);
        system.debug('Get Member Record...'+memRec);
        return memRec;
    }

    // This method is used to set last login time stamp when user log in
    WebService static void setLastLogin(string cMemberId, DateTime lastLoginDate)
    {
        if(cm == null) cm = new CommunityManagement();
        cm.setLastLogin(cMemberId, DateTime.valueOf(lastLoginDate));
    }
    /*  Registration 2.0 changes */
    WebService static string getErrorPrefix(){
        if(cm == null) cm = new CommunityManagement();
        return cm.getErrorPrefix(); 
    }
    WebService static string getWarningPrefix(){
        if(cm == null) cm = new CommunityManagement();
        return cm.getWarningPrefix(); 
    }
    // This method returns the eligibility for the given email id
    Webservice static string getSubGroupId(string email,string communityId){
        if(cm == null) cm = new CommunityManagement();
        string subGrpId = cm.getSubGroupId(email, communityId);
        return subGrpId; 
        // Returns null if the community Id provided is not valid
        // Returns Subscription Group Id up on success
    }
    // This method is called druing new Registrant Creation
    WebService static string addCMemberAndSubscriptions(CommunityManagement.MemberRecord memRec, List<SubscriptionManagementAPI.SubscriptionDetails> subList){
        if(cm == null) cm = new CommunityManagement();
        string result = cm.addCMemberAndSubscriptions(memRec, subList);
        return result;
        // Returns cMemberId on success
        // Returns Error Message that starts with either 'sf_error:' or 'sf_warning:'
        // When it has 'sf_warning:' - the Registrant is created but it failed with either contact mapping or subscriptions
    }
    // This method is called from edit profile
    WebService static string setCMemberAndSubscriptions(String cMemberId, CommunityManagement.MemberRecord memRec, List<SubscriptionManagementAPI.SubscriptionDetails> subList){
        if(cm == null) cm = new CommunityManagement();
        string result = cm.setCMemberAndSubscriptions(cMemberId, memRec, subList);
        return result;
        // Returns cMemberId on success
        // Returns Error Message that starts with either 'sf_error:' or 'sf_warning:'
        // When it has 'sf_warning:' - the Registrant is created but it failed with either contact mapping or subscriptions
    }
}