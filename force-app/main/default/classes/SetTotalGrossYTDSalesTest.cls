@isTest
private class SetTotalGrossYTDSalesTest {

    static testMethod void myUnitTest() {
    	
    	Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
		sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
    	
    	Account ac = new Account(name = 'Test Account',BillingCountry = 'USA', channel__c = 'Institutional');
    	insert ac;
    	
    	Contact con = new Contact(lastname = 'Test Contact',accountId = ac.Id, MailingCountry = 'USA');
    	insert con; 
    	
    	Fund_Goal__c fg = new Fund_Goal__c();
    	insert fg;
    	
        SalesConnect__Contact_Portfolio_Breakdown__c sc = new SalesConnect__Contact_Portfolio_Breakdown__c();
        sc.Name = 'Test';
        sc.Fund_Goal__c =fg.Id; 
        sc.SalesConnect__Contact__c = con.Id;
        insert sc;
    }
}