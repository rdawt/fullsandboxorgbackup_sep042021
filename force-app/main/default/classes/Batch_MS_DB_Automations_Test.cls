@isTest
public with sharing class Batch_MS_DB_Automations_Test 
{
    @TestSetup
    static void makeData()
    {
        List<Channel__c> lstChannels = new List<Channel__c>();
        Channel__c financialAdvisorSFDC = new Channel__c(Channel_Name__c='Financial Advisor', Channel_Description__c='Financial Advisor', Status__c = 'Active',Channel_Type__c = 'SFDC',Allow_Quarterly_Analysis__c = true);
        lstChannels.add(financialAdvisorSFDC);
        insert lstChannels;

        List<Territory__c> lstTerritory = new List<Territory__c>();
        Territory__c nwBD = new Territory__c(Channel__c=financialAdvisorSFDC.Id,Territory_Name__c = 'NORTHWEST' ,Status__c = 'Active',Name='NORTHWEST' );
        lstTerritory.add(nwBD);
        insert lstTerritory;

        Account objAcc01 = (Account)SL_TestDataFactory.createSObject(new Account(Name='Morgan Stanley', Channel__c = 'Financial Advisor', 
                            BillingCountry='United States'), false);
        insert objAcc01;

        Contact conObj01 = (Contact)SL_TestDataFactory.createSObject(new Contact(LastName='Test Contact 01', Territory__c = nwBD.Name,
                            AccountId=objAcc01.Id,MailingCountry='United States', Email='testUser01@gmail.com'),false);
        insert conObj01;

        Contact conObj02 = (Contact)SL_TestDataFactory.createSObject(new Contact(LastName='Test Contact 02', Territory__c = nwBD.Name,
                            AccountId=objAcc01.Id,MailingCountry='United States', Email='testUser02@gmail.com'),false);
        insert conObj02;

        Fund__c fnd1=new Fund__c(Name='Biotech ETF (BBH)',Fund_Type__c='ETF',Fund_Ticker_Symbol__c='BBH');
        insert fnd1;

        Fund__c fnd2=new Fund__c(Name='Africa Index ETF (AFK)',Fund_Type__c='ETF',Fund_Ticker_Symbol__c='MOAT');
        insert fnd2;

        List<History_Contact_Portfolio_Breakdown__c> lstContactPBHistory = new List<History_Contact_Portfolio_Breakdown__c>();
        lstContactPBHistory.add((History_Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
                    new History_Contact_Portfolio_Breakdown__c(
                            SalesConnect_Current_Assets__c = 125000000,
                            SalesConnect_Contact__c= conObj01.Id, 
                            Fund_Lookup__c = fnd1.Id,
                            Firm_Branch_Lookup__c = objAcc01.Id,
                            Channel_Lookup__c = financialAdvisorSFDC.Id, 
                            Asset_Date__c = Date.newInstance(2021, 05, 15), 
                            Fund_Vehicle_Type__c='ETF',
                            Name = 'IIG',
                            Quarter__c = 'FQ2 FY 2021',
                            SFDC_External_Id__c = 'ExternalId001'), false));

        lstContactPBHistory.add((History_Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
                    new History_Contact_Portfolio_Breakdown__c(
                            SalesConnect_Current_Assets__c = 125000000,
                            SalesConnect_Contact__c= conObj01.Id, 
                            Fund_Lookup__c = fnd1.Id,
                            Firm_Branch_Lookup__c = objAcc01.Id,
                            Channel_Lookup__c = financialAdvisorSFDC.Id, 
                            Asset_Date__c = Date.newInstance(2021, 05, 15), 
                            Fund_Vehicle_Type__c='Mutual Fund',
                            Name = 'IIG',
                            Quarter__c = 'FQ2 FY 2021',
                            SFDC_External_Id__c = 'ExternalId002'), false));

        lstContactPBHistory.add((History_Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
                    new History_Contact_Portfolio_Breakdown__c(
                            SalesConnect_Current_Assets__c = 100000,
                            SalesConnect_Contact__c= conObj01.Id, 
                            Fund_Lookup__c = fnd1.Id,
                            //SFDC_Fund_Id__c = fnd1.Id, 
                            Firm_Branch_Lookup__c = objAcc01.Id,
                            Channel_Lookup__c = financialAdvisorSFDC.Id, 
                            //SFDC_Channel_Id__c =financialAdvisorSFDC.Id ,
                            Asset_Date__c = Date.newInstance(2021, 05, 15), 
                            Fund_Vehicle_Type__c='ETF',
                            Name = 'IIG',
                            Quarter__c = 'FQ2 FY 2021',
                            SFDC_External_Id__c = 'ExternalId003'), false));

        lstContactPBHistory.add((History_Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
                    new History_Contact_Portfolio_Breakdown__c(
                            SalesConnect_Current_Assets__c = 100000,
                            SalesConnect_Contact__c= conObj01.Id, 
                            Fund_Lookup__c = fnd1.Id,
                            //SFDC_Fund_Id__c = fnd1.Id, 
                            Firm_Branch_Lookup__c = objAcc01.Id,
                            Channel_Lookup__c = financialAdvisorSFDC.Id, 
                            //SFDC_Channel_Id__c =financialAdvisorSFDC.Id ,
                            Asset_Date__c = Date.newInstance(2021, 05, 15), 
                            Fund_Vehicle_Type__c='Mutual Fund',
                            Name = 'IIG',
                            Quarter__c = 'FQ2 FY 2021',
                            SFDC_External_Id__c = 'ExternalId004'), false));

        lstContactPBHistory.add((History_Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
                    new History_Contact_Portfolio_Breakdown__c(
                            SalesConnect_Current_Assets__c = 25000,
                            SalesConnect_Contact__c= conObj01.Id, 
                            Fund_Lookup__c = fnd1.Id,
                            //SFDC_Fund_Id__c = fnd1.Id, 
                            Firm_Branch_Lookup__c = objAcc01.Id,
                            Channel_Lookup__c = financialAdvisorSFDC.Id, 
                            //SFDC_Channel_Id__c =financialAdvisorSFDC.Id ,
                            Asset_Date__c = Date.newInstance(2021, 05, 15), 
                            Fund_Vehicle_Type__c='ETF',
                            Name = 'IIG',
                            Quarter__c = 'FQ2 FY 2021',
                            SFDC_External_Id__c = 'ExternalId005'), false));

        lstContactPBHistory.add((History_Contact_Portfolio_Breakdown__c)SL_TestDataFactory.createSObject(
                    new History_Contact_Portfolio_Breakdown__c(
                            SalesConnect_Current_Assets__c = 50000,
                            SalesConnect_Contact__c= conObj01.Id, 
                            Fund_Lookup__c = fnd1.Id,
                            //SFDC_Fund_Id__c = fnd1.Id, 
                            Firm_Branch_Lookup__c = objAcc01.Id,
                            Channel_Lookup__c = financialAdvisorSFDC.Id, 
                            //SFDC_Channel_Id__c =financialAdvisorSFDC.Id ,
                            Asset_Date__c = Date.newInstance(2021, 05, 15), 
                            Fund_Vehicle_Type__c='Mutual Fund',
                            Name = 'IIG',
                            Quarter__c = 'FQ2 FY 2021',
                            SFDC_External_Id__c = 'ExternalId006'), false));
        insert lstContactPBHistory;
    }

    @isTest
    public static void testBatch_MS_DB_Automations_Prospect()
    {
        Map<String, Dashboard_Automations_Filters__mdt> mapDashboard_Automations_Filters = Dashboard_Automations_Filters__mdt.getAll();

        Test.startTest();
            Database.executeBatch(new Batch_MS_DB_Automations('Prospect'), 1);
        Test.stopTest();
    }

    @isTest
    public static void testBatch_MS_DB_Automations_Client()
    {

        Test.startTest();
            Database.executeBatch(new Batch_MS_DB_Automations('Client'), 1);
        Test.stopTest();
    }

    @isTest
    public static void testBatch_MS_DB_Automations_Customer()
    {

        Test.startTest();
            Database.executeBatch(new Batch_MS_DB_Automations('Customer'), 1);
        Test.stopTest();
    }
}