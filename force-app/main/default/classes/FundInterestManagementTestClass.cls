@isTest
private class FundInterestManagementTestClass
{
    static testMethod void validateFundInterestManagement()
    {
      Account acc = new Account(Name = 'kjhireb',Channel__c='Proffessional',BillingCountry='USA');
      insert acc;
      Contact con=new Contact(FirstName='ABC',LastName='Test',Email='abc@aol.in', AccountId=acc.Id, MailingCountry='US',MF_Client__c='NDR',ETF_Client__c='AFK',Fund_Client_All__c='NDR,AFK');
      insert con;
      Fund__c fnd=new Fund__c(Name='Test',Fund_Type__c='MF',Fund_Group__c='International',Share_Class__c='A',Fund_Ticker_Symbol__c='MFTST');
      insert fnd;
      Fund__c fnd1=new Fund__c(Name='Test1',Fund_Type__c='ETF',Fund_Group__c='International',Fund_Ticker_Symbol__c='ETFTST1');
      insert fnd1;
      Fund__c fnd2=new Fund__c(Name='Test2',Fund_Type__c='MF',Fund_Group__c='International',Share_Class__c='C',Fund_Ticker_Symbol__c='MFTST1');
      insert fnd2;
      Fund__c fnd3=new Fund__c(Name='Test3',Fund_Type__c='ETF',Fund_Group__c='International',Fund_Ticker_Symbol__c='ETFTST');
      insert fnd3;
      Fund_Interest__c fndInt=new Fund_Interest__c(Contact__c=con.Id,Fund__c=fnd.Id,Level_Of_Interest__c='Interested',Account__c=acc.Id,Fund_Client__c=True);
      insert fndInt;
      Fund_Interest__c fndInt1=new Fund_Interest__c(Contact__c=con.Id,Fund__c=fnd1.Id,Level_Of_Interest__c='Interested',Account__c=acc.Id,Fund_Client__c=True);
      insert fndInt1;
      Fund_Interest__c fndInt2=new Fund_Interest__c(Contact__c=con.Id,Fund__c=fnd2.Id,Level_Of_Interest__c='NOT Interested',Account__c=acc.Id,Fund_Client__c=False);
      insert fndInt2;
      Fund_Interest__c fndInt3=new Fund_Interest__c(Contact__c=con.Id,Fund__c=fnd3.Id,Level_Of_Interest__c='NOT Interested',Account__c=acc.Id,Fund_Client__c=False);
      insert fndInt3;
      ApexPages.currentPage().getParameters().put('cId',con.Id);
      ApexPages.StandardController sc = new ApexPages.StandardController(fnd);
      FundInterestManagement fndcls=new FundInterestManagement(sc);
      fndcls.getIntLevelOptions();
      //fndcls.getFundTypeOptions();
      fndcls.GetFundInterest();
      FundInterestManagement.Recordset rd=new FundInterestManagement.Recordset();
      rd.FundId = fnd.Id;
      rd.FundInterestId = fndInt.Id;
      rd.Level_Of_Interest = 'NOT Interested';
      List<FundInterestManagement.Recordset> lstrec=new List<FundInterestManagement.Recordset>();
      lstrec.add(rd);
      fndcls.lstRSet = lstrec;
      FundInterestManagement.Recordset rd1=new FundInterestManagement.Recordset();
      rd1.FundInterestId = fndInt.Id;
      rd1.Level_Of_Interest = 'Interested';
      lstrec.add(rd1);
      FundInterestManagement.Recordset rd3=new FundInterestManagement.Recordset();
      rd3.FundInterestId = fndInt.Id;
      rd3.Level_Of_Interest = '--None--';
      lstrec.add(rd3);
      fndcls.setFundInterests(); 
      fndcls.cancelRecord();
      set<Id> conSet= new set<Id>();
      conSet.add(con.Id);
      fndcls.updateContact(conSet);    
    }
}