@isTest
private class PreventDeleteFundTest {

    static testMethod void test() {
    	try{
	        Account acc = new Account(Name = 'kjhireb', Channel__c = 'RIA', BillingCountry='US');
	        insert acc;
	        Contact con = new Contact(FirstName='ABC',LastName='Test',Email='abc@aol.in', AccountId=acc.Id, MailingCountry='US');
	        insert con;
	        Fund__c fnd = new Fund__c(Name='Test',Fund_Type__c='ETF',Fund_Group__c='International',Share_Class__c='1X');
	        insert fnd;
	        Fund_Interest__c fndInt=new Fund_Interest__c(Contact__c=con.Id,Fund__c=fnd.Id,Level_Of_Interest__c='Interested');
	        insert fndInt;
	        delete fnd;
    	}catch(Exception e){
    		system.debug('Could not delete fund....'+e.getMessage());
    	}
    }
}