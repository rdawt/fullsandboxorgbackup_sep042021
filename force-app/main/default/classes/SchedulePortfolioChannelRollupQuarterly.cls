public class SchedulePortfolioChannelRollupQuarterly implements Schedulable{

    public void execute(SchedulableContext sc)
    { 
        BatchPortfolioChannelRollupQuarterly batch = new BatchPortfolioChannelRollupQuarterly(true,new List<Id>(),'ETF');
        database.executebatch(batch,1); 
    }
}