public class SchedulePortfolioChannelRollupMonthly implements Schedulable {
   
    public void execute(SchedulableContext sc)
    { 
        BatchPortfolioChannelRollupMonthly batch = new BatchPortfolioChannelRollupMonthly(true,new List<Id>(),'ETF');
        database.executebatch(batch,1); 
    }
    
}