public class BatchContactTerritoryRuleGen implements Database.Batchable<sObject>, Database.Stateful{

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String strQuery = 'SELECT Id,Salesconnect_Contact_Id__c,Territory_Lookup__c FROM Contact_Rule_Information__c WHERE Status__c = \'Active\'';
        return Database.getQueryLocator(strQuery); 
    }

    public void execute(Database.BatchableContext bc, List<Contact_Rule_Information__c> records) {
        Map<String, Contact_Rule_Information__c> mapRules = new Map<String, Contact_Rule_Information__c>();
        for(Contact_Rule_Information__c record: records){
            mapRules.put(record.Salesconnect_Contact_Id__c,record);
        }
        List<Contact> lstContacts = [SELECT Id,Territory_From_Rule__c,SalesConnect__Contact_ID__c FROM Contact WHERE SalesConnect__Contact_ID__c In :mapRules.keySet()];
        for(Contact con : lstContacts){
            con.Territory_From_Rule__c = mapRules.get(con.SalesConnect__Contact_ID__c).Territory_Lookup__c;
        }
        update lstContacts;
    }

    public void finish(Database.BatchableContext bc) {

    }
}