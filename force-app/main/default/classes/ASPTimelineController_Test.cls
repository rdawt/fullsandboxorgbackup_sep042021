/*
Author: Sheetal Chougule
Test class for: ASP_ActivityTimelineController, ActvityTimelineModel & ActivityTimelineGroup
*/
@IsTest
private class ASPTimelineController_Test 
{
	private static integer TASK_COUNT = 5;
    private static integer CALL_COUNT = 5;
    private static integer EVENT_COUNT = 5;
    private static integer EMAIL_COUNT = 5;
    
    private static final String FIRM_ACCOUNT_RECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Firm').getRecordTypeId();
    private static final String BRANCH_ACCOUNT_RECORDTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Branch').getRecordTypeId();

    @testSetup static void setup(){
		//Insert Accounts
        List<Account> lstAccounts = new List<Account> {new Account(RecordtypeId=BRANCH_ACCOUNT_RECORDTYPEID, BillingCountry='United States',Name='Test Account 1', AccountNumber='A1', AnnualRevenue=12345.67)};
		insert lstAccounts;

        List<Contact> lstContacts = new List<Contact> {new Contact(LastName='Test Contact 1', AccountId=lstAccounts[0].Id,MailingCountry='United States')};
        lstContacts.add(new Contact(LastName='Test Contact 2', AccountId=lstAccounts[0].Id,MailingCountry='United States'));
		insert lstContacts;

        List<ASP_Team__c> lstTeams = new List<ASP_Team__c> {new ASP_Team__c(Name = 'Test Team 1.0',ASP_Source__c='Salesforce',ASP_Status__c='Active',Discovery_Team_ID__c = '12312356')};//
        insert lstTeams;
        List<ASP_Team_Member__c> lstTeamMembers = new List<ASP_Team_Member__c>();
        ASP_Team_Member__c teamMember;
        for(integer iCount= 0;iCount<2;iCount++)
        {
            teamMember = new ASP_Team_Member__c();
            teamMember.ASP_Member_Role__c = 'Advisor';
            teamMember.ASP_Source__c = 'Salesforce';
            teamMember.ASP_Status__c = 'Active';
            teamMember.ASP_Team_Member__c = lstContacts[iCount].Id;
            teamMember.Teams__c = lstTeams[0].Id;
            lstTeamMembers.add(teamMember);          
        }
        insert lstTeamMembers;


        //Insert Activities

        //Add Tasks
        List<Task> lstTasks = new List<Task>();
        lstTasks.addAll(addTasks(TASK_COUNT, 'Task', lstAccounts[0].Id, lstContacts[0].Id));
        lstTasks.addAll(addTasks(TASK_COUNT, 'Task', lstAccounts[0].Id, lstContacts[1].Id));
        lstTasks.addAll(addTasks(TASK_COUNT, 'Email', lstAccounts[0].Id, lstContacts[1].Id)); 

        lstTasks.addAll(addTasks(CALL_COUNT, 'Call', lstAccounts[0].Id, lstContacts[0].Id));              
        lstTasks.addAll(addTasks(CALL_COUNT, 'Call', lstAccounts[0].Id, lstContacts[1].Id));
        insert lstTasks;

        //Add Events
        List<Event> lstEvents = new List<Event>();
        lstEvents.addAll(addEvents(EVENT_COUNT, lstAccounts[0].Id, lstContacts[0].Id));
        lstEvents.addAll(addEvents(EVENT_COUNT, lstAccounts[0].Id, lstContacts[1].Id));
        insert lstEvents;

        List<Task> emailTasks = [Select Id from Task where TaskSubtype =: 'Email'];
       
        insertEmails(EMAIL_COUNT, lstAccounts[0].Id, lstContacts[0].Id);
        insertEmails(EMAIL_COUNT, lstAccounts[0].Id, lstContacts[1].Id);
	}
    
    @IsTest
    private static void testActivitiesForTeams_IncludeChildren(){
        List<ASP_Team__c> lstTeams = [SELECT Id FROM ASP_Team__c];

        Test.startTest();
        List<ActivityTimelineGroup> groups = ASP_ActivityTimelineController.getActivityTimeline(lstTeams[0].Id, true);
        //Calculate the months for the closed task groupings
        //Add 1 to include the current month in calculations
        Integer maxCount = Math.max(Math.max(TASK_COUNT, CALL_COUNT), EMAIL_COUNT);
        Integer monthsForGrouping = Date.today().month() - Date.today().addDays(maxCount*-10).month() + 1;

        List<ActivityTimelineModel> lstActivities = new List<ActivityTimelineModel>();
        For(ActivityTimelineGroup groupObj : groups){
            lstActivities.addAll(groupObj.items);
        }
        System.assertEquals(TASK_COUNT*2 + CALL_COUNT*2 + EVENT_COUNT*2, lstActivities.size());

        Test.stopTest();
    }

    @IsTest
    private static void testActivitiesForAccount_IncludeChildren(){
        List<Account> lstAccounts = [SELECT Id FROM Account];

        Test.startTest();
        List<ActivityTimelineGroup> groups = ASP_ActivityTimelineController.getActivityTimeline(lstAccounts[0].Id, true);
        //Calculate the months for the closed task groupings
        //Add 1 to include the current month in calculations
        Integer maxCount = Math.max(Math.max(TASK_COUNT, CALL_COUNT), EMAIL_COUNT);
        Integer monthsForGrouping = Date.today().month() - Date.today().addDays(maxCount*-10).month() + 1;

        //Assert the groups size - add 1 for the upcoming tasks grouping
        //System.assertEquals(monthsForGrouping + 1, groups.size());

        List<ActivityTimelineModel> lstActivities = new List<ActivityTimelineModel>();
        For(ActivityTimelineGroup groupObj : groups){
            lstActivities.addAll(groupObj.items);
        }
        System.assertEquals(TASK_COUNT*2 + CALL_COUNT*2 + EVENT_COUNT*2 + EMAIL_COUNT*2, lstActivities.size());

        Test.stopTest();
    }
    
    private static List<Task> addTasks(Integer numberOfTasks, String subType, Id whatId, Id whoId){
        String closedStatus, openStatus;
        For(TaskStatus ts : [SELECT MasterLabel, IsClosed FROM TaskStatus]){
            if(ts.IsClosed){
                closedStatus = ts.MasterLabel;
            } else {
                openStatus = ts.MasterLabel;
            }
        }

        List<Task> lstTasks = new List<Task>();
        For(Integer i=0; i<numberOfTasks; i++){
            Task newTask = new Task(
                Subject = 'Test Subject (' + subType + ') ' + i, 
                TaskSubType = subType,
                ActivityDate = Date.today().addDays(i*-10),
                Description = 'Test Description ' + i,
				Status = Math.mod(i, 2) == 0 ? closedStatus : openStatus
            );
            
            if(whatId != null){
                newTask.WhatId = whatId;
            }
            if(whoId != null){
                newTask.WhoId = whoId;
            }
            
            lstTasks.add(newTask);
		}
        
        return lstTasks;
    }

    private static List<Event> addEvents(Integer numberOfEvents, Id whatId, Id whoId){
        List<Event> lstEvents = new List<Event>();
        For(Integer i=0; i<numberOfEvents; i++){
            Event newEvent = new Event(
                Subject = 'Test Event ' + i, 
                EventSubType = 'Event',
                StartDateTime = System.now(),
                EndDateTime = System.now().addHours(1),
                Description = 'Test Description ' + i
            );
            
            if(whatId != null){
                newEvent.WhatId = whatId;
            }
            if(whoId != null){
                newEvent.WhoId = whoId;
            }
            
            lstEvents.add(newEvent);
		}
        
        return lstEvents;
    }

    private static void insertEmails(Integer numberOfEmails, Id whatId, Id whoId){
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        For(Integer i=0; i <numberOfEmails; i++){
            EmailMessage newEmail = new EmailMessage(
                RelatedToId = whatId,
                MessageDate = Date.today().addDays(i*-10),
                Subject = 'Test Email ' + i, 
                TextBody = 'Test Description ' + i,
                FromName = UserInfo.getName(),
                FromAddress = UserInfo.getUserEmail(),
                ToAddress = UserInfo.getUserEmail()
            );            
            lstEmails.add(newEmail);
           
		}
        Database.insert(lstEmails);

        List<EmailMessageRelation> lstRelations = new List<EmailMessageRelation>();
        For(EmailMessage emailObj : lstEmails){
            EmailMessageRelation fromRelationObj = new EmailMessageRelation(
                EmailMessageId = emailObj.Id,
                RelationType = 'FromAddress',
                RelationId = whoId,
                RelationAddress = UserInfo.getUserEmail()
            );
            lstRelations.add(fromRelationObj);

            EmailMessageRelation toRelationObj = new EmailMessageRelation(
                EmailMessageId = emailObj.Id,
                RelationType = 'ToAddress',
                RelationId = whoId,
                RelationAddress = UserInfo.getUserEmail()
            );
            lstRelations.add(toRelationObj);
        }

        Database.insert(lstRelations);
    }


}