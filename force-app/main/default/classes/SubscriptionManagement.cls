global class SubscriptionManagement 
{
    public map<String,Boolean> checkOldSubscribeVal=new map<String,Boolean>();
    public list<RecordSet> lstRSet{get;set;}
    public string strFirstName{get;set;}
    public string strLastName{get;set;}
    public String strEm{get;set;}
    public Boolean chkemailvalid{get;set;}
    public Boolean chkStatus{get;set;}
    public Boolean isSubscribed{get;set;}
    public Boolean hasEmailOpt{get;set;}
    private list<RecordSet> lstRSetBackup;
    private String conId{get;set;}
    public String subGrId{get;set;}
    private string cId;
    // flag to check if there is any un-subscribe event occured
    // PP mail should not be sent on un-subscribe event even if PPSent Dates are null due to migration(or Grandfathering) mistake.
    private map<String,Boolean> isUnSubEventmap = new map<String,Boolean>();
    private Boolean isUnSubscribeEvent = true;
    
    // Constuctor - to initialise the object
    public SubscriptionManagement() 
    {
        try
        {
            Contact con=[Select Id,Email,FirstName,LastName,HasOptedOutOfEmail,Subscription_Group__c from Contact Where Id=:ApexPages.currentPage().getParameters().get('cId') LIMIT 1];
            if(con != null)
            {
            	system.debug('Contact.........'+con);
                conId = con.Id;
                strFirstName = con.FirstName;
                strLastName = con.LastName;
                subGrId = con.Subscription_Group__c;
                if(con.Email!=null && !(con.Email.equals('')))
                    strEm=con.Email;
                else
                    strEm=null;
                if(con.HasOptedOutOfEmail!=true)
                    hasEmailOpt=false;
                else
                    hasEmailOpt=true;
            }
        }
        catch(Exception e)
        {
            system.debug('Exception initialising SM Class............'+e.getMessage());
            // To handle invalid or deleted contact id or the page is loaded with no contact id provided
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
             'Exception Occured due to Invalid Contact Id : '+e.getMessage());
            ApexPages.addMessage(msg);
        }
    }

    // This Getsubscriptions method will list all the subscriptions related to that subscriptiongroupId
    public list<Recordset> GetSubscriptionsInt()
    {
        lstRSet = new list<RecordSet>();
        list<Subscription_Group_Subscription__c> lstSGS =new list<Subscription_Group_Subscription__c>();
        lstSGS = [SELECT s.Subscription__r.Subscription_Type__r.Id, s.Sort_Order_For_Form__c,s.Subscription__r.Name,s.Subscription__r.Subscription_Type__r.Name, 
                s.Subscription__r.Subscription_Display_Name__c, s.Subscription__r.Id,s.Subscription_Group__r.Id, s.Subscription_Group__r.Name,s.Subscription__r.IsActive__c
                FROM Subscription_Group_Subscription__c s WHERE s.Subscription_Group__c =: subGrId and s.Subscription__r.IsActive__c=:true order by s.Subscription__r.Name,s.Sort_Order_For_Form__c];
        system.debug('Sub Grp Sub List.........'+lstSGS);
        // For every subscripion group id of the given set of contact ids - 
        // get the related subs_gr_sub and put them to the method setSubscription 
        list<Id> setSubscription = new list<Id>();
        if(!lstSGS.isEmpty())
            for(Subscription_Group_Subscription__c sgs: lstSGS)
                setSubscription.add(sgs.Subscription__r.Id);
        
        list<Subscription_Member__c> lstSM = [SELECT s.Id, s.Subscription__c,s.Subscription_Unsubscribed_Date__c,s.Subscribed__c, s.Contact__c 
                                                FROM Subscription_Member__c s WHERE s.Contact__c =: conId ];
        system.debug('Subscription Member List.........'+lstSM);
        //Checking if either of this is true then we are displaying a custom message on the VF page otherwise list the available subscriptions
        if(hasEmailOpt!=true && (strEm!=null && !(strEm.equals(''))))
        {
            if(!lstSGS.isEmpty())
            {
                if(!lstRSet.isEmpty()) 
                lstRSet.clear();
                for(Subscription_Group_Subscription__c sgs: lstSGS)
                {
                    //loop through every record in the subscription_groupd_subscription for the given set of subscription id in sub_grp 
                    //and put them into record set which will be returned
                    RecordSet rs = new RecordSet();
                    rs.SubGroup = sgs.Subscription_Group__r.Name;
                    rs.SubGroupId = sgs.Subscription_Group__r.Id;
                    rs.SubType = sgs.Subscription__r.Subscription_Type__r.Name;
                    rs.SubTypeId = sgs.Subscription__r.Subscription_Type__r.Id;
                    rs.SubName = sgs.Subscription__r.Name;
                    rs.SubId = sgs.Subscription__r.Id;
                    for(Subscription_Member__c sm: lstSM)
                    {
                        if(sm.Subscription__c == sgs.Subscription__r.Id)
                        {
                            rs.Subscribed = sm.Subscribed__c;
                            rs.subUnsubDate=sm.Subscription_Unsubscribed_Date__c;
                            rs.SubMemId = sm.Id;
                        }
                    }
                    lstRSet.add(rs);
                }
                lstRSetBackup = new list<RecordSet>();
                lstRSetBackup = lstRSet.clone();
                checkOldSubscribeVal=new map<String,Boolean>();
                for(RecordSet rec:lstRSetBackup)
                    checkOldSubscribeVal.put(rec.SubId,rec.Subscribed);
				system.debug('Return List.........'+lstRSet);
                return lstRSet;
            }
        }
        return null;
    }

    public PageReference setSubscribtion()
    {
        map<String, Subscription_Member__c> mapChanges = new map<String, Subscription_Member__c>();
        list<Subscription_Member__c> lstRecInsert = new list<Subscription_Member__c>();
        list<Subscription_Member__c> lstRecUpdate = new list<Subscription_Member__c>();
        list<Subscription_Member__c> lstRec = new list<Subscription_Member__c>();
        
        lstRec = [Select Id,Subscription__c,Subscribed__c,Subscription_Unsubscribed_Date__c,Contact__c,Composite_Key_ExternalId_SId_CId_OR_LId__c from Subscription_Member__c where Contact__c=:conId];
        system.debug('Record List.........'+lstRec);
        for(Subscription_Member__c subMem : lstRec)
            mapChanges.put(subMem.Subscription__c, subMem);
        
        for(RecordSet sm : lstRSet)
        {
            if(mapChanges.containsKey(sm.SubId))
            { // Update
                if(checkOldSubscribeVal.size()>0)
                {
                    Subscription_Member__c smem = new Subscription_Member__c(Id = mapChanges.get(sm.SubId).Id);
                    smem.Subscribed__c = sm.Subscribed;
                    if(checkOldSubscribeVal.get(sm.SubId) == false && sm.Subscribed == true)
                    { // if previously un-subscribed & subscribing now
                        isUnSubEventmap.put(sm.SubId,false); // PP email can be sent in case of grand-fathering mistakes
                        smem.Subscription_Unsubscribed_Date__c = null;
                        lstRecUpdate.add(smem);
                        isSubscribed = true;
                    }
                    if(checkOldSubscribeVal.get(sm.SubId) == true && sm.Subscribed == false)
                    { // if previously subscribed & un-subscribing now
                        isUnSubEventmap.put(sm.SubId,true); // This is un-subscribe event. So PP email should not be sent
                        smem.Subscription_Unsubscribed_Date__c = system.now();
                        lstRecUpdate.add(smem);
                    }
                }
            }
            else
            { // Insert
                if(sm.Subscribed == true){
                    isUnSubEventmap.put(sm.SubId,false); // This is new subscription. So PP email should be sent
                    isSubscribed = true;
                    Subscription_Member__c smem = new Subscription_Member__c();
                    smem.Contact__c = conId;
                    smem.Subscription__c = sm.SubId;
                    smem.Subscribed__c = sm.Subscribed;
                    lstRecInsert.add(smem);
                }               
            }
        }
         
        for(RecordSet rs : lstRSet)
            if(isUnSubEventmap.get(rs.SubId)==false)
                isUnSubscribeEvent = false;
        
        if(!lstRecUpdate.isEmpty())
        { // actual Update on Database
            Database.SaveResult[] sr = Database.update(lstRecUpdate,false);
            for(Database.SaveResult srRec : sr)
            {
                if(srRec.isSuccess())
                    chkStatus=true; 
                else 
                { // If the acutal update to databse fails, give the message back to the user
                    Database.Error err = srRec.getErrors()[0];
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage());
                    ApexPages.addMessage(msg);
                    chkStatus=False;
                    return null; 
                }
            }
        }
        if(!lstRecInsert.isEmpty())
        { // actual Insert on the Database
            Database.SaveResult[] sr = Database.Insert(lstRecInsert,false);
            for(Database.SaveResult srRec : sr)
            {
                if(srRec.isSuccess())
                    chkStatus=true; 
                else 
                { // If the actual insert fails, give the message back to the user
                    Database.Error err = srRec.getErrors()[0];
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage());
                    ApexPages.addMessage(msg);
                    chkStatus=False;
                    return null; 
                }
            }
        }
        /*boolean b = UpdateContactPrivacy(conId);
        if (b==false){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
            'Error occured while sending the Privacy Policy Email for new Subscription or while updating the contact Privacy Policy Sent Date');
            ApexPages.addMessage(msg);
        }*/
        PageReference contPage = new PageReference('/'+conId);
        return contPage;
    }

/********************** PRIVACY POLICY WILL NOT BE SENT FROM SALES FORCE HEREAFTER - 1/9/14
    //Method to update the Privacy Policy sent date field in the Contact object & send the PP email for new subscribers
    public boolean UpdateContactPrivacy(String conId)
    {
        Contact conRecord;
        User usr;
        list<Contact> lstContact=new list<Contact>();
        
        try{
            if(conId != null)
                conRecord=[Select Id,OwnerId,Email,Privacy_Policy_Sent_Date__c,Privacy_Policy_Web_Ack_Date__c from Contact Where Id=:conId];
            
            if(conRecord.OwnerId != null)
                usr = [Select Name from User where IsActive=: true and Id =: conRecord.OwnerId];
            
            if(conRecord==null)
                return false;
            
            //This condition checks the PP email functionality and sends the email for the first time when it is subscribed and updates the date
            if(conRecord.Privacy_Policy_Sent_Date__c==null && conRecord.Privacy_Policy_Web_Ack_Date__c==null && chkStatus==true && isSubscribed==true && isUnSubscribeEvent==false)
            {
                Contact conData=new Contact(Id=conRecord.Id);
                conData.Privacy_Policy_Sent_Date__c=System.now();
                if(lstContact.size()==0)
                    lstContact.add(conData);
                
                //This is where we are calling the custom setting email template and sending the privacy policy email accordingly
                list<Privacy_policy_Email_Template__c> tempName = Privacy_policy_Email_Template__c.getall().values();
                for(EmailTemplate etemp : [select id from EmailTemplate where Name=:tempName[0].Name and isactive=TRUE ])
                {
                    messaging.Singleemailmessage mail=new messaging.Singleemailmessage();
                    mail.setSenderDisplayName(usr.Name);
                    mail.setReplyTo(conRecord.Email);
                    mail.setTargetObjectId(conRecord.Id);
                    mail.setTemplateId(etemp.id);
                    messaging.sendEmail(new messaging.Singleemailmessage[]{mail});
                }
            }
            if(isSubscribed==true && chkStatus==false)
            {
                Contact conData=new Contact(Id=conRecord.Id);
                conData.Privacy_Policy_Sent_Date__c=System.now();
                lstContact.add(conData);
            }
            if(!lstContact.isEmpty())
                update lstContact;
        }
        catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
             'Exception occured while sending the Privacy Policy Email for new Subscription or while updating the contact Privacy Policy Sent Date\n'+e.getMessage());
            ApexPages.addMessage(msg);
            return false;
        }
        return true;
    }
 */ 
    // This method takes back to previous page in cancel action. 
    public PageReference cancelRecord() {
        PageReference contPage = new PageReference('/'+conId);
        return contPage;
    }
    
    /*  WRAPPER CLASS   */
    public class Recordset {
        public Boolean Subscribed {get; set;} //Sub Mem --> Subscribed
        public String SubMemId {get; set;} //Sub Mem --> Sub Mem Id
        public String SubId {get; set;} //Sub Mem --> Subscription --> Id
        public String SubName {get; set;} //Sub Mem --> Subscription --> Name
        public DateTime subUnsubDate{get;set;}
        public String SubTypeId{get; set;} //Sub Mem --> Subscription --> Subscription Type --> Id
        public String SubType{get; set;} //Sub Mem --> Subscription --> Subscription Type --> Name
        public String SubGroupId{get; set;} //Sub Mem --> Subscription --> SGS --> SG --> Id
        public String SubGroup{get; set;} //Sub Mem --> Subscription --> SGS --> SG --> Name
    }

}