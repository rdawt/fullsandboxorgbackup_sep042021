public class BatchPortfolioChannelRollupMonthly implements Database.Batchable<sObject>, Database.Stateful{
    
    String fundVehicleType;
    String strYear = '2021';
    Integer iFundBatchSize = 10;
    //Integer iQuarter = 1;
    String strMonth = '0';
    Map<String, Id> mapETFChannels = new Map<String, Id>();
    Map<String, Id> mapMFChannels = new Map<String, Id>();
    PortfolioChannelRollupService service = new PortfolioChannelRollupService();
    List<Id> lstProcessingFundIds = new List<Id>();
    List<Id> lstPendingFundIds = new List<Id>();

    public BatchPortfolioChannelRollupMonthly(Boolean isInitialBatch,List<Id> lstPendingFunds,String strFundVehicleType) {
        //mapETFChannels = service.getChannelMap('ETF');
        //mapMFChannels = service.getChannelMap('Mutual Fund');
        fundVehicleType = strFundVehicleType;
        if(isInitialBatch && lstPendingFunds.isEmpty()){
            List<Fund__c> lstFunds = [SELECT Id FROM Fund__c WHERE Fund_Vehicle_Type__c =:fundVehicleType AND Fund_Status__c = 'Open' AND BU_Region__c = 'USA'];//'Mutual Fund',
            for(Fund__c fund: lstFunds){
                if(lstProcessingFundIds.size() <= iFundBatchSize){
                    lstProcessingFundIds.add(fund.Id);
                }else{
                    lstPendingFundIds.add(fund.Id);
                }               
            }
        }else if(!(isInitialBatch && lstPendingFunds.isEmpty())){
            for(Id fundId: lstPendingFunds){
                if(lstProcessingFundIds.size() <= iFundBatchSize){
                    lstProcessingFundIds.add(fundId);
                }else{
                    lstPendingFundIds.add(fundId);
                }      
            }
        }
        Integer iCurrentYear = Date.Today().year();
        strYear = String.valueOf(iCurrentYear);
        Integer iCurrentMonth = Date.Today().Month();
        strMonth = String.valueOf(iCurrentMonth);

        System.debug('Processing : '+lstProcessingFundIds.size());
        System.debug('Pending : '+lstPendingFundIds.size());
        System.debug('--------------------------------------');
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String strQuery = 'SELECT Id FROM Channel__c WHERE Allow_Quarterly_Analysis__c = true AND Status__c = \'Active\'';
        return Database.getQueryLocator(strQuery);
    }

    public void execute(Database.BatchableContext bc, List<Channel__c> records) {
        for(Channel__c channel : records){
            service.upsertRollupRecords(lstProcessingFundIds,channel.Id,strYear,0,fundVehicleType,strMonth); 
        }
    }

    public void finish(Database.BatchableContext bc) {
        if(!lstPendingFundIds.isEmpty() && fundVehicleType == 'ETF'){
            BatchPortfolioChannelRollupMonthly batch = new BatchPortfolioChannelRollupMonthly(false,lstPendingFundIds,fundVehicleType);
            database.executebatch(batch,1); 
        }else if(lstPendingFundIds.isEmpty() && fundVehicleType == 'ETF'){
            BatchPortfolioChannelRollupMonthly batch = new BatchPortfolioChannelRollupMonthly(true,new List<Id>(),'Mutual Fund');
            database.executebatch(batch,1); 
        }else if(!lstPendingFundIds.isEmpty() && fundVehicleType == 'Mutual Fund'){
            BatchPortfolioChannelRollupMonthly batch = new BatchPortfolioChannelRollupMonthly(true,lstPendingFundIds,fundVehicleType);
            database.executebatch(batch,1); 
        }
    }
}