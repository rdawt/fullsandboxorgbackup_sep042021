/**
    Author: Vidhya Krishnan
    Created Date: 3/11/2013
    Description: Schedule Apex Code to update the Activity
                    Marketo Open Email Activities are deleted
                    
Modified : 10/16/14
 - Changed the filter from "Summary Recap contains Opened Email" to "Summary Recap does not contact Clicked Email"
 - added Today filter in the query to avoid Too Many Query error  
*/
global class updateMarketoTask implements Schedulable {
    global void execute(SchedulableContext SC){
        list<Task> marketoTask = new list<Task>();
        list<Task> marketoDeleteList = new list<Task>();
        string marketoAPIUserId = '005A00000032QAD';
        string msg;
        try{
            /*
            // Execute this code from developer Console to schedule this apex class hourly
            String CRON_EXP = '0 0 * * * ?';
            updateMarketoTask umt = new updateMarketoTask();
            system.schedule('Delete Marketo Tasks Hourly', CRON_EXP, umt);
            
            // To monitor the progress use this code
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                FROM CronTrigger WHERE Id = :ctx.getTriggerId()];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
            */
            if(Test.isRunningTest()){
            marketoTask = [select id, Summary_Recap__c from Task where ownerId =: marketoAPIUserId and Status != 'Clicked Link'];
            }
            else{
            marketoTask = [select id, Summary_Recap__c from Task where ownerId =: marketoAPIUserId and Status != 'Clicked Link' limit 50000];
            }
            //AggregateResult[] delCount = [select count(id) from Task where ownerId =: marketoAPIUserId and Status != 'Clicked Link' ];
            If ( marketoTask .size() > 10000) {
                SendEmailNotification se = new SendEmailNotification('MKTO Send & Open Count for Deletion has high delete count: ' + marketoTask .size() );

           }
            
            system.debug('List of Marketo Task......'+marketoTask);
            for(Task t : marketoTask){
                if(t.Summary_Recap__c != null && !t.Summary_Recap__c.contains('Clicked Email:')){
                    if ((marketoDeleteList.size()) >= 10000)break;
                    marketoDeleteList.add(t);
                }
            }
            system.debug('List of Marketo Task......'+marketoDeleteList);
            if(!marketoDeleteList.isEmpty())
                 delete marketoDeleteList;
            /*
            if(!marketoOpenTask.isEmpty()){
                SendEmailNotification se = new SendEmailNotification('Success: Scheduled Apex for updating Marketo Related Task completed');
                msg = 'Scheduled Apex for updating Marketo Related Task completed for the day ' 
                        +system.today()+'. \nNo. of Marketo open Email task deleted are: '+marketoOpenTask.size();
                se.sendEmail(msg);
            }*/
        }catch(Exception e){SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for deleting Marketo Related Task Failed');msg = 'Exception Occurred while updating the Makreto Related Task. \n '+e.getMessage() + ' ' + marketoDeleteList;se.sendEmail(msg);
        }
    }
    
 /*   @isTest
    static void testUpdateMarketoTest() {
        try{
            Test.startTest();
            string jobId = System.schedule('testTaskScheduleApex', '0 0 0 5 7 ? 2022',  new updateMarketoTask());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals('0 0 0 5 7 ? 2022', ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            System.assertEquals('2022-07-05 00:00:00',String.valueOf(ct.NextFireTime));
            
            Account acc = new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA');
            insert acc;
            Contact con = new Contact(FirstName = 'ABC', LastName = 'ABC', Email = 'abc@gmail.com', AccountId = acc.Id, MailingCountry = 'USA');
            insert con;
            Task marketoTask1 = new Task (ownerid = '005A00000032QAD', whatid = acc.id, activityDate = date.today(), status = 'Completed', whoid = con.id, Summary_Recap__c = 'Opened Email:Marketo Test', Description = 'abc');
            insert marketoTask1;
            Task marketoTask2 = new Task (ownerid = '005A00000032QAD', whatid = acc.id, activityDate = date.today(), status = 'Completed', whoid = con.id, Summary_Recap__c = 'Send Email:Marketo Test', Description = 'abc');
            insert marketoTask2;
            
            Test.stopTest();
        }catch(Exception e){}
    }
   */
}