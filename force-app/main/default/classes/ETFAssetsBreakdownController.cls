public with sharing class ETFAssetsBreakdownController {
   
    @AuraEnabled(cacheable=true)
    public static List<ETFAssetsSummaryWrapper> getETFAssetsByFund(Date assetsDate){
        List<ETFAssetsSummaryWrapper> lstETFAssetsSummary = new List<ETFAssetsSummaryWrapper>();
        List<AggregateResult> aggregateResult = [SELECT count(Id) recordCount,sum(Monthly_Asset__c) totalAssets,Asset_Date__c,SFDCFundID__c,SFDCFundID__r.Name fundName,SFDCFundID__r.Fund_Ticker_Symbol__c fundTicker
                                                FROM ETF_Assets_By_Firm__c 
                                                WHERE Asset_Date__c = 2021-06-30 AND Insurance_Flow_Opportunity__c = false AND CreatedDate != last_week
                                                GROUP BY Asset_Date__c,SFDCFundID__c,SFDCFundID__r.Name,SFDCFundID__r.Fund_Ticker_Symbol__c
                                                ORDER BY sum(Monthly_Asset__c) DESC];
        
        if(!aggregateResult.isEmpty()){
            for (AggregateResult result : aggregateResult)  {
                lstETFAssetsSummary.add(new ETFAssetsSummaryWrapper((Id)result.get('SFDCFundID__c'),(String)result.get('fundName'),(String)result.get('fundTicker'),(Date)result.get('Asset_Date__c'),(Decimal)result.get('totalAssets'),(Integer)result.get('recordCount')));
            }
        }
       return lstETFAssetsSummary; 
    }

    @AuraEnabled(cacheable=true)
    public static List<ETFAssetsSummaryWrapper> getETFAssetsByFundChannel(Date assetsDate,Id fundId){
        List<ETFAssetsSummaryWrapper> lstETFAssetsSummary = new List<ETFAssetsSummaryWrapper>();
        List<AggregateResult> aggregateResult = [SELECT Account__r.Channel__c channelName,SUM(Monthly_Asset__c) totalAssets,count(Id) recordCount,Asset_Date__c
                                                FROM ETF_Assets_By_Firm__c 
                                                WHERE Asset_Date__c = 2021-06-30 AND Insurance_Flow_Opportunity__c = false AND SFDCFundID__c = :fundId
                                                GROUP BY Account__r.Channel__c,Asset_Date__c
                                                ORDER BY sum(Monthly_Asset__c) DESC];
        
        if(!aggregateResult.isEmpty()){
            for (AggregateResult result : aggregateResult)  {
                lstETFAssetsSummary.add(new ETFAssetsSummaryWrapper(fundId,(String)result.get('channelName'),(Date)result.get('Asset_Date__c'),(Decimal)result.get('totalAssets'),(Integer)result.get('recordCount')));
            }
        }
       return lstETFAssetsSummary; 
    }

    @AuraEnabled(cacheable=true) 
    public static List<ETFAssetsSummaryWrapper> getETFAssetsByFundChannelFirm(Date assetsDate,Id fundId,String channelName){
        List<ETFAssetsSummaryWrapper> lstETFAssetsSummary = new List<ETFAssetsSummaryWrapper>();
        List<ETF_Assets_By_Firm__c> lstEtfAssets = [SELECT Id,Account__c,Account__r.Channel__c,Account__r.Name,Asset_Date__c,Monthly_Asset__c,Broadridge_Firm_Name__c,Name,SFDCFundID__r.Name,SFDCFundID__r.Fund_Ticker_Symbol__c 
                        FROM ETF_Assets_By_Firm__c 
                        WHERE Asset_Date__c = 2021-06-30 AND SFDCFundID__c = :fundId AND Insurance_Flow_Opportunity__c = false AND Account__r.Channel__c = :channelName
                        ORDER BY Monthly_Asset__c DESC];
        
        if(!lstEtfAssets.isEmpty()){
            for (ETF_Assets_By_Firm__c result : lstEtfAssets)  {
                lstETFAssetsSummary.add(new ETFAssetsSummaryWrapper(fundId,result.Account__r.Channel__c,result.Id,result.Account__c,result.Account__r.Name,result.Asset_Date__c,result.Monthly_Asset__c));
            }
        }
        return lstETFAssetsSummary; 
    }

    @AuraEnabled(cacheable=true) 
    public static List<ETFAssetsSummaryWrapper> getETFAssetsByFundChannelBroadridgeFirm(Date assetsDate,Id etfAssetsByFundId){
        
        System.debug('etfAssetsByFundId --> '+etfAssetsByFundId);
        List<ETFAssetsSummaryWrapper> lstETFAssetsSummary = new List<ETFAssetsSummaryWrapper>();
        List<BR_Account_Asset__c > lstBrAccountAssets = [SELECT Account_Id__r.Name,Asset_Balance__c,Asset_Date__c,Channel__c,ETF_Assets_By_Firm_Id__c,Firm_Id__c,Id,Name,Office_Id__c,Product_Id__c,RecordType__c,Record_Type_SFDC_FirmLevel__c,Salesforce_Channel__c 
                                                        FROM BR_Account_Asset__c 
                                                        WHERE ETF_Assets_By_Firm_Id__c = :etfAssetsByFundId
                                                        ORDER BY Asset_Balance__c DESC];
        
        if(!lstBrAccountAssets.isEmpty()){
            for (BR_Account_Asset__c  result : lstBrAccountAssets)  {
                lstETFAssetsSummary.add(new ETFAssetsSummaryWrapper(result.Id,result.Account_Id__c,result.Account_Id__r.Name,result.Asset_Balance__c,result.Asset_Date__c));
            }
        }
        return lstETFAssetsSummary; 
    }

    public class ETFAssetsSummaryWrapper
    {
        @AuraEnabled
        public String Key;
        @AuraEnabled
        public Id fundId;
        @AuraEnabled
        public Decimal monthlyAssets;
        @AuraEnabled
        public Integer recordCount;
        @AuraEnabled
        public Date assetsDate;
        @AuraEnabled
        public String fundName;
        @AuraEnabled
        public string fundTickerSymbol;
        @AuraEnabled
        public string channelName;

        @AuraEnabled
        public string accountName;
        @AuraEnabled
        public Id accountId;
        @AuraEnabled
        public Id etfAssetsByFundId;

        public ETFAssetsSummaryWrapper(Id fundId,String fundName,String fundTicker,Date assetsDate,Decimal totalAssets,Integer recordCount){
            this.Key = fundId;
            this.fundId = fundId;
            this.fundName = fundName;
            this.fundTickerSymbol = fundTicker;
            this.assetsDate = assetsDate;
            this.monthlyAssets = totalAssets;
            this.recordCount = recordCount;
        }

        public ETFAssetsSummaryWrapper(Id fundId,String channelName,Date assetsDate,Decimal totalAssets,Integer recordCount){
            this.Key = fundId + '_' + channelName;  
            this.fundId = fundId;
            this.channelName = channelName;
            this.assetsDate = assetsDate;
            this.monthlyAssets = totalAssets;
            this.recordCount = recordCount;
        }

        public ETFAssetsSummaryWrapper(Id fundId,String channelName,Id etfAssetsByFundId,Id accountId,String accountName, Date assetsDate,Decimal totalAssets){
            this.Key = fundId + '_' + channelName + '_' + accountId;  
            this.etfAssetsByFundId = etfAssetsByFundId;
            this.accountId = accountId;
            this.accountName = accountName;
            this.fundId = fundId;
            this.channelName = channelName;
            this.assetsDate = assetsDate;
            this.monthlyAssets = totalAssets;
            this.recordCount = recordCount;
        }

        public ETFAssetsSummaryWrapper(Id brAccountAssetId,Id accountId,String accountName,Decimal totalAssets, Date assetsDate){
            this.Key = brAccountAssetId;  
            this.accountId = accountId;
            this.accountName = accountName;
            this.assetsDate = assetsDate;
            this.monthlyAssets = totalAssets;
        }


    }

}