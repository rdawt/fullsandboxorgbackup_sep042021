public class SL_AccountTriggerHandler {
    public Map<Id, Contact> mapContacts;    
    //public List<Contact> lstContacts;  // Commented out this variable as it is hitting limit for 500001 Query rows and collecting in list
    public SendEmailNotification se;
    public SL_AccountTriggerHandler(List<Account> lstNewAccounts){
        
        /*
        Set<Id> setAccountIds = new Set<Id>();
        
        mapContacts = new Map<Id, Contact>();
        
        //lstContacts = new List<Contact>();
        se  = new SendEmailNotification('Error: Account Trigger failed');
        
        for(Account objAccount : lstNewAccounts)
        {   
            setAccountIds.add(objAccount.Id);
        }
        
        if(setAccountIds.size() > 0)
        {
            for(Contact objContact: [SELECT Id, AccountId, OwnerId, MailingCountry, DB_Source__c, 
                                        Service_Level__c, AUM__c , SQ_NPos__c,                            
                                        SQ_NNeg__c, Channel_NEW__c 
                                        FROM Contact WHERE AccountId != NULL AND AccountId IN :setAccountIds])
            {
                mapContacts.put(objContact.Id, objContact);
            }       
        }
        */
    }
    
    // Commenting this code out as In Contact Trigger we have same functionality and this was getting called again when Contact 
    // is inserted and Count is updated on Account. 
    // To avoid this going in loop and already present login in Contact object, commenting out the code.
    public void onBeforeUpdate(List<Account> lstNewAccounts){
    /*
        Map<Id, Integer> mapAccId_NoOfContacts = new Map<Id, Integer>();
        for(Contact objContact : mapContacts.values()){
            if(!mapAccId_NoOfContacts.containsKey(objContact.AccountId))
                mapAccId_NoOfContacts.put(objContact.AccountId, 1);
            else
            {
                Integer intCurrentCount = mapAccId_NoOfContacts.get(objContact.AccountId);
                mapAccId_NoOfContacts.put(objContact.AccountId, intCurrentCount + 1);
            }
        }
        try{
            for (Account objAccount : lstNewAccounts){
                if(objAccount.BillingCountry != null){
                    if(mapAccId_NoOfContacts.containsKey(objAccount.Id))
                        objAccount.Number_of_Contacts__c = mapAccId_NoOfContacts.get(objAccount.Id);
                    else
                        objAccount.Number_of_Contacts__c = 0;
                }
            }
            
        }
        catch(Exception ex){
            se.sendEmail('Exception Occurred while updating Number of Contact Field : \n'+ex.getMessage()+lstNewAccounts);
        }
        
    */   
    
    system.debug('>>>>>>>>>>>>>>>This is for the deployment merge>>>>>>>>>>>>>>>>>');
    
    }
    
    // Commneted out the code to Sequoia
    public void onAfterUpdate(List<Account> lstNewAccounts, Map<Id,Account> mapOldAccounts){
        Set<String> de = new set<string>();
        Set<String> sq = new set<string>();
        //DynamicEligibility dehandler = new DynamicEligibility();
        /*
        for (Account objAccount : lstNewAccounts){
            // When the channel value changes, the Dynamic Eligibility & the Sequoia Qualification Field Values should be changed accordingly.          
            if(objAccount.Channel__c != mapOldAccounts.get(objAccount.Id).Channel__c){
                de.add('update');
                // if only the channel value chaned from RIA or to RIA (because the AUM__c field in contact depends on channel RIA)
                if(objAccount.Channel__c == 'RIA' || mapOldAccounts.get(objAccount.Id).Channel__c == 'RIA')
                    sq.add('update');
            }
        } // End of Account FOR Loop
        // Sequoia Fields Update
        
        if(!sq.isEmpty()){
            List<Contact> conUpdList = new List<Contact>();
            try{
                system.debug('Entering.... Account After Trigger - to update Sequoia Fields in contact');
                Integer NPos = 0; Integer NNeg = 0;
                for(Contact c : mapContacts.values()){
                    if(c.MailingCountry != null){
                        Npos = Integer.valueOf(c.SQ_NPos__c);
                        NNeg = Integer.valueOf(c.SQ_NNeg__c);
                        if(c.Channel_NEW__c != null && c.AUM__c != null){
                            if(c.Channel_NEW__c.contains('RIA') && c.AUM__c >=150) {NPos = NPos+1; NNeg = NNeg-1;}
                            else if(c.Channel_NEW__c.contains('RIA') && c.AUM__c < 150) {NNeg = NNeg+1;NPos = NPos-1;}
                            else if(!c.Channel_NEW__c.contains('RIA') && c.AUM__c >= 50) {NPos = NPos+1;NNeg = NNeg-1;}//logically wrong
                            else if(!c.Channel_NEW__c.contains('RIA') && c.AUM__c < 50) {NNeg = NNeg+1;NPos = NPos-1;}//same here
                        }
                        c.SQ_NPos__c = NPos;
                        c.SQ_NNeg__c = NNeg;
                        conUpdList.add(c);
                    }
                }
                if(conUpdList.size()>0)
                    update conUpdList;
            }
            catch(Exception e){
                se.sendEmail('Exception Occurred while updating contacts Sequoia Qualification Fields : \n'+e.getMessage()+lstContacts);
            }
        }
        */
        
        //system.debug('Account trigger contact list....'+mapContacts.values());
        /*
        List<Contact> cList = new List<Contact>();
        for(Contact c : lstContacts)
            //if(c.MailingCountry != null)
                cList.add(c);
        */  
        // Dynamic Eligibility call
        /* Comment this code on 5th September 7.00AM IST
        if(!de.isEmpty() && mapContacts != NULL && mapContacts.size() > 0)
            //dehandler.handleEligibility(mapContacts.values(),'account');
        */    
    }
}