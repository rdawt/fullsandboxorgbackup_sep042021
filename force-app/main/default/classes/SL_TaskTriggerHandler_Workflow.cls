public class SL_TaskTriggerHandler_Workflow 
{
    /*  Workflow Deactived
        Populate Task Summary recap                             -   https://vaneck--full.cs24.my.salesforce.com/01Q190000002ELK
        Update Summary Recap                                    -   https://vaneck--full.cs24.my.salesforce.com/01QA0000000QnUa
        Email Activity Type                                     -   https://vaneck--full.cs24.my.salesforce.com/01QA00000008wWH
        Close Marketo Open Activities Clicked Link              -   https://vaneck--full.cs24.my.salesforce.com/01QA0000000QmWB
        Close Marketo Open Activities Opened Email              -   https://vaneck--full.cs24.my.salesforce.com/01QA0000000QmWC
        Close Marketo and Marketo MVIS Open Activities Other    -   https://vaneck--full.cs24.my.salesforce.com/01QA0000000QmWD
    */
    
    public void updateTaskBasicInformation(List<Task> lstNew, Map<Id, Task> mapOld, Boolean isUpdate)
    {
        updateEmailActivityType(lstNew, mapOld,isUpdate);
        updateCloseMarketoOpenActivities(lstNew, mapOld,isUpdate);
        updateCloseMarketoAndMarketoMVISOpenActivitiesOther(lstNew, mapOld,isUpdate);
        updateSummaryRecap(lstNew, mapOld,isUpdate);
    }
    
    public void updateEmailActivityType(List<Task> lstNew, Map<Id, Task> mapOld, Boolean isUpdate)
    {
        // mapOld : if this function is running from Before Insert then it will be null
        // mapOld : if this function is running from Before Update then it will be not null
        
        if(!lstNew.isEmpty())
        {
            for(Task objTask : lstNew)
            {
                
                // hanlde Insert Use Case
                if(!isUpdate){              
                    
                    if(objTask.ActivityDate != null)
                        objTask.Activity_Date_Derived__c = objTask.ActivityDate;
                    
                    /*
                    else 
                        objTask.Activity_Date_Derived__c = Date.today().addDays(10);    
                    */              
                    
                }
                
                // handle Update Use case
                else{
                    
                    // getting old Value
                    task OldTask = mapOld.get(objTask.Id);
                    
                    // checking in the value gets changed
                    if(objTask.ActivityDate != null
                        && OldTask.ActivityDate != objTask.ActivityDate){
                        objTask.Activity_Date_Derived__c = objTask.ActivityDate;
                    }
                }
                
                if( objTask.Subject != NULL && objTask.Subject != '')
                { 
                    if( (isUpdate == FALSE && String.valueOf(objTask.Subject).contains('Email') ) ||
                        (isUpdate == TRUE && objTask.Subject != mapOld.get(objTask.Id).Subject && String.valueOf(objTask.Subject).contains('Email')) )  
                    {
                        objTask.Activity_Type__c = 'Email';
                        objTask.Purpose__c = 'Client Event';    
                    }
                }
            }
        }
    }
    
    public void updateCloseMarketoOpenActivities(List<Task> lstNew, Map<Id, Task> mapOld, Boolean isUpdate)
    {
        // mapOld : if this function is running from Before Insert then it will be null
        // mapOld : if this function is running from Before Update then it will be not null
        
        if(!lstNew.isEmpty())
        {
            Id marketoAPIUserId = [SELECT Id, Name FROM User WHERE FirstName ='Marketo' AND LastName = 'API' LIMIT 1].Id;
            for(Task objTask : lstNew)
            {
                system.debug('>>>>>>>>>>>>>>>>objTask.CreatedById>>>>>>>>>>>>>>' + objTask.CreatedById);
                system.debug('>>>>>>>>>>>>>>>>marketoAPIUserId>>>>>>>>>>>>>>' + marketoAPIUserId);
                system.debug('>>>>>>>>>>>>>>>>objTask.CreatedById>>>>>>>>>>>>>>' + objTask.OwnerId);
                
                if(objTask.Subject != NULL && objTask.Subject != '')
                {    
                    if( (isUpdate == false && objTask.OwnerId == marketoAPIUserId) ||
                        (isUpdate == true && objTask.OwnerId != mapOld.get(objTask.Id).OwnerId && objTask.OwnerId == marketoAPIUserId
                                             && objTask.CreatedById == marketoAPIUserId) )
                    {
                        if(String.valueOf(objTask.Subject).contains('Clicked Link'))
                        {
                            objTask.Status = 'Clicked Link';    
                        }
                        else if(String.valueOf(objTask.Subject).contains('Opened Email'))
                        {
                            objTask.Status = 'Opened Email';
                        }
                    }
                }
            }
        }
    }
    
    
    public void updateCloseMarketoAndMarketoMVISOpenActivitiesOther(List<Task> lstNew, Map<Id, Task> mapOld, Boolean isUpdate)
    {
        // mapOld : if this function is running from Before Insert then it will be null
        // mapOld : if this function is running from Before Update then it will be not null
        
        if(!lstNew.isEmpty())
        {
            Id marketoAPIUserId = [SELECT Id, Name FROM User WHERE FirstName ='Marketo' AND LastName = 'API' LIMIT 1].Id;
            Id marketoMVISUserId = [SELECT Id, Name FROM User WHERE FirstName ='Marketo' AND LastName = 'MVIS API' LIMIT 1].Id;
            for(Task objTask : lstNew)
            {
                if(objTask.Subject != NULL && objTask.Subject != '')
                {    
                    if( (isUpdate == false && (objTask.OwnerId == marketoAPIUserId || objTask.OwnerId == marketoMVISUserId) ) ||
                        (isUpdate == true && objTask.OwnerId != mapOld.get(objTask.Id).OwnerId 
                                            && (objTask.OwnerId == marketoAPIUserId || objTask.OwnerId == marketoMVISUserId) 
                                            && (objTask.CreatedById == marketoAPIUserId || objTask.CreatedById == marketoMVISUserId)) ) 
                    {
                        if(!String.valueOf(objTask.Subject).contains('Clicked Link') && !String.valueOf(objTask.Subject).contains('Opened Email'))
                        {
                            objTask.Status = 'Email Sent';  
                        }
                    }
                }
            }
        }
    }
    
    public void updateSummaryRecap(List<Task> lstNew, Map<Id, Task> mapOld, Boolean isUpdate)
    {
        // mapOld : if this function is running from Before Insert then it will be null
        // mapOld : if this function is running from Before Update then it will be not null
        
        if(!lstNew.isEmpty())
        {
            Id marketoAPIUserId = [SELECT Id, Name FROM User WHERE FirstName ='Marketo' AND LastName = 'API' LIMIT 1].Id;
            Id marketoMVISUserId = [SELECT Id, Name FROM User WHERE FirstName ='Marketo' AND LastName = 'MVIS API' LIMIT 1].Id;
            for(Task objTask : lstNew)
            {
                if( (isUpdate == false && (objTask.OwnerId == marketoAPIUserId || objTask.OwnerId == marketoMVISUserId) ) ||
                    (isUpdate == true && objTask.OwnerId != mapOld.get(objTask.Id).OwnerId 
                                            && (objTask.OwnerId == marketoAPIUserId || objTask.OwnerId == marketoMVISUserId) 
                                            && (objTask.CreatedById == marketoAPIUserId || objTask.CreatedById == marketoMVISUserId) ) ) 
                {
                    if(objTask.Subject != NULL)
                    {
                        objTask.Summary_Recap__c = objTask.Subject;
                        
                        if(objTask.Summary_Recap__c != NULL)
                            objTask.Description = 'Summary Recap: ' + objTask.Summary_Recap__c + '\n\n' + 'Description: ' + objTask.Description;
                    }
                }
            }
        }
    }
    
}