global class Batch_LPL_DB_Automations implements Database.Batchable<sObject> 
{
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        // LPL - 001A000000FjFjtIAF
        // Morgan Stanley - 001A000000FjERXIA3

        String query = '';

        Map<String, Dashboard_Automations_Filters__mdt> mapDashboard_Automations_Filters = Dashboard_Automations_Filters__mdt.getAll();
        if(!mapDashboard_Automations_Filters.isEmpty())
        {
            if(mapDashboard_Automations_Filters.containskey('DB'))
            {
                String strAccountId = '';
                if(!Test.isRunningTest())
                {
                    strAccountId = mapDashboard_Automations_Filters.get('DB').Single_Hardcoded_Account_Id__c;
                }
                else {
                    strAccountId = [Select Id, Name from Account where Name = 'Morgan Stanley' limit 1].Id;
                }
                query = 'SELECT Id ' + 
                            'From Account ' + 
                            'where Id =: strAccountId'; //or ParentId = \'001A000000FjERXIA3\'
            }
        }
        
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<Account> lstAccount) 
    {
        try 
        {
            if(!lstAccount.isEmpty())
            {
                List<Fund_Wise_Client_Snapshot__c> lstFund_Wise_Client_Snapshot = new List<Fund_Wise_Client_Snapshot__c>();

                List<Fund_Wise_Client_Snapshot__c> lstFund_Wise_Client_Snapshot_Client = new List<Fund_Wise_Client_Snapshot__c>();
                List<Fund_Wise_Client_Snapshot__c> lstFund_Wise_Client_Snapshot_Customer = new List<Fund_Wise_Client_Snapshot__c>();

                
                Map<String, Territory__c> mapTerritory = new Map<String, Territory__c>();

                for(Territory__c objTerritory : [SELECT Id, Name from Territory__c])
                {
                    mapTerritory.put(objTerritory.Name, objTerritory);
                }

                Map<String, Date> mapQuarter_Dates = new Map<String, Date>();
                mapQuarter_Dates.put('FQ1 FY 2021', Date.newInstance(2021, 03, 31) );
                mapQuarter_Dates.put('FQ2 FY 2021', Date.newInstance(2021, 06, 30) );
                mapQuarter_Dates.put('FQ3 FY 2021', Date.newInstance(2021, 09, 30) );
                mapQuarter_Dates.put('FQ4 FY 2021', Date.newInstance(2021, 12, 31) );

                Map<String, Integer> mapKey_ClientCount = new Map<String, Integer>();
                Map<String, Integer> mapKey_CustomerCount = new Map<String, Integer>();

                // Client
                for(AggregateResult ar : [SELECT count_Distinct(SalesConnect_Contact__c)cnt, 
                                            format(SUM(SalesConnect_Current_Assets__c)), Quarter__c,  
                                            Firm_Branch_Lookup__c, Firm_Branch_Lookup__r.Recordtype.Name, 
                                            SalesConnect_Contact__r.Territory__c 
                                            FROM History_Contact_Portfolio_Breakdown__c 
                                            WHERE SalesConnect_Current_Assets__c >= 1000000 and 
                                                    (Firm_Branch_Lookup__r.Name like 'MS-%' OR Firm_Branch_Lookup__r.Name like 'Morgan Stanley%'   ) and 
                                                        ( NOT(Fund_Lookup__r.Name like '%Gold Miners%')) and 
                                                    Quarter__c != '' and 
                                                    Fund_Vehicle_Type__c = 'ETF'                                                      
                                            GROUP BY Firm_Branch_Lookup__c, Firm_Branch_Lookup__r.Recordtype.Name, 
                                                        Quarter__c, SalesConnect_Contact__r.Territory__c])
                {
                    Fund_Wise_Client_Snapshot__c objFund_Wise_Client_Snapshot = new Fund_Wise_Client_Snapshot__c();
                    objFund_Wise_Client_Snapshot.Firm__c = (Id) ar.get('Firm_Branch_Lookup__c');
                    //objFund_Wise_Client_Snapshot.Fund__c = (Id) ar.get('Fund_Lookup__c');
                    objFund_Wise_Client_Snapshot.Total_Clients__c = (Decimal) ar.get('cnt');
                    objFund_Wise_Client_Snapshot.Type__c = 'Total Counts';
                    objFund_Wise_Client_Snapshot.Quarter__c = (String) ar.get('Quarter__c');
                    //objFund_Wise_Client_Snapshot.Territory_Derived_From_Contact__c = (String) ar.get('ContactOwnerTerritory_Derived__c');
                    
                    String strTerritoryName = (String) ar.get('Territory__c');
                    if(mapTerritory.containskey(strTerritoryName))
                        objFund_Wise_Client_Snapshot.Territory__c = mapTerritory.get(strTerritoryName).Id;
                    
                    String strQuarter = (String) ar.get('Quarter__c');
                    if(mapQuarter_Dates.containskey(strQuarter))
                            objFund_Wise_Client_Snapshot.As_Of_Date__c = mapQuarter_Dates.get(strQuarter);
                        
                    lstFund_Wise_Client_Snapshot_Client.add(objFund_Wise_Client_Snapshot);

                }

                // Customer
                for(AggregateResult ar : [SELECT count_Distinct(SalesConnect_Contact__c)cnt, 
                                            format(SUM(SalesConnect_Current_Assets__c)), Quarter__c,  
                                            Firm_Branch_Lookup__c, Firm_Branch_Lookup__r.Recordtype.Name, 
                                            SalesConnect_Contact__r.Territory__c 
                                            FROM History_Contact_Portfolio_Breakdown__c 
                                            WHERE SalesConnect_Current_Assets__c >= 100000 and SalesConnect_Current_Assets__c < 1000000 and 
                                                    (Firm_Branch_Lookup__r.Name like 'MS-%' OR Firm_Branch_Lookup__r.Name like 'Morgan Stanley%'   ) and 
                                                        ( NOT(Fund_Lookup__r.Name like '%Gold Miners%')) and 
                                                    Quarter__c != '' and 
                                                    Fund_Vehicle_Type__c = 'ETF'                                                      
                                            GROUP BY Firm_Branch_Lookup__c, Firm_Branch_Lookup__r.Recordtype.Name, 
                                                        Quarter__c, SalesConnect_Contact__r.Territory__c])
                {
                    System.debug('?????Customer Result??ar??????????' + ar);
                    Fund_Wise_Client_Snapshot__c objFund_Wise_Client_Snapshot = new Fund_Wise_Client_Snapshot__c();
                    objFund_Wise_Client_Snapshot.Firm__c = (Id) ar.get('Firm_Branch_Lookup__c');
                    //objFund_Wise_Client_Snapshot.Fund__c = (Id) ar.get('Fund_Lookup__c');
                    objFund_Wise_Client_Snapshot.Total_Customers__c = (Decimal) ar.get('cnt');
                    objFund_Wise_Client_Snapshot.Type__c = 'Total Counts';
                    objFund_Wise_Client_Snapshot.Quarter__c = (String) ar.get('Quarter__c');
                    //objFund_Wise_Client_Snapshot.Territory_Derived_From_Contact__c = (String) ar.get('ContactOwnerTerritory_Derived__c');
                    
                    String strTerritoryName = (String) ar.get('Territory__c');
                    if(mapTerritory.containskey(strTerritoryName))
                        objFund_Wise_Client_Snapshot.Territory__c = mapTerritory.get(strTerritoryName).Id;
                    

                    String strQuarter = (String) ar.get('Quarter__c');
                    if(mapQuarter_Dates.containskey(strQuarter))
                            objFund_Wise_Client_Snapshot.As_Of_Date__c = mapQuarter_Dates.get(strQuarter);

                    lstFund_Wise_Client_Snapshot_Customer.add(objFund_Wise_Client_Snapshot);
                }
                

                if(!lstFund_Wise_Client_Snapshot_Client.isEmpty())
                    insert lstFund_Wise_Client_Snapshot_Client;

                if(!lstFund_Wise_Client_Snapshot_Customer.isEmpty())
                    insert lstFund_Wise_Client_Snapshot_Customer;
                
            }

            
        } 
        catch (Exception e) 
        {
            System.debug('????????Got Exceptions???????????' + e.getMessage());
            System.debug('????????Got Exceptions??Line Number?????????' + e.getLineNumber());
        }
    } 

    

    /*
                
    */
    
    
    /*
    public Integer getDaysInQuarter(String strQuarter, Integer intFirstMonth, Integer intSecondMonth, Integer intThirdMonth, Integer intSelectedYear)
    {
        Integer intTotalDays = 0;

        Date dtStartFirstMonth = Date.newInstance(intSelectedYear, intFirstMonth, 1);
        Integer numberDaysFirst = date.daysInMonth(intSelectedYear, intFirstMonth);

        Date dtStartSecondMonth = Date.newInstance(intSelectedYear, intSecondMonth, 1);
        Integer numberDaysSecond = date.daysInMonth(intSelectedYear, intSecondMonth);

        Date dtStartThirdMonth = Date.newInstance(intSelectedYear, intThirdMonth, 1);
        Integer numberDaysThird = date.daysInMonth(intSelectedYear, intThirdMonth);

        intTotalDays = numberDaysFirst + numberDaysFirst + numberDaysFirst;

        return intTotalDays;

    }

    public Map<String, Date> getSelectedYearQuarterDates(Integer intSelectedMonth, Integer intSelectedYear)
    {
        Map<String, QuarterDates> mapQuarterDate = new Map<String, QuarterDates>();
        String strSelectedYear = String.valueOf(intSelectedYear);
        String strSelectedMonth = String.valueOf(intSelectedMonth);
        String strSelectedQuarter = '';

        QuarterDates objQuarterDates = new QuarterDates();

        if(intSelectedMonth >= 1 && intSelectedMonth <= 3)
        {
            Date dtStartMonth = Date.newInstance(intSelectedYear, 1, 1);
            Integer numberDays = getDaysInQuarter('Q1', 1, 2, 3, intSelectedYear);
            Date dtEndMonth = dtStartMonth.addDays(numberDays - 1);

            strSelectedQuarter = strSelectedYear + ':Q1';
            objQuarterDates.dtStartDate = dtStartMonth;
            objQuarterDates.dtEndDate = dtEndMonth;
            mapQuarterDate.put('Q1', objQuarterDates);
        }
        else if(intSelectedMonth >= 4  && intSelectedMonth <= 6)
        {
            Date dtStartMonth = Date.newInstance(intSelectedYear, 4, 1);
            Integer numberDays = getDaysInQuarter('Q2', 4, 5, 6, intSelectedYear);
            Date dtEndMonth = dtStartMonth.addDays(numberDays - 1);

            strSelectedQuarter = strSelectedYear + ':Q2';
            objQuarterDates.dtStartDate = dtStartMonth;
            objQuarterDates.dtEndDate = dtEndMonth;
            mapQuarterDate.put('Q2', objQuarterDates);
        }
        else if(intSelectedMonth >= 7 && intSelectedMonth <= 9)
        {
            Date dtStartMonth = Date.newInstance(intSelectedYear, 7, 1);
            Integer numberDays = getDaysInQuarter('Q3', 7, 8, 9, intSelectedYear);
            Date dtEndMonth = dtStartMonth.addDays(numberDays - 1);

            strSelectedQuarter = strSelectedYear + ':Q3';
            objQuarterDates.dtStartDate = dtStartMonth;
            objQuarterDates.dtEndDate = dtEndMonth;
            mapQuarterDate.put('Q3', objQuarterDates);
        }
        else if(intSelectedMonth >= 10  && intSelectedMonth <= 12)
        {
            Date dtStartMonth = Date.newInstance(intSelectedYear, 10, 1);
            Integer numberDays = getDaysInQuarter('Q4', 10, 11, 12, intSelectedYear);
            Date dtEndMonth = dtStartMonth.addDays(numberDays - 1);

            strSelectedQuarter = strSelectedYear + ':Q4';
            objQuarterDates.dtStartDate = dtStartMonth;
            objQuarterDates.dtEndDate = dtEndMonth;
            mapQuarterDate.put('Q4', objQuarterDates);
        }

        return mapQuarterDate;
    }
    */
     
    global void finish(Database.BatchableContext BC) 
    {
        // execute any post-processing operations like sending email
    }
}