global class batchContactUpdate implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       // Set<Id> TaskIdsFromTaskRelation = new Set<Id>();
      //  for (TaskRelation TaskRel : [Select TaskId from TaskRelation where RelationId in (Select Id from Contact Where Contact_Status__c != 'DELETE' AND MailingCountry = 'United States')])
        //AccountId = '001A000000i8CAs')])
      //  {
     //       TaskIdsFromTaskRelation.add(TaskRel.TaskId);
    //    }
        
        //String query = 'SELECT Id,ActivityDate,whoid FROM Task WHERE Id IN :TaskIdsFromTaskRelation AND Subject = \'call\' AND Status = \'call completed\' ORDER BY whoid,activitydate desc';
        String query = 'Select Id,name,email from Contact Where Contact_Status__c != \'DELETE\' AND MailingCountry = \'United States\' and SalesConnect__Rep_Type__c != \'partnership\' and LastActivityDate != null and DB_Source__c != \'MVIS Registrant\' and IsTestContact__c != true and IS_User__c != true' ; // and id = \'003A000000GCUJi\'';
    
            //String query = 'SELECT Id, FirstName,LastName FROM Contact';
            //String query = 'SELECT Id,ActivityDate,whoid FROM Task WHERE Id in (Select TaskId from TaskRelation where RelationId in (Select Id from Contact)) AND Subject = \'call\' AND Status = \'call completed\' ORDER BY ActivityDate desc LIMIT 1'
            //'SELECT Id,ActivityDate,whoid FROM Task WHERE Id in (Select TaskId from TaskRelation where RelationId in (Select Id from Contact where AccountId =\'001A000000i8CAs\')) AND Subject = \'call\' AND Status = \'call completed\' ORDER BY ActivityDate desc LIMIT 1'
         return Database.getQueryLocator(query);
        }
 
    global void execute(Database.BatchableContext BC, List<Contact> scope)
     {
         //Create a new list of Contacts for updating;
         list<Contact> conList= new list<Contact>();
         date loopLastHumanTouchDate;
         string loopWhoId;
         string loopContactId;
         Contact parentTaskContact;
      try{   
           //  for(Task tsk : scope)
             for(Contact contact: scope)
             {
             
                 loopContactId =  string.valueof(contact.get('Id'));
                 List<EventWhoRelation> ContIdWithEventMAxDateList;
                 List<TaskWhoRelation> ContIdWithTaskMAxDateList;
                 string activityType;
                 
                  ContIdWithEventMAxDateList = [Select Id, Event.ID,Event.ActivityDate,Event.whoid,relationid,eventid,event.subject From EventWhoRelation Where  Event.Result__c = 'held' and Event.whoid != null and relationid = :loopContactId ORDER BY relationid, event.activitydate desc LIMIT 1]; //ORDER BY event.whoid,event.activitydate desc
                  ContIdWithTaskMAxDateList  = [Select Id, Task.ID,Task.ActivityDate,Task.whoid,relationid,taskid, task.type,Task.Subject From taskWhoRelation Where (task.subject ='Call' or task.subject like 'email%') and task.status IN('call completed', 'call received','Email Sent') and task.whoid != null and relationid = :loopContactId ORDER BY relationid, task.activitydate desc LIMIT 1]; //ORDER BY task.whoid,task.activitydate desc
                   
                  If ((!ContIdWithEventMAxDateList.isEmpty()) && (!ContIdWithTaskMAxDateList.isEmpty())) {
                      //Event activity date is > Task
                      If ((ContIdWithEventMAxDateList[0].event.activitydate) >=  (ContIdWithTaskMAxDateList[0].task.activitydate)) {
                      
                          loopLastHumanTouchDate = ContIdWithEventMAxDateList[0].event.activitydate;
                          activityType = ContIdWithEventMAxDateList[0].event.subject;
                          system.debug('**Inside-EventDate>TaskDate ------' +ContIdWithEventMAxDateList[0].event.activitydate+' '+loopContactId+' @@ '+contact.email);
                      
                      }
                      //Task Activity Date is > than Event
                      else if  ((ContIdWithTaskMAxDateList[0].Task.activitydate) >  (ContIdWithEventMAxDateList[0].Event.activitydate)){
                      
                          loopLastHumanTouchDate = ContIdWithTaskMAxDateList[0].task.activitydate;
                          activityType = ContIdWithTaskMAxDateList[0].Task.subject;
                          system.debug('**Inside-EventDate<TaskDate ------' +ContIdWithEventMAxDateList[0].event.activitydate+' '+loopContactId+' @@ '+contact.email);
                      
                 
                      }
                      system.debug('**HERE 1***');
                      parentTaskContact = new contact(Id = loopContactId, LastHumanContactDate__c = loopLastHumanTouchDate, LastHumanContactActivityType__c =  activityType);
                      system.debug('**HERE 2***');
                  
                  }
                  //no entry for the Events but Task only exists for the contact so u use the Task Activity Date for the most recent info.
                  if ((ContIdWithEventMAxDateList.isEmpty()) && (!ContIdWithTaskMAxDateList.isEmpty()) ) {
                      loopLastHumanTouchDate = ContIdWithTaskMAxDateList[0].task.activitydate;
                     system.debug('**HERE 3***'+' '+contact.email);
                      //activityType = ContIdWithTaskMAxDateList[0].task.type; //NO-is this causing index out of bound error?
                      activityType = ContIdWithTaskMAxDateList[0].task.subject;
                      parentTaskContact = new contact(Id = loopContactId, LastHumanContactDate__c = loopLastHumanTouchDate, LastHumanContactActivityType__c =  activityType);
                      system.debug('**Inside-OnlyWhenTaskisFoundNOEventFoundForContact------' +ContIdWithTaskMAxDateList[0].task.activitydate+' '+loopContactId+' @@ '+contact.email);
                      
                  }
                   //no entry for the Task but Event only exists for the contact so u use the Event Activity Date for the most recent info.
                  if ((!ContIdWithEventMAxDateList.isEmpty()) && (ContIdWithTaskMAxDateList.isEmpty()) ) {
                      system.debug('**HERE 3.5!!***'+' '+contact.email);
                      loopLastHumanTouchDate = ContIdWithEventMAxDateList[0].Event.activitydate;
                      system.debug('**HERE 4***'+' '+contact.email);
                      activityType = ContIdWithEventMAxDateList[0].event.subject;
                      parentTaskContact = new contact(Id = loopContactId, LastHumanContactDate__c = loopLastHumanTouchDate, LastHumanContactActivityType__c =  activityType);
                      system.debug('**Inside-OnlyWhenEventisFoundNOTASKFoundForContact------' +ContIdWithEventMAxDateList[0].event.activitydate+' '+loopContactId+' @@ '+contact.email);
                  }
               
                 
             }
             //
             if (parentTaskContact != null){
                 conList.add(parentTaskContact);
             }
             //update happens after completing the for loop
             if (conList != null){
                 update conList;
             }
             
      }catch (Exception e) {
            // exeption
             
            string error = 'Exception occured in  Batch Apex failed while processing the loop in Events or sObjects..... '+e.getMessage()+conList;
            system.debug('error catch at the exeption level-error catch bloc ------' +error);
            SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Batch Update Of Last Human Interation Date processing module');
            String    msg = 'Exception Occurred in Apex while writing to LastHumanInteraction Date update in Contact \n '+e.getMessage() + ' ' +loopContactId;
            se.sendEmail(msg);
            //system.debug('List of Record Types........'+'rt');
         }
    }  
    global void finish(Database.BatchableContext BC)
    {
    }
}