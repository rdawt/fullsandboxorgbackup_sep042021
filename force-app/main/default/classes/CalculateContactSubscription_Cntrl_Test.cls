@isTest
private class CalculateContactSubscription_Cntrl_Test 
{
    @TestSetup
    private static void setUp(){
        User sysAdminUserObj = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'AdminUser',
            Email = 'AdminUser@admin.com',
            Username = 'SalesforceUser@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        
        System.runAs(sysAdminUserObj){
            Subscription__c subObj = (Subscription__c)SL_TestDataFactory.createSObject(
                                                                                        new Subscription__c(Name='Test Subscription',
                                                                                                            IsActive__c=true,
                                                                                                            Subscription_Display_Name__c='Test Display Name'    
                                                                                      ),true);
            Subscription__c subObj1 = (Subscription__c)SL_TestDataFactory.createSObject(
                                                                                        new Subscription__c(Name='Test Subscription1',
                                                                                                            IsActive__c=true,
                                                                                                            Subscription_Display_Name__c='Test Display Name1'   
                                                                                      ),true);
            
           Subscription_Group__c subGroupObj = (Subscription_Group__c)SL_TestDataFactory.createSObject(
                                                                                        new Subscription_Group__c(Name='Unassigned Eligibility',
                                                                                                                  IsActive__c=true
                                                                                       ),true);
            Account accObj = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account',
                                                                                       Channel__c='RIA',
                                                                                       BillingCountry='India',ID_Status2__c='0 - Not Interested'
                                                                                      ),true);
            Contact conObj = (Contact)SL_TestDataFactory.createSObject(new Contact(LastName='Test Contact',
                                                                                   AccountId=accObj.Id,MailingCountry='India',
                                                                                   HasOptedOutOfEmail=false,Email='testUSer@gmail.com',
                                                                                   Subscription_Group__c=subGroupObj.Id
                                                                                      ),true);
            Subscription_Group_Subscription__c sgsObj = (Subscription_Group_Subscription__c)SL_TestDataFactory.createSObject(
                                                                                                            new Subscription_Group_Subscription__c(
                                                                                                                        Subscription_Group__c=subGroupObj.Id,
                                                                                                                        Subscription__c=subObj.Id
                                                                                                                       ),true);
            Subscription_Group_Subscription__c sgsObj1 = (Subscription_Group_Subscription__c)SL_TestDataFactory.createSObject(
                                                                                                            new Subscription_Group_Subscription__c(
                                                                                                                        Subscription_Group__c=subGroupObj.Id,
                                                                                                                        Subscription__c=subObj1.Id
                                                                                                                       ),true);
            
            Subscription_Member__c smObj = (Subscription_Member__c)SL_TestDataFactory.createSObject(
                                                                                new Subscription_Member__c(
                                                                                    Contact__c=conObj.Id,
                                                                                    Subscription__c=subObj.Id,
                                                                                    Subscribed__c=true
                                                                                ),true);
            
        } 
    }
    
    /*
        Testing getSubscriptionDetails method
    */
    @isTest
    private static void testGetSubscriptionDetails(){
        Contact conObj = [SELECT Id FROM Contact WHERE LastName='Test Contact' LIMIT 1];
        User adminUserObj = [SELECT Id FROM User WHERE UserName='SalesforceUser@amamama.com'];
        System.runAs(adminUserObj){
            Test.startTest();
            CalculateContactSubscription_Controller.conAndSubDetailWrapper returnResult = CalculateContactSubscription_Controller.getSubscriptionDetails(conObj.Id);
            Test.stopTest(); 
            System.assertEquals(2,returnResult.wrapperLst.size());
        }
    }
    
    /*
        Testing setSubscription method
    */
    /*
    @isTest
    private static void testSetSubscription(){
        Contact conObj = [SELECT Id FROM Contact WHERE LastName='Test Contact' LIMIT 1];
        User adminUserObj = [SELECT Id FROM User WHERE UserName='SalesforceUser@amamama.com'];
        System.runAs(adminUserObj){
            Subscription__c subObj = [SELECT Id FROM Subscription__c WHERE Name='Test Subscription' LIMIT 1];
            Subscription__c subObj1 = [SELECT Id FROM Subscription__c WHERE Name='Test Subscription1' LIMIT 1];
            
            list<CalculateContactSubscription_Controller.RecordSet> lstOldRSet = new list<CalculateContactSubscription_Controller.RecordSet>();
            list<CalculateContactSubscription_Controller.RecordSet> lstNewRSet = new list<CalculateContactSubscription_Controller.RecordSet>();
            list<CalculateContactSubscription_Controller.RecordSet> lstROldSetBackup = new list<CalculateContactSubscription_Controller.RecordSet>();
            
            list<Subscription_Member__c> lstSM = [SELECT Id,Subscription__c,Subscribed__c,
                                                         Subscription_Unsubscribed_Date__c
                                                    FROM Subscription_Member__c];
            
            for(Subscription_Group_Subscription__c sgsObj : [SELECT Id,Subscription__c,Subscription__r.Id,
                                                                    Subscription__r.Name,Subscription__r.Subscription_Type__r.Name
                                                                FROM Subscription_Group_Subscription__c])
            {
                lstOldRSet.add(CalculateContactSubscription_Controller.createWrapper(sgsObj,lstSM));
            }
            
            for(CalculateContactSubscription_Controller.RecordSet wrapperObj : lstOldRSet){
                if(wrapperObj.SubName == 'Test Subscription')
                    wrapperObj.Subscribed = false;
                else if(wrapperObj.SubName == 'Test Subscription1')
                    wrapperObj.Subscribed = true;
                lstNewRSet.add(wrapperObj);
            }
            
            for(Subscription_Group_Subscription__c sgsObj : [SELECT Id,Subscription__c,Subscription__r.Id,
                                                                    Subscription__r.Name,Subscription__r.Subscription_Type__r.Name
                                                                FROM Subscription_Group_Subscription__c])
            {
                lstROldSetBackup.add(CalculateContactSubscription_Controller.createWrapper(sgsObj,lstSM));
            }
            
            Test.startTest();
                CalculateContactSubscription_Controller.setSubscription(conObj.Id,lstROldSetBackup,lstNewRSet);
            Test.stopTest(); 
            System.assertEquals(1, [SELECT Id,Subscription__r.Name 
                                        FROM Subscription_Member__c 
                                        WHERE Subscription__r.Name='Test Subscription'
                                            AND Subscribed__c=false].size());
            System.assertEquals(1, [SELECT Id,Subscription__r.Name 
                                        FROM Subscription_Member__c 
                                        WHERE Subscription__r.Name='Test Subscription1'
                                            AND Subscribed__c=true
                                            AND Contact__c=:conObj.Id].size());
        }
    }
    */
    
    /*
        Testing getBaseUrl method
    */
    @isTest
    private static void testGetBaseUrl(){
        User adminUserObj = [SELECT Id FROM User WHERE UserName='SalesforceUser@amamama.com'];
        System.runAs(adminUserObj){
            Test.startTest();
                String urlStr = CalculateContactSubscription_Controller.getBaseUrl();
            Test.stopTest(); 
            System.assertEquals(true,urlStr != NULL);
        }
    }
}