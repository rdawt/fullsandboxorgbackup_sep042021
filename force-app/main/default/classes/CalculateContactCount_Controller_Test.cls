@isTest
private class CalculateContactCount_Controller_Test 
{
    @TestSetup
    static void prepareTestData()
    {
        SL_TestDataFactory.createTestDataForContact();
    }
    
    // checking this scenario for Contact linked to Account
    @isTest 
    static void testcountContactsForEachAccount_ContactPresent() {
        
        Test.startTest();
        
        Account objAccount = [Select Id from Account limit 1];
        String strMessage = CalculateContactCount_Controller.countContactsForEachAccount(objAccount.Id);

        objAccount = [Select Id, Number_of_Contacts__c from Account where Id =: objAccount.Id limit 1];

        System.assertEquals(strMessage, 'Success');
        System.assertEquals(objAccount.Number_of_Contacts__c, 1);
        
        Test.stopTest();
        
    }

    // checking this scenario for No Contact linked to Account
    @isTest 
    static void testcountContactsForEachAccount_NoContact() {
        
        Account objAccount = [Select Id from Account limit 1];
        List<Contact> lstContact = [Select Id from Contact where AccountId =: objAccount.Id];
        delete lstContact;

        Test.startTest();
        String strMessage = CalculateContactCount_Controller.countContactsForEachAccount(objAccount.Id);
        
        objAccount = [Select Id, Number_of_Contacts__c from Account where Id =: objAccount.Id limit 1];

        System.assertEquals(strMessage, 'Success');
        System.assertEquals(objAccount.Number_of_Contacts__c, 0);

        Test.stopTest();
        
    }

    // check this scenario for Exception
    @isTest 
    static void testcountContactsForEachAccount_Exception() {
        
        Test.startTest();
        String strMessage = CalculateContactCount_Controller.countContactsForEachAccount(null);
        
        System.assertEquals(strMessage, 'Failed');
        
        Test.stopTest();
        
    }
    
    
}