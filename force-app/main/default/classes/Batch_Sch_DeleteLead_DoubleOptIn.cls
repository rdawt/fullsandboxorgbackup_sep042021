global class Batch_Sch_DeleteLead_DoubleOptIn implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        // We now call the batch class to be scheduled
        Batch_DeleteLead_DoubleOptIn objclass = new Batch_DeleteLead_DoubleOptIn ();
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(objclass, 10);
    }
   
}