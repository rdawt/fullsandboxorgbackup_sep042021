public class LeadTrigger_Handler 
{
    public LeadTrigger_Handler(List<Lead> lstNewLeads)
    {
        
    }
    
    public void onBeforeUpdate(List<Lead> lstNewLeads)
    {
        updateLeadCountry(lstNewLeads);
    }
    
    public void onAfterUpdate(List<Lead> lstNewLeads, Map<Id,Lead> mapOldLeads)
    {
        onLeadConversionDoubleOptFunctionality(lstNewLeads, mapOldLeads);
    }

    public void updateLeadCountry(List<Lead> lstNewLeads)
    {
        try
        {
            Set<Id> setCommunityRegistrantIds = new Set<Id>();
            Map<Id, Community_Registrant__c> mapCommunity_Registrant = new Map<Id, Community_Registrant__c>();
    
            if(!lstNewLeads.isEmpty())
            {
                for(Lead objLead : lstNewLeads)
                {
                    if(objLead.Community_Registrant__c != NULL)
                    {
                        setCommunityRegistrantIds.add(objLead.Community_Registrant__c);
                    }
                }
            }
    
            if(!setCommunityRegistrantIds.isEmpty())
            {
                for(Community_Registrant__c objCR : [SELECT Id, Email__c , Country__c, 
                                                        Subscription_Options__c, Investor_Type__c, 
                                                        SFDC_Subscription_Ids_CSV__c, Subscription_Group__c
                                                        FROM Community_Registrant__c 
                                                        WHERE ID IN: setCommunityRegistrantIds])
                {
                    mapCommunity_Registrant.put(objCR.Id, objCR);
                }
    
                if(!lstNewLeads.isEmpty())
                {
                    for(Lead objLead : lstNewLeads)
                    {
                        if(objLead.Community_Registrant__c != NULL && 
                            mapCommunity_Registrant.containskey(objLead.Community_Registrant__c) && 
                            mapCommunity_Registrant.get(objLead.Community_Registrant__c).Country__c != NULL)
                        {
                            if(objLead.Country == null || objLead.Country == '')
                            {
                                objLead.Country =  mapCommunity_Registrant.get(objLead.Community_Registrant__c).Country__c;
                            }
                        }
                    }
                }
            }
        }
        catch(Exception e)
        {
            system.debug('Error in trigger '+e.getMessage());
        }
    }

    public void onLeadConversionDoubleOptFunctionality(List<Lead> lstNewLeads, Map<Id,Lead> mapOldLeads)
    {
        try
        {
            Set<Id> setAccountIds = new Set<Id>();
            Map<Id, Account> mapAccount = new Map<Id, Account>();

            Set<Id> setContactIds = new Set<Id>();
            Map<Id, Contact> mapContact = new Map<Id, Contact>();

            Set<Id> setCommunityRegistrantIds = new Set<Id>();
            Map<Id, Community_Registrant__c> mapCommunity_Registrant = new Map<Id, Community_Registrant__c>();
            Map<Id, Community_Member__c> mapCommunity_Member = new Map<Id, Community_Member__c>();

            List<Account> lstDeleteAccounts = new List<Account>();
            List<Contact> lstContacts = new List<Contact>();
            List<Community_Registrant__c> lstCommunityRegistrant = new List<Community_Registrant__c>();
            List<Community_Member__c> lstCommunityMembers = new List<Community_Member__c>();
            List<Subscription_Member__c> lstSubscriptionMember = new List<Subscription_Member__c>();
            List<Subscription_Group_Subscription__c> lstSubscriptionGroupSubscription = new List<Subscription_Group_Subscription__c>();

            Account objDefaultRegistrantFirm = new Account();
            objDefaultRegistrantFirm =  [SELECT Id, Name from Account where Name = 'Default Registrant Firm'];

            Account objIndividualInvesterFirm = new Account();
            objIndividualInvesterFirm =  [SELECT Id, Name from Account where Name = 'Individual Investor - Retail'];

            if(!lstNewLeads.isEmpty())
            {
                for(Lead objLead : lstNewLeads)
                {
                    if((objLead.Status == 'GDPR Opted-In' ||  objLead.Status == 'Webinar Subscriber' ) && 
                        mapOldLeads.get(objLead.Id).Status != objLead.Status )
                    {
                        if(objLead.Community_Registrant__c != NULL)
                        {
                            setCommunityRegistrantIds.add(objLead.Community_Registrant__c);
                        }
                    }
                }
            }

            if(!setCommunityRegistrantIds.isEmpty())
            {
                for(Community_Registrant__c objCR : [SELECT Id, Email__c , Country__c, Contact__c, isActive__c,
                                                        Subscription_Options__c, Investor_Type__c, 
                                                        SFDC_Subscription_Ids_CSV__c, Subscription_Group__c
                                                        FROM Community_Registrant__c 
                                                        WHERE ID IN: setCommunityRegistrantIds])
                {
                    mapCommunity_Registrant.put(objCR.Id, objCR);
                }

                for(Community_Member__c objCM : [SELECT Id, Community_Registrant__c, Contact__c, Community__c 
                                                    FROM Community_Member__c 
                                                    WHERE Community_Registrant__c IN: setCommunityRegistrantIds])
                {
                    mapCommunity_Member.put(objCM.Community_Registrant__c, objCM);
                }

                /*
                
                */
                /*
                if(!setAccountIds.isEmpty())
                {
                    for(Account objAccount : [SELECT Id,  NewChannel__c
                                                FROM Account 
                                                WHERE ID IN: setAccountIds])
                    {
                        mapAccount.put(objAccount.Id, objAccount);
                    }
                }
                */

                List<Database.LeadConvert> lstLeadConvert = new List<Database.LeadConvert>();
                if(!lstNewLeads.isEmpty())
                {

                    for(Lead objLead : lstNewLeads)
                    {
                        if(objLead.IsConverted == false &&  
                            (objLead.Status == 'GDPR Opted-In' ||  objLead.Status == 'Webinar Subscriber' ) && 
                            mapOldLeads.get(objLead.Id).Status != objLead.Status )
                        {
                            if(objLead.Community_Registrant__c != NULL && 
                                mapCommunity_Registrant.containskey(objLead.Community_Registrant__c) &&
                                objLead.Activation_Approved__c != NULL)
                            {
                                Database.LeadConvert lc = new Database.LeadConvert();
                                lc.setLeadId(objLead.id);
                                lc.setConvertedStatus(objLead.Status);

                                if(mapCommunity_Registrant.get(objLead.Community_Registrant__c).Investor_Type__c == 'Individual Investor')
                                    lc.setAccountId(objIndividualInvesterFirm.Id);
                                else {
                                    lc.setAccountId(objDefaultRegistrantFirm.Id);
                                }

                                lstLeadConvert.add(lc);
                            }
                        }
                    }
                }

                Set<Id> setLeadConvertedIds = new set<Id>();
                Map<Id, Id> mapLeadConvertedId_ContactId = new Map<Id, Id>();
                Map<Id, Id> mapLeadConvertedId_AccountId = new Map<Id, Id>();
                if(!lstLeadConvert.isEmpty())
                {
                    List<Database.LeadConvertResult> lstLeadConvertResults = Database.convertLead(lstLeadConvert);
                    for(Database.LeadConvertResult objResult : lstLeadConvertResults)
                    {
                        system.debug('????????Check Lead is Converted or Not??????????' + objResult.getLeadId());
                        system.debug('????????Check Lead is Converted or Not??????????' + objResult.isSuccess());
                        system.debug('????????Check Lead is Converted or Not??????????' + objResult.getContactId());
                        system.debug('????????Check Lead is Converted or Not??????????' + objResult.getAccountId());
                        system.debug('????????Check Lead is Converted or Not??????????' + objResult.getErrors());

                        if(objResult.isSuccess() == true)
                        {
                            setAccountIds.add(objResult.getAccountId());
                            setContactIds.add(objResult.getContactId());
                            setLeadConvertedIds.add(objResult.getLeadId());
                            mapLeadConvertedId_ContactId.put(objResult.getLeadId(), objResult.getContactId());
                            mapLeadConvertedId_AccountId.put(objResult.getLeadId(), objResult.getAccountId());
                        }
                    }

                    if(!setContactIds.isEmpty())
                    {
                        for(Contact objContact : [SELECT Id 
                                                    FROM Contact 
                                                    WHERE ID IN: setContactIds])
                        {
                            mapContact.put(objContact.Id, objContact);
                        }
                    }

                    if(!setAccountIds.isEmpty())
                    {
                        for(Account objAccount : [SELECT Id,  NewChannel__c
                                                    FROM Account 
                                                    WHERE ID IN: setAccountIds])
                        {
                            mapAccount.put(objAccount.Id, objAccount);
                        }
                    }

                    for(Lead objLead : lstNewLeads)
                    {
                        if(setLeadConvertedIds.contains(objLead.Id))
                        {
                            Contact objContact = new Contact(Id=mapLeadConvertedId_ContactId.get(objLead.Id));
                            objContact.Email = mapCommunity_Registrant.get(objLead.Community_Registrant__c).Email__c;
                            objContact.Auto_Subscription_Group__c = mapCommunity_Registrant.get(objLead.Community_Registrant__c).Subscription_Group__c;
                            objContact.Subscription_Group__c = mapCommunity_Registrant.get(objLead.Community_Registrant__c).Subscription_Group__c;
                            objContact.DB_Source__c = 'LeadConverted-DoubleOptedIn';
                            objContact.Activation_Approved__c = objLead.Activation_Approved__c;
                            objContact.Activation_Approval_Detail__c = objLead.Activation_Approval_Detail__c;
                            objContact.Activation_Requested__c = objLead.Activation_Requested__c;
                            objContact.Deactivation_Request_Detail__c = objLead.Deactivation_Request_Detail__c;
                            objContact.Deactivation_Requested__c = objLead.Deactivation_Requested__c;
                            objContact.Community_Registrant__c = objLead.Community_Registrant__c;
                            objContact.MailingCountry = mapCommunity_Registrant.get(objLead.Community_Registrant__c).Country__c;

                            if(mapAccount.containsKey(mapLeadConvertedId_AccountId.get(objLead.Id)))
                                objContact.InvestorTpSD__c = mapAccount.get(mapLeadConvertedId_AccountId.get(objLead.Id)).NewChannel__c;

                            lstContacts.add(objContact);

                            if(!mapCommunity_Registrant.isEmpty() && mapCommunity_Registrant.containskey(objLead.Community_Registrant__c))
                            {
                                Community_Registrant__c objCommunity_Registrant = mapCommunity_Registrant.get(objLead.Community_Registrant__c);
                                objCommunity_Registrant.Contact__c = mapLeadConvertedId_ContactId.get(objLead.Id);
                                objCommunity_Registrant.isActive__c = true;
                                lstCommunityRegistrant.add(objCommunity_Registrant);
                            }

                            if(mapCommunity_Member.containskey(objLead.Community_Registrant__c))
                            {
                                Community_Member__c objCommunity_Member = mapCommunity_Member.get(objLead.Community_Registrant__c);
                                objCommunity_Member.Contact__c = mapLeadConvertedId_ContactId.get(objLead.Id);
                                lstCommunityMembers.add(objCommunity_Member);
                            }

                            String strSFDC_Subscription_Ids_CSV = '';
                            strSFDC_Subscription_Ids_CSV = mapCommunity_Registrant.get(objLead.Community_Registrant__c).SFDC_Subscription_Ids_CSV__c;

                            if(strSFDC_Subscription_Ids_CSV != NULL && strSFDC_Subscription_Ids_CSV != '')
                                strSFDC_Subscription_Ids_CSV = strSFDC_Subscription_Ids_CSV.deleteWhitespace();

                            List<String> lstSubscriptionIds = new List<String>();
                            if(strSFDC_Subscription_Ids_CSV != NULL && strSFDC_Subscription_Ids_CSV != '')
                            {
                                if(strSFDC_Subscription_Ids_CSV.contains(','))
                                    lstSubscriptionIds = strSFDC_Subscription_Ids_CSV.split(',');
                                else {
                                    lstSubscriptionIds.add(strSFDC_Subscription_Ids_CSV);
                                }
                            }

                            if(!lstSubscriptionIds.isEmpty())
                            {
                                for(String strSubscriptionId : lstSubscriptionIds)
                                {
                                    if(strSubscriptionId != null && strSubscriptionId != '')
                                    {
                                        Subscription_Member__c objSubscription_Member = new Subscription_Member__c();
                                        objSubscription_Member.Subscription__c = Id.valueOf(strSubscriptionId);
                                        objSubscription_Member.Contact__c = mapLeadConvertedId_ContactId.get(objLead.Id);
                                        objSubscription_Member.Subscribed__c = true;
                                        lstSubscriptionMember.add(objSubscription_Member);
                                    }
                                }
                            }
                        }
                    }

                    system.debug('????Before ??lstContacts???????' + lstContacts);
                    if(!lstContacts.isEmpty())
                        update lstContacts;

                    system.debug('????After  contact update??lstContacts???????' + lstCommunityRegistrant);
                    if(!lstCommunityRegistrant.isEmpty())
                        update lstCommunityRegistrant;

                    system.debug('????After  contact update??lstContacts???????' + lstCommunityMembers);
                    if(!lstCommunityMembers.isEmpty())
                        update lstCommunityMembers;

                    system.debug('????After SGS update ??lstContacts???????' + lstSubscriptionMember);
                    if(!lstSubscriptionMember.isEmpty())
                        insert lstSubscriptionMember;
                }
            }

        }
        catch(Exception e)
        {
            system.debug('Error in trigger '+e.getMessage());
        }
    }
}