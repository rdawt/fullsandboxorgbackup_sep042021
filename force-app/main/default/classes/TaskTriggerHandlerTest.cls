@isTest
public class TaskTriggerHandlerTest 
{
    
    @isTest
    static void testTaskTriggerHandler() {
        Profile pf= [Select Id from profile where Name='Marketo User']; 
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 
        User uu=new User(firstname = 'Marketo', 
                         lastName = 'API', 
                         email = uniqueName + '@test' + orgId + '.org', 
                         Username = uniqueName + '@test' + orgId + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = uniqueName.substring(18, 23), 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf.Id
                        ); 
        
        
        User objUser = (User)SL_TestDataFactory.createSObject(uu,true);
        system.runAs(objUser){
            Test.startTest();
            SL_TestDataFactory.createTestDataForCustomSetting();
            Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
            sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
            //create account
            Account acc = new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA');
            acc.PM_Calls_Year_Actual_Num__c = 0;
            acc.Analyst_Calls_Year_Actual_Num__c = 0;
            acc.Calls_Year_No_Analyst_PM_Actual_Num__c = 0;
            acc.Emails_Year__c = 0;
            insert acc;
            
            //create contact
            Contact cont = new Contact(accountid = acc.id, lastname = 'Testcontact', MailingCountry = 'USA');
            insert cont;
            
            //create Oppty
            Opportunity oppty = new Opportunity(accountid = acc.id, closedate=date.today(), name='opptyname', amount = 100, stagename = 'Sample stage');
            insert oppty;  
            
            Task marketoTask = new Task (ownerid = objUser.Id, whatid = acc.id, activityDate = date.today(), status = 'Completed', whoid = cont.id, subject = 'Marketo Test', Description = 'abc');
            insert marketoTask;
            
            Test.stopTest();
        }
    }
    
    @isTest
    static void testTaskTriggerHandlerAU() {
        Profile pf= [Select Id from profile where Name='Australia']; 
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 
        User uu=new User(firstname = 'Marketo', 
                         lastName = 'API', 
                         email = uniqueName + '@test' + orgId + '.org', 
                         Username = uniqueName + '@test' + orgId + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = uniqueName.substring(18, 23), 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf.Id
                        ); 
        
        
        User objUser = (User)SL_TestDataFactory.createSObject(uu,true);
        system.runAs(objUser){
            Test.startTest();
            SL_TestDataFactory.createTestDataForCustomSetting();
            Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
            sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
            //create account
            Account acc = new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA');
            acc.PM_Calls_Year_Actual_Num__c = 0;
            acc.Analyst_Calls_Year_Actual_Num__c = 0;
            acc.Calls_Year_No_Analyst_PM_Actual_Num__c = 0;
            acc.Emails_Year__c = 0;
            insert acc;
            
            Contact cont1 = new Contact(accountid = acc.id, lastname = 'Testcontact', MailingCountry = 'Australia');
            insert cont1;
            Task marketoAUTask = new Task (ownerid = objUser.Id, whatid = acc.id, activityDate = date.today(), 
                                            status = 'Completed', whoid = cont1.id, ContactOwner__c=cont1.id,
                                            subject = 'Clicked Link : Marketo Test', Description = 'abc');
            insert marketoAUTask; 
            Task marketoAUTask1 = new Task (ownerid = objUser.Id, whatid = acc.id, activityDate = date.today(), 
                                            status = 'Completed', whoid = cont1.id, ContactOwner__c=cont1.id,
                                            subject = 'Opened Email : Marketo Test', Description = 'abc');
            insert marketoAUTask1;
            system.assert(marketoAUTask.Id != NULL);
            system.assert(marketoAUTask1.Id != NULL);
               
            List<Task> lstFollowUptask = [SELECT Id FROM Task WHERE whoid =:cont1.Id];
            system.assertEquals(lstFollowUptask.size(),3);
            Test.stopTest();
        }
    }
    
    @isTest
    static void testTaskTriggerHandlerInsert_Update_Delete() {
        Profile pf= [Select Id from profile where Name='Australia']; 
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 
        User uu=new User(firstname = 'Marketo', 
                         lastName = 'API', 
                         email = uniqueName + '@test' + orgId + '.org', 
                         Username = uniqueName + '@test' + orgId + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = uniqueName.substring(18, 23), 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf.Id
                        ); 
        
        
        User objUser = (User)SL_TestDataFactory.createSObject(uu,true);
        system.runAs(objUser){
            Test.startTest();
            SL_TestDataFactory.createTestDataForCustomSetting();
            Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
            sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
            //create account
            Account acc = new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA');
            acc.PM_Calls_Year_Actual_Num__c = 0;
            acc.Analyst_Calls_Year_Actual_Num__c = 0;
            acc.Calls_Year_No_Analyst_PM_Actual_Num__c = 0;
            acc.Emails_Year__c = 0;
            insert acc;
            
            Contact cont1 = new Contact(accountid = acc.id, lastname = 'Testcontact', MailingCountry = 'Australia');
            insert cont1;
            Task marketoAUTask = new Task (ownerid = objUser.Id, whatid = acc.id, activityDate = date.today(), 
                                            status = 'Completed', whoid = cont1.id, ContactOwner__c=cont1.id,
                                            subject = 'Clicked Link : Marketo Test', Description = 'abc');
            insert marketoAUTask; 
            
            marketoAUTask.Subject = 'Opened Email : Marketo Test';
            update marketoAUTask;
            
            delete marketoAUTask;
            Test.stopTest();
        }
    }

    //Delete by Australia profile user his own tasks and this should be allowed
    @isTest
    static void testOnBeforeDeleteSce01() {

        insert new Activity_Validate_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(),Task_Delete_Profiles__c='Australia', Task_Delete_Users__c= 'Marketo API',Is_Delete_Logic_Active__c = true);
        
        Profile ausProfile= [SELECT Id FROM profile WHERE Name='Australia' LIMIT 1]; 
        User ausUser = SL_TestDataFactory.createTestUser(ausProfile.Id, 'Aus', 'Test User');
        insert ausUser;

        system.runAs(ausUser){
            //create account
            Account objAcc = (Account)SL_TestDataFactory.createSObject(new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA'), true);
            //create contact
            Contact objCon = (Contact)SL_TestDataFactory.createSObject(new Contact(accountid = objAcc.id, lastname = 'Test Contact', MailingCountry = 'USA'), true);

            SL_TestDataFactory.createTestDataForCustomSetting();
            //create 02 Tasks
            List<Task> lstTask = new List<Task>();
            lstTask.add(new Task (ownerid = ausUser.Id, whatid = objAcc.Id, activityDate = date.today(), status = 'Completed', whoid = objCon.Id, subject = 'Test Task 01', Description = 'Test Description 01'));
            lstTask.add(new Task (ownerid = ausUser.Id, whatid = objAcc.Id, activityDate = date.today(), status = 'Completed', whoid = objCon.Id, subject = 'Test Task 02', Description = 'Test Description 02'));
            insert lstTask;

            List<Task> lstTaskBefore = [SELECT Id FROM Task];

            Test.startTest();
            delete lstTask;
            Test.stopTest();

            List<Task> lstTaskAfter = [SELECT Id FROM Task];
            system.assertEquals(lstTaskBefore.size() , 2);
            system.assertEquals(lstTaskAfter.size() , 0);
        }
    }

    //Delete other user task by System Administrator and this should be allowed
    @isTest
    static void testOnBeforeDeleteSce02() {

       // insert new Activity_Validate_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(),Task_Delete_Profiles__c='Australia', Task_Delete_Users__c= 'Marketo API',Is_Delete_Logic_Active__c = true);

        Profile ausProfile= [SELECT Id FROM profile WHERE Name='Australia' LIMIT 1]; 
        User ausUser = SL_TestDataFactory.createTestUser(ausProfile.Id, 'Aus', 'Test User');
        insert ausUser;
        Id taskId;

        system.runAs(ausUser){
            //create account
            Account objAcc = (Account)SL_TestDataFactory.createSObject(new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA'), true);
            //create contact
            Contact objCon = (Contact)SL_TestDataFactory.createSObject(new Contact(accountid = objAcc.id, lastname = 'Test Contact', MailingCountry = 'USA'), true);

            SL_TestDataFactory.createTestDataForCustomSetting();
            //create 01 Tasks
            Task task = new Task (ownerid = ausUser.Id, whatid = objAcc.Id, activityDate = date.today(), status = 'Completed', whoid = objCon.Id, subject = 'Test Task 01', Description = 'Test Description 01');
            insert task;
            taskId = task.Id;
        }

        Profile adminProfile= [SELECT Id FROM profile WHERE Name='System Administrator' LIMIT 1]; 
        User adminUser = SL_TestDataFactory.createTestUser(adminProfile.Id, 'Admin', 'Test User');
        insert adminUser;
        system.runAs(adminUser){
            List<Task> lstTaskBefore = [SELECT Id FROM Task WHERE Id =:taskId];

            Test.startTest();
            delete lstTaskBefore;
            Test.stopTest();

            List<Task> lstTaskAfter = [SELECT Id FROM Task WHERE Id =:taskId];
            system.assertEquals(lstTaskBefore.size() , 1);
            system.assertEquals(lstTaskAfter.size() , 0);
        }
    }

    //Try to delete Standard profile user his own tasks, but this should be not allowed
    @isTest
    static void testOnBeforeDeleteSce03() {

        insert new Activity_Validate_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(),Task_Delete_Profiles__c='Australia', Task_Delete_Users__c= 'Marketo API',Is_Delete_Logic_Active__c = true);

        Profile stndProfile= [SELECT Id FROM profile WHERE Name='Standard User' LIMIT 1]; 
        User stndUser = SL_TestDataFactory.createTestUser(stndProfile.Id, 'Standard', 'Test User');
        insert stndUser;

        system.runAs(stndUser){
            //create account
            Account objAcc = (Account)SL_TestDataFactory.createSObject(new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA'), true);
            //create contact
            Contact objCon = (Contact)SL_TestDataFactory.createSObject(new Contact(accountid = objAcc.id, lastname = 'Test Contact', MailingCountry = 'USA'), true);

            SL_TestDataFactory.createTestDataForCustomSetting();
            //create 02 Tasks
            List<Task> lstTask = new List<Task>();
            lstTask.add(new Task (ownerid = stndUser.Id, whatid = objAcc.Id, activityDate = date.today(), status = 'Completed', whoid = objCon.Id, subject = 'Test Task 01', Description = 'Test Description 01'));
            lstTask.add(new Task (ownerid = stndUser.Id, whatid = objAcc.Id, activityDate = date.today(), status = 'Completed', whoid = objCon.Id, subject = 'Test Task 02', Description = 'Test Description 02'));
            insert lstTask;

            List<Task> lstTaskBefore = [SELECT Id FROM Task];

            Test.startTest();
            try{
                delete lstTask;
                System.assert(false);
            }
            catch(Exception e)
            {
                System.assert(true);
                System.assert(e.getMessage().contains('You cannot delete this Task - Please contact your Administrator for assistance.'));
            }
            Test.stopTest();

            List<Task> lstTaskAfter = [SELECT Id FROM Task];
            system.assertEquals(lstTaskBefore.size() , 2);
            system.assertEquals(lstTaskAfter.size() , 2);
        }
    }

}