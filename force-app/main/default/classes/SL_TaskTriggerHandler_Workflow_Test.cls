@isTest
private class SL_TaskTriggerHandler_Workflow_Test 
{
    public static integer recordCount = 200;
    
    public static User objTestUser = [SELECT Id, Name FROM User WHERE FirstName ='Marketo' AND LastName = 'API' LIMIT 1];
    
    //Get Record Types
    public static Map<String, Schema.RecordTypeInfo> accountRecordTypeNameToIdMap = Schema.SObjectType.Account.getRecordTypeInfosByName();
    public static Id SalesConnectFirm = accountRecordTypeNameToIdMap.get('SalesConnect Firm').getRecordTypeId();
        
    public static Subscription_Group__c objSubscription_Group = (Subscription_Group__c)SL_TestDataFactory.createSObject(new Subscription_Group__c
                                                                                        (Name='Subscription', IsActive__c = true, 
                                                                                        Description__c = 'Test Description',
                                                                                        Eligibility_Sort_Order__c=1, KASL_Filter__c = 'ETF Trader'), True);
    public static Account objAccount = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 1', Channel__c = 'Bank', OwnerId=objTestUser.Id,
                                                                                        RecordTypeId = SalesConnectFirm,
                                                                                        BillingCountry='United States'), True);
    public static Contact objContact = (Contact)SL_TestDataFactory.createSObject(new Contact(AccountId = objAccount.Id, 
                                                                                        Unsubscribe_Public__c = 'Unsubscribe me from future emails',
                                                                                        FirstName='Test', LastName='Contact', OwnerId=objTestUser.Id,
                                                                                        Email='test@testvaneck.com.test',
                                                                                        MailingCountry = 'United States',
                                                                                        Subscription_Group__c = objSubscription_Group.id
                                                                                        ), True);
    @isTest
    public static void updateEmailActivityType_Insert()
    {
        system.runAs(objTestUser)
        {
            Test.startTest();
                TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
                settings.isActive_Task__c = TRUE;
                settings.isApex_Task_Workflows__c = TRUE;
                upsert settings custSettings__c.Id;
                
                system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
                                    'Expecting this Custom Setting value as true before doing any insert on Task');
                                        
                List<Task> lstTask = (List<Task>)SL_TestDataFactory.createSObjectList(
                                        new Task(Subject='Email', WhoId=objContact.Id,
                                                WhatId = objAccount.Id, ActivityDate = Date.today(),
                                                OwnerId=objTestUser.Id),
                                         recordCount, true);
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTask and Activity_Type__c = 'Email'].size(), 
                                            'Expecting the Task should have Activity_Type__c = Email as it is been update from APEX Trigger'); 
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTask and Purpose__c = 'Client Event'].size(), 
                                            'Expecting the Task should have Purpose__c = Client Event as it is been update from APEX Trigger');                                                  
            Test.stopTest();    
        }
    }
    
    @isTest
    public static void updateEmailActivityType_Update()
    {
        system.runAs(objTestUser)
        {
            Test.startTest();
                // Insert Start
                TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
                settings.isActive_Task__c = TRUE;
                settings.isApex_Task_Workflows__c = TRUE;
                upsert settings custSettings__c.Id;
                
                system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
                                    'Expecting this Custom Setting value as true before doing any insert on Task');
                                    
                List<Task> lstTask = (List<Task>)SL_TestDataFactory.createSObjectList(
                                        new Task(Subject='Unknown Test', WhoId=objContact.Id,
                                                WhatId = objAccount.Id, ActivityDate = Date.today(),
                                                OwnerId=objTestUser.Id),
                                         recordCount, true);
                // Insert Complete
                
                // Update Start
                List<Task> lstTaskUpdate = new List<Task>();
                for(Task objTask : lstTask)
                {
                    objTask.Subject = 'Email';
                    lstTaskUpdate.add(objTask);
                }
                
                if(!lstTaskUpdate.isEmpty())
                    update lstTaskUpdate;                        
                                         
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTaskUpdate and Activity_Type__c = 'Email'].size(), 
                                            'Expecting the Task should have Activity_Type__c = Email as it is been update from APEX Trigger'); 
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTaskUpdate and Purpose__c = 'Client Event'].size(), 
                                            'Expecting the Task should have Purpose__c = Client Event as it is been update from APEX Trigger');                                                  
            Test.stopTest();    
        }
    }   
    
    @isTest
    public static void updateCloseMarketoOpenActivities_Insert_ClickedLink()
    {
        system.runAs(objTestUser)
        {
            Test.startTest();
                TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
                settings.isActive_Task__c = TRUE;
                settings.isApex_Task_Workflows__c = TRUE;
                upsert settings custSettings__c.Id;
                
                system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
                                    'Expecting this Custom Setting value as true before doing any insert on Task');
                                        
                List<Task> lstTask = (List<Task>)SL_TestDataFactory.createSObjectList(
                                        new Task(Subject='Clicked Link', WhoId=objContact.Id,
                                                WhatId = objAccount.Id, ActivityDate = Date.today(),
                                                OwnerId=objTestUser.Id),
                                         recordCount, true);
                
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTask and Status = 'Clicked Link'].size(), 
                                            'Expecting the Task should have Status = Clicked Link as it is been update from APEX Trigger'); 
            Test.stopTest();    
        }
    }
    
    @isTest
    public static void updateCloseMarketoOpenActivities_Update_ClickedLink()
    {
        User objOtherUser = [Select Id from User where isActive = true limit 1];
        system.runAs(objTestUser)
        {
            Test.startTest();
                // Insert Start
                TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
                settings.isActive_Task__c = TRUE;
                settings.isApex_Task_Workflows__c = TRUE;
                upsert settings custSettings__c.Id;
                
                system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
                                    'Expecting this Custom Setting value as true before doing any insert on Task');
                                    
                List<Task> lstTask = (List<Task>)SL_TestDataFactory.createSObjectList(
                                        new Task(Subject='Unknown Test', WhoId=objContact.Id,
                                                WhatId = objAccount.Id, ActivityDate = Date.today(),
                                                OwnerId=objOtherUser.Id),
                                         recordCount, true);
                // Insert Complete
                
                // Update Start
                List<Task> lstTaskUpdate = new List<Task>();
                for(Task objTask : lstTask)
                {
                    objTask.Subject = 'Clicked Link';
                    objTask.OwnerId = objTestUser.Id;
                    lstTaskUpdate.add(objTask);
                }
                
                if(!lstTaskUpdate.isEmpty())
                    update lstTaskUpdate;                        
                                         
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTaskUpdate and Status = 'Clicked Link'].size(), 
                                            'Expecting the Task should have Status = Clicked Link as it is been update from APEX Trigger'); 
            Test.stopTest();    
        }
    }
    
    @isTest
    public static void updateCloseMarketoOpenActivities_Insert_OpenedEmail()
    {
        system.runAs(objTestUser)
        {
            Test.startTest();
                TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
                settings.isActive_Task__c = TRUE;
                settings.isApex_Task_Workflows__c = TRUE;
                upsert settings custSettings__c.Id;
                
                system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
                                    'Expecting this Custom Setting value as true before doing any insert on Task');
                                        
                List<Task> lstTask = (List<Task>)SL_TestDataFactory.createSObjectList(
                                        new Task(Subject='Opened Email', WhoId=objContact.Id,
                                                WhatId = objAccount.Id, ActivityDate = Date.today(),
                                                OwnerId=objTestUser.Id),
                                         recordCount, true);
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTask and Status = 'Opened Email'].size(), 
                                            'Expecting the Task should have Status = Opened Email as it is been update from APEX Trigger'); 
            Test.stopTest();    
        }
    }
    
    @isTest
    public static void updateCloseMarketoOpenActivities_Update_OpenedEmail()
    {
        User objOtherUser = [Select Id from User where isActive = true limit 1];
        
        system.runAs(objTestUser)
        {
            Test.startTest();
                // Insert Start
                TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
                settings.isActive_Task__c = TRUE;
                settings.isApex_Task_Workflows__c = TRUE;
                upsert settings custSettings__c.Id;
                
                system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
                                    'Expecting this Custom Setting value as true before doing any insert on Task');
                                    
                List<Task> lstTask = (List<Task>)SL_TestDataFactory.createSObjectList(
                                        new Task(Subject='Unknown Test', WhoId=objContact.Id,
                                                WhatId = objAccount.Id, ActivityDate = Date.today(),
                                                OwnerId=objOtherUser.Id),
                                         recordCount, true);
                // Insert Complete
                
                // Update Start
                List<Task> lstTaskUpdate = new List<Task>();
                for(Task objTask : lstTask)
                {
                    objTask.Subject = 'Opened Email';
                    objTask.OwnerId = objTestUser.Id;
                    lstTaskUpdate.add(objTask);
                }
                
                if(!lstTaskUpdate.isEmpty())
                    update lstTaskUpdate;                        
                                         
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTaskUpdate and Status = 'Opened Email'].size(), 
                                            'Expecting the Task should have Activity_Type__c = Email as it is been update from APEX Trigger'); 
            Test.stopTest();    
        }
    }
    
    @isTest
    public static void updateCloseMarketoAndMarketoMVISOpenActivitiesOther_Insert_ClickedLink()
    {
        system.runAs(objTestUser)
        {
            Test.startTest();
                TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
                settings.isActive_Task__c = TRUE;
                settings.isApex_Task_Workflows__c = TRUE;
                upsert settings custSettings__c.Id;
                
                system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
                                    'Expecting this Custom Setting value as true before doing any insert on Task');
                                        
                List<Task> lstTask = (List<Task>)SL_TestDataFactory.createSObjectList(
                                        new Task(Subject='Unknown Test Email', WhoId=objContact.Id,
                                                WhatId = objAccount.Id, ActivityDate = Date.today(),
                                                OwnerId=objTestUser.Id),
                                         recordCount, true);
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTask and Status = 'Email Sent'].size(), 
                                            'Expecting the Task should have Status = Opened Email as it is been update from APEX Trigger'); 
            Test.stopTest();    
        }
    }
    
    @isTest
    public static void updateCloseMarketoAndMarketoMVISOpenActivitiesOther_Update_ClickedLink()
    {
        User objOtherUser = [Select Id from User where isActive = true limit 1];
        
        system.runAs(objTestUser)
        {
            Test.startTest();
                // Insert Start
                TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
                settings.isActive_Task__c = TRUE;
                settings.isApex_Task_Workflows__c = TRUE;
                upsert settings custSettings__c.Id;
                
                system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
                                    'Expecting this Custom Setting value as true before doing any insert on Task');
                                    
                List<Task> lstTask = (List<Task>)SL_TestDataFactory.createSObjectList(
                                        new Task(Subject='Unknown Test', WhoId=objContact.Id,
                                                WhatId = objAccount.Id, ActivityDate = Date.today(),
                                                OwnerId=objOtherUser.Id),
                                         recordCount, true);
                // Insert Complete
                
                // Update Start
                List<Task> lstTaskUpdate = new List<Task>();
                for(Task objTask : lstTask)
                {
                    objTask.Subject = 'Uknown Test Email';
                    objTask.OwnerId = objTestUser.Id;
                    lstTaskUpdate.add(objTask);
                }
                
                if(!lstTaskUpdate.isEmpty())
                    update lstTaskUpdate;                        
                                         
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTaskUpdate and Status = 'Email Sent'].size(), 
                                            'Expecting the Task should have Activity_Type__c = Email as it is been update from APEX Trigger'); 
            Test.stopTest();    
        }
    }
    
    @isTest
    public static void updateSummaryRecap_Insert_SummaryRecap()
    {
        system.runAs(objTestUser)
        {
            Test.startTest();
                TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
                settings.isActive_Task__c = TRUE;
                settings.isApex_Task_Workflows__c = TRUE;
                upsert settings custSettings__c.Id;
                
                system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
                                    'Expecting this Custom Setting value as true before doing any insert on Task');
                                        
                List<Task> lstTask = (List<Task>)SL_TestDataFactory.createSObjectList(
                                        new Task(Subject='Unknown Test Email - Open Email', WhoId=objContact.Id,
                                                WhatId = objAccount.Id, ActivityDate = Date.today(),
                                                OwnerId=objTestUser.Id),
                                         recordCount, true);
                
                for(Task objTask : [Select Id, Subject, Summary_Recap__c from Task])
                {
                    system.debug('>>>>>>>>>>>objTask>>>>>>>>>>>>' + objTask.Subject);
                    system.debug('>>>>>>>>>>>objTask>>>>>>>>>>>>' + objTask.Summary_recap__c);
                }
                
                                         
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTask and 
                                                    Summary_Recap__c like 'Unknown Test Email - Open Email%'].size(), 
                                            'Expecting the Task should have Summary Recap same as Subject as it is been update from APEX Trigger'); 
            Test.stopTest();    
        }
    }
    
    @isTest
    public static void updateSummaryRecap_Update_SummaryRecap()
    {
        User objOtherUser = [Select Id from User where isActive = true limit 1];
        
        system.runAs(objTestUser)
        {
            Test.startTest();
                // Insert Start
                TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
                settings.isActive_Task__c = TRUE;
                settings.isApex_Task_Workflows__c = TRUE;
                upsert settings custSettings__c.Id;
                
                system.assertEquals(TRUE, TriggerOnOff__c.getOrgDefaults().isApex_Task_Workflows__c, 
                                    'Expecting this Custom Setting value as true before doing any insert on Task');
                                    
                List<Task> lstTask = (List<Task>)SL_TestDataFactory.createSObjectList(
                                        new Task(Subject='Unknown Test', WhoId=objContact.Id,
                                                WhatId = objAccount.Id, ActivityDate = Date.today(),
                                                OwnerId=objOtherUser.Id),
                                         recordCount, true);
                // Insert Complete
                
                // Update Start
                List<Task> lstTaskUpdate = new List<Task>();
                for(Task objTask : lstTask)
                {
                    objTask.Subject = 'Uknown Test Email - Clicked Link';
                    objTask.OwnerId = objTestUser.Id;
                    objTask.ActivityDate = Date.today().addDays(1);
                    lstTaskUpdate.add(objTask);
                }
                
                if(!lstTaskUpdate.isEmpty())
                    update lstTaskUpdate;                        
                                         
                system.assertEquals(recordCount, [Select Id from Task where Id IN: lstTaskUpdate and 
                                                    Summary_Recap__c like 'Uknown Test Email - Clicked Link%'].size(), 
                                            'Expecting the Task should have Summary Recap same as Subject as it is been update from APEX Trigger'); 
            Test.stopTest();    
        }
    }
    
}