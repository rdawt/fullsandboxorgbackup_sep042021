@IsTest 
public with sharing class BatchUpdateSCInternalFirmIdTest {

    @TestSetup static void setup()
    {
        Account objAcc01 = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 1', Channel__c = 'Financial Advisor', 
                            BillingCountry='United States',Firm_CRD__c = '8174'), false);
        insert objAcc01;

        Account objAcc02 = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 2', Channel__c = 'Financial Advisor', 
                            BillingCountry='United States',Firm_CRD__c = '40258'), false);
        insert objAcc02;

        Account objAcc03 = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 3', Channel__c = 'Financial Advisor', 
                            BillingCountry='United States',Firm_CRD__c = '106108'), false);
        insert objAcc03;
    }

    @IsTest static void testUpdateSCInternalFirmIdBatch() {

        Test.startTest();
        BatchUpdateSCInternalFirmId batch = new BatchUpdateSCInternalFirmId(); 
        database.executebatch(batch);
        Test.stopTest();

        List<Account> lstAccounts = [SELECT Id,Name,Internal_Firm_ID__c FROM Account WHERE Internal_Firm_ID__c != null];
        
        System.assert(lstAccounts != null);
        System.assertEquals(lstAccounts.size() , 3);      
    }

}