/*
TaskTriggerHandler
==================

This is the trigger handler for the task triggers
    - for tasks related to institutional accounts, update the corresponding totals on the related accounts

Version History
---------------

Created by Redkite on Dec 17, 2012

- Annoyance 19/19/2012 - delete flag argument added to updateAccounts to deal with strange potential SF bug that makes trigger.old items lose data in after delete
     not pretty, but prettier than two nearly identical 100 line functions

- Modified Jan 17, 2013
    Added filtering by profile ID, will only operate on tasks who's ownerid user profile == 00eA0000000RIrB (the production id for the 'Istitutional US User')

- Modified Mar 7, 2013
    Added Marketo Activity Custom object record from Marketo Task

- Modified Oct 15, 2014
    Commented out Contact Pace code as they are not used any more by US Inst Sales
    Marketo Activity Insert failing once Send Email started to Sync - hence bug fixing - eliminating the Task which dont have WhoId in it.
*/


public without sharing class TaskTriggerHandler {
    public static Boolean isHandleRecursion = false;
    public static Id marketoAPIOwnerId =    [SELECT ID FROM User WHERE FirstName = 'Marketo' AND LastName='API' LIMIT 1].Id; //005A00000032QAD
    public TaskTriggerHandler () {
        
    }
    
    // Trigger Methods
    public void onBeforeInsert(List<Task> listNew) 
    {
        updateTask_ContactInformation(listNew, true);
    }
    
    // Trigger Methods
    public void onBeforeUpdate(List<Task> listNew) 
    {
        updateTask_ContactInformation(listNew, false);
    }
    
    
    
    // This fucntion is updating the Task 4 field
    // ContactOwner__c
    // IsVEContact__c
    // IsTestContact__c
    // FirmName__c
    public void updateTask_ContactInformation(List<Task> listNew, Boolean isInsert)
    {
        Set<Id> setWhoId = new Set<Id>();
        Map<Id, Contact> mapContact = new Map<Id, Contact>();
        Set<Id> setContactId = new Set<Id>();
        Map<Id, String> mapContactChannel = new Map<Id, String>();

        for(Task objTask : listNew)
        {
            if(objTask.WhoId != NULL)
            {
                if( objTask.WhoId.getSobjectType() == Schema.Contact.SObjectType)
                {
                    setWhoId.add(objTask.WhoId);
                }
            }
        }
        
        if(!setWhoId.isEmpty())
        {
            for(Contact objContact : [Select Id,IS_User__c, IsTestContact__c, Channel_New__c, Account.Id, Account.Name from Contact where Id IN: setWhoId])
            {
                mapContact.put(objContact.Id, objContact);
            }
        }
        
        for(Task objTask : listNew)
        {
            if(objTask.WhoId != NULL)
            {
                if( objTask.WhoId.getSobjectType() == Schema.Contact.SObjectType)
                {
                    objTask.ContactOwner__c = objTask.WhoId;
                    
                    if(mapContact.containskey(objTask.WhoId))
                    {
                        objTask.Firm_Channel_Primary_Contacts_CF__c = mapContact.get(objTask.WhoId).Channel_New__c;
                        objTask.IsVEContact__c = mapContact.get(objTask.WhoId).IS_User__c;
                        objTask.IsTestContact__c = mapContact.get(objTask.WhoId).IsTestContact__c;
                        
                        if(isInsert == true)
                            objTask.FirmName__c = mapContact.get(objTask.WhoId).Account.Name;
                    }
                }
                
                system.debug('>>>>>>>>>>>>>objTask.ContactOwner__c>>>>>>>>>>>>' + objTask.ContactOwner__c);
            }
        }
    }
    
    // Trigger Methods
    public void onAfterInsert(List<Task> listNew) {
        
        /*
        list<task>tasksAssignedToMarketo = new list<task>();
        list<task>tasksAssignedToAUOwner = new list<task>();
        tasksAssignedToMarketo = filterTaskListByAssigneeMarketo(listNew);
        tasksAssignedToAUOwner = filterTaskListByCountry(tasksAssignedToMarketo);
        
        if (tasksAssignedToMarketo.size() > 0)
            addMarketoActivity(tasksAssignedToMarketo);
        // If we put else if.. then we are not getting any error
        if(tasksAssignedToAUOwner.size() > 0) // Adding below for loop so that in Future Notation Function can have Id and we can query all information over there
        { 
            Set<Id> setTaskId = new Set<Id>();
            for(Task objTask : tasksAssignedToAUOwner)
            {
                setTaskId.add(objTask.Id);
            }
            addFollowupTask(setTaskId);
            
        }
        */
        
        //'005A00000032QAD'; // Marketo User Id
        
        if( (listNew[0].CreatedById == marketoAPIOwnerId && listNew[0].OwnerId == marketoAPIOwnerId )
            ) //|| SL_MarketoRecords_Service.isProcssingFromJSON == true
            createMarketoActivity(listNew);
            
        if( (listNew[0].CreatedById == marketoAPIOwnerId 
            && listNew[0].ContactMailingCountry__c != NULL && listNew[0].ContactMailingCountry__c == 'Australia' )
                ) //|| SL_MarketoRecords_Service.isProcssingFromJSON == true 
        {
            if(TaskTriggerHandler.isHandleRecursion == false)
                createFollowUpTask(listNew);
        }
            
    }
    
    public void createFollowUpTask(List<Task> listNew)
    {
        list<task>tasksAssignedToAUOwner = new list<task>();
        for(Task t : listNew)
        {
            if(t.ContactMailingCountry__c == 'Australia')
            {
                tasksAssignedToAUOwner.add(t);
            }
        }
        //tasksAssignedToAUOwner = filterTaskListByCountry(listNew);
        if(tasksAssignedToAUOwner.size() > 0) // Adding below for loop so that in Future Notation Function can have Id and we can query all information over there
        { 
            /*
            Set<Id> setTaskId = new Set<Id>();
            for(Task objTask : tasksAssignedToAUOwner)
            {
                setTaskId.add(objTask.Id);
            }
            */
            //createMarketoTaskRecords(tasksAssignedToAUOwner);
            addFollowupTask(tasksAssignedToAUOwner);

            
        }
    }
    
    /* Commented by Vishruth/Shankar as this is still in progress
      private void createMarketoTaskRecords(list<task> lstAustraliaTask){
        list<MarketoTask__c> lstMarketoTask = new list<MarketoTask__c>();
        
        //Set<String> setMTConditions = new Set<String>();
        //for(MarketoTaskCondition__mdt obj : [Select Id, masterlabel from MarketoTaskCondition__mdt])
        //{
        //    setMTConditions.add(obj.MasterLabel);
        //}

       // String strMTConditions = '';
       // strMTConditions = setMTConditions.toString();
        
        try
        {
            for(Task objTask : lstAustraliaTask)
            {
                //if((!t.Subject.contains(strMTConditions))
                //{
                    MarketoTask__c marketo = new MarketoTask__c();
                    marketo.ActivityDate__c = String.valueOf(objTask.ActivityDate);
                    marketo.ContactMailingCountry__c = objTask.ContactMailingCountry__c;
                    marketo.ContactOwner__c = objTask.ContactOwner__c;
                    marketo.CreatedBy__c = objTask.CreatedById;
                    marketo.Description__c = objTask.Description;
                    marketo.FirmName__c = objTask.FirmName__c;
                    marketo.OwnerId__c = objTask.OwnerId;
                    marketo.Priority__c = objTask.Priority;
                    marketo.Status__c = objTask.Status;
                    marketo.Subject__c = objTask.Subject;
                    marketo.Summary_Recap__c = objTask.Summary_Recap__c;
                    marketo.TaskSubtype__c = objTask.TaskSubtype;
                    marketo.Type__c = objTask.Type;
                    marketo.WhatId__c = objTask.WhatId;
                    marketo.WhoId__c = objTask.WhoId;
                    marketo.Task_Id__c = objTask.Id;
                    
                    lstMarketoTask.add(marketo);
                //}
            }

            if(!lstMarketoTask.isEmpty())
                insert lstMarketoTask;
        }
        catch(Exception e){

        }
    }*/
    
    public void createMarketoActivity(List<Task> listNew)
    {
        list<task>tasksAssignedToMarketo = new list<task>();
        for(task t : listNew){
            if(t.ownerId == marketoAPIOwnerId)
                tasksAssignedToMarketo.add(t);
        }
        //tasksAssignedToMarketo = filterTaskListByAssigneeMarketo(listNew, marketoAPIOwnerId);
        if (tasksAssignedToMarketo.size() > 0)
            addMarketoActivity(tasksAssignedToMarketo);
    }
    
    private void addMarketoActivity(list<task> tasks){
        // add new record to MarketoActivity object - if opens
        // close the task - if clicks
        list<Marketo_Activitiy__c> marketoToAdd = new list<Marketo_Activitiy__c>();
        Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
        SendEmailNotification se = new SendEmailNotification('Error: Task Trigger Handler Exception');
        try{
            for(task t : tasks){
                // handle in workflow rule to close the clicked task
                if((!t.Subject.contains('Clicked Link'))&& t.WhoId != null){
                    Marketo_Activitiy__c marketo = new Marketo_Activitiy__c();
                    marketo.ActivityDate__c = t.ActivityDate;
                    marketo.Comments__c = t.Description;
                    marketo.Who__c = t.WhoId;
                    marketo.Priority__c = t.Priority;
                    marketo.What__c = t.AccountId;
                    marketo.Status__c = t.Status;
                    marketo.Subject__c = t.Subject;
                    marketo.Task_Id__c = t.Id;
                    
                    marketoToAdd.add(marketo);
                    system.debug('new marketo activity record........'+marketo);
                }
            }
            if(!marketoToAdd.isEmpty())
                insert marketoToAdd;
            // handle in scheduled apex to delete the open task
        }catch(Exception e){
            se.sendEmail('Exception Occurred in the Marketo Task Trigger - '+e.getMessage()+' \n '+tasks + e.getStackTraceString());
        }
    }
    
    //@future
    private void addFollowupTask(List<Task> tasks){
        //list<Task> tasks = new List<Task>();
        //tasks = [Select Id, Subject, Description ,ActivityDate, WhoId, WhatId, OwnerId, AccountId, ContactOwnerID__c, 
        //            Priority from Task where Id IN: setTaskId FOR UPDATE];
        //            
        
        system.debug('Inside addFollowupTask method.......'+tasks);         
        
        list<Task> follwupTasks = new list<Task>();
        list<id> conIdList = new list<id>();
        Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
        SendEmailNotification se = new SendEmailNotification('Error: Task Trigger Handler Exception');
        try{
            //for(Task t : tasks)
            //    conIdList.add(t.whoId);
            //get all the owner ids of cotnact's mapped in the list of tasks
            // Commenting this below code as we have created new Contact Lookup field and will use Formula Field to get Owner Information
            // WE will get this Owner information in before insert
            // Commented this code on 24 Sep 2019.
            //list<Contact> conlist =  [select id, ownerId from contact where id IN: conIdList FOR UPDATE];
            //map<Id, Contact> conOwnerMap = new map<Id, Contact>(conlist);

            for(Task t : tasks){
                if( 
                    ( (t.Subject != NULL && t.Subject !='' && t.Subject.contains('Clicked Link') && 
                        (t.Description != NULL && t.Description != '' && !t.Description.contains('unsubscribe'))) ) 
                                            ||
                    (t.Subject != NULL && t.Subject !='' && t.Subject.contains('Opened Email') && 
                        (t.Subject != NULL && t.Subject !='' && t.Subject.contains('Weekly Trade Idea'))) )
                {
                    // create followup task for the contact owner
                    Task newTask = new Task();
                    if(t.ActivityDate != NULL)
                    {
                        newTask.ActivityDate = (t.ActivityDate + 14);
                        newTask.ReminderDateTime = (t.ActivityDate + 13);
                    }
                    newTask.Description = t.Description;
                    newTask.WhoId = t.WhoId;
                    
                    // get the ownerid of the contact fromt the above map
                    //Contact c = conOwnerMap.get(t.WhoId);
                    newTask.OwnerId = t.ContactOwnerID__c;
                    
                    newTask.WhatId = t.AccountId;
                    newTask.Priority = t.Priority;
                    newTask.Status = 'Open';
                    newTask.Subject = t.Subject;
                    newTask.Summary_Recap__c = t.Subject;
                    system.debug('NEW AU TASK........'+newTask);
                    follwupTasks.add(newTask);
                }
            }
            if(!follwupTasks.isEmpty()){
                system.debug('inside the creation of follow up task');
                TaskTriggerHandler.isHandleRecursion = true;
                insert follwupTasks;
            }
        }catch(DMLException e){
            String strErrorBody = '';
            strErrorBody = 'Exception Line Number : ' + e.getLineNumber() + ' \n ' + 
                            'Exception Type Name : ' + e.getTypeName() +  ' \n ' + 
                            'Exception Stack Tracing : ' + e.getStackTraceString() + ' \n ' + 
                            'Exception Message : ' + e.getMessage();
                              
            se.sendEmailMoreDetails(strErrorBody);
        }
    }
    
    /*  Commented this function as it is not used now
    private List<task> filterTaskListByAssigneeMarketo(list<task> tasks, Id marketoAPIOwnerId){
        List<task> tasksToReturn = new list<task>();
        //Id marketoOwnerId = [SELECT ID FROM User WHERE FirstName = 'Marketo' AND LastName='API' LIMIT 1].Id;//'005A00000032QAD'; // Marketo User Id
        
        for(task t : tasks){
            if(t.ownerId == marketoAPIOwnerId)
                tasksToReturn.add(t);
        }
        system.debug('tasksToReturn.....'+tasksToReturn);
        return tasksToReturn;
    }
    */
    
    /* Commented this function as it is not used now 
    private List<task> filterTaskListByCountry(list<task> tasks){
        /* Commented out the code on 26 Sep 2019 and get the Contact from Australia direct from Contact Lookup
        system.debug('Inside Filter Task List.......'+tasks);
        list<task> tasksToReturn = new list<task>();
        list<id> conIdList = new list<id>();
        map<Id,Id> taskConIds = new map<Id,Id>();
        for(Task t : tasks){
            conIdList.add(t.whoId);
            taskConIds.put(t.id, t.whoId);
        }
        system.debug('Con Id List.....'+conIdList);
        list<Contact> conlist =  [select id, MailingCountry from contact where id IN: conIdList and MailingCountry = 'Australia'];
        system.debug('Marketo Task with  Contact in AU List.......'+conlist);
        
        for(Task t : tasks){
            for(Contact c : conlist)
                if(taskConIds.get(t.Id) == c.id)
                    tasksToReturn.add(t);
        }
        system.debug('tasksToReturn.....'+tasksToReturn);
        */
        /*
        system.debug('Inside Filter Task List.......'+tasks);
        list<task> tasksToReturn = new list<task>();
        for(Task t : tasks)
        {
            if(t.ContactMailingCountry__c == 'Australia')
            {
                tasksToReturn.add(t);
            }
        }
        system.debug('tasksToReturn.....'+tasksToReturn);
        return tasksToReturn;
    }
    */
    
    //Tharaka De Silva on 19-08-2020 ITSALES-1376
    //Added condition for safe deletion - allowed to delete Tasks owned by Task Owner from Australia profiles and Admins
    public void onBeforeDelete(List<Task> lstOldTasks){ 
        Activity_Validate_Settings__c settings = Activity_Validate_Settings__c.getOrgDefaults();
        if(settings.Is_Delete_Logic_Active__c){
            Set<Id> setProfileIds = new Set<Id>();
            Set<Id> setUserIds = new Set<Id>();
            Id currentUserId = UserInfo.getUserId();
            Id currentUserProfileId = UserInfo.getProfileId();
            
            if(settings.Task_Delete_Profiles__c != null){
                Set<String> setProfileNames = new Set<String>();
                setProfileNames.addAll(settings.Task_Delete_Profiles__c.split(','));
                for(Profile pr : [SELECT Id FROM profile WHERE Name In :setProfileNames]){
                    setProfileIds.add(pr.Id);
                }
            }
            if(settings.Task_Delete_Users__c != null){
                Set<String> setUserNames = new Set<String>();
                setUserNames.addAll(settings.Task_Delete_Users__c.split(','));
                for(User ur : [SELECT Id FROM User WHERE Name In :setUserNames]){
                    setUserIds.add(ur.Id);
                }
            }     
            Profile pfAdmin= [SELECT Id FROM profile WHERE Name='System Administrator' LIMIT 1]; 

            for (Task oldTask : lstOldTasks){
                //To delete the task : Current user profile should be equal to given profile from custom settings (Ex: Australia) and current user should be equal to task owner.
                //Also, allowed delete to special task owner records which are configured in Custom settings(Ex: marketo user) if only that user has enough permissions to delete that task
                if (setProfileIds.contains(currentUserProfileId) && (oldTask.OwnerId == currentUserId || setUserIds.contains(oldTask.OwnerId))){
                    return;
                }else if(currentUserProfileId == pfAdmin.Id){
                    return;
                }else {
                    oldTask.addError('You cannot delete this Task - Please contact your Administrator for assistance.');
                }
            }
        }
    }
}