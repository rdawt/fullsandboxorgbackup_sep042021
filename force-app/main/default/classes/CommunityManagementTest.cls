/**
    Author  :       Vidhya Krishnan
    Date Created:   3/21/12
    Description:    Test class for Community Management Base Class
 */
@isTest
private class CommunityManagementTest// extends DataStructures
{
    static testMethod void testCommunityManagement() {
        CommunityManagement cm = new CommunityManagement();
        Subscription_Group__c sg = new Subscription_Group__c();
        sg.Name = 'Test Group'; insert sg;
        Account acc = new Account(Name = 'CodeTest8', Channel__c = 'RIA', BillingCountry = 'USA');
        insert acc;
        Contact con=new Contact(FirstName='Test',LastName='User', AccountId = acc.Id, MailingCountry = 'USA');
        insert con;
        Community__c c = new Community__c(Name = 'Test Community', Is_Active__c = true, Default_Subscription_Group__c = sg.Id);
        insert c;
        Subscription__c s = new Subscription__c();
        s.Name = 'Test Subscription'; s.IsActive__c = true; s.Subscription_Indicator_Marker__c = 'test';
        s.Subscription_Display_Name__c = 'Test Subscription';
        insert s;
        CommunityManagement.MemberRecord mr = new CommunityManagement.MemberRecord();
        CommunityManagement.MemberRecord mr1 = new CommunityManagement.MemberRecord();
        CommunityManagement.MemberRecord mr2 = new CommunityManagement.MemberRecord();
        mr.communityId = String.valueOf(c.Id); mr.leadId = '00000'; mr.contactId = con.Id;
        mr.email = 'test@test.com'; mr.cUserName  = 'vaneck'; mr.passWord  = 'global1955'; mr.isActive  = true;
        mr.firstName  = 'test'; mr.lastName  = 'test'; mr.companyName  = 'test'; mr.jobTitle  = 'test';
        mr.address1  = 'test'; mr.address2  = 'test'; mr.city  = 'test'; mr.state  = 'test';
        mr.country  = 'test'; mr.zipcode  = 'test'; mr.phone  = 'test'; mr.loginCount = 1; mr.subscriptionGroupId = sg.Id;
        mr.investorType  = 'test'; mr.aum  = 'test'; mr.isFP  = 'true'; mr.subscriptionRequested  = 'test,test';
        mr.privacyAckDate = system.today(); mr.lastLogin = system.today(); mr.dbSourceCreateDate = system.today();
        boolean b = cm.addMemberRecord(mr);
        mr2.communityId = String.valueOf(c.Id); mr2.leadId = '00000'; mr2.contactId = con.Id;
        mr2.email = 'test3@test.com'; mr2.cUserName  = 'vaneck3'; mr2.passWord  = 'global1955'; mr2.isActive  = true;
        mr2.firstName  = 'test'; mr2.lastName  = 'test'; mr2.companyName  = 'test'; mr2.jobTitle  = 'test';
        mr2.address1  = 'test'; mr2.address2  = 'test'; mr2.city  = 'test'; mr2.state  = 'test';
        mr2.country  = 'test'; mr2.zipcode  = 'test'; mr2.phone  = 'test'; mr2.loginCount = 1; mr2.subscriptionGroupId = sg.Id;
        mr2.investorType  = 'test'; mr2.aum  = 'test'; mr2.isFP  = 'true'; mr2.subscriptionRequested  = 'test,test';
        mr2.privacyAckDate = system.today(); mr.lastLogin = system.today(); mr2.dbSourceCreateDate = system.today();
        Community_Registrant__c cr1 = new Community_Registrant__c();
        cr1.User_Name__c = 'test1'; cr1.Email__c = 'test1@test.com'; cr1.First_Name__c = 'test1';
        cr1.Last_Name__c = 'test1'; cr1.Company__c = 'test1';cr1.Address1__c = 'test1'; cr1.City__c = 'test';
        cr1.State__c = 'test'; cr1.Country__c = 'test'; cr1.Zip_Code__c = 'test'; cr1.Password__c = 'test';
        //sv test add the lines for getSubGroupId
        string subs1= cm.getSubGroupId( mr.email,  mr.communityId);
        if (subs1 !=null) {mr.subscriptionGroupId = subs1; cr1.Subscription_Group__c = subs1; }
        //end sv test code
        insert cr1;
        Community_Member__c cm1 = new Community_Member__c(Community__c = c.Id, Community_Registrant__c = cr1.Id);
        insert cm1;
        list<SubscriptionManagementAPI.SubscriptionDetails> subList = new list<SubscriptionManagementAPI.SubscriptionDetails>();
        SubscriptionManagementAPI.SubscriptionDetails sub = new SubscriptionManagementAPI.SubscriptionDetails();
        sub.contactId = con.Id; sub.subscriptionId = s.Id; sub.newValue = '1'; sub.oldValue = '0';
        subList.add(sub);
        mr1 = cm.getMemberRecord(cr1.Id);
        mr1.address2 = ''; mr1.aum = '';
        b = cm.setMemberRecord(cr1.Id, mr1);
        string s1 = cm.getCMemberId(c.Id,'test1');
        s1 = cm.isEmailTaken(c.Id, 'test123@test.com');
        s1 = cm.isEmailTaken(c.Id, 'test1@test.com');
        b = cm.isActive(cr1.Id);
        s1 = cm.isEmailTaken(c.Id, 'test1@test.com');
        s1 = cm.isEmailTaken(c.Id, 'test1@ml.com');
        s1 = cm.getPassword(cr1.Id);
        b = cm.setPassword(cr1.Id, 'test1');
        b = cm.setActive(cr1.Id, true);
        c = cm.getCommunityDetails(c.Id);
        s1 = cm.getCMemberType(cr1.Id);
        list<string> st = cm.getCommunityIds(cr1.User_Name__c);
        cm.setLastLogin(cr1.Id, system.today());
        cm.addCMemberAndSubscriptions(mr2, subList);
        cm.getConSubGrpId(mr2.email);
        cm.getDefaultSubGrpId(mr2.communityId);
        cm.getDomainEligiblity(mr2.email);
        cm.getErrorPrefix();
        cm.getSubGroupId(mr2.email, mr2.communityId);
        cm.setCMemberAndSubscriptions(mr2.cMemberId, mr2, subList);
        cm.updateSubReq(mr2.cMemberId, subList);
        //sv added for improved coverage
        
        
        cm.getSubGroupIdForCTA(mr.email, mr.communityId,'test');
        if ((mr1.communityId == 'a0qA000000412iE') || (mr1.communityId == 'a0qK0000001SbJK')|| (mr1.communityId == 'a0q3B000000IvkjQAC') || (mr1.communityId == 'a0qA00000042OLfIAM'))
                {
                    CreateConFromReg ccfr = new CreateConFromReg(cr1,mr1.communityId);
                    String s00 = ccfr.checkExistingContact();
                    if(s00.startsWith('WARNING:')){
                       string errMsg =  s00;
                       
                    }
                    else{
                         update cr1;
                    }
                }
        
        //
        //sv test code to improve coverage for CTA -getCMemberRecordV2
        SendEmailNotification se = new SendEmailNotification('Error: Community Management Class failed');
         mr1 = cm.getCMemberRecordV2(mr.email);
         string emailpassed = mr.email;
         try{
          list<Community_Registrant__c> cRegList = new list<Community_Registrant__c> ();
          
          if (cRegList.isEmpty()) { return; }
         
         s1 = cm.isEmailTaken(c.Id, 'test123@test.com');
        s1 = cm.isEmailTaken(c.Id, 'test1@test.com');
        b = cm.isActive(cr1.Id);
        s1 = cm.isEmailTaken(c.Id, 'test1@test.com');
        s1 = cm.isEmailTaken(c.Id, 'test1@ml.com');
        s1 = cm.getPassword(cr1.Id);
        b = cm.setPassword(cr1.Id, 'test1');
        b = cm.setActive(cr1.Id, true);
        c = cm.getCommunityDetails(c.Id);
        s1 = cm.getCMemberType(cr1.Id);
        list<string> stv = cm.getCommunityIds(cr1.User_Name__c);
        cm.setLastLogin(cr1.Id, system.today());
        cm.addCMemberAndSubscriptions(mr2, subList);
        cm.getConSubGrpId(mr2.email);
        cm.getDefaultSubGrpId(mr2.communityId);
        cm.getDomainEligiblity(mr2.email);
        cm.getErrorPrefix();
        cm.getSubGroupId(mr2.email, mr2.communityId);
        cm.setCMemberAndSubscriptions(mr2.cMemberId, mr2, subList);
        string retval = cm.getSubGroupIdForCTA('test1@test.com',mr1.communityId,'individual investor');
        cm.updateSubReq(mr2.cMemberId, subList);  
           }
           catch(Exception e){
            se.sendEmail('Exception Occurred in getCMemberV2**new method added by sv**- method for the given email id instead of cMemberId ** : '+emailpassed+' \n and the error is : '+e.getMessage()+' ; '+e.getStackTraceString());
            //return null;
        }
         //sv end test code
         
         //sv testing start again
         string ss = cm.getActive('abcd@abcd.com');
         //end sv test
         
    }
    
    static testmethod void sfdctest(){
    
    String email='testuser@gmail.com';
    
    CommunityManagement obj=new CommunityManagement();
    obj.getActive(email);
    }
}