@isTest
public with sharing class LeadTrigger_Handler_Test 
{
    @TestSetup
    private static void setUp()
    {
        User sysAdminUserObj = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'AdminUser',
            Email = 'AdminUser@admin.com',
            Username = 'SalesforceUser@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        
        System.runAs(sysAdminUserObj)
        {
            TriggerOnOff__c settings = TriggerOnOff__c.getOrgDefaults();
            settings.isActive_Lead__c = TRUE;
            upsert settings custSettings__c.Id;

            Account objAccountDefaultRegistrant = (Account)SL_TestDataFactory.createSObject(new Account(Name='Default Registrant Firm', Channel__c = 'Financial Advisor', 
                            BillingCountry='United States'), false);
            insert objAccountDefaultRegistrant;

            Account objAccountIndividualInvestor = (Account)SL_TestDataFactory.createSObject(new Account(Name='Individual Investor - Retail', Channel__c = 'Financial Advisor', 
                            BillingCountry='United States'), false);
            insert objAccountIndividualInvestor;

            Subscription__c subObj = (Subscription__c)SL_TestDataFactory.createSObject(
                                                                                        new Subscription__c(Name='Test Subscription',
                                                                                                            IsActive__c=true,
                                                                                                            Subscription_Display_Name__c='Test Display Name'    
                                                                                      ),true);
            Subscription__c subObj1 = (Subscription__c)SL_TestDataFactory.createSObject(
                                                                                        new Subscription__c(Name='Test Subscription1',
                                                                                                            IsActive__c=true,
                                                                                                            Subscription_Display_Name__c='Test Display Name1'   
                                                                                      ),true);
            
           Subscription_Group__c subGroupObj = (Subscription_Group__c)SL_TestDataFactory.createSObject(
                                                                                        new Subscription_Group__c(Name='Unassigned Eligibility',
                                                                                                                  IsActive__c=true
                                                                                       ),true);
            
            Community__c c = new Community__c(Name = 'Test Community', Is_Active__c = true, Default_Subscription_Group__c = subGroupObj.Id);
            insert c;

            Community_Registrant__c cr1 = new Community_Registrant__c();
            cr1.User_Name__c = 'test1'; cr1.Email__c = 'test1@test.com'; cr1.First_Name__c = 'test1';
            cr1.Country__c = 'United States';
            cr1.Last_Name__c = 'test1'; cr1.Company__c = 'test1';cr1.Address1__c = 'test1'; cr1.City__c = 'test';
            cr1.State__c = 'test'; cr1.Country__c = 'test'; cr1.Zip_Code__c = 'test'; cr1.Password__c = 'test';
            cr1.SFDC_Subscription_Ids_CSV__c = subObj.Id + ',' + subObj1.Id;
            cr1.Subscription_Group__c = subGroupObj.Id;
            insert cr1;

            Community_Member__c cm1 = new Community_Member__c(Community__c = c.Id,Community_Registrant__c = cr1.Id);
            insert cm1;
            System.debug('Community_Registrant Id :'+cr1.Id);
            Lead objLead = new Lead( FirstName = 'Test', LastName = 'Sample', Status = 'Open',
                                        Channel__c = 'Bank', Organization_Type__c = 'Bank BDs',
                                        Company = 'Testing Sample Co', Community_Registrant__c=cr1.Id,
                                        Activation_Approval_Detail__c = 'Test',
                                        LeadSource = 'Web',
                                        Activation_Approved__c = Datetime.now(),
                                        Activation_Requested__c = Datetime.now() );  
            insert objLead;  
        }
    }

    @isTest
    public static void testLeadConversionDoubleOptFunctionality()
    {
        
        Lead objLead = [Select Id, FirstName, LastName, Status , Country, 
                            Channel__c, Organization_Type__c, 
                            Company, Community_Registrant__c, Activation_Approval_Detail__c,
                            LeadSource, Activation_Approved__c,Activation_Requested__c
                            from lead limit 1];
        objLead.Status = 'GDPR Opted-In';
        update objLead;

        /*
        Database.LeadConvert lc = new database.LeadConvert();  
        lc.setLeadId( objLead.Id );  
        lc.setOwnerId(Userinfo.getUserId());
        lc.setBypassAccountDedupeCheck(true);
        lc.setBypassContactDedupeCheck(true);
        lc.setDoNotCreateOpportunity( true );  
        lc.setConvertedStatus('GDPR Opted-In');  
     
        Database.LeadConvertResult lcr = Database.convertLead(lc, false); 
        System.assertEquals(true, lcr.isSuccess(), 'Expecting Successfull Lead Conversion in Lead Trigger');
            */
    }
}