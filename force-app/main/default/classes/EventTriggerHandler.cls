/*

EventTriggerHandler
==================

This is the trigger handler for the event triggers
	- for events related to institutional accounts, update the corresponding totals on the related accounts

Version History
---------------

Created by Redkite on Dec 19, 2012

Annoyance 12/19/2012 - delete flag argument added to updateAccounts to deal with strange potential SF bug that makes trigger.old items lose data in after delete
 not pretty, but prettier than two nearly identical 100 line functions

- Modified Jan 17, 2013
	Added filtering by profile ID, will only operate on tasks who's ownerid user profile == 00eA0000000RIrB (the production id for the 'Istitutional US User')

- Modified Oct 15, 2014
	Commented out Contact Pace code as they are not used any more by US Inst Sales
*/

public without sharing class EventTriggerHandler {
	/*********************
	public EventTriggerHandler() {
	
	}
	
	// Trigger Methods
	public void onAfterInsert(List<Event> listNew) {
		
		List<event>eventsAssignedToInstitutionalUsers = new list<event>();
		eventsAssignedToInstitutionalUsers = filterEventListByAssigneeInstProfile(listNew);
		if (eventsAssignedToInstitutionalUsers.size() > 0) {
			updateAccounts(eventsAssignedToInstitutionalUsers, false);
		}
	}
	
	public void onAfterUpdate(Map<id, Event> oldmap, List<Event> listNew) {
		
		List<event>eventsAssignedToInstitutionalUsers = new list<event>();
		eventsAssignedToInstitutionalUsers = filterEventListByAssigneeInstProfile(listNew);
		if (eventsAssignedToInstitutionalUsers.size() > 0) {
			updateAccounts(eventsAssignedToInstitutionalUsers, false);
		}
	}
	
	public void onBeforeDelete(List<Event> listOld) {
		
		List<event>eventsAssignedToInstitutionalUsers = new list<event>();
		eventsAssignedToInstitutionalUsers = filterEventListByAssigneeInstProfile(listOld);
		if (eventsAssignedToInstitutionalUsers.size() > 0) {
			updateAccounts(eventsAssignedToInstitutionalUsers, true);
		}
	}
	
	public void onAfterUndelete(Map<id, Event> oldmap, List<Event> listNew) {
		
		List<event>eventsAssignedToInstitutionalUsers = new list<event>();
		eventsAssignedToInstitutionalUsers = filterEventListByAssigneeInstProfile(listNew);
		if (eventsAssignedToInstitutionalUsers.size() > 0) {
			updateAccounts(eventsAssignedToInstitutionalUsers, false);
		}
	}
	
	//Methods
	private void updateAccounts(List<Event> eventList, Boolean isBeforeDelete) {
		
		Set<id> whoIds = new Set<id>();
		Set<id> acctWhatIds = new Set<id>();
		Set<id> opptyWhatIds = new set<id>();
		List<Account> associatedAccounts = new List<Account>();
		List<Opportunity> associatedOpptys = new List<Opportunity>();
		List<Contact> associatedContacts = new List<Contact>();
		Map<id, Account> accounts = new Map<id, Account>();
		Map<id, id> contacts = new Map<id,id>();
		Map<id, id> opptys = new Map<id, id>();
		List<Event> eventsFromWhoIds = new List<Event>();
		List<Event> eventsFromWhatIds = new List<Event>();
		List<Event> eventsFromOpptyAccounts = new List<Event>();
		List<Event> eventMasterList = new List<Event>();
		Set<Id> usedEventIds = new Set<Id>();
		SendEmailNotification se = new SendEmailNotification('Error: Event Trigger Handler Exception');
		
		try {
			//get the relevant who and what ids
			
			for(Event e : eventList) {
				if(e.whatId != null) {
					if(((String)e.whatId).startsWith('001')){
						acctWhatIds.add(e.whatId);
					} else if (((String)e.whatId).startsWith('006')) {
						opptyWhatIds.add(e.whatId);
					}
				}
				if(e.whoId != null) {
					if(((String)e.WhoId).startsWith('003')){
						whoIds.add(e.whoId);
					}
				}
			}
			
			//if we have some events to work with, do work, otherwise this is the end of the line
			if(whoIds.size() > 0 || opptyWhatIds.size() > 0 || acctWhatIds.size() > 0) {
			
				//query	
				associatedAccounts = [select id from Account a where Channel__c = 'institutional' AND id IN :acctWhatIds];
				associatedContacts = [select id, Account.id from Contact c where Account.Channel__c = 'institutional' and c.id in :whoIds];
				 
				 //build maps
				for(Account a : associatedAccounts) {
					accounts.put(a.id, a);
				}
			
				for(Contact c : associatedContacts) {
					contacts.put(c.id, c.Account.id);
					
					//if this account is not already in the accounts list, add it
					if(!accounts.containsKey(c.Account.id)) {
						accounts.put(c.Account.id, c.Account);
					}
				}
				
				//query	
				eventsFromWhatIds = [SELECT id, Investment_Team_Involvement__c, Whatid, Whoid	
									FROM Event 
									WHERE Result__c = 'Held' 
									AND whatid IN :accounts.keySet() 
									AND ActivityDate = THIS_YEAR];
				eventsFromWhoIds = [SELECT id, Investment_Team_Involvement__c, Whatid, Whoid 
									FROM Event 
									WHERE Result__c = 'Held'
									AND whoId IN :contacts.keySet() 
									and ActivityDate = THIS_YEAR];
	
				//add in optys which are associated with tasks from contact associated tasks
				for(Event t : eventsFromWhoIds) {
					if(t.whatid != null) {
						if (((String)t.whatId).startsWith('006')) {
							if (!opptyWhatIds.contains(t.whatId)) {
								opptyWhatIds.add(t.whatId);
							}
						}
					}
				}
				
				associatedOpptys = [select id, Account.id from Opportunity o where account.channel__c = 'institutional' and o.id IN :opptyWhatIds];		
									
				for(Opportunity o : associatedOpptys) {
					opptys.put(o.id, o.Account.id);
					
					if(!accounts.containsKey(o.Account.id)) {
						accounts.put(o.Account.id, o.Account);
					}
				}
				
				eventsFromOpptyAccounts = [SELECT id, Investment_Team_Involvement__c, whatid, whoid
											FROM Event
											where Result__c = 'Held'
											and whatId IN :opptys.keySet()
											and ActivityDate = THIS_YEAR];
					 
				//combine event lists
				eventMasterList = new List<Event>();
				
				eventMasterList.addAll(eventsFromWhatIds);
				eventMasterList.addAll(eventsFromWhoIds);
				eventMasterList.addAll(eventsFromOpptyAccounts);
				
				//If Is BeforeDelete, add events to the used event list that are about to be deleted, excluding them from the tally
				if(isBeforeDelete){
					for (Event e : eventList) {
						usedEventIds.add(e.id);
					}
				}
				//zero out fields on accounts 
				for(Id a : accounts.keySet()) {
					accounts.get(a).PM_Meetings_Year_Actual_Num__c = 0;
					accounts.get(a).Analyst_Meetings_Year_Actual_Num__c = 0;
					accounts.get(a).Meetings_Year_No_Analyst_PM_Actual_Num__c = 0;
				}
				
				//blast through the event list
				if (eventMasterList.size() > 0) {
					for (Event t : eventMasterList) {
						if(!usedEventIds.contains(t.id)) { 
							if(t.Investment_Team_Involvement__c == 'PM') {
								if (t.whatid != null && ((String)t.whatId).startsWith('001')) {
									accounts.get(t.whatid).PM_Meetings_Year_Actual_Num__c += 1;
								} else if (t.whatid != null && ((String)t.whatId).startsWith('006')) {
									accounts.get(opptys.get(t.whatid)).PM_Meetings_Year_Actual_Num__c += 1;
								} else if (t.whoid != null && ((String)t.WhoId).startsWith('003')) {
									accounts.get(contacts.get(t.whoid)).PM_Meetings_Year_Actual_Num__c += 1;
								}
							} else if (t.Investment_Team_Involvement__c == 'Analyst') {
								if (t.whatid != null && ((String)t.whatId).startsWith('001')) {
									accounts.get(t.whatid).Analyst_Meetings_Year_Actual_Num__c += 1;
								} else if (t.whatid != null && ((String)t.whatId).startsWith('006')) {
									accounts.get(opptys.get(t.whatid)).Analyst_Meetings_Year_Actual_Num__c += 1;
								} else if (t.whoid != null && ((String)t.WhoId).startsWith('003')) {
									accounts.get(contacts.get(t.whoid)).Analyst_Meetings_Year_Actual_Num__c += 1;
								}
							} else if (t.Investment_Team_Involvement__c == 'No Investment Team') {
								if (t.whatid != null && ((String)t.whatId).startsWith('001')) {
									accounts.get(t.whatid).Meetings_Year_No_Analyst_PM_Actual_Num__c += 1;
								} else if (t.whatid != null && ((String)t.whatId).startsWith('006')) {
									accounts.get(opptys.get(t.whatid)).Meetings_Year_No_Analyst_PM_Actual_Num__c += 1;
								} else if (t.whoid != null && ((String)t.WhoId).startsWith('003')) {
									accounts.get(contacts.get(t.whoid)).Meetings_Year_No_Analyst_PM_Actual_Num__c += 1;
								}
							}
						}
						usedEventIds.add(t.id);// add event id to used events list
					}
				
					//Update the account!!!!!	
					update accounts.values();
				}
			}
		
		}catch(Exception e){
			se.sendEmail('Exception Occurred in the Event Trigger - '+e.getMessage()+' \n '+accounts + e.getStackTraceString());
		}
	}
	
	private List<event> filterEventListByAssigneeInstProfile(List<event> events) {
		List<event> eventsToReturn = new list<event>();
		List<id> ownerIds = new list<id>();
		List<user> users = new list<user>();
		set<id> userIds = new set<id>();
		String institutionalUserProfile = '00eA0000000RIrB';
		
		for (event e : events) {
			ownerIds.add(e.ownerid);
		}
		users = [select u.id, u.profileid from user u where u.profileid = :institutionalUserProfile AND u.id IN :ownerIds];
		
		for (user u : users) {
			userIds.add(u.id);
		}
		
		for(event e : events) {
			if (userIds.contains(e.ownerid)) {
				eventsToReturn.add(e);
			}
		}
		
		return eventsToReturn;
	}
	
	@isTest (seealldata=true)
	static void testEventTriggerHandler() {
		//create account
		Account acc = new Account(name = 'Test Account', channel__c = 'Institutional', BillingCountry = 'USA');
				acc.PM_Meetings_Year_Actual_Num__c = 0;
				acc.Analyst_Meetings_Year_Actual_Num__c = 0;
				acc.Meetings_Year_No_Analyst_PM_Actual_Num__c = 0;
		insert acc;
		
		//create contact
		Contact cont = new Contact(accountid = acc.id, lastname = 'Testcontact', MailingCountry = 'USA');
		insert cont;
		
		// Create oppty
		Opportunity oppty = new Opportunity(accountid = acc.id, closedate=date.today(), name='opptyname', amount = 100, stagename = 'Sample stage');
		insert oppty;	
		
		//create events
		//whoid
		//pm meeting
		Event whoPmMeetingEvent = new Event(whoid = cont.id, activityDate = date.today(), Result__c = 'Held', portfolio_manager__c = 'Charles Cameron', durationinminutes = 10, activityDateTime = datetime.now(), ownerId = '005A0000000PGpx');
		
		//analyst meeting
		Event whoAnalystMeetingEvent = new Event(whoid = cont.id, activitydate = date.today(), Result__c = 'Held', analyst__c = 'Joseph Foster', durationinminutes = 10, activityDateTime = datetime.now(), ownerId = '005A0000000PGpx');
		
		//no team meeting
		Event whoNoTeamMeetingEvent = new Event(whoid = cont.id, activityDate = date.today(), Result__c = 'Held', durationinminutes = 10, activityDateTime = datetime.now(), ownerId = '005A0000000PGpx');
				
		//whatid
		//Account Whatids	
		//pm meeting
		Event whatPmMeetingEvent = new Event(whatid = acc.id, activityDate = date.today(), Result__c = 'Held', portfolio_manager__c = 'Charles Cameron', durationinminutes = 10, activityDateTime = datetime.now(), ownerId = '005A0000000PGpx');
		
		//analyst call
		Event whatAnalystMeetingEvent = new Event(whatid = acc.id, activityDate = date.today(), Result__c = 'Held', analyst__c = 'Joseph Foster', durationinminutes = 10, activityDateTime = datetime.now(), ownerId = '005A0000000PGpx');
		
		//no team call
		Event whatNoTeamMeetingEvent = new Event(whatid = acc.id, activityDate = date.today(), Result__c = 'Held', durationinminutes = 10, activityDateTime = datetime.now(), ownerId = '005A0000000PGpx');
		
		//pm meeting
		Event whatOpptyPmMeetingEvent = new Event(whatid = oppty.id, activityDate = date.today(), Result__c = 'Held', portfolio_manager__c = 'Charles Cameron', durationinminutes = 10, activityDateTime = datetime.now(), ownerId = '005A0000000PGpx');
		
		//analyst call
		Event whatOpptyAnalystMeetingEvent = new Event(whatid = oppty.id, activityDate = date.today(), Result__c = 'Held', analyst__c = 'Joseph Foster', durationinminutes = 10, activityDateTime = datetime.now(), ownerId = '005A0000000PGpx');
		
		//no team call
		Event whatOpptyNoTeamMeetingEvent = new Event(whatid = oppty.id, activityDate = date.today(), Result__c = 'Held', durationinminutes = 10, activityDateTime = datetime.now(), ownerId = '005A0000000PGpx');
		
		Event[] eventsToInsert = new Event[]{whoPmMeetingEvent, whoAnalystMeetingEvent, whoNoTeamMeetingEvent, whatPmMeetingEvent, whatAnalystMeetingEvent, whatNoTeamMeetingEvent, whatOpptyPmMeetingEvent, whatOpptyAnalystMeetingEvent, whatOpptyNoTeamMeetingEvent};
		
		insert eventsToInsert;
		update eventsToInsert;
		
		acc = [Select id, PM_Meetings_Year_Actual_Num__c, analyst_meetings_year_actual_num__c, Meetings_year_no_analyst_pm_actual_num__c from Account a where a.id = :acc.id]; 
		
		//assert that accounts counts all equal 2
		System.assertEquals(3, acc.PM_Meetings_Year_Actual_Num__c);
		System.assertEquals(3, acc.Analyst_Meetings_Year_Actual_Num__c);
		System.assertEquals(3, acc.Meetings_Year_No_Analyst_PM_Actual_Num__c);
	
		//Delete The Events
		delete eventsToInsert;
		
		acc = [Select id,	PM_Meetings_Year_Actual_Num__c, analyst_meetings_year_actual_num__c, meetings_year_no_analyst_pm_actual_num__c from Account a where a.id = :acc.id]; 
		
		//assert that accounts counts all equal 0
		System.assertEquals(0, acc.PM_Meetings_Year_Actual_Num__c);
		System.assertEquals(0, acc.Analyst_Meetings_Year_Actual_Num__c);
		System.assertEquals(0, acc.Meetings_Year_No_Analyst_PM_Actual_Num__c);
		
		//UnDelete The Events
		undelete eventsToInsert;
		
		acc = [Select id, Emails_Year__c, PM_Meetings_Year_Actual_Num__c, analyst_meetings_year_actual_num__c, meetings_year_no_analyst_pm_actual_num__c from Account a where a.id = :acc.id]; 
		
		//assert that accounts counts all equal 0
		System.assertEquals(3, acc.PM_Meetings_Year_Actual_Num__c);
		System.assertEquals(3, acc.Analyst_Meetings_Year_Actual_Num__c);
		System.assertEquals(3, acc.Meetings_Year_No_Analyst_PM_Actual_Num__c);
	}
	**************************/
}