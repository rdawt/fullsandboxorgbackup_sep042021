/**************************************************************************************
* @Author: Tharaka De Silva
*
* This class implement the Schedulable interface in the purpose of calling BatchAccSalesByFundUpsert.
* This schedulable will upsert the Accounts Sales by Fund data in to backup object quarterly.
***************************************************************************************/
public class BatchScheduleAccSalesByFundUpsert implements Schedulable{
   
    public void execute(SchedulableContext sc)
    { 
        BatchAccSalesByFundUpsert batch = new BatchAccSalesByFundUpsert(); 
        database.executebatch(batch,200);
    }
}