@isTest
public class GIPS_Summary_With_Activities_Test 
{
    @TestSetup
    static void makeData()
    {
        Account objAccount = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 1', Channel__c = 'Financial Advisor', 
                            BillingCountry='United States'), false);
        insert objAccount;

        Contact objContact = (Contact)SL_TestDataFactory.createSObject(new Contact(LastName='Test Contact 01',
                            AccountId=objAccount.Id,MailingCountry='United States', Email='testUser01@gmail.com'),false);
        insert objContact;

        Fund__c objFund = (Fund__c)SL_TestDataFactory.createSObject( new Fund__c(Name='Test',Fund_Type__c='MF', GIPS__c = true, 
                            BU_Region__c='USA',
                            Fund_Group__c='International',Share_Class__c='A',Fund_Ticker_Symbol__c='MFTST'), false);
        insert objFund;

        GIPS__c objGIPS = (GIPS__c)SL_TestDataFactory.createSObject(new GIPS__c(
            Fund__c = objFund.Id,
            Contact__c = objContact.Id,
            Account__c = objContact.AccountId,
            Account_Name__c = objContact.Account.Name,
            GIPS_Mail_Out_By__c = UserInfo.getUserId(),
            Recorded_By__c = UserInfo.getUserId(),
            Recorded_By_Name__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName(),
            GIPS_YesNo__c = 'No',
            GIPS_SentDate__c = Date.today(),
            GIPS_OptOut_Discontinue_Date__c = Date.today().addYears(1),
            GIPS_MailOut_Date__c = Date.today(),
            GIPS_RenewalDate__c = Date.today().addYears(1)));
        
        insert objGIPS; 
    }    

    @isTest
    public static void GIPS_Summary_With_Activities_Contructor()
    {
        Contact objContact = [Select Id from Contact limit 1];
        ApexPages.StandardController std = new ApexPages.StandardController(objContact);
        GIPS_Summary_With_Act_Event objclass = new GIPS_Summary_With_Act_Event(std);
    }

    @isTest
    public static void GIPS_Summary_With_Act_Event_addDefault()
    {
        Contact objContact = [Select Id from Contact limit 1];
        ApexPages.StandardController std = new ApexPages.StandardController(objContact);
        GIPS_Summary_With_Act_Event objclass = new GIPS_Summary_With_Act_Event(std);
        objclass.selectedFunds = 'Test';
        objclass.addSelectedFundToList();
        objclass.addDefaultAttendee(objContact);
        objclass.addDefaultFunds();
        objclass.handleAddGIPS();
        objclass.handleAddFund();
        objclass.handleAddAttendee();
    }

    @isTest
    public static void GIPS_Summary_With_Act_Event_PageReferences()
    {
        Contact objContact = [Select Id from Contact limit 1];
        ApexPages.StandardController std = new ApexPages.StandardController(objContact);
        GIPS_Summary_With_Act_Event objclass = new GIPS_Summary_With_Act_Event(std);
        objclass.launchGIPSSummaryEvent();
        objclass.launchGIPSSummaryTask();
        objclass.handleCancelClick();
    }

    
    @isTest
    public static void GIPS_Summary_With_Act_Event_handleSaveClick()
    {
        Contact objContact = [Select Id from Contact limit 1];
        ApexPages.StandardController std = new ApexPages.StandardController(objContact);
        GIPS_Summary_With_Act_Event objclass = new GIPS_Summary_With_Act_Event(std);
        objclass.addDefaultAttendee(objContact);
        objclass.addDefaultFunds();
        objclass.handleAddGIPS();
        objclass.handleAddFund();
        objclass.handleAddAttendee();
        objclass.handleSaveClick();
    }

    
    @isTest
    public static void GIPS_Summary_With_Act_Event_getGIPSRecordsRelatedToContact()
    {
        Contact objContact = [Select Id from Contact limit 1];
        ApexPages.StandardController std = new ApexPages.StandardController(objContact);
        GIPS_Summary_With_Act_Event objclass = new GIPS_Summary_With_Act_Event(std);
        objclass.handleAddGIPS();
        objclass.getGIPSRecordsRelatedToContact();
    }
}