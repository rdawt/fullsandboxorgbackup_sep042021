@isTest
public class ChartJSCharts_Controller_Test 
{
    public static integer recordCount = 5;
    public static User objUser1 = [Select Id, EMEADBAccess__c, Division from User where isActive=true and Profile.Name = 'System Administrator' limit 1];
    public static User objUser2 = [Select Id from User where isActive=true and Profile.Name = 'Institutional Swiss Admin' limit 1];

    @TestSetup
    public static void createTestData()
    {
        Profile pf= [Select Id from profile where Name='System Administrator']; 
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 
        User uu=new User(firstname = 'ABC', 
                         lastName = 'XYZ', 
                         email = uniqueName + '@test' + orgId + '.org', 
                         Username = uniqueName + '@test' + orgId + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = uniqueName.substring(18, 23), 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         EMEADBAccess__c = 'Edit',
                         Division = 'Think ETF',
                         Email_Score_Weightage__c=1,Meeting_Score_Weightage__c=1,
                         Unknown_Score_Weightage__c=1,Call_Score_Weightage__c=1,
                         Conference_Call__c=1,Conference_Meeting__c=1,Meeting_One_on_One__c=1,
                         Meeting_Educational__c=1,Meeting_Social__c=1,Webinar__c=1,
                         Seminar_Workshop__c=1,Other__c=1,
                         ProfileId = pf.Id
                        ); 
        insert uu;

        System.runAs(uu)
        {
            
            List<Task> lstAllTask = new List<Task>();
            List<Task> lstTaskEmail = (List<Task>)SL_TestDataFactory.createSObjectList(
                                    new Task(Subject='Email', ActivityDate = Date.today(),
                                            OwnerId=objUser1.Id),
                                        recordCount, false);
            List<Task> lstTaskMeeting = (List<Task>)SL_TestDataFactory.createSObjectList(
                                            new Task(Subject='Meeting', ActivityDate = Date.today().addDays(-30),
                                                    OwnerId=objUser1.Id),
                                                recordCount, false);
            List<Task> lstTaskCall = (List<Task>)SL_TestDataFactory.createSObjectList(
                                            new Task(Subject='Call', ActivityDate = Date.today().addDays(-20),
                                                    OwnerId=objUser1.Id),
                                                recordCount, false);
            List<Task> lstTaskUnknown = (List<Task>)SL_TestDataFactory.createSObjectList(
                                            new Task(Subject='Unknown', ActivityDate = Date.today().addDays(-10),
                                                    OwnerId=objUser1.Id),
                                                recordCount, false);
            lstAllTask.addAll(lstTaskEmail);
            lstAllTask.addAll(lstTaskMeeting);
            lstAllTask.addAll(lstTaskCall);
            lstAllTask.addAll(lstTaskUnknown);

            if(!lstAllTask.isEmpty())
                insert lstAllTask;
            
            SL_TestDataFactory.createTestDataForContact();
            Account objAccount = [Select Id from Account limit 1];  
            
            List<Opportunity> lstOpportunity = (List<Opportunity>)SL_TestDataFactory.createSObjectList(
                                                    new Opportunity(Name='Opportuntiy', AccountId=objAccount.Id, 
                                                                    CloseDate= date.today().addDays(10), StageName='Notified win/Pending funding'
                                                                    ),
                                                        1, false);
            if(!lstOpportunity.isEmpty())
                insert lstOpportunity;
            
            List<Event> lstAllEvent= new List<Event>();
            List<Event> lstEventEmail = (List<Event>)SL_TestDataFactory.createSObjectList(
                                            new Event(Subject='Email', ActivityDate = Date.today(), DurationInMinutes=10, 
                                                ActivityDateTime = Datetime.now(), OwnerId=objUser2.Id),
                                                recordCount, false);
            List<Event> lstEventMeeting = (List<Event>)SL_TestDataFactory.createSObjectList(
                                            new Event(Subject='Meeting', ActivityDate = Date.today().addDays(-30), DurationInMinutes=10, 
                                                ActivityDateTime = Datetime.now(), OwnerId=objUser2.Id),
                                                recordCount, false);
            List<Event> lstEventCall = (List<Event>)SL_TestDataFactory.createSObjectList(
                                            new Event(Subject='Call', ActivityDate = Date.today().addDays(-20), DurationInMinutes=10,
                                                ActivityDateTime = Datetime.now(), OwnerId=objUser2.Id),
                                                recordCount, false);
            List<Event> lstEventUnknown = (List<Event>)SL_TestDataFactory.createSObjectList(
                                                new Event(Subject='Unknown', ActivityDate = Date.today().addDays(-10), DurationInMinutes=10,
                                                ActivityDateTime = Datetime.now(), OwnerId=objUser2.Id),
                                                recordCount, false);
            List<Event> lstEventZoom = (List<Event>)SL_TestDataFactory.createSObjectList(
                                                new Event(Subject='Zoom', Glance__c = true, ActivityDate = Date.today().addDays(-10), DurationInMinutes=10,
                                                ActivityDateTime = Datetime.now(), OwnerId=objUser2.Id),
                                                recordCount, false);
            lstAllEvent.addAll(lstEventEmail);
            lstAllEvent.addAll(lstEventMeeting);
            lstAllEvent.addAll(lstEventCall);
            lstAllEvent.addAll(lstEventUnknown);
            lstAllEvent.addAll(lstEventZoom);

            if(!lstAllEvent.isEmpty())
                insert lstAllEvent;
        }
    }   
    
    @isTest
    public static void testGetOnLoadRecords()
    {
        User objCurrentLoginUser =  [Select Id,Name, EMEADBAccess__c, 
                                        Unknown_Score_Weightage__c, Call_Score_Weightage__c, 
                                        Email_Score_Weightage__c, Meeting_Score_Weightage__c,
                                        Conference_Call__c, Conference_Meeting__c,Meeting_Educational__c,
                                        Meeting_One_on_One__c,Meeting_Social__c,Seminar_Workshop__c,
                                        Webinar__c,Other__c,
                                        Last_Score_Weightage_Saved__c
                                        from User where EMEADBAccess__c= 'Edit' and Division='Think ETF' limit 1];
        System.runAs(objCurrentLoginUser)
        {
            ChartJSCharts_Controller.getOnLoadRecords();
        }
    }

    @isTest
    public static void testGetProfileName()
    {
        ChartJSCharts_Controller.getProfileName();
    }

    // First Chart
    //All Users Filter
    @isTest
    public static void testGetTaskAndEventRecords_ALLUsers() // Testing for Date Range with Both Min and Max
    {
        List<String> lstOwnerId = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown', 'Zoom'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsMin_ALLUsers() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsMax_ALLUsers() // Testing for Date Range with Max Only
    {
        List<String> lstOwnerId = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsBothNull_ALLUsers() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{'ALL'};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    //Specific Users
    @isTest
    public static void testGetTaskAndEventRecords_User2() // Testing for Date Range with Both Min and Max
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsMin_User2() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsMax_User2() // Testing for Date Range with Max Only
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsBothNull_User2() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecords_User1() // Testing for Date Range with Both Min and Max
    {
        List<String> lstOwnerId = new List<String>{objUser1.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsMin_User1() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{objUser1.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsMax_User1() // Testing for Date Range with Max Only
    {
        List<String> lstOwnerId = new List<String>{objUser1.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsBothNull_User1() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{objUser1.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecords(lstOwnerId, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    // Opportunity Chart
    @isTest
    public static void testgetOpportunityRecords_User1() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{objUser1.Id};
        ChartJSCharts_Controller.getOpportunityRecords(lstOwnerId);
    }

    @isTest
    public static void testgetOpportunityRecords_User2() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        ChartJSCharts_Controller.getOpportunityRecords(lstOwnerId);
    }

    @isTest
    public static void testgetOpportunityRecords_ALLUser() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{'ALL', objUser1.Id, objUser2.Id};
        ChartJSCharts_Controller.getOpportunityRecords(lstOwnerId);
    }

    // Third Chart
    @isTest
    public static void testGetTaskAndEventRecordsByUser_User2() // Testing for Date Range with Both Min and Max
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsByUser(lstOwnerId, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsByUsersMin_User2() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsByUser(lstOwnerId, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsByUserMax_User2() // Testing for Date Range with Max Only
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsByUser(lstOwnerId, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsByUserBothNull_User2() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsByUser(lstOwnerId, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsByUser_User1() // Testing for Date Range with Both Min and Max
    {
        List<String> lstOwnerId = new List<String>{objUser1.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsByUser(lstOwnerId, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    // Fourth Chart
    @isTest
    public static void testGetTaskAndEventRecordsByUserScore_User2() // Testing for Date Range with Both Min and Max
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsByUserScore(lstOwnerId, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsByUsersMinScore_User2() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsByUserScore(lstOwnerId, date.today().addDays(-45), null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsByUserMaxScore_User2() // Testing for Date Range with Max Only
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsByUserScore(lstOwnerId, null, date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsByUserBothNullScore_User2() // Testing for Date Range with Min Only
    {
        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsByUserScore(lstOwnerId, null, null,lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsByUserScore_User1() // Testing for Date Range with Both Min and Max
    {
        List<String> lstOwnerId = new List<String>{objUser1.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsByUserScore(lstOwnerId, date.today().addDays(-45), date.today(),lstFilterActivityTypeSelected,intScoreSelected, 1, 1, 5, 1,false);
    }
    
    @isTest
    public static void testSaveCurrentUserDetails_User1() // Testing for Date Range with Both Min and Max
    {
        User objUser = ChartJSCharts_Controller.getMasterRegionUserDetails('Think ETF');
        objUser = ChartJSCharts_Controller.saveCurrentUserDetails(objUser , 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
    }

    @isTest
    public static void testGetBG_BDColor() // Testing for Date Range with Both Min and Max
    {
        List<String> lstType = new List<String>();
        Map<String, List<String>> mapBDColor = new Map<String, List<String>>();

        lstType = new List<String>{'Email', 'Meeting', 'Call', 'Unknown', 'Opportunity', 'LineScoreChart-Email', 'LineScoreChart-Call',
                                     'LineScoreChart-Meeting', 'LineScoreChart-Unknown', 'LineScoreChart-Total', 
                                     'User_0', 'User_1', 'User_2', 'User_3', 'User_4', 'User_5', 
                                     'User_6', 'User_7', 'User_8', 'User_9', 'User_10', 'User_11', 
                                     'User_12', 'User_13', 'User_14', 'User_15'};
        
        for(String strType : lstType)
            mapBDColor = ChartJSCharts_Controller.getBG_BDColor(strType);
    }

    // Fifth Chart
    @isTest
    public static void testGgetTaskAndEventRecordsBySalesUserScore_User2() // Testing for Date Range with Both Min and Max
    {
        User objUser = ChartJSCharts_Controller.getMasterRegionUserDetails('Think ETF');
        objUser = ChartJSCharts_Controller.saveCurrentUserDetails(objUser , 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'ALL','Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsBySalesUserScore(lstOwnerId, date.today().addDays(-45), 
                                                                        date.today(),lstFilterActivityTypeSelected,
                                                                        intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsBySalesUserScoreMinScore_User2() // Testing for Date Range with Min Only
    {
        User objUser = ChartJSCharts_Controller.getMasterRegionUserDetails('Think ETF');
        objUser = ChartJSCharts_Controller.saveCurrentUserDetails(objUser , 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsBySalesUserScore(lstOwnerId, date.today().addDays(-45), 
                                                                        null,lstFilterActivityTypeSelected,
                                                                        intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsBySalesUserScoreMaxScore_User2() // Testing for Date Range with Max Only
    {
        User objUser = ChartJSCharts_Controller.getMasterRegionUserDetails('Think ETF');
        objUser = ChartJSCharts_Controller.saveCurrentUserDetails(objUser , 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsBySalesUserScore(lstOwnerId, null, date.today(),
                                                                        lstFilterActivityTypeSelected,
                                                                        intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsBySalesUserScoreBothNullScore_User2() // Testing for Date Range with Min Only
    {
        User objUser = ChartJSCharts_Controller.getMasterRegionUserDetails('Think ETF');
        objUser = ChartJSCharts_Controller.saveCurrentUserDetails(objUser , 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

        List<String> lstOwnerId = new List<String>{objUser2.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsBySalesUserScore(lstOwnerId, null, null,
                                                                        lstFilterActivityTypeSelected,
                                                                        intScoreSelected, 1, 1, 5, 1,false);
    }

    @isTest
    public static void testGetTaskAndEventRecordsBySalesUserScore_User1() // Testing for Date Range with Both Min and Max
    {
        User objUser = ChartJSCharts_Controller.getMasterRegionUserDetails('Think ETF');
        objUser = ChartJSCharts_Controller.saveCurrentUserDetails(objUser , 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
        
        List<String> lstOwnerId = new List<String>{objUser1.Id};
        List<String> lstFilterActivityTypeSelected = new List<String>{'Email','Call','Meeting','Unknown'};
        Integer intScoreSelected = 100;
        ChartJSCharts_Controller.getTaskAndEventRecordsBySalesUserScore(lstOwnerId, date.today().addDays(-45), 
                                                                        date.today(),lstFilterActivityTypeSelected,
                                                                        intScoreSelected, 1, 1, 5, 1,false);
    }
}