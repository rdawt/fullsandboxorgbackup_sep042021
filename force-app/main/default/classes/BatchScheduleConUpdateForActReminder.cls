public with sharing class BatchScheduleConUpdateForActReminder implements Schedulable{
   
    public void execute(SchedulableContext sc)
    { 
        BatchContactUpdateForActivityReminder batch = new BatchContactUpdateForActivityReminder(); 
        database.executebatch(batch,200);
    }
}