/*
    Author  :       Vidhya Krishnan
    Date Created:   7/5/12
    Description:    Community Registration 2.0 - When a new Registration is created
                    1. If matching Contact exist, it maps the Registrant to existing Contact
                    2. If not, it creates new Contact with Registrant Record details & into default Account
                    3. If dupe contact exist, creates task to the contact owners
    Last Date Modified: May.20, 2016-SV added the fix for Dup Error on Community Member JN object during the SOAP API call from Ektron to addCMember plus included Community Ids when they
                    they are passed with 15 digits instead of standard 18 digits when called from external apps like Ektron
*/

public class CreateConFromReg{
    public String regId{get;set;}
    public String conId{get;set;}
    public string ownerId{get;set;}
    public string cAccountId{get;set;}
    public string strFirstName{get;set;}
    public string strLastName{get;set;}
    public string strEmail{get;set;}
    public string strCompany{get;set;}
    public string strTitle{get;set;}
    public string strPhone{get;set;}
    public string strAddress1{get;set;}
    public string strAddress2{get;set;}
    public string strCity{get;set;}
    public string strState{get;set;}
    public string strCountry{get;set;}
    public string strZipCode{get;set;}
    public DateTime privacyAckDate{get; set;} // Privacy policy awk Date
    public string strDbSource{get;set;}
    public string subGrpId{get;set;}
    public Boolean newCon{get;set;}
    public Community_Registrant__c cReg{get;set;}
    public string munchkinTokenListJSON{get;set;}
    public string campaignId{get;set;}
    public string myCommunityId{get;set;}
    public SendEmailNotification se = new SendEmailNotification('Error: Create Contact From Registrant Class Failed');
    public string setUpCall{get;set;}
    public string setUpCallRequestReason{get;set;}

    // Constructor to initialise all the variables
    public CreateConFromReg(Community_Registrant__c cReg, Id communityId){
        try{
            this.cReg = cReg;
            
            if(cReg != null){
                strFirstName = cReg.First_Name__c; strLastName = cReg.Last_Name__c; strEmail = cReg.Email__c; regId = cReg.Id; strCompany = cReg.Company__c; 
                strTitle = cReg.Title__c; strPhone = cReg.Phone__c; strAddress1 = cReg.Address1__c; strAddress2 = cReg.Address2__c; strCity = cReg.City__c; 
                strCountry = cReg.Country__c; strState = cReg.State__c; strZipCode = cReg.Zip_Code__c; conId = null;
                privacyAckDate = cReg.Privacy_Policy_Web_Ack_Date__c; subGrpId = cReg.Subscription_Group__c; munchkinTokenListJSON = cReg.MunchkinTokenListJSON__c; 
                campaignId = cReg.CampaignId__c; myCommunityId = communityId; setUpCall = cReg.Set_Up_Call__c; setUpCallRequestReason = cReg.Set_Up_Call_Request_Reason__c;
            }
            List<Community__c> communityList = new List<Community__c>();
            communityList = [select Default_Owner__c, Default_Account__c, Default_DB_Source__c from Community__c where Id =: communityId];
            if(!communityList.isEmpty()){
                ownerId = communityList[0].Default_Owner__c;
                cAccountId = communityList[0].Default_Account__c;
                strDbSource = communityList[0].Default_DB_Source__c;
            }
        }catch(Exception e){se.sendEmail('Exception Occurred in CreateConFromReg Constructor: \n'+e.getMessage()+cReg);
        }
    }
    
    // Method to check if the existing Contact with provided email id & map it if exists. 
    public string checkExistingContact(){
        List<Contact> conList = new List<Contact>();
        Boolean b0,b1;
        // search con for given email id
        /*  Exclude MVIS Contacts and create Intentional Dupe for MVIS Contacts */
        //when NO matching Contact is found in Contact object create a new Contact with Task created for CTA
        conList = [select id from Contact where Email =: strEmail and DB_Source__c != 'MVIS Registrant'];
       
        if(conList.isEmpty() || conList == null || conList.size() == 0){
        system.debug('inside no match');
            // if no match found, create new Contact & new Subscriptions
            newCon = true;
            system.debug('Creating new Contact.....');
            conId = addContact();
            if(conId == null)
                return 'WARNING: Creating new Contact from Registrant failed';
            else
               
                 try {
                         //try{
                                //must add a condition to see if the Community Member Record exists in JN Object
                         //       List<Community_Member__c> commList = new List<Community_Member__c>();
                         //       commList = [select id from Community_Member__c where Community__c = :myCommunityId and Community_Registrant__c = :regId LIMIT 1];
                          //      if(commList.isEmpty() || commList == null || commList.size() == 0){
                          //          Community_Member__c cMem = new Community_Member__c();
                           //         cMem.Community__c = myCommunityId; 
                           //         cMem.Community_Registrant__c = cReg.Id; //regId??
                                    //cMem.Community_Registrant__c = regId;
                            //        cMem.Contact__c = conId;
                             //       insert cMem;
                            //     }
                            //  }catch(Exception e)
                            //    {
                             //     se.sendEmail('Exception occured in Creating Community Member Junction Object: \n'+e.getMessage()+conId);
                             //     return null;
                             //    }
                          //end SV code for this segment    //create a CTA Commnunity Entry with the Contact Id and CR Id in the Campaign Member Junction Object-SV Sep.2015
                        //Called only for CTA Community ID
                        if (myCommunityId == 'a0q3B000000Co2YQAS' || myCommunityId =='a0qA000000412iEIAQ'|| myCommunityId == 'a0q3B000000Co2Y' || myCommunityId =='a0qA000000412iE') {
                            b1 = CreateTaskForLeadCollection(conId, cReg);
                          }
                        //if(b1 == false)
                        //return null; SV-non fatal, allowed to proceed.'WARNING: Creating task for Lead Collection/CTA for a  given Registrant failed....'+conId;
                        //return 'WARNING: Multiple Contact found for the given Registrant Email Id....'+conIds;
                    }catch(Exception e){ se.sendEmail('Exception the call to CreateTaskForLeadCollection method: \n'+e.getMessage()+conId); return null;
                         }
                 try {
                        //Called only for CTA Community ID
                        if (myCommunityId == 'a0q3B000000Co2YQAS' || myCommunityId =='a0qA000000412iEIAQ'|| myCommunityId == 'a0q3B000000Co2Y' || myCommunityId =='a0qA000000412iE') {
                            b0 = CreateCampMemForLeadCollection(conId, cReg);
                          }
                       // if(b0 == false)
                        //return null; SV-non fatal, allowed to proceed.'WARNING: Creating Campaign Member for Lead Collection/CTA for a  given Registrant failed....'+conId;
                    
                     }catch(Exception e){ se.sendEmail('Exception the call to CreateCampMemForLeadCollection method: \n'+e.getMessage()+conId); return null;
                         }        
                         
                return conId;
        }
        else if(conList.size() == 1){
         system.debug('inside match==1');
            // if exist match the contact to registrant
            newCon = false;
            try{
                    conId = conList[0].Id;
                    cReg.Contact__c = conId;
                    update cReg;  
                    //SV-must update the Contact during update to catch the Munchkin TokenList-next 2 lines
                    if(munchkinTokenListJSON != null) { conList[0].munchkinTokenListJSON__c = munchkinTokenListJSON;  }
                    update ConList;
                    
                        try{
                                //must add a condition to see if the Community Member Record exists in JN Object
                                List<Community_Member__c> commList = new List<Community_Member__c>();
                                commList = [select id from Community_Member__c where Community__c = :myCommunityId and Community_Registrant__c = :regId LIMIT 1];
                                if(commList.isEmpty() || commList == null || commList.size() == 0){
                                    Community_Member__c cMem = new Community_Member__c();
                                     cMem.Community__c = myCommunityId; 
                                    cMem.Community_Registrant__c = cReg.Id; //regId??
                                    //cMem.Community_Registrant__c = regId;
                                    cMem.Contact__c = conId;
                                    insert cMem;
                                 }
                        }catch(Exception e){se.sendEmail('Exception occured in Creating Community Member Junction Object: \n'+e.getMessage()+conId);return null;}
                          //end SV code for this segment    //create a CTA Commnunity Entry with the Contact Id and CR Id in the Campaign Member Junction Object-SV Sep.2015
                    system.debug('1-Matching Contact Id for the given Community Registrant is -for a Community Member who is new but a matching Contact already exists....'+conId);
                    //Called only for CTA Community ID
                    if (myCommunityId == 'a0q3B000000Co2YQAS' || myCommunityId =='a0qA000000412iEIAQ'|| myCommunityId == 'a0q3B000000Co2Y' || myCommunityId =='a0qA000000412iE') {
                        b1 = CreateTaskForLeadCollection(conId, cReg);
                     }
                    //if(b1 == false)
                    //return null; //SV-nonfatal allowed to proceed-'WARNING: Creating task for Lead Collection/CTA for a  given Registrant failed-from setMember....'+conId;
                    // assumption here is if this Contact and Community Member is already there 
                    //Campaign Member Table no need to add the Same Contact/Community Member into Campaign Member object
                    //Called only for CTA Community ID
                    if (myCommunityId == 'a0q3B000000Co2YQAS' || myCommunityId =='a0qA000000412iEIAQ'|| myCommunityId == 'a0q3B000000Co2Y' || myCommunityId =='a0qA000000412iE') {
                        b0 = CreateCampMemForLeadCollection(conId, cReg);
                     }
                    //if(b0 == false)
                    //return null; //SV-nonfatal so allowed to proceed-'WARNING: Creating Campaign Member for Lead Collection/CTA for a  given Registrant failed-from setMember(given Invalid Campaign Id....'+campaignId;
                }catch(Exception e){se.sendEmail('Exception Occurred in checkExistingContact method: \n'+e.getMessage()+strEmail);return 'WARNING: Mapping Contact Id to Registrant record failed: \n'+e.getMessage();}     
            system.debug('Matching Contact Id for the given Community Registrant is -for a Community Member who is new but a matching Contact already exists....'+conId); 
            //update cReg;   //this update done early                    
            return conId;
            }
        else if(conList.size() > 1){
            // Multiple matches found - Dupes - So Contact cannot be updated to map the Registrant or to Subscribe
            List<Id> conIds = new List<Id>();
            for(Contact c : conList)
                conIds.add(c.Id);
            Boolean b = CreateTask(conIds, cReg);
            if(b == false)
                return 'WARNING: Creating task for dupe Contacts for given Registrant failed....'+conIds;
            return 'WARNING: Multiple Contact found for the given Registrant Email Id....'+conIds;
        }
        return 'WARNING: Unidentified error occurred while mapping Registrant to a Contact';
    }

    // Method to add new Contact with given Registrant values
    public string addContact(){
        Contact con = new Contact();
        if(ownerId != null && ownerId != '') con.OwnerId = ownerId;
        if(strFirstName != null && strFirstName != '') con.FirstName = strFirstName;
        if(strLastName != null && strLastName != '') con.LastName = strLastName;
        if(strEmail != null && strEmail != '') con.Email = strEmail;
        if(cAccountId != null && cAccountId != '') con.AccountId = cAccountId;
            else
                /****   This is a hard coded value - so provide appropriate Account Id in each Sanbox & Production  */  
                //cAccountId = '001T000000wZ3Fr'; // For CMS Sandbox
                cAccountId = '001A000000u9gkE'; // For Production
        if(regId != null && regId != '') con.Community_Registrant__c = regId; // Add Reg Id here to avoid Dupe Blocker issues
        if(strTitle != null && strTitle != '') con.Title = strTitle;
        if(strPhone != null && strPhone != '') con.Phone = strPhone;
        if(strAddress1 != null && strAddress1 != ''){
            if(strAddress2 != null && strAddress2 != '') con.MailingStreet = strAddress1+'\n'+strAddress2;
            else con.MailingStreet = strAddress1;
        }
        if(strCity != null && strCity != '') con.MailingCity = strCity;
        if(strState != null && strState != '') con.MailingState = strState;
        if(strCountry != null && strCountry != '') con.MailingCountry = strCountry;
        if(strZipCode != null && strZipCode != '') con.MailingPostalCode = strZipCode;
        if(strDbSource != null && strDbSource != '') con.DB_Source__c = strDbSource;
        if(privacyAckDate != null) con.Privacy_Policy_Web_Ack_Date__c = DateTime.valueOf(privacyAckDate);
        if(subGrpId != null) { con.Subscription_Group__c = subGrpId; con.Auto_Subscription_Group__c = subGrpId; }
        if(munchkinTokenListJSON != null) { con.munchkinTokenListJSON__c = munchkinTokenListJSON;  }
        
        try{
            system.debug('Contact values before insert....'+con);
            try{
                insert con;
            }catch(Exception e){
                se.sendEmail('Exception Occurred in addContact method-while inserting a Contact Record in Contat Object: \n'+e.getMessage()+con);
                return null;
              }

            cReg.Contact__c = con.Id;
            try{
                update cReg;
            }catch(Exception e){                se.sendEmail('Exception Occurred in addContact method-while updating cReg with the newly created Con. Id: \n'+e.getMessage()+con);                return null;
              }

            return con.id;
        }catch(Exception e){            se.sendEmail('Exception Occurred in addContact method: \n'+e.getMessage()+con);            return null;
        }
    }
    
    // Method to create task
    public boolean CreateTask(List<Id> conIds, Community_Registrant__c cReg){
        List<Task> newTaskList = new List<Task>();
        List<Contact> conLst = new List<Contact>();
        conLst = [select Id, FirstName, LastName, Email, AccountId, OwnerId from Contact where Id IN: conIds];
        string Comments, sub;
        for(Contact c : conLst){
            sub = 'System Alert: Dupe Contact is preventing Registrant\'s Subscription';
            Comments = 'The Community Registrant '+cReg.First_Name__c+ ' '+cReg.Last_Name__c+ ' (Id: '+cReg.Id+')';
            Comments = Comments+' cannot subscribe to any Subscriptions due to the existance of dupe Contact Record \n\n';
            Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+conIds+'\n\n';
            Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
            newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Open', Subject = sub, IsReminderSet = true, Summary_Recap__c = sub,
                                    ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = c.Id, WhatId = c.AccountId, OwnerId = c.OwnerId));
        }
        try{
            if(newTaskList.size() > 0)
                insert newTaskList;
        }catch(Exception e){            se.sendEmail('Exception Occurred in CreateTask method: \n'+e.getMessage()+conIds);            return false;
        }
        return true;
    }
    
    
    // Method to create task for CTA Community for Campaign Responses - SV
    public boolean CreateTaskForLeadCollection(String conId, Community_Registrant__c cReg){
        List<Task> newTaskList = new List<Task>();
        List<Contact> conLst = new List<Contact>();
        List<Campaign> campaign = new List<Campaign>();
        campaign = [select Id, Name from Campaign where Id = :campaignId];
        try { 
            if (campaign != null && !campaign .isEmpty() && campaign.size() == 1){
                string campaignName = campaign[0].Name;
                conLst = [select Id, FirstName, LastName, Email, AccountId, OwnerId from Contact where Id = :conId];
                string Comments, sub;
                for(Contact c : conLst){
                    sub = campaignName + ' : Lead Collection - Follow-up Required with Community Registrant for CTA Campaign for their response';
                    Comments = 'The Community Registrant '+cReg.First_Name__c+ ' '+cReg.Last_Name__c+ ' (Id: '+cReg.Id+')';
                    if ((cReg.Set_Up_Call__c != null) && (cReg.Set_Up_Call__c == 'Yes')){
                         Comments = Comments+' has requested to Setup a Call in response to the CTA Campaign(name):' + campaignName + 'and the call request reason is: ' +setUpCallRequestReason + '\n\n';
                         //Assign to Matt Bartlett? below is the prod. id (uncomment the below line in Prod.)for Matt; CMS,FULL it would put to Greg D
                         //ownerId = '005A0000003nNB8';
                    }
                     else if ((cReg.Set_Up_Call__c != null) && ((cReg.Set_Up_Call__c == 'No') || (cReg.Set_Up_Call__c == '')|| (setUpCall == ' '))){
                        Comments = Comments+' has responsed to the CTA Campaign :  ' + campaignName + '\n\n';
                    }
                    else {
                                Comments = 'In Else: '+ Comments + ': ' + cReg.Set_Up_Call_Request_Reason__c;
                            }
                    
                    //Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+conIds+'\n\n';
                    //Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
                    //next two lines are commented to remove task specfic input while creating the Activity record for CTA
                    //newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Open', Subject = sub, IsReminderSet = true, Summary_Recap__c = sub,
                    //                        ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = c.Id, WhatId = c.AccountId, OwnerId = c.OwnerId)); //ownerId
                    //List<Community__c> communityList = new List<Community__c>();
                   //communityList = [select Default_Owner__c, Default_Account__c, Default_DB_Source__c from Community__c where Id =: myCommunityId];
                   // if(!communityList.isEmpty()){
                    //    ownerId = communityList[0].Default_Owner__c;
                   // }
                    newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Open', Subject = sub, IsReminderSet = false, Summary_Recap__c = sub,
                                            ActivityDate = System.today(), WhoId = c.id, WhatId = c.AccountId, OwnerId = ownerId)); //ownerId is the Community Owner as the CTA tasks are assigned Greg
                }
            }
            //bad Campaign Id was passed so insert without a Valid Campaign ID
            else{
                string Comments, sub;
                conLst = [select Id, FirstName, LastName, Email, AccountId, OwnerId from Contact where Id = :conId];
                for(Contact c : conLst){
                    sub = 'Lead Collection - Follow-up Required with Community Registrant for CTA Campaign for their response';
                    Comments = 'The Community Registrant '+cReg.First_Name__c+ ' '+cReg.Last_Name__c+ ' (CR Id: '+cReg.Id+')';
                    if ((cReg.Set_Up_Call__c != null) && (cReg.Set_Up_Call__c == 'Yes')){
                         Comments = Comments+' has responsed to the CTA Campaign(Invalid SFDC Campaign Id is passed from Ecktron Scenario)Campaign Id given is :  ' + campaignId + ' for the Contact Id: ' + conId + ' and the call request reason is: ' +setUpCallRequestReason + '\n\n';
                         //Assign to Matt Bartlett? below is the prod. id (uncomment the below line in Prod.)for Matt; CMS,FULL it would put to Greg D
                         //ownerId = '005A0000003nNB8';
                    }
                     else if ((cReg.Set_Up_Call__c != null) && ((cReg.Set_Up_Call__c == 'No') || (cReg.Set_Up_Call__c == '')|| (setUpCall == ' '))){                         Comments = Comments+' has responsed to the CTA Campaign(Invalid SFDC Campaign Id is passed from Ecktron Scenario)Campaign Id given is :  ' + campaignId + ' for the Contact Id: ' + conId + '\n\n';
                         //Assign to Matt Bartlett? below is the prod. id (uncomment the below line in Prod.)for Matt; CMS,FULL it would put to Greg D
                         //ownerId = '005A0000003nNB8';
                    }
                    else {
                                //Comments = 'In Else: '+ Comments + ': ' + cReg.Set_Up_Call_Request_Reason__c;
                                Comments = 'In Else: '+ Comments +' has responsed to the CTA Campaign(Invalid SFDC Campaign Id is passed from Ecktron Scenario)Campaign Id given is :  ' + campaignId + ' for the Contact Id:' + conId +'\n\n';
                                //Assign to Matt Bartlett? below is the prod. id (uncomment the below line in Prod.)for Matt; CMS,FULL it would put to Greg D
                                 //ownerId = '005A0000003nNB8';
                            }
                    
                    
                    
                   
                    //Comments = Comments+'List of Dupe Contact Ids found with same email id are : '+conIds+'\n\n';
                    //Comments = Comments+'Please merge the dupe Contacts and manually subscribe them to the given set of Subscriptions requested when registering in our site.';
                    //List<Community__c> communityList = new List<Community__c>();
                    //communityList = [select Default_Owner__c, Default_Account__c, Default_DB_Source__c from Community__c where Id =: myCommunityId];
                   //if(!communityList.isEmpty()){
                    //    ownerId = communityList[0].Default_Owner__c;
                   //  }
                    newTaskList.add(new Task(Description = Comments, Priority = 'Normal', Status = 'Open', Subject = sub, IsReminderSet = false, Summary_Recap__c = sub,
                                            ActivityDate = System.today(), WhoId = c.id, WhatId = c.AccountId, OwnerId = ownerId)); //ownerId is the Community Owner as the CTA tasks are assigned Greg
                }
            }
             if(newTaskList.size() > 0)
                insert newTaskList;
        }catch(Exception e){
            se.sendEmail('Exception Occurred in CreateTask method for CTA Lead Collection: \n'+e.getMessage()+ 'Campaign Id: ' + campaignId + 'Contact Id: ' +conId);
            return false; 
       
        }
        return true;
    }
    
    
    // Method to create Campaign Member insert for CTA Community for Campaign Responses - SV
    public boolean CreateCampMemForLeadCollection(String conId, Community_Registrant__c cReg){
         try{            
                if(conId != null && cReg.CampaignId__c != null){
                   List<Campaign> campId;
                   
                   system.debug('cReg.CampaignId__c' + cReg.CampaignId__c);
                   campId = [Select Id from Campaign where id =:cReg.CampaignId__c];
                   if(!campId.isEmpty() && campId != null && campId.size() == 1)
                   {
                       CampaignMember campMember = new campaignMember();
                       List<CampaignMember> campList = new List<CampaignMember>();
                   
                       campMember.Status = 'Responded';
                       //campMember.Contact = con;
                       campMember.ContactId = conId;
                       campList = [select id from CampaignMember where CampaignId = :cReg.CampaignId__c  and ContactId = :conId];
                       if(campList.isEmpty() || campList == null || campList.size() == 0){
                       // if no match found, create new Campaign Member Entry for the new Campaign for the same Registrant as well
                           system.debug('Continue Creating new Campaign Member Entry again.....');
                           campMember.CampaignId  = campaignId;
                           campMember.CampaignId = cReg.CampaignId__c ;
                           insert campMember;
                           return true;
                       }
                       else{
                           //return false when Contact Id is already found in the Campaign Member object
                           return true;
                       }
                    }
                    //return false when Campaign Id is not found in SFDC-invalid Campaign ID is passed from Ecktron
                   else{
                       se.sendEmail('Insert to Campaign Member Object failed due to the Invalid Campaign Id from Ecktron: ' + cReg.CampaignId__c + ' for the Contact Id: '+ conId);return false;
                   }
            }
            else{
                 //return false when Campaign Id or ContactId is passed as null from SetMember-from Ecktron
                 return false;  
                }
        }catch(Exception e){
                se.sendEmail('Exception Occurred in CreateCampaign Member method for CTA Lead Collection for a given : \n'+e.getMessage()+'Contact Id: '+conId +'Campaign Id :'+  campaignId);
                return false;
            }
       //return false;
    
    }
    
    
     //insert Campaign member table for Lead Collection project
     //   Campaign camp = new Campaign(Name='CampNewCTA',IsActive=true);
     //   insert camp;
      //  campMember.CampaignId = camp.Id;
        //campMember. = 'None';
       // campMember.Campaign = camp;
        //string s1 = 'responded to' + ' ' + camp.Name + ' ' + camp;
       //end lead collection Campaign member table    
}