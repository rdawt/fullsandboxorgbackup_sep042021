@isTest
public with sharing class GIPS_Summary_Controller_Test 
{
    @TestSetup
    static void makeData()
    {
        Account objAccount = (Account)SL_TestDataFactory.createSObject(new Account(Name='Test Account 1', Channel__c = 'Financial Advisor', 
                            BillingCountry='United States'), false);
        insert objAccount;

        Contact objContact = (Contact)SL_TestDataFactory.createSObject(new Contact(LastName='Test Contact 01',
                            AccountId=objAccount.Id,MailingCountry='United States', Email='testUser01@gmail.com'),false);
        insert objContact;

        Contact objContact2 = (Contact)SL_TestDataFactory.createSObject(new Contact(LastName='Test Contact 02',
                            AccountId=objAccount.Id,MailingCountry='United States', Email='testUser02@gmail.com'),false);
        insert objContact2;

        Fund__c objFund = (Fund__c)SL_TestDataFactory.createSObject( new Fund__c(Name='Test',Fund_Type__c='MF', GIPS__c = true, 
                            BU_Region__c='USA',
                            Fund_Group__c='International',Share_Class__c='A',Fund_Ticker_Symbol__c='MFTST'), false);
        insert objFund;

        Fund__c objFund2 = (Fund__c)SL_TestDataFactory.createSObject( new Fund__c(Name='Test2',Fund_Type__c='MF', GIPS__c = true, 
                            BU_Region__c='USA',
                            Fund_Group__c='International',Share_Class__c='A',Fund_Ticker_Symbol__c='EME'), false);
        insert objFund2;

        GIPS__c objGIPS = (GIPS__c)SL_TestDataFactory.createSObject(new GIPS__c(
            Fund__c = objFund.Id,
            Contact__c = objContact.Id,
            Account__c = objContact.AccountId,
            Account_Name__c = objContact.Account.Name,
            GIPS_Mail_Out_By__c = UserInfo.getUserId(),
            Recorded_By__c = UserInfo.getUserId(),
            Recorded_By_Name__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName(),
            GIPS_YesNo__c = 'No',
            GIPS_SentDate__c = Date.today(),
            GIPS_OptOut_Discontinue_Date__c = Date.today().addYears(1),
            GIPS_MailOut_Date__c = Date.today(),
            GIPS_RenewalDate__c = Date.today().addYears(1)));
        
        insert objGIPS;   

        Task objTask = (Task)SL_TestDataFactory.createSObject(new Task(
            WhoId = objContact.Id, 
            ActivityDate = Date.today(),
            OwnerId = system.userinfo.getUserId(),
            RIA_Activity_Score__c = 10,
            Status = 'Call Completed',
            US_Advisory_Call_Type__c = 'Sales Call',
            IsVEContact__c = false,
            IsTestContact__c = false,
            ContactOwner__c = objContact.Id, 
            WhatId = objAccount.Id, 
            GIPS__c = true,
            GIPS_Client_Strategy__c = 'EME',
            GIPS_Prospect_Select_Strategies__c = 'EME',
            Subject = 'Test Subject for Task'), true);

        Event objEvent = (Event)SL_TestDataFactory.createSObject(new Event(
            OwnerId = system.userinfo.getUserId(),
            WhoId = objContact.Id, 
            ContactOwner__c = objContact.Id, 
            WhatId = objAccount.Id, 
            ActivityDate=Date.today(),
            StartDateTime=Date.today(),
            EndDateTime=Date.today(),
            Result__c = 'Held',
            RIA_Activity_Score__c = 10,
            glance__c = true,
            GIPS__c = true,
            GIPS_Client_Strategy__c = 'EME',
            GIPS_Prospect_Select_Strategies__c = 'EME',
            Subject = 'Test Subject for Event'), true);

        EventRelation objEventRelation = new EventRelation(EventId = objEvent.Id, RelationId = objContact2.Id, isParent = false, isInvitee = false);
        insert objEventRelation;
        
        TaskRelation objTaskRelation = new TaskRelation(TaskId = objTask.Id, RelationId = objContact2.Id);
        insert objTaskRelation;
    }   
    
    @isTest
    public static void testGetOnLoadRecords_Task()
    {
        Task objTask = [Select Id from Task limit 1];
        GIPS_Summary_Controller objClass = new GIPS_Summary_Controller();
        
        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        mapGetOnLoadRecords = GIPS_Summary_Controller.getOnLoadRecords_Task(objTask.Id);
    }

    @isTest
    public static void testGetOnLoadRecords_Event()
    {
        Event objEvent = [Select Id from Event limit 1];
        GIPS_Summary_Controller objClass = new GIPS_Summary_Controller();
        
        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        mapGetOnLoadRecords = GIPS_Summary_Controller.getOnLoadRecords_Event(objEvent.Id);
    }

    @isTest
    public static void testGetOnLoadRecords_Contact()
    {
        Contact objContact = [Select Id from Contact limit 1];
        GIPS_Summary_Controller objClass = new GIPS_Summary_Controller();
        
        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        mapGetOnLoadRecords = GIPS_Summary_Controller.getOnLoadRecords_Contact(objContact.Id);
    }

    @isTest
    public static void testGetOnLoadRecords_Account()
    {
        Account objAccount = [Select Id from Account limit 1];
        GIPS_Summary_Controller objClass = new GIPS_Summary_Controller();
        
        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        mapGetOnLoadRecords = GIPS_Summary_Controller.getOnLoadRecords_Account(objAccount.Id);
    }

    @isTest
    public static void testSaveGIPSRecord_Contact()
    {
        Fund__c objFund = [Select Id from Fund__c limit 1];
        Contact objContact = [Select Id,AccountId from Contact limit 1];
        GIPS_Summary_Controller objClass = new GIPS_Summary_Controller();
        
        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        mapGetOnLoadRecords = GIPS_Summary_Controller.getOnLoadRecords_Contact(objContact.Id);

        List<GIPS__c> lstGIPS = new List<GIPS__c>();
        lstGIPS.addAll(GIPS_Summary_Controller.addGIPSSummary('Contact', objContact.Id, new List<GIPS__c>()));

        for(GIPS__c objGIPS : lstGIPS)
        {
            objGIPS.Fund__c = objFund.Id;
            objGIPS.Contact__c = objContact.Id;
            objGIPS.Account__c = objContact.AccountId;
            objGIPS.GIPS_SentDate__c = Date.today();
            objGIPS.Recorded_By_Name__c = UserInfo.getUserId();
            objGIPS.GIPS_Mail_Out_By__c = UserInfo.getUserId();
            objGIPS.GIPS_MailOut_Date__c = Date.today();
        }
        GIPS_Summary_Controller.saveContactGIPSRecord(lstGIPS, objContact.Id);
    }

    @isTest
    public static void testSaveGIPSRecord_Account()
    {
        Fund__c objFund = [Select Id from Fund__c limit 1];
        Contact objContact = [Select Id,AccountId from Contact limit 1];
        GIPS_Summary_Controller objClass = new GIPS_Summary_Controller();
        
        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        mapGetOnLoadRecords = GIPS_Summary_Controller.getOnLoadRecords_Contact(objContact.Id);

        List<GIPS__c> lstGIPS = new List<GIPS__c>();
        lstGIPS.addAll(GIPS_Summary_Controller.addGIPSSummary('Account', objContact.AccountId, new List<GIPS__c>()));

        for(GIPS__c objGIPS : lstGIPS)
        {
            objGIPS.Fund__c = objFund.Id;
            objGIPS.Contact__c = objContact.Id;
            objGIPS.Account__c = objContact.AccountId;
            objGIPS.GIPS_SentDate__c = Date.today();
            objGIPS.Recorded_By_Name__c = UserInfo.getUserId();
            objGIPS.GIPS_Mail_Out_By__c = UserInfo.getUserId();
            objGIPS.GIPS_MailOut_Date__c = Date.today();
        }
        GIPS_Summary_Controller.saveAccountGIPSRecord(lstGIPS, objContact.AccountId);
    }
    
    @isTest
    public static void testSaveGIPSRecord_Task()
    {
        Fund__c objFund = [Select Id from Fund__c limit 1];
        Contact objContact = [Select Id,AccountId from Contact limit 1];
        Task objTask = [Select Id  from Task limit 1];

        GIPS_Summary_Controller objClass = new GIPS_Summary_Controller();
        
        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        mapGetOnLoadRecords = GIPS_Summary_Controller.getOnLoadRecords_Contact(objContact.Id);

        List<GIPS__c> lstGIPS = new List<GIPS__c>();
        lstGIPS.addAll(GIPS_Summary_Controller.addGIPSSummary('Task', objTask.Id, new List<GIPS__c>()));

        for(GIPS__c objGIPS : lstGIPS)
        {
            objGIPS.Fund__c = objFund.Id;
            objGIPS.Contact__c = objContact.Id;
            objGIPS.Account__c = objContact.AccountId;
            objGIPS.GIPS_SentDate__c = Date.today();
            objGIPS.Recorded_By_Name__c = UserInfo.getUserId();
            objGIPS.GIPS_Mail_Out_By__c = UserInfo.getUserId();
            objGIPS.GIPS_MailOut_Date__c = Date.today();
        }
        GIPS_Summary_Controller.saveContactGIPSRecord(lstGIPS, objContact.Id);
    }

    @isTest
    public static void testSaveGIPSRecord_Event()
    {
        Fund__c objFund = [Select Id from Fund__c limit 1];
        Contact objContact = [Select Id,AccountId from Contact limit 1];
        Event objEvent = [Select Id  from Event limit 1];

        GIPS_Summary_Controller objClass = new GIPS_Summary_Controller();
        
        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        mapGetOnLoadRecords = GIPS_Summary_Controller.getOnLoadRecords_Contact(objContact.Id);

        List<GIPS__c> lstGIPS = new List<GIPS__c>();
        lstGIPS.addAll(GIPS_Summary_Controller.addGIPSSummary('Event', objEvent.Id, new List<GIPS__c>()));

        for(GIPS__c objGIPS : lstGIPS)
        {
            objGIPS.Fund__c = objFund.Id;
            objGIPS.Contact__c = objContact.Id;
            objGIPS.Account__c = objContact.AccountId;
            objGIPS.GIPS_SentDate__c = Date.today();
            objGIPS.Recorded_By_Name__c = UserInfo.getUserId();
            objGIPS.GIPS_Mail_Out_By__c = UserInfo.getUserId();
            objGIPS.GIPS_MailOut_Date__c = Date.today();
        }
        GIPS_Summary_Controller.saveContactGIPSRecord(lstGIPS, objContact.Id);
    }
}