/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-02-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   11-11-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
global class Batch_Create_Payee_Attendance_Records implements Database.Batchable<sObject> 
{
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        // collect the batches of records or objects to be passed to execute
        String query = 'SELECT Id, MasterLabel, DeveloperName FROM DB_InternalWholeSaler_Users__mdt';
        system.debug('>>>>>>>>>>query>>>>>>>>>>>' + query);
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<DB_InternalWholeSaler_Users__mdt> lstDB_InternalWholeSaler_Users) 
    {
        Integer intSelectedMonth = Date.today().month();
        Integer intSelectedYear = Date.today().year();

        DB_InternalWholeSaler_Controller.getCurrentMonthPayeeAttendanceRecordsDynamic(intSelectedMonth, intSelectedYear);
        
    }   
     
    global void finish(Database.BatchableContext BC) 
    {
        // execute any post-processing operations like sending email
    }
}