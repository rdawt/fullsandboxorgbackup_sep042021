/**************************************************************************************
* @Author: Tharaka De Silva
* @Date: 2021-03-30
*
* This class implement the Schedulable interface in the purpose of calling BatchConSalesByFundUpsert.
* This schedulable will upsert all the records in to History_Contact_Portfolio_Breakdown__c object from 
* SalesConnect__Contact_Portfolio_Breakdown__c object
***************************************************************************************/
public class BatchScheduleConSalesByFundUpsert implements Schedulable{

    public void execute(SchedulableContext sc)
    {
        BatchConSalesByFundUpsert batch = new BatchConSalesByFundUpsert(); 
        database.executebatch(batch,200); 
    }
}