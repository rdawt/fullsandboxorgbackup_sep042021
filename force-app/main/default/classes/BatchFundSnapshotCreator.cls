public class BatchFundSnapshotCreator implements Database.Batchable<sObject>, Database.Stateful{
      
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String strQuery = 'Select count_Distinct(SalesConnect_Contact__c), format(SUM(SalesConnect_Current_Assets__c)), Quarter__c, Fund_Lookup__c fundName, '
                        + 'Fund_Lookup__r.Fund_Ticker_Symbol__c, SalesConnect_Contact__r.Territory__c from History_Contact_Portfolio_Breakdown__c where '
                        + 'SalesConnect_Current_Assets__c >= 100000 and Fund_Lookup__r.Fund_Ticker_Symbol__c IN (\'EME\')  and SalesConnect_Current_Assets__c < 1000000 and Firm_Branch_Lookup__c = \'001A000000FjFjtIAF\' and Quarter__c IN ( \'FQ1 FY 2021\') '
                        + 'group by Quarter__c, Fund_Lookup__c, Fund_Lookup__r.Fund_Ticker_Symbol__c, SalesConnect_Contact__r.Territory__c ';
                        // +'SalesConnect__YTD_Redemptions__c,SalesConnect__YTD_Sales__c,SFDC_Channel_Id__c,SFDC_Fund_Id__c,SFDC_Fund_Ticker__c,LastModifiedDate,Fund_Vehicle_Type__c FROM SalesConnect__Firm_Portfolio_Breakdown__c WHERE Fund_Vehicle_Type__c = \'Mutual Fund\'';
        return Database.getQueryLocator(strQuery);
    }

    public void execute(Database.BatchableContext bc, List<AggregateResult> records) {
        System.debug(records);
        for (AggregateResult ar : records){
          System.debug((String)ar.get('fundName'));
           //     strChannelKey = (String)ar.get('channel');  
        }
    }

    public void finish(Database.BatchableContext bc) {

    }

}