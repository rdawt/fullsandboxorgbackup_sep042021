global class SL_UpdatePayeeSummaryBatch_Schedulable implements Schedulable 
{
   global void execute(SchedulableContext SC) 
   {
      String jobId = Database.executeBatch(new SL_UpdatePayeeSummaryBatch(), 100);
   }
}