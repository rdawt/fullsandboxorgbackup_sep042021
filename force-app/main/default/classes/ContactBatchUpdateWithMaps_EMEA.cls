/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 12-01-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   11-14-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
global class ContactBatchUpdateWithMaps_EMEA implements Database.Batchable < sObject >{

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        // Set<Id> TaskIdsFromTaskRelation = new Set<Id>();
        //  for (TaskRelation TaskRel : [Select TaskId from TaskRelation where RelationId in (Select Id from Contact Where Contact_Status__c != 'DELETE' AND MailingCountry = 'United States')])
        //AccountId = '001A000000i8CAs')])
        //  {
        //       TaskIdsFromTaskRelation.add(TaskRel.TaskId);
        //    }
        ContactUpdateDateSetting__c contactExtractDays = ContactUpdateDateSetting__c.getInstance('ActivityLastModifiedDateTimeline') ;
        string activityLastDays =   contactExtractDays.ActivityLastModifiedDateTimeline__c;
        string criteria = 'last_N_days:'+activityLastDays ;
        //String query = 'SELECT Id,ActivityDate,whoid FROM Task WHERE Id IN :TaskIdsFromTaskRelation AND Subject = \'call\' AND Status = \'call completed\' ORDER BY whoid,activitydate desc';
        //String query = 'Select Id,name,email from Contact Where Contact_Status__c != \'DELETE\' AND MailingCountry = \'United States\' and SalesConnect__Rep_Type__c != \'partnership\' and DB_Source__c != \'MVIS Registrant\' and IsTestContact__c != true and lastmodifieddate >= '+criteria+' ' ; // and id in (select relationid from eventwhorelation)';
        // below is active from Dec.12,2018
        String query = 'Select Id,name,email from Contact Where Contact_Status__c != \'DELETE\' AND Owner.profileid in (\'00eA0000000egONIAY\' ,  \'00eA0000000RIrQIAW\' , \'00eA0000000ehcBIAQ\') and SalesConnect__Rep_Type__c != \'partnership\' and DB_Source__c != \'MVIS Registrant\' and IsTestContact__c != true' ;
        // and id in (select relationid from eventwhorelation) Or id in (select relationid from taskwhorelation); //  and id = \'003A000001pihog\'' ; //and LastActivityDate != null  and id = \'003A000000GCUJi\'' <= bob higgins has both events n tasks; =>contact with no task:003A000001pihog
    
        //String query = 'SELECT Id, FirstName,LastName FROM Contact';
        //String query = 'SELECT Id,ActivityDate,whoid FROM Task WHERE Id in (Select TaskId from TaskRelation where RelationId in (Select Id from Contact)) AND Subject = \'call\' AND Status = \'call completed\' ORDER BY ActivityDate desc LIMIT 1'
        //'SELECT Id,ActivityDate,whoid FROM Task WHERE Id in (Select TaskId from TaskRelation where RelationId in (Select Id from Contact where AccountId =\'001A000000i8CAs\')) AND Subject = \'call\' AND Status = \'call completed\' ORDER BY ActivityDate desc LIMIT 1'
        system.debug('**start query**  '+ query );
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List <contact> scope)
    {
         //Create a new list of Contacts for updating;
         list<Contact> conList= new list<Contact>();
         string loopContactId;
         
         system.debug('**Inside-Execute**   --> : '+ scope);
         //map to get all the activity dates into a list
         
        try{
            Map<Id, EventWhoRelation> eventWhoRelationMap = new Map<Id, EventWhoRelation>();
            /*
            for(EventWhoRelation ewrForMap : [Select Id, Event.ID,Event.ActivityDate,Event.whoid,relationid,eventid,event.subject 
                                                From EventWhoRelation Where  Event.Result__c = 'held' and Event.whoid != null and 
                                                relationid IN :scope  and 
                                                event.lastmodifieddate = Today
                                                ORDER BY event.activitydate desc])
            */
            for(EventWhoRelation ewrForMap : [Select Id, Event.ID,Event.ActivityDate,Event.whoid,relationid,eventid,event.subject 
                                                From EventWhoRelation 
                                                Where  Event.Result__c = 'held' and Event.whoid != null and 
                                                        relationid IN :scope 
                                                        and event.lastmodifieddate = today
                                                ORDER BY event.activitydate desc])
            {   
                if(!eventWhoRelationMap.containsKey(ewrForMap.relationid)){
                    eventWhoRelationMap.put(ewrForMap.relationid, ewrForMap);
                    system.debug('**Inside-ewrforMap------Activity Date' +ewrForMap.event.activitydate+' '+'Contact Id with max event date '+ ewrForMap.relationid+' @@ '+contact.email+'Event Id-->>'+ewrForMap.Event.ID);
                }
                else{
                    system.debug('**outside-ewrforMap------Activity Date' +ewrForMap.event.activitydate+' '+'Contact Id with max event date '+ ewrForMap.relationid+' @@ '+contact.email+'Event Id-->>'+ewrForMap.Event.ID);
                }
            }
              
            Map<Id, TaskWhoRelation> taskWhoRelationMap = new Map<Id, TaskWhoRelation>();
            //?? temp commented on dec.3, 2018
            /*
            for(TaskWhoRelation twrforMap : [Select Id, Task.ID,Task.ActivityDate,Task.whoid,relationid,taskid, task.type,Task.Subject 
                                                From taskWhoRelation 
                                                Where   (task.subject ='Call' or task.subject like 'email%') and 
                                                (task.status= 'call completed' or task.status= 'call received' or task.status='Email Sent' ) and 
                                                task.whoid != null and relationid = :scope and 
                                                task.lastmodifieddate = today
                                                ORDER BY task.activitydate desc])
            */
            for(TaskWhoRelation twrforMap : [Select Id, Task.ID,Task.ActivityDate,Task.whoid,relationid,taskid, task.type,Task.Subject 
                                                From taskWhoRelation 
                                                Where   (task.subject ='Call' or task.subject like 'email%') and 
                                                    (task.status= 'call completed' or task.status= 'call received' or task.status='Email Sent' ) 
                                                    and task.whoid != null and relationid = :scope  
                                                    and task.lastmodifieddate = today
                                                ORDER BY task.activitydate desc])
            {
                if(!taskWhoRelationMap.containsKey(twrforMap.relationid)){
                    taskWhoRelationMap.put(twrforMap.relationid, twrforMap);
                    system.debug('**Inside-twrforMap------Activity Date' +twrForMap.task.activitydate+' '+'Contact Id with max task date '+twrForMap.relationid+' @@ '+contact.email+'Task Id-->>'+twrForMap.Task.ID);
                }
                else{
                    system.debug('**Outside-twrforMap------Activity Date' +twrForMap.task.activitydate+' '+'Contact Id with max task date '+twrForMap.relationid+' @@ '+contact.email+'Task Id-->>'+twrForMap.Task.ID);
                }
            }
           
            // end temp comment dec 03 2018 */  
            //end map processing
            //  list<EventWhoRelation> eventIdDateList = new list<EventWhoRelation>();
            //   eventIdDateList = [Select Id, Event.ID,Event.ActivityDate,Event.whoid,relationid,eventid,event.subject From EventWhoRelation Where  Event.Result__c = 'held' and Event.whoid != null and relationid IN :scope ORDER BY relationid, event.activitydate desc LIMIT 1];
              
            //  for(Task tsk : scope)
            try{
                for(Contact contact: scope)
                    {
                        date loopLastHumanTouchDateEvent;
                        date loopLastHumanTouchDateTask;
                        string loopWhoId;
                    
                        Contact activityContactToBeUpdated;
                        loopContactId =  string.valueof(contact.get('Id'));
                        List<EventWhoRelation> ContIdWithEventMAxDateList;
                        List<TaskWhoRelation> ContIdWithTaskMAxDateList;
                        string eventActivityType;
                        string taskActivityType;
                        
                        //map check for test
                        if (eventWhoRelationMap.size() > 0 && eventWhoRelationMap.containsKey(contact.id)){
                            system.debug('**HERE Inside Map Contains Key Match for EVENT***'+' '+contact.email);
                            EventWhoRelation ewr = eventWhoRelationMap.get(contact.id);
                            loopLastHumanTouchDateEvent =  ewr.Event.activitydate;
                            system.debug('**HERE in side to get the matching contact latest **EVENT **activity date from map for the Contact main loop***'+' '+contact.id);
                            eventActivityType = ' EventID-->> ' + ewr.event.id;
                            system.debug('**Inside-Contact For Loop in Event------Event Date' +ewr.event.activitydate+' '+loopContactId+' @@ '+contact.id);
                        }
                        else{
                            system.debug('**In Else where there is no matching Event for-Contact in For Loop for Event------'+' '+loopContactId+' @@ '+contact.id);
                        }
                             
                        if (taskWhoRelationMap.size() > 0 && taskWhoRelationMap.containsKey(contact.id)){
                        
                            system.debug('**HERE Inside Map Contains Key Match For TASK***'+' '+contact.email);
                            taskWhoRelation twr = taskWhoRelationMap.get(contact.id);
                            loopLastHumanTouchDateTask =  twr.Task.activitydate;
                            system.debug('**HERE in side to get the matching contact latest **TASK** activity date from map for the Contact main loop***'+' '+contact.id);
                            taskActivityType = ' TaskID-->> ' + twr.task.id;
                            system.debug('**Inside-Contact for Loop in Task------Task Date' +twr.task.activitydate+' '+loopContactId+' @@ '+contact.id);
                        }
                        if ((loopLastHumanTouchDateEvent != null) && (loopLastHumanTouchDateTask != null)){
                            //Event Activity Date is greater than Task Activity Date
                            if (loopLastHumanTouchDateEvent >= loopLastHumanTouchDateTask)  {
                                activityContactToBeUpdated = new contact(Id = loopContactId, LastHumanContactDate__c = loopLastHumanTouchDateEvent, LastHumanContactActivityType__c =  eventActivityType); //Reached_Date__c, Reached_Activity_Detail__c
                                //test code blocked for deploy =>activityContactToBeUpdated = new contact(Id = loopContactId, Reached_Date__c= loopLastHumanTouchDateEvent, Reached_Activity_Detail__c=  'Dec-04-R2** ' + eventActivityType); //Reached_Date__c, Reached_Activity_Detail__c
                                system.debug('**Inside-EVENT Date is > than Task--- Date' +loopLastHumanTouchDateEvent+' '+loopContactId+' @@EVENT ID>> '+eventActivityType);
                            }
                            else if (loopLastHumanTouchDateTask >= loopLastHumanTouchDateEvent){
                                activityContactToBeUpdated = new contact(Id = loopContactId, LastHumanContactDate__c = loopLastHumanTouchDateTask, LastHumanContactActivityType__c =  taskActivityType); 
                                    //test code blocked for deploy =activityContactToBeUpdated = new contact(Id = loopContactId, ReachedTaskDate__c = loopLastHumanTouchDateTask, ReachTaskDetail__c =  'Dec-04-R2** ' + taskActivityType); 
                                system.debug('**Inside-TASK Date is > than Event--- Date' +loopLastHumanTouchDateTask+' '+loopContactId+' @@TASK ID>> '+taskActivityType);
                            }
                        }
                        //make it event date as the last successful human touch date
                        else if ((loopLastHumanTouchDateEvent != null) && (loopLastHumanTouchDateTask == null)) {
                            activityContactToBeUpdated = new contact(Id = loopContactId, LastHumanContactDate__c = loopLastHumanTouchDateEvent, LastHumanContactActivityType__c =  eventActivityType); //Reached_Date__c, Reached_Activity_Detail__c
                            //test code blocked for deploy =activityContactToBeUpdated = new contact(Id = loopContactId, Reached_Date__c= loopLastHumanTouchDateEvent, Reached_Activity_Detail__c=  'Dec-04-R2** ' + eventActivityType);
                            system.debug('**Inside-Event Date is > than Task(Task date is null)--- Date' +loopLastHumanTouchDateEvent+' '+loopContactId+' @@EventID>> '+eventActivityType);
                        }
                        //make it task date as the last successful human touch date
                        else if ((loopLastHumanTouchDateEvent == null) && (loopLastHumanTouchDateTask != null)) {
                            activityContactToBeUpdated = new contact(Id = loopContactId, LastHumanContactDate__c = loopLastHumanTouchDateTask, LastHumanContactActivityType__c =  taskActivityType); 
                            //test code blocked for deploy =activityContactToBeUpdated = new contact(Id = loopContactId, ReachedTaskDate__c = loopLastHumanTouchDateTask, ReachTaskDetail__c =  'Dec-04-R2** ' + taskActivityType); 
                            system.debug('**Inside-TASK Date is > than Event(Event date is null)--- Date' +loopLastHumanTouchDateTask+' '+loopContactId+' @@TASK ID>> '+taskActivityType);
                        }
                        //both Event and Task Dates are null so never bother to insert that contact with any Last Successful Human Touch Date 
                        else if ((loopLastHumanTouchDateEvent == null) && (loopLastHumanTouchDateTask == null)) {
                            //no contact is added for update to the conList
                            system.debug('**Inside-Both TASK and Event Dates are NULL-->>' +' '+loopContactId);
                        }
                            
                        //  ContIdWithEventMAxDateList = [Select Id, Event.ID,Event.ActivityDate,Event.whoid,relationid,eventid,event.subject From EventWhoRelation Where  Event.Result__c = 'held' and Event.whoid != null and relationid = :loopContactId ORDER BY relationid, event.activitydate desc LIMIT 1]; //ORDER BY event.whoid,event.activitydate desc
                        //ContIdWithTaskMAxDateList  = [Select Id, Task.ID,Task.ActivityDate,Task.whoid,relationid,taskid, task.type,Task.Subject From taskWhoRelation Where   (task.subject ='Call' or task.subject like 'email%') and (task.status= 'call completed' or task.status= 'call received' or task.status='Email Sent' ) and task.whoid != null and relationid = :loopContactId ORDER BY relationid, task.activitydate desc LIMIT 1]; //ORDER BY task.whoid,task.activitydate desc
                            
                        /*      
                        If ((!ContIdWithEventMAxDateList.isEmpty()) && (!ContIdWithTaskMAxDateList.isEmpty())) {
                            //Event activity date is > Task
                            If ((ContIdWithEventMAxDateList[0].event.activitydate) >=  (ContIdWithTaskMAxDateList[0].task.activitydate)) {
                            
                                loopLastHumanTouchDate = ContIdWithEventMAxDateList[0].event.activitydate;
                                activityType = ContIdWithEventMAxDateList[0].event.subject;
                                system.debug('**Inside-EventDate>TaskDate ------' +ContIdWithEventMAxDateList[0].event.activitydate+' '+loopContactId+' @@ '+contact.email);
                            
                            }
                            //Task Activity Date is > than Event
                            else if  ((ContIdWithTaskMAxDateList[0].Task.activitydate) >  (ContIdWithEventMAxDateList[0].Event.activitydate)){
                            
                                loopLastHumanTouchDate = ContIdWithTaskMAxDateList[0].task.activitydate;
                                activityType = ContIdWithTaskMAxDateList[0].Task.subject;
                                system.debug('**Inside-EventDate<TaskDate ------' +ContIdWithEventMAxDateList[0].event.activitydate+' '+loopContactId+' @@ '+contact.email);
                            
                        
                            }
                            system.debug('**HERE 1***');
                            parentTaskContact = new contact(Id = loopContactId, LastHumanContactDate__c = loopLastHumanTouchDate, LastHumanContactActivityType__c =  activityType);
                            system.debug('**HERE 2***');
                        
                        }
                        //no entry for the Events but Task only exists for the contact so u use the Task Activity Date for the most recent info.
                        if ((ContIdWithEventMAxDateList.isEmpty()) && (!ContIdWithTaskMAxDateList.isEmpty()) ) {
                            loopLastHumanTouchDate = ContIdWithTaskMAxDateList[0].task.activitydate;
                            system.debug('**HERE 3***'+' '+contact.email);
                            //activityType = ContIdWithTaskMAxDateList[0].task.type; //NO line is safe t uncomment is required-is this causing index out of bound error?
                            activityType = ContIdWithTaskMAxDateList[0].task.subject;
                            parentTaskContact = new contact(Id = loopContactId, LastHumanContactDate__c = loopLastHumanTouchDate, LastHumanContactActivityType__c =  activityType);
                            system.debug('**Inside-OnlyWhenTaskisFoundNOEventFoundForContact------' +ContIdWithTaskMAxDateList[0].task.activitydate+' '+loopContactId+' @@ '+contact.email);
                            
                        }
                         */  
                               //no entry for the Task but Event only exists for the contact so u use the Event Activity Date for the most recent info.
                            //  if ((!ContIdWithEventMAxDateList.isEmpty()) && (ContIdWithTaskMAxDateList.isEmpty()) ) {
                         
                         //     if ((!ContIdWithEventMAxDateList.isEmpty())  ) {
                         //         system.debug('**HERE 3.5!!***'+' '+contact.email);
                         //         loopLastHumanTouchDate = ContIdWithEventMAxDateList[0].Event.activitydate;
                         //         system.debug('**HERE 4***'+' '+contact.email);
                         //         activityType = ContIdWithEventMAxDateList[0].event.subject;
                         //         parentTaskContact = new contact(Id = loopContactId, LastHumanContactDate__c = loopLastHumanTouchDate, LastHumanContactActivityType__c =  activityType);
                         //         system.debug('**Inside-OnlyWhenEventisFoundNOTASKFoundForContact------' +ContIdWithEventMAxDateList[0].event.activitydate+' '+loopContactId+' @@ '+contact.email);
                             //  }
                             
                        
                             
                          //   }
                        if (activityContactToBeUpdated != null){
                            conList.add(activityContactToBeUpdated);
                            activityContactToBeUpdated = null;
                        } 
                            
                    } //end of for loop for 200 records
                        //update contact outside the for loop
                    try{
                        if (conList != null){
                            system.debug('*After For Loop Execute Scope inside conlist update**   --> : '+ conList);
                            update conList;
                        }
                        else{
                            system.debug('*Empty ConList for update in this batch!!***->>After For Loop Execute Scope inside conlist update**   --> : '+ conList);
                        }
                        }catch (Exception e)
                            {
                                string error = 'Exception occured in  Batch Apex failed while updating the conList with the contact to update the date..... '+e.getMessage()+conList;
                                system.debug('error catch at the exeption level-error catch bloc at the update contact statement ------' +error);
                                SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Batch Update Of Last Human Interation Date processing module');
                                String    msg = 'Exception Occurred in Apex while writing to LastHumanInteraction Date update(updateconList) **in UPDATE Statment** in Contact \n '+e.getMessage() + ' ' +conList;
                                se.sendEmail(msg);
                            }
                    } catch (Exception e) {
                        string error = 'Exception occured in  Batch Apex failed while processing the loop in Events or sObjects..... '+e.getMessage()+conList;
                        system.debug('error catch at the exeption level-error catch bloc ------' +error);
                        SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Batch Update Of Last Human Interation Date processing module');
                        String    msg = 'Exception Occurred in Apex while writing to LastHumanInteraction Date update in Contact-in For LOOOP level \n '+e.getMessage() + ' ' +loopContactId;
                        se.sendEmail(msg);
                        }
        }catch (Exception e) {
           string error = 'Exception occured in  Batch Apex failed while processing the try catch section of Map SOQL..... '+e.getMessage()+scope;
           system.debug('error catch at the exeption level-error catch bloc of the map load from original call in the beginning of Execution ------' +error);
           SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Batch Update Of Last Contact activity using MAPS processing module');
           String    msg = 'Exception Occurred in Apex while writing to Last Contact Date update in Contact- outside of LOOP level in class using MAPS  \n '+e.getMessage() + ' ' +loopContactId;
           se.sendEmail(msg); 
        }   
     }       
    
    global void finish(Database.BatchableContext BC) {}
}