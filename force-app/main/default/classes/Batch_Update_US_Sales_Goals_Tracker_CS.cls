/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-14-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   11-14-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
global class Batch_Update_US_Sales_Goals_Tracker_CS implements Database.Batchable<sObject> 
{
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        // collect the batches of records or objects to be passed to execute
        String query = 'SELECT Id, AssetDate__c, Previous_Asset_Date__c , Previous_Previous_Asset_Date__c, Name FROM BroadridgeAssetDate__c where AssetDate__c !=NULL';
        system.debug('>>>>>>>>>>query>>>>>>>>>>>' + query);
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<BroadridgeAssetDate__c> lstBroadridgeAssetDate) 
    {
        List<US_Sales_Goals_Tracker__c> lstUS_Sales_Goals_Tracker_Update = new List<US_Sales_Goals_Tracker__c>();
        List<BroadridgeAssetDate__c> lstBroadridgeAssetDate_Update = new List<BroadridgeAssetDate__c>();
        system.debug('>>>>>>>>>>lstBroadridgeAssetDate>>>>>>>>>>>' + lstBroadridgeAssetDate);
        Date maxETFAssetsByFirmDate ;
        for(AggregateResult objAR : [SELECT max(Asset_Date__c) maxDate FROM ETF_Assets_By_Firm__c WHERE Notes__c = null AND FirmOwner__c <> 'Phil Kirouac'])
        {
            maxETFAssetsByFirmDate =  Date.valueOf(objAR.get('maxDate'));
        } 
        
        if(!lstBroadridgeAssetDate.isEmpty())
        {
            Boolean isChanged = false;
            Date dtCurrentAssetDate;
            Date dtPreviousAssetDate;
            //Date dtDtPreviousAssetDate;
            String strErrorMessage = '';

            for(BroadridgeAssetDate__c objBroadridgeAssetDate : lstBroadridgeAssetDate)
            {
                if(objBroadridgeAssetDate.AssetDate__c != NULL && objBroadridgeAssetDate.AssetDate__c != objBroadridgeAssetDate.Previous_Asset_Date__c &&
                    objBroadridgeAssetDate.AssetDate__c > objBroadridgeAssetDate.Previous_Asset_Date__c)
                {
                    isChanged = true;
                    dtCurrentAssetDate = objBroadridgeAssetDate.AssetDate__c;
                    dtPreviousAssetDate = objBroadridgeAssetDate.Previous_Asset_Date__c;
                    //dtDtPreviousAssetDate = objBroadridgeAssetDate.Previous_Previous_Asset_Date__c
                }
                // This condition is added for to look if ETL is failed to update Custom Setting and then 
                //when it is running the job it should consider Date from Date which was loaded to ETF_Assets_By_Firm__c
                else if(objBroadridgeAssetDate.AssetDate__c != NULL && maxETFAssetsByFirmDate != NULL && 
                maxETFAssetsByFirmDate > objBroadridgeAssetDate.AssetDate__c)
                {
                    isChanged = true;
                    dtCurrentAssetDate = maxETFAssetsByFirmDate; 
                    dtPreviousAssetDate = objBroadridgeAssetDate.AssetDate__c;
                    strErrorMessage = 'Date discrepancy detected, Latest MAX AssetDate copied from ETF_Assets_By_Firm__c to BroadridgeAssetDate';
                }
                else
                    strErrorMessage = 'Condition didn\'t match OR Previous Date is greater than Current Asset Date OR Dates have not changed';
            }

            if(isChanged)
            {
                for(US_Sales_Goals_Tracker__c objUS_Sales_Goals_Tracker : [SELECT Id, Current_asset_Date__c , Previous_Asset_Date__c
                                                                            FROM US_Sales_Goals_Tracker__c
                                                                            WHERE Status__c ='Active' and 
                                                                            Fund_Vehicle_Type__c='ETF' and 
                                                                            Current_Asset_Date__c !=: dtCurrentAssetDate ])
                {
                    US_Sales_Goals_Tracker__c objUS_Sales_Goals_Tracker_Update = new US_Sales_Goals_Tracker__c(Id=objUS_Sales_Goals_Tracker.Id);
                    objUS_Sales_Goals_Tracker_Update.Current_Asset_Date__c = dtCurrentAssetDate;
                    if(dtPreviousAssetDate != NULL)
                        objUS_Sales_Goals_Tracker_Update.Previous_Asset_Date__c = dtPreviousAssetDate;
                    
                    lstUS_Sales_Goals_Tracker_Update.add(objUS_Sales_Goals_Tracker_Update);
                }
                try 
                {
                    system.debug('>>>>>>>>>>lstUS_Sales_Goals_Tracker_Update>>>>>>>>>>>' + lstUS_Sales_Goals_Tracker_Update);
                    if(!lstUS_Sales_Goals_Tracker_Update.isEmpty())
                    {
                        update lstUS_Sales_Goals_Tracker_Update;


                        for(BroadridgeAssetDate__c objBroadridgeAssetDate : lstBroadridgeAssetDate)
                        {
                            BroadridgeAssetDate__c objBroadridgeAssetDateUpdate = new BroadridgeAssetDate__c(Id=objBroadridgeAssetDate.Id);
                            if(maxETFAssetsByFirmDate != NULL)
                                objBroadridgeAssetDateUpdate.AssetDate__c = maxETFAssetsByFirmDate; 
                            objBroadridgeAssetDateUpdate.Previous_Asset_Date__c = dtCurrentAssetDate; 
                            objBroadridgeAssetDateUpdate.Previous_Previous_Asset_Date__c = dtPreviousAssetDate; 
                            objBroadridgeAssetDateUpdate.Error_Message__c = strErrorMessage;
                            lstBroadridgeAssetDate_Update.add(objBroadridgeAssetDateUpdate);
                        }

                        system.debug('>>>>>>>>>>lstBroadridgeAssetDate_Update>>>>>>>>>>>' + lstBroadridgeAssetDate_Update);
                        if(!lstBroadridgeAssetDate_Update.isEmpty())
                        {
                            update lstBroadridgeAssetDate_Update;
                        }
                    }
                } catch(Exception e) {
                    System.debug('???????????????Got an Exception????????????' + e);
                }
            }
            else if(strErrorMessage != '')
            {
                for(BroadridgeAssetDate__c objBroadridgeAssetDate : lstBroadridgeAssetDate)
                {
                    BroadridgeAssetDate__c objBroadridgeAssetDateUpdate = new BroadridgeAssetDate__c(Id=objBroadridgeAssetDate.Id);
                    objBroadridgeAssetDateUpdate.Error_Message__c = strErrorMessage;
                    lstBroadridgeAssetDate_Update.add(objBroadridgeAssetDateUpdate);
                }

                system.debug('>>>>>>>>>>lstBroadridgeAssetDate_Update>>>>>>>>>>>' + lstBroadridgeAssetDate_Update);
                if(!lstBroadridgeAssetDate_Update.isEmpty())
                {
                    update lstBroadridgeAssetDate_Update;
                }
            }
        }
         
    }   
     
    global void finish(Database.BatchableContext BC) 
    {
        // execute any post-processing operations like sending email
    }
}