public class BatchPortfolioChannelRollupQuarterly implements Database.Batchable<sObject>, Database.Stateful {
    
    String fundVehicleType;
    String strYear = '2021';
    Integer iFundBatchSize = 10;
    Integer iQuarter = 0;
    Map<String, Id> mapETFChannels = new Map<String, Id>();
    Map<String, Id> mapMFChannels = new Map<String, Id>();
    PortfolioChannelRollupService service = new PortfolioChannelRollupService();
    List<Id> lstProcessingFundIds = new List<Id>();
    List<Id> lstPendingFundIds = new List<Id>();

    public BatchPortfolioChannelRollupQuarterly(Boolean isInitialBatch,List<Id> lstPendingFunds,String strFundVehicleType) {
        
        fundVehicleType = strFundVehicleType;
        if(isInitialBatch && lstPendingFunds.isEmpty()){
            List<Fund__c> lstFunds = [SELECT Id FROM Fund__c WHERE Fund_Vehicle_Type__c =:fundVehicleType AND Fund_Status__c = 'Open' AND BU_Region__c = 'USA'];//'Mutual Fund',
            for(Fund__c fund: lstFunds){
                if(lstProcessingFundIds.size() <= iFundBatchSize){
                    lstProcessingFundIds.add(fund.Id);
                }else{
                    lstPendingFundIds.add(fund.Id);
                }               
            }
        }else if(!(isInitialBatch && lstPendingFunds.isEmpty())){
            for(Id fundId: lstPendingFunds){
                if(lstProcessingFundIds.size() <= iFundBatchSize){
                    lstProcessingFundIds.add(fundId);
                }else{
                    lstPendingFundIds.add(fundId);
                }      
            }
        }
        Integer iCurrentYear = System.Today().year();
        strYear = String.valueOf(iCurrentYear);
        iQuarter = [Select Number From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].Number;
        
        System.debug('Processing : '+lstProcessingFundIds.size());
        System.debug('Pending : '+lstPendingFundIds.size());
        System.debug('--------------------------------------');
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String strQuery = 'SELECT Id FROM Channel__c WHERE Allow_Quarterly_Analysis__c = true AND Status__c = \'Active\'';
        return Database.getQueryLocator(strQuery);
    }

    public void execute(Database.BatchableContext bc, List<Channel__c> records) {
        for(Channel__c channel : records){
            service.upsertRollupRecords(lstProcessingFundIds,channel.Id,strYear,iQuarter,fundVehicleType,'0'); 
        }
    }

    public void finish(Database.BatchableContext bc) {
        if(!lstPendingFundIds.isEmpty() && fundVehicleType == 'ETF'){
                BatchPortfolioChannelRollupQuarterly batch = new BatchPortfolioChannelRollupQuarterly(false,lstPendingFundIds,fundVehicleType);
                database.executebatch(batch,1); 
        }else if(lstPendingFundIds.isEmpty() && fundVehicleType == 'ETF'){
                BatchPortfolioChannelRollupQuarterly batch = new BatchPortfolioChannelRollupQuarterly(true,new List<Id>(),'Mutual Fund');
                database.executebatch(batch,1); 
        }else if(!lstPendingFundIds.isEmpty() && fundVehicleType == 'Mutual Fund'){
                BatchPortfolioChannelRollupQuarterly batch = new BatchPortfolioChannelRollupQuarterly(true,lstPendingFundIds,fundVehicleType);
                database.executebatch(batch,1); 
        }
    }

}