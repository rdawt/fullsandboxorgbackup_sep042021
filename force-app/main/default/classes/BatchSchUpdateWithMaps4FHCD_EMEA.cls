public class BatchSchUpdateWithMaps4FHCD_EMEA  implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        // We now call the batch class to be scheduled for calculating FHD for Contacts with Tasks and Events
        contactFHCDateUpdate_EMEA b = new contactFHCDateUpdate_EMEA ();  
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,200);
    }
   
}