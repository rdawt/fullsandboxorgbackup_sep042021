global class US_Sales_Metrics_Controller 
{
    public static String strDefaultOwnerOnLoad = 'ALL';
    public static Integer countOfTerritory = 0;

    @AuraEnabled
    public static Map<String, Object> getOnLoadRecords()
    {
        Date startDate = date.newinstance(Date.today().year(), 1, 1);
        Date endDate = date.newinstance(Date.today().year(), 12, 31);

        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        mapGetOnLoadRecords.put('OwnersOptions', getAllPortfolioOptions());
        mapGetOnLoadRecords.put('SelectedOwnersOption', 'ALL'); 
        mapGetOnLoadRecords.put('activityDateMin', startDate);
        mapGetOnLoadRecords.put('activityDateMax', endDate); 
        return mapGetOnLoadRecords;    
    }

    @AuraEnabled
    public static Map<String, Object> getOnLoadRecordsETF()
    {
        Date startDate = date.newinstance(Date.today().year(), 1, 1);
        Date endDate = date.newinstance(Date.today().year(), 12, 31);

        Map<String, Object> mapGetOnLoadRecords = new Map<String, Object>();
        mapGetOnLoadRecords.put('OwnersOptionsETF', getAllPortfolioOptionsETF());
        mapGetOnLoadRecords.put('SelectedOwnersOptionETF', 'ALL'); 
        mapGetOnLoadRecords.put('activityDateMinETF', startDate);
        mapGetOnLoadRecords.put('activityDateMaxETF', endDate); 
        return mapGetOnLoadRecords;    
    }

    @AuraEnabled
    public static List<Map<String, String>> getAllPortfolioOptions()
    {
        List<Map<String, String>> lstPortfolio = new List<Map<String, String>>();
        lstPortfolio.add(new Map<String, String>{'value' =>'ALL', 'label'=>'ALL', 'selected' => 'true'} );

        Map<String, List<YTDSalesPortfolioChannelRollUp__c>> mapYTDSalesPortfolioChannelRollUp = getYTDSalesPortfolioChannelRollUp();
        List<String> lstFundName = new List<String>();
        lstFundName.addAll(mapYTDSalesPortfolioChannelRollUp.keySet());

        Map<String, String> mapFundName_Ticker = getFundTickerInformation(lstFundName);
        List<String> lstFundTicker = new List<String>();

        lstFundTicker.addAll(mapFundName_Ticker.keySet());

        lstFundTicker.sort();
        
        for(String strFundName : lstFundTicker)
        {
            lstPortfolio.add(new Map<String, String>{'value' =>strFundName, 'label'=>strFundName, 'selected' => 'false'} );
        }

        return lstPortfolio;
    }

    @AuraEnabled
    public static List<Map<String, String>> getAllPortfolioOptionsETF()
    {
        List<Map<String, String>> lstPortfolio = new List<Map<String, String>>();
        lstPortfolio.add(new Map<String, String>{'value' =>'ALL', 'label'=>'ALL', 'selected' => 'true'} );

        List<Datasets> lstDatasets = new list<Datasets>();
        Map<String, AggregateResult> mapETF_Assets_By_Firm = getETF_Assets_By_Firm();
        List<String> lstSelectedFundName = new List<String>();
        List<String> lstAllFundName = new List<String>();
        lstAllFundName.addAll(mapETF_Assets_By_Firm.keySet());

        lstAllFundName.sort();
        
        for(String strFundName : lstAllFundName)
        {
            lstPortfolio.add(new Map<String, String>{'value' =>strFundName, 'label'=>strFundName, 'selected' => 'false'} );
        }

        return lstPortfolio;
    }

    // ----------------------------Start Sales By Fund Code----------------------------//
    @AuraEnabled
    public static string getSalesByFundChart(List<String> lstSelectedFund, Date activityDateMin, Date activityDateMax)
    {
        String strJSONData = '';

        List<Datasets> lstDatasets = new list<Datasets>();
        Map<String, List<YTDSalesPortfolioChannelRollUp__c>> mapYTDSalesPortfolioChannelRollUp = getYTDSalesPortfolioChannelRollUp();        
        List<String> lstSelectedFundName = new List<String>();
        List<String> lstAllFundName = new List<String>();
        lstAllFundName.addAll(mapYTDSalesPortfolioChannelRollUp.keySet());       

        Map<String, String> mapFundName_Ticker = getFundTickerInformation(lstAllFundName);        
        List<String> lstFundTicker = new List<String>();
        lstFundTicker.addAll(mapFundName_Ticker.keySet());
       

        lstFundTicker.sort();

        for(String strFundTicker : lstSelectedFund)
        {
            if(strFundTicker != 'ALL')
            {
                if(mapFundName_Ticker.containsKey(strFundTicker) && mapFundName_Ticker.get(strFundTicker) != NULL && mapFundName_Ticker.get(strFundTicker) != '')
                    lstSelectedFundName.add(mapFundName_Ticker.get(strFundTicker));
                else
                    lstSelectedFundName.add(strFundTicker);
            }
        }        
        lstAllFundName.clear();

        List<YTDSalesPortfolioChannelRollUp__c> lstYTDSalesPortfolioChannelRollUp = new List<YTDSalesPortfolioChannelRollUp__c>();
        
        for(String strFundTicker : lstFundTicker) //mapYTDSalesPortfolioChannelRollUp.keyset()
        {
            String strFundName = mapFundName_Ticker.get(strFundTicker);
            lstAllFundName.add(strFundName);
            if(lstSelectedFundName.contains(strFundName))
            {
                if(mapYTDSalesPortfolioChannelRollUp.containskey(strFundName))
                    lstYTDSalesPortfolioChannelRollUp.addAll(mapYTDSalesPortfolioChannelRollUp.get(strFundName));
            }
        }

        for(String strSalesType : getSalesType())
        {
            Datasets objDatasetsE = getSalesByFund_FundWise(strSalesType, activityDateMin, activityDateMax,
                                                            lstAllFundName , lstYTDSalesPortfolioChannelRollUp);             
            lstDatasets.add(objDatasetsE);
        }        
        
        PrepareData objPrepareData = new PrepareData();
        objPrepareData.labels = lstFundTicker;
        objPrepareData.datasets = lstDatasets;

        return System.json.serialize(objPrepareData);

    }

    public static Datasets getSalesByFund_FundWise(String strType,Date activityDateMin, Date activityDateMax,
                                                    List<String> lstAllFundName  , 
                                                    List<YTDSalesPortfolioChannelRollUp__c> lstYTDSalesPortfolioChannelRollUp )
    {
        Map<String, Decimal> mapFundWiseCount = new Map<String, Decimal>();
        mapFundWiseCount = getFundInitialise(lstAllFundName);
        
        for(YTDSalesPortfolioChannelRollUp__c objYTDSalesPortfolioChannelRollUp : lstYTDSalesPortfolioChannelRollUp)
        {
            if(objYTDSalesPortfolioChannelRollUp.ProcessedDate__c >= activityDateMin && objYTDSalesPortfolioChannelRollUp.ProcessedDate__c <= activityDateMax)
            {
                if(strType == 'Net' && objYTDSalesPortfolioChannelRollUp.NetSalesCalculated__c != NULL)
                {
                    decimal tempNetVal = objYTDSalesPortfolioChannelRollUp.NetSalesCalculated__c;
                    if(mapFundWiseCount.containsKey(objYTDSalesPortfolioChannelRollUp.Portfolio__c)){
                        tempNetVal += mapFundWiseCount.get(objYTDSalesPortfolioChannelRollUp.Portfolio__c);                        
                    }
                    mapFundWiseCount.put(objYTDSalesPortfolioChannelRollUp.Portfolio__c, tempNetVal );                                        
                }
                else if(strType == 'Gross' && objYTDSalesPortfolioChannelRollUp.Gross_Sales__c != NULL)
                {
                    decimal tempNetVal = objYTDSalesPortfolioChannelRollUp.Gross_Sales__c;
                    if(mapFundWiseCount.containsKey(objYTDSalesPortfolioChannelRollUp.Portfolio__c)){
                        tempNetVal += mapFundWiseCount.get(objYTDSalesPortfolioChannelRollUp.Portfolio__c);                        
                    }
                    mapFundWiseCount.put(objYTDSalesPortfolioChannelRollUp.Portfolio__c, tempNetVal );  
                    
                }
                else if(strType == 'Redemption' && objYTDSalesPortfolioChannelRollUp.Redemption__c != NULL)
                {
                    decimal tempNetVal = objYTDSalesPortfolioChannelRollUp.Redemption__c;
                    if(mapFundWiseCount.containsKey(objYTDSalesPortfolioChannelRollUp.Portfolio__c)){
                        tempNetVal += mapFundWiseCount.get(objYTDSalesPortfolioChannelRollUp.Portfolio__c);                        
                    }
                    mapFundWiseCount.put(objYTDSalesPortfolioChannelRollUp.Portfolio__c, tempNetVal ); 
                } 
            }
        }
        
        Datasets objDatasets = new Datasets();
        objDatasets.type = 'bar';
        objDatasets.label = strType;
        objDatasets.data = mapFundWiseCount.values();
        objDatasets.backgroundColor = getChannelBGColorRandom_SalesType(strType, lstAllFundName.size(), getSalesType() , 'Sales Fund').get(strType);
        objDatasets.borderColor = getChannelBGColorRandom_SalesType(strType, lstAllFundName.size(), getSalesType() ,'Sales Fund').get(strType);
        objDatasets.borderWidth = 1;
        return objDatasets;
    }

    public static Map<String, List<YTDSalesPortfolioChannelRollUp__c>> getYTDSalesPortfolioChannelRollUp()
    {
        Map<String, List<YTDSalesPortfolioChannelRollUp__c>> mapYTDSalesPortfolioChannelRollUp = new Map<String, List<YTDSalesPortfolioChannelRollUp__c>>();
        List<YTDSalesPortfolioChannelRollUp__c> lstYTDSalesPortfolioChannelRollUp = new List<YTDSalesPortfolioChannelRollUp__c>();
        lstYTDSalesPortfolioChannelRollUp = [SELECT Id, Portfolio__c, NetSalesCalculated__c, Gross_Sales__c, 
                                                                                        Redemption__c , ProcessedDate__c
                                                                                        from YTDSalesPortfolioChannelRollUp__c
                                                                                        order by NetSalesCalculated__c ASC];
        for(YTDSalesPortfolioChannelRollUp__c obj : lstYTDSalesPortfolioChannelRollUp)

        {
            if(!mapYTDSalesPortfolioChannelRollUp.containsKey(obj.Portfolio__c))
                mapYTDSalesPortfolioChannelRollUp.put(obj.Portfolio__c, new List<YTDSalesPortfolioChannelRollUp__c>{obj});
            else
                mapYTDSalesPortfolioChannelRollUp.get(obj.Portfolio__c).add(obj);
        }
        return mapYTDSalesPortfolioChannelRollUp;
    }

    public static Map<String, String> getFundTickerInformation(List<String> lstFundNames)
    {
        Map<String, String> mapFundName_Ticker = new Map<String, String>();
        
        for(Fund__c obj : [SELECT Id, Name , Fund_Ticker_Symbol__c, Sales_Connect_Portfolio_Name__c 
                                                from Fund__c 
                                                where Sales_Connect_Portfolio_Name__c IN: lstFundNames and Fund_Ticker_Symbol__c != '' 
                                                order by Name ASC])
        {
            mapFundName_Ticker.put(obj.Fund_Ticker_Symbol__c, obj.Sales_Connect_Portfolio_Name__c);
        }
        return mapFundName_Ticker;
    }

    public static Map<String, String> getFundTickerInformationFromFundName(List<String> lstFundNames)
    {
        Map<String, String> mapFundName_Ticker = new Map<String, String>();
        
        for(Fund__c obj : [SELECT Id, Name , Fund_Ticker_Symbol__c, Sales_Connect_Portfolio_Name__c 
                                                from Fund__c 
                                                where Sales_Connect_Portfolio_Name__c IN: lstFundNames and Fund_Ticker_Symbol__c != '' 
                                                order by Name ASC])
        {
            mapFundName_Ticker.put(obj.Sales_Connect_Portfolio_Name__c, obj.Fund_Ticker_Symbol__c);
        }
        return mapFundName_Ticker;
    }

    // ----------------------------END Sales By Channel Code----------------------------//

    // ----------------------------Start Sales By Fund Code ETF----------------------------//
    @AuraEnabled
    public static string getSalesByFundChart_ETF(List<String> lstSelectedFund, Date activityDateMin, Date activityDateMax)
    {
        String strJSONData = '';

        List<Datasets> lstDatasets = new list<Datasets>();
        Map<String, AggregateResult> mapETF_Assets_By_Firm = getETF_Assets_By_Firm();
        List<String> lstSelectedFundName = new List<String>();
        List<String> lstAllFundName = new List<String>();
        lstAllFundName.addAll(mapETF_Assets_By_Firm.keySet());

        List<AggregateResult> lstETF_Assets_By_Firm = new List<AggregateResult>();

        for(String strFundTicker : lstSelectedFund)
        {
            if(strFundTicker != 'ALL')
            {
                if(lstAllFundName.contains(strFundTicker))
                    lstSelectedFundName.add(strFundTicker);
            }
        }
        List<Decimal> lstDecimal = new List<Decimal>();

        lstAllFundName.sort();
        Decimal dcTotalValue = 0;

        for(String strFundName : lstAllFundName) //mapYTDSalesPortfolioChannelRollUp.keyset()
        {
            Decimal objDecValue = 0;
            if(lstSelectedFundName.contains(strFundName))
            {
                if(mapETF_Assets_By_Firm.containskey(strFundName))
                {
                    objDecValue = getSalesByFund_ETFFundWise(activityDateMin, activityDateMax, mapETF_Assets_By_Firm.get(strFundName)); 
                }
            }
            dcTotalValue += objDecValue;
            lstDecimal.add(objDecValue);
        }
        
        Datasets objDatasets = new Datasets();
        objDatasets.label = 'Total : $ ' + (Math.abs(dcTotalValue) >= 1000000000 ? (dcTotalValue / 1000000000).setScale(2) + 'B' : Math.abs(dcTotalValue) >= 1000000 ? 
                                          (dcTotalValue / 1000000).setScale(2) + 'M' : Math.abs(dcTotalValue) >= 1000 ? (dcTotalValue / 1000).setScale(2) + 'K' : dcTotalValue.setScale(2)+'');
        objDatasets.data = lstDecimal;
        objDatasets.backgroundColor = getChannelBGColorRandom_SalesType('', lstAllFundName.size(), lstAllFundName , 'Sales ETF').get('Sales ETF');
        objDatasets.borderColor = getChannelBGColorRandom_SalesType('', lstAllFundName.size(), lstAllFundName , 'Sales ETF').get('Sales ETF');
        objDatasets.borderWidth = 1;
        lstDatasets.add(objDatasets);
        
        PrepareData objPrepareData = new PrepareData();
        objPrepareData.labels = lstAllFundName;
        objPrepareData.datasets = lstDatasets;

        return System.json.serialize(objPrepareData);

    }

    public static Decimal getSalesByFund_ETFFundWise(Date activityDateMin, Date activityDateMax,
                                                        AggregateResult objETF_Assets_By_Firm)
    {
        Decimal objDecValue = 0;
        Date dtAssetDate = (Date) objETF_Assets_By_Firm.get('asset_date__c');

        if(dtAssetDate >= activityDateMin && dtAssetDate <= activityDateMax)
            objDecValue = (Decimal) (objETF_Assets_By_Firm.get('mnthAsset'));
        return objDecValue;
    }

    public static Map<String, AggregateResult> getETF_Assets_By_Firm()
    {
        BroadridgeAssetDate__c objBroadridgeAssetDate = BroadridgeAssetDate__c.getOrgDefaults();
        Date assetDate = objBroadridgeAssetDate.AssetDate__c;
        Map<String, AggregateResult> mapETF_Assets_By_Firm = new Map<String, AggregateResult>();
        List<AggregateResult> lstETF_Assets_By_Firm = new List<AggregateResult>();
        lstETF_Assets_By_Firm = [SELECT Name,asset_date__c,sum(Fund_YTM_NNA__c) mnthAsset  
                                                    FROM ETF_Assets_By_Firm__c 
                                                    WHERE allocation_etf__C=true and Asset_Date__c = :assetDate group by name, asset_date__c order by sum(Fund_YTM_NNA__c) desc];
        for(AggregateResult obj : lstETF_Assets_By_Firm)
        {
            mapETF_Assets_By_Firm.put(String.valueOf(obj.get('Name')), obj);
        }
        return mapETF_Assets_By_Firm;
    }

    @AuraEnabled
    public static Map<String, Decimal> getTableDataForSalesByFund_ETF(){
        BroadridgeAssetDate__c objBroadridgeAssetDate = BroadridgeAssetDate__c.getOrgDefaults();
        Date assetDate = objBroadridgeAssetDate.AssetDate__c;
        Map<String, Decimal> returnMap = new Map<String, Decimal>();
        AggregateResult[] aggregateResults = [SELECT Name, SUM(Fund_YTM_NNA__c) netAmount FROM ETF_Assets_By_Firm__c WHERE allocation_etf__c = true and Asset_Date__c = :assetDate GROUP BY name ORDER BY SUM(Fund_YTM_NNA__c) DESC];
        for(AggregateResult ar : aggregateResults)
        {
            returnMap.put((String)ar.get('Name'), (Decimal)ar.get('netAmount'));
        }
        return returnMap;
    }

    @AuraEnabled
    public static Map<String, Decimal> getTableDataForPortfolioNetSales(){
        Map<String, Decimal> returnMap = new Map<String, Decimal>();
        AggregateResult[] aggregateResults = [SELECT Portfolio__c,SUM(NetSalesCalculated__c) netAmount FROM YTDSalesPortfolioChannelRollUp__c GROUP BY Portfolio__c ORDER BY SUM(NetSalesCalculated__c) DESC];
        for(AggregateResult ar : aggregateResults)
        {
            returnMap.put((String)ar.get('Portfolio__c'), (Decimal)ar.get('netAmount'));
        }
        return returnMap;
    }
    // ----------------------------END Sales By Channel Code----------------------------//

    @AuraEnabled
    public static Map<String, Decimal> getFundInitialise(List<String> lstFundName){
        Decimal countOfFunds = lstFundName.size();
        Map<String, Decimal> mapFundInitial = new Map<String, Decimal>();
        for(String strFund : lstFundName)
        {
            mapFundInitial.put(strFund, 0);
        }
        return mapFundInitial;
    }

    @AuraEnabled
    public static List<String> getSalesType(){
        List<String> lstSales = new List<String>();
        
        lstSales.add('Net');
        lstSales.add('Gross');
        lstSales.add('Redemption');
        return lstSales;
    }

    @AuraEnabled
    public static Map<String, List<String>> getChannelBGColorRandom_SalesType(String strType, Integer intSize, List<String> lstAllType, String strFundType)
    {
        Map<String, List<String>> mapBGColor = new Map<String, List<String>>();
        List<String> lstBGColor = new List<String>();

        if(strFundType == 'Sales Fund')
        {
            for(String strSalesType : lstAllType)
            {
                List<String> lstBGColorChannel = new List<String>();
                String strColor = '';

                if(strSalesType == 'Net')
                    strColor = 'rgba(0, 232, 240, 1)';
                if(strSalesType == 'Gross')
                    strColor = 'rgba(71, 255, 151, 1)';
                if(strSalesType == 'Redemption')
                    strColor = 'rgba(235, 0, 0, 1)';

                for(Integer i=0; i < intSize; i++)
                {
                    lstBGColorChannel.add(strColor);
                }
                mapBGColor.put(strSalesType, lstBGColorChannel);
            }
        }

        if(strFundType == 'Sales ETF')
        {
            List<String> lstBGColorChannel = new List<String>();
            for(String strFundName : lstAllType)
            {
                lstBGColorChannel.add('rgba(34, 105, 180, 1)');
            }

            mapBGColor.put('Sales ETF', lstBGColorChannel);
        }

        return mapBGColor;

    }

    @AuraEnabled
    public static List<PlainReport> getPlainReportforActiveFunds()
    {
        List<PlainReport> lstActiveFundsReport = new List<PlainReport>();
        map<string,SalesbyFundVals> mapFund_NETResult = new map<string,SalesbyFundVals>();
        Map<String, String> mapFund_GROSSResult = new Map<String, String>();
        Map<String, String> mapFund_REDEMPTIONResult = new Map<String, String>();
        
        mapFund_NETResult = getActiveFunds_NETResults();        
        mapFund_GROSSResult = getActiveFunds_GROSSResults();
        mapFund_REDEMPTIONResult = getActiveFunds_REDEMPTIONResults();        

        Map<String, String> mapFundName_Ticker = new Map<String, String>();
        List<String> lstFundNames = new List<String>();
        List<String> lstFundTickersSorted = new List<String>();
        lstFundNames.addAll(mapFund_NETResult.keySet());

        lstFundNames.sort();

        mapFundName_Ticker = getFundTickerInformationFromFundName(lstFundNames);
        lstFundTickersSorted.addAll(mapFundName_Ticker.values());
        lstFundTickersSorted.sort();

        Map<String, PlainReport> mapPlainReport = new Map<String, PlainReport>();

        // Getting information in map for later sorting processing
        for(String  strFundName : lstFundNames)
        {
            PlainReport objClass = new PlainReport();
            objClass.decNET =  mapFund_NETResult.get(strFundName).decValue;
            objClass.strNET = mapFund_NETResult.get(strFundName).strValue;          
            objClass.decGross =  mapFund_GROSSResult.get(strFundName);            
            objClass.decRedemption = mapFund_REDEMPTIONResult.get(strFundName);

            if(objClass.decGross.contains('('))
                objClass.decGrossStyleClass = 'color:red'; 

            if(objClass.strNET.contains('('))
                objClass.decNETStyleClass = 'color:red';

            if(objClass.decRedemption.contains('('))
                objClass.decRedemptionClass = 'color:red';

            if(mapFundName_Ticker.containsKey(strFundName))
                objClass.strFundTicker = mapFundName_Ticker.get(strFundName);
            
            objClass.strFundName = strFundName;
            
            mapPlainReport.put(objClass.strFundName, objClass);
        }

        // Processing this for sorting based on Fund Ticker
        for(String strFundTicker : lstFundNames)
        {
            if(mapPlainReport.containsKey(strFundTicker))
            {
                lstActiveFundsReport.add(mapPlainReport.get(strFundTicker));
            }
        }
        
        lstActiveFundsReport.sort();
        return lstActiveFundsReport;

    }

    @AuraEnabled
    public static String getMAXProcessedDate_ActiveFunds()
    {
        List<YTDSalesPortfolioChannelRollUp__c> lstYTDSalesPortfolioChannelRollUp = [SELECT Id, ProcessedDate__c 
                                                                                        from YTDSalesPortfolioChannelRollUp__c 
                                                                                        order by ProcessedDate__c DESC limit 1];
        if(lstYTDSalesPortfolioChannelRollUp.size() > 0 && lstYTDSalesPortfolioChannelRollUp[0].ProcessedDate__c != NULL){
            Datetime dT = lstYTDSalesPortfolioChannelRollUp[0].ProcessedDate__c;
            Date d = date.newinstance(dT.year(), dT.month(), dT.day());
            return String.valueOf(d.addDays(1));
        }
        else
            return ''; 
    }

    public static map<string,SalesbyFundVals> getActiveFunds_NETResults()
    {
        map<string,SalesbyFundVals> mapFund_NETResult = new map<string,SalesbyFundVals>();
        for(AggregateResult objAR : [SELECT Portfolio__c, format(sum(NetSalesCalculated__c)) netSales,
                                        sum(NetSalesCalculated__c) decSales
                                        FROM YTDSalesPortfolioChannelRollUp__c 
                                        group by Portfolio__c order by sum(NetSalesCalculated__c)])
        {
            SalesbyFundVals vals = new SalesbyFundVals((decimal) objAr.get('decSales'), string.valueOf(objAr.get('netSales')));         
            mapFund_NETResult.put(String.valueOf(objAR.get('Portfolio__c')), vals);
        }

        return mapFund_NETResult;
    }

    public static Map<String, String> getActiveFunds_GROSSResults()
    {
        Map<String, String> mapFund_NETResult = new Map<String, String>();
        for(AggregateResult objAR : [SELECT Portfolio__c, format(sum(Gross_Sales__c)) grossSales
                                        FROM YTDSalesPortfolioChannelRollUp__c 
                                        group by Portfolio__c order by sum(Gross_Sales__c)])
        {
            mapFund_NETResult.put(String.valueOf(objAR.get('Portfolio__c')), String.valueOf(objAr.get('grossSales')) );
        }

        return mapFund_NETResult;
    }

    public static Map<String, String> getActiveFunds_REDEMPTIONResults()
    {
        Map<String, String> mapFund_NETResult = new Map<String, String>();
        for(AggregateResult objAR : [SELECT Portfolio__c, format(sum(Redemption__c)) redemptionSales
                                        FROM YTDSalesPortfolioChannelRollUp__c 
                                        group by Portfolio__c order by sum(Redemption__c)])
        {
            mapFund_NETResult.put(String.valueOf(objAR.get('Portfolio__c')), String.valueOf(objAr.get('redemptionSales')) );
        }

        return mapFund_NETResult;
    }

    
    global class PlainReport implements Comparable
    {
        @AuraEnabled public String strFundName;
        @AuraEnabled public String strFundTicker;
        @AuraEnabled public String strNET;
        @AuraEnabled public decimal decNET; 
        @AuraEnabled public String decGross;
        @AuraEnabled public String decRedemption;
        @AuraEnabled public String decNETStyleClass;
        @AuraEnabled public String decGrossStyleClass;
        @AuraEnabled public String decRedemptionClass;
        
        // Implement the compareTo() method
        global Integer compareTo(Object compareTo) {
            PlainReport compareToRep = (PlainReport)compareTo;
            if (decNET == compareToRep.decNET) return 0;
            if (decNET > compareToRep.decNET) return -1;
            return 1;        
        }
    }
    
    public class SalesbyFundVals{
        @AuraEnabled public decimal decValue {get;set;}
        @AuraEnabled public string strValue {get;set;}
        public SalesbyFundVals(decimal decValue, string strValue){
            this.decValue = decValue;
            this.strValue = strValue;
        }
    }


    public class Datasets {
        public String type;
        public boolean fill;
        public String label;
        public List<Decimal> data;
        public Map<Id, Integer> userdata;
        public List<String> backgroundColor;
        public List<String> borderColor;
        public Integer borderWidth;
    }

    public class PrepareData {
        public List<String> labels;
        public List<Datasets> datasets;
    }

}