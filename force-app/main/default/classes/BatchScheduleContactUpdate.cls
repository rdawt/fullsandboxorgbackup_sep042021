global class BatchScheduleContactUpdate implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled
        batchContactUpdate b = new batchContactUpdate ();
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,Integer.valueOf(label.ScheduleContactUpdateSize));
    }
   
}