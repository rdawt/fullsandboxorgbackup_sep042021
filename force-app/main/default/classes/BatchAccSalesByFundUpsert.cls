/********************************************************************************************
* @Author: Tharaka De Silva
* @Date : 2021-03-25

* Batch class that upsert Account Sales by Fund data in to Account Sales by Fund History object
* With this batch, allows to upsert all the records in SalesConnect__Firm_Portfolio_Breakdown__c object 
* in to History_Firm_Portfolio_Breakdown__c object
* *******************************************************************************************/
public class BatchAccSalesByFundUpsert implements Database.Batchable<sObject>, Database.Stateful {
    
    List<String> lstErrorMsgs = new List<String>();
    String strQuarter;
    Map<String, Id> mapDSTFundMapping = new Map<String, Id>();

    /************************************************************************
    * Constructor for BatchAccSalesByFundUpsert. 
    * Get the current fiscal quater
    ************************************************************************/
    public BatchAccSalesByFundUpsert(){
        List<Period> lstPeriods = [Select FullyQualifiedLabel,Number,StartDate From Period Where Type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER];
        if(lstPeriods.size() > 0){
            strQuarter = lstPeriods[0].FullyQualifiedLabel;
        }
        List<Fund_Mapping__c> lstFundMapping = [SELECT Name,SFDC_Fund_Lookup__c FROM Fund_Mapping__c WHERE Status__c = 'Active'];
        if(lstFundMapping.size() > 0){
            for(Fund_Mapping__c mapping: lstFundMapping){
                mapDSTFundMapping.put(mapping.Name,mapping.SFDC_Fund_Lookup__c);
            }
        }
    }

    /************************************************************************
    * Starts up the batch using the query specified in strQuery string
    * @param bc					Context of the batch
    * @return     				The query locator
    ************************************************************************/
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        String strQuery = 'SELECT Id,SalesConnect__External_ID__c,SalesConnect__Firm_ID__c,Name,SalesConnect__Account__c,SalesConnect__Current_Assets__c,SalesConnect__Last_Year_Redemptions__c,'
                        +'SalesConnect__Last_Year_Sales__c,SalesConnect__MTD_Redemptions__c,SalesConnect__MTD_Sales__c,SalesConnect__Yesterday_Redemptions__c,SalesConnect__Yesterday_Sales__c,'
                        +'SalesConnect__YTD_Redemptions__c,SalesConnect__YTD_Sales__c,SFDC_Channel_Id__c,SFDC_Fund_Id__c,SFDC_Fund_Ticker__c,LastModifiedDate,Fund_Vehicle_Type__c FROM SalesConnect__Firm_Portfolio_Breakdown__c ';
                        // +'SalesConnect__YTD_Redemptions__c,SalesConnect__YTD_Sales__c,SFDC_Channel_Id__c,SFDC_Fund_Id__c,SFDC_Fund_Ticker__c,LastModifiedDate,Fund_Vehicle_Type__c FROM SalesConnect__Firm_Portfolio_Breakdown__c WHERE Fund_Vehicle_Type__c = \'Mutual Fund\'';
        return Database.getQueryLocator(strQuery);
    } 

    /************************************************************************
    * Executes the batch and upsert the records in to History_Firm_Portfolio_Breakdown__c object
    *
    * @param bc					Context of the batch
    * @param records			The list of objects (SalesConnect__Firm_Portfolio_Breakdown__c)
    ************************************************************************/
    public void execute(Database.BatchableContext bc, List<SalesConnect__Firm_Portfolio_Breakdown__c> records) {
        try{
            List<History_Firm_Portfolio_Breakdown__c> lstHistory = new List<History_Firm_Portfolio_Breakdown__c>();
            History_Firm_Portfolio_Breakdown__c history;
            if(strQuarter == null){
                System.debug('Eror : No Quarter information found');
                lstErrorMsgs.add('No Quarter information found  \n');
            }else{
                for(SalesConnect__Firm_Portfolio_Breakdown__c record: records){
                    history = new History_Firm_Portfolio_Breakdown__c();
                    history.SFDC_External_Id__c = record.SalesConnect__External_ID__c + '-' + strQuarter;
                    history.Account_Sales_by_Fund_Id__c = record.Id; 
                    history.SalesConnect_External_ID__c = record.SalesConnect__External_ID__c;
                    history.SalesConnect_Firm_ID__c = record.SalesConnect__Firm_ID__c;
                    history.Name = record.Name;
                    history.SalesConnect_Account__c = record.SalesConnect__Account__c;
                    history.SalesConnect_Current_Assets__c = record.SalesConnect__Current_Assets__c;
                    history.SalesConnect_Last_Year_Redemptions__c = record.SalesConnect__Last_Year_Redemptions__c;
                    history.SalesConnect_Last_Year_Sales__c = record.SalesConnect__Last_Year_Sales__c;
                    history.SalesConnect_MTD_Redemptions__c = record.SalesConnect__MTD_Redemptions__c;
                    history.SalesConnect_MTD_Sales__c = record.SalesConnect__MTD_Sales__c;
                    history.SalesConnect_Yesterday_Redemptions__c = record.SalesConnect__Yesterday_Redemptions__c;
                    history.SalesConnect_Yesterday_Sales__c = record.SalesConnect__Yesterday_Sales__c;
                    history.SalesConnect_YTD_Redemptions__c = record.SalesConnect__YTD_Redemptions__c;
                    history.SalesConnect_YTD_Sales__c = record.SalesConnect__YTD_Sales__c;
                    history.SFDC_Channel_Id__c = record.SFDC_Channel_Id__c;
                    //history.SFDC_Fund_Id__c = record.SFDC_Fund_Id__c;
                    history.Asset_Date__c = record.LastModifiedDate.date();
                    history.Fund_Vehicle_Type__c = record.Fund_Vehicle_Type__c;
                    //history.Fund_Lookup__c = record.SFDC_Fund_Id__c;
                    //history.SFDC_Fund_Ticker__c = record.SFDC_Fund_Ticker__c;
                    if(record.Fund_Vehicle_Type__c == 'Mutual Fund'){
                        history.Fund_Lookup__c = record.SFDC_Fund_Id__c;
                        history.SFDC_Fund_Id__c = record.SFDC_Fund_Id__c;
                        history.SFDC_Fund_Ticker__c = record.SFDC_Fund_Ticker__c;
                    }else{
                        if(mapDSTFundMapping.containsKey(record.Name)){
                            history.Fund_Lookup__c = mapDSTFundMapping.get(record.Name);
                            history.SFDC_Fund_Id__c = mapDSTFundMapping.get(record.Name);
                        }
                    }

                    history.Channel_Lookup__c = record.SFDC_Channel_Id__c;
                    history.Quarter__c = strQuarter;
                    lstHistory.add(history);
                }
                Database.UpsertResult[] results = Database.upsert(lstHistory, History_Firm_Portfolio_Breakdown__c.SFDC_External_Id__c,false);
               
                //Error log --> send to email
                for (Database.UpsertResult ur : results) {
                    if (!ur.isSuccess()) {
                        for(Database.Error objErr : ur.getErrors()) {
                            System.debug('Upsert error : '+objErr.getMessage());
                            lstErrorMsgs.add(objErr.getStatusCode() + ': ' + objErr.getMessage()+ '\n');
                         }
                    } 
                }
            }
        }catch (Exception ex){
            System.debug('Exception : '+ex.getStackTraceString());
            lstErrorMsgs.add(ex.getStackTraceString() + '\n');
        }
    }

    /************************************************************************
    * Send the email notification if any errors has occured
    *
    * @param bc					Context of the batch
    ************************************************************************/
    public void finish(Database.BatchableContext bc) {
        if(lstErrorMsgs.size() > 0){
            String allString = String.join(lstErrorMsgs,',');
            SendEmailNotification se = new SendEmailNotification('Error: Scheduled Apex for Batch Update Of Accounts Sales By Fund');
            se.toAddresses.add('tdesilva.consultant@vaneck.com');
            se.sendEmail(allString);
        }
    }
}