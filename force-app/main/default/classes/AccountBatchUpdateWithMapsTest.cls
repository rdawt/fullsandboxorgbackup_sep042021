@isTest
private class AccountBatchUpdateWithMapsTest  {

     @isTest  static void testAccountBatchUpdateWithMaps() {
     
     ContactUpdateDateSetting__c contactExtractDays = new  ContactUpdateDateSetting__c ();
        contactExtractDays.Name ='ActivityLastModifiedDateTimeline';
        contactExtractDays.ActivityLastModifiedDateTimeline__c = '14';
        insert contactExtractDays ;
        
        string activityLastDays =   ContactUpdateDateSetting__c.getInstance('ActivityLastModifiedDateTimeline').ActivityLastModifiedDateTimeline__c;
        string criteria = 'last_N_days:'+activityLastDays ;
     
        EventTriggerHandler eth = new EventTriggerHandler();
        DMLException e = null;
        Set<ID> accIds = new Set<ID>();
      
        List<Contact> conlist = new List<Contact>();
    
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
    
        Account acc = new Account(Name = 'TEST-WFA-Unbranched--', BillingCountry='United States',Channel__c='Insurance');
        insert acc;
        Contact con = new Contact(LastName='Aalund****',FirstName='Gail-coninscheduleclass',email='aalundgailAscon@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        Contact con2 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAscon2@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        Contact con3 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAscon3@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        Contact con4 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAscon4@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        Contact con5 = new Contact(LastName='Aalund2****',FirstName='Gail2-coninscheduleclass',email='aalundgailAscon5@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');

  
        conlist.add(con);
        conList.add(con2);
        conList.add(con3);
        conList.add(con4);
        conList.add(con5);
        insert conList;     
        
        string Comments;
        
        Event e1 = new Event(Activity_Type__c='Meeting', Description = 'just a test', Result__c = 'Held',  Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-4, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=2,ActivityDateTime=System.today());
        
        Task t1 = new Task(status='email sent',Activity_Type__c='Call', Description = 'just a test', Result__c = 'Held',  Subject = 'email', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-2, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId);
                                        
         
        Task t7 = new Task(status='call completed',Activity_Type__c='Call', Description = 'just a test for con4', Result__c = 'Held',  Subject = 'call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-5, ReminderDateTime = System.now()+1, WhoId = con4.Id, WhatId = con4.AccountId, OwnerId = con4.OwnerId);
        
        
        Task t8 = new Task(status='call completed',Activity_Type__c='Call', Description = 'just a test for con4-2ndtask with task date > than event date', Result__c = 'Held',  Subject = 'call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = con5.Id, WhatId = con5.AccountId, OwnerId = con5.OwnerId);
                                        
         
        Task t6 = new Task(Activity_Type__c='Call',Description = 'just a test', Result__c = 'Held',  Subject = 'call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()+10, ReminderDateTime = System.now()-2, WhoId = con2.Id, WhatId = con.AccountId, OwnerId = con2.OwnerId);
                                        
        Task t5 = new Task(Activity_Type__c='Call',Description = Comments, Priority = 'Normal', Status = 'call completed', Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId);
         Comments = Comments+'2nd task for con with who id in task object';
         
        event e5 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                                        ActivityDate = System.today()+7, ReminderDateTime = System.now()+7, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con2.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()+7);
        
        
        event e6 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                                        ActivityDate = System.today()+3, ReminderDateTime = System.now()+7, WhoId = con2.Id, WhatId = con2.AccountId, OwnerId = con2.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()+7);
        event e7 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                                        ActivityDate = System.today()+3, ReminderDateTime = System.now()+7, WhoId = con3.Id, WhatId = con3.AccountId, OwnerId = con3.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today()+7);
                                        
        event e8 = new Event(Activity_Type__c='Meeting',result__c='held',Description = Comments,   Subject = 'Meeting', IsReminderSet = true, Summary_Recap__c = 'summary recap3',
                                        ActivityDate = System.today(), ReminderDateTime = System.now(), WhoId = con4.Id, WhatId = con4.AccountId, OwnerId = con4.OwnerId,DurationInMinutes=29,ActivityDateTime=System.today());                                        
                                                                     
        List<Task> lstTask = new List<Task>();
        List<Event> lstEvent = new List<Event>();
         
        lstTask.add(t1);
        lstTask.add(t5);
        lstTask.add(t6);
        lstTask.add(t7);
        lstTask.add(t8);
        
        insert lstTask;
         
        lstEvent.add(e1);
        lstEvent.add(e5);
        lstEvent.add(e6);
        lstEvent.add(e7);
        lstEvent.add(e8);
        
        insert lstEvent;
         
         // **start teset**
        Test.startTest();

        try{
            contactFHCDateUpdateWithMaps batchJobForXtractingActivities = new contactFHCDateUpdateWithMaps ();
            dataBase.executeBatch(batchJobForXtractingActivities ,200 );

            AccountBatchUpdateWithMaps batchJobForXtractingActivities_Account = new AccountBatchUpdateWithMaps ();
            dataBase.executeBatch(batchJobForXtractingActivities_Account ,200 );

            Test.stopTest();
        }catch(Exception except1){
            System.assert(except1!=null);
        }
    }
    
    @isTest
    static void schedulerTest2() 
    {
        Test.startTest();
        
        ContactUpdateDateSetting__c contactExtractDays = new  ContactUpdateDateSetting__c ();
        contactExtractDays.Name ='ActivityLastModifiedDateTimeline';
        contactExtractDays.ActivityLastModifiedDateTimeline__c = '14';
        insert contactExtractDays ;
        
        string activityLastDays =   ContactUpdateDateSetting__c.getInstance('ActivityLastModifiedDateTimeline').ActivityLastModifiedDateTimeline__c;
        string criteria = 'last_N_days:'+activityLastDays ;
        String CRON_EXP = '0 0 0 15 3 ? *';
        List<Contact> conlist = new List<Contact>();
        
        Subscription_Group__c objSG = new Subscription_Group__c(Name='Unassigned Eligibility', IsActive__c= true);
        sObject objSGInserted = SL_TestDataFactory.createSObject(objSG,true);
        
        Account acc = new Account(Name = 'TEST-WFA-Unbranched--', BillingCountry='United States',Channel__c='Insurance');
        insert acc;
        Contact con = new Contact(LastName='Aalund****',FirstName='Gail-coninscheduleclass',email='aalundgailAsconinbatchsheduleupdatetest@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        Contact conx = new Contact(LastName='Aalund****',FirstName='Gail-coninscheduleclass',email='xaalundgailxtest@bac.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        Contact cony = new Contact(LastName='Aalund****',FirstName='Gail-coninscheduleclass',email='yaalundgailxtest@bacy.com',AccountId=acc.Id,MailingStreet='abc', MailingCountry='United States',Contact_Status__c = 'Client',OwnerId ='005A0000000Oqpm');
        
        
        List<Contact> lstContact = new List<Contact>();
        lstContact.add(con);
        lstContact.add(conx);
        lstContact.add(cony);
        
        insert lstContact;
        
        Event e1 = new Event(Activity_Type__c='Meeting', Description = 'just a test', Result__c = 'Held',  Subject = null, IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()-4,ActivityDateTime=System.today()-4, ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId,DurationInMinutes=2);
        Task t1 = new Task(status='email sent',Activity_Type__c='Call', Description = 'just a test', Result__c = 'Held',  Subject = 'email', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                         ActivityDate = System.today()-4,ReminderDateTime = System.now()+1, WhoId = con.Id, WhatId = con.AccountId, OwnerId = con.OwnerId);
        Task t2 = new Task(status='email sent',Activity_Type__c='Call', Description = 'just a test', Result__c = 'Held',  Subject = 'email', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                         ActivityDate = System.today()-4,ReminderDateTime = System.now()+1, WhoId = cony.Id, WhatId = cony.AccountId, OwnerId = cony.OwnerId);
        Task t5 = new Task(Activity_Type__c='Call',Description = 'just loaded for more coverage', Priority = 'Normal', Status = 'call completed', Subject = 'Call', IsReminderSet = true, Summary_Recap__c = 'summary recap1',
                                        ActivityDate = System.today()+1, ReminderDateTime = System.now()+1, WhoId = cony.Id, WhatId = cony.AccountId, OwnerId = cony.OwnerId);
        
        List<Task> lstTask = new List<Task>();
        lstTask.add(t1);
        lstTask.add(t2);
        lstTask.add(t5);
        
        insert lstTask;
        
        String jobId1 = System.schedule('ScheduleApexClassTestForUpdatingLastHumanContact_Contact',  CRON_EXP, new BatchScheduleUpdateWithMaps4FHCD ());

        String jobId2 = System.schedule('ScheduleApexClassTestForUpdatingLastHumanCContact_Account',  CRON_EXP, new AccountBatchUpdateWithMaps_Sch ());
           
        Test.stopTest();
   }
}