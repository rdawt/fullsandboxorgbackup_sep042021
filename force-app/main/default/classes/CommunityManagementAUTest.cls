/**
    Author  :       Vidhya Krishnan
    Date Created:   10/15/12
    Description:    Test class for Community Management AU Base Class
 */
@isTest
private class CommunityManagementAUTest {

    static testMethod void testCommunityManagementAU() {
        CommunityManagementAU cm = new CommunityManagementAU();
        Subscription_Group__c sg = new Subscription_Group__c();
        sg.Name = 'Test Group'; insert sg;
        Subscription__c s = new Subscription__c();
        s.Name = 'Test Subscription'; s.IsActive__c = true; s.Subscription_Indicator_Marker__c = 'test';
        s.Subscription_Display_Name__c = 'Test Subscription';
        insert s;
        Community__c c = new Community__c(Name = 'Test AU Community', Is_Active__c = true, Default_Subscription_Group__c = sg.Id);
        insert c;
        Account acc = new Account(Name = 'TSTCD', Channel__c = 'RIA', BillingCountry = 'USA');
        insert acc;
        Contact con=new Contact(FirstName='Test',LastName='User', AccountId = acc.Id, MailingCountry = 'USA');
        insert con;
        CommunityManagementAU.AUMemberRecord mr = new CommunityManagementAU.AUMemberRecord();
        CommunityManagementAU.AUMemberRecord mr1 = new CommunityManagementAU.AUMemberRecord();
        CommunityManagementAU.AUMemberRecord mr2 = new CommunityManagementAU.AUMemberRecord();
        mr.communityId = String.valueOf(c.Id); mr.leadId = '00000'; mr.contactId = con.Id;
        mr.email = 'test@test.com'; mr.cUserName  = 'vaneck'; mr.passWord  = 'global1955'; mr.isActive  = true;
        mr.firstName  = 'test'; mr.lastName  = 'test'; mr.companyName  = 'test'; mr.jobTitle  = 'test';
        mr.address1  = 'test'; mr.address2  = 'test'; mr.city  = 'test'; mr.state  = 'test';
        mr.country  = 'test'; mr.zipcode  = 'test'; mr.phone  = 'test'; mr.loginCount = 1; mr.subscriptionGroupId = sg.Id;
        mr.investorType  = 'test'; mr.aum  = 'test'; mr.isFP  = 'true'; mr.subscriptionRequested  = 'test,test';
        mr.privacyAckDate = system.today(); mr.lastLogin = system.today(); mr.dbSourceCreateDate = system.today();
        boolean b = cm.addMemberRecord(mr);
        mr2.communityId = String.valueOf(c.Id); mr2.leadId = '00000'; mr2.contactId = con.Id;
        mr2.email = 'test3@test.com'; mr2.cUserName  = 'vaneck3'; mr2.passWord  = 'global1955'; mr2.isActive  = true;
        mr2.firstName  = 'test'; mr2.lastName  = 'test'; mr2.companyName  = 'test'; mr2.jobTitle  = 'test';
        mr2.address1  = 'test'; mr2.address2  = 'test'; mr2.city  = 'test'; mr2.state  = 'test';
        mr2.country  = 'test'; mr2.zipcode  = 'test'; mr2.phone  = 'test'; mr2.loginCount = 1; mr2.subscriptionGroupId = sg.Id;
        mr2.investorType  = 'test'; mr2.aum  = 'test'; mr2.isFP  = 'true'; mr2.subscriptionRequested  = 'test,test';
        mr2.privacyAckDate = system.today(); mr.lastLogin = system.today(); mr2.dbSourceCreateDate = system.today();
        Community_Registrant__c cr1 = new Community_Registrant__c();
        cr1.User_Name__c = 'test1'; cr1.Email__c = 'test1@test.com'; cr1.First_Name__c = 'test1';
        cr1.Last_Name__c = 'test1'; cr1.Company__c = 'test1';cr1.Address1__c = 'test1'; cr1.City__c = 'test';
        cr1.State__c = 'test'; cr1.Country__c = 'test'; cr1.Zip_Code__c = 'test'; cr1.Password__c = 'test';
        insert cr1;
        Community_Member__c cm1 = new Community_Member__c(Community__c = c.Id, Community_Registrant__c = cr1.Id);
        insert cm1;
        list<SubscriptionManagementAPI.SubscriptionDetails> subList = new list<SubscriptionManagementAPI.SubscriptionDetails>();
        SubscriptionManagementAPI.SubscriptionDetails sub = new SubscriptionManagementAPI.SubscriptionDetails();
        sub.contactId = c.Id; sub.subscriptionId = s.Id; sub.newValue = 'true'; sub.oldValue = 'false';
        subList.add(sub);
        mr1 = cm.getMemberRecord(cr1.Id);
        mr1.address2 = ''; mr1.aum = '';
        b = cm.setMemberRecord(cr1.Id, mr1);
        string s1 = cm.getCMemberId(c.Id,'test1');
        s1 = cm.isEmailTaken(c.Id, 'test@test.com');
        s1 = cm.getPassword(cr1.Id);
        b = cm.setPassword(cr1.Id, 'test1');
        b = cm.isActive(cr1.Id);
        b = cm.setActive(cr1.Id, true);
        c = cm.getCommunityDetails(c.Id);
        s1 = cm.getCMemberType(cr1.Id);
        cm.setLastLogin(cr1.Id, system.today());
        cm.addCMemberAndSubscriptions(mr2, subList);
        cm.getConSubGrpId(mr2.email);
        cm.getDefaultSubGrpId(mr2.communityId);
        cm.getDomainEligiblity(mr2.email);
        cm.getErrorPrefix();
        cm.getSubGroupId(mr2.email, mr2.communityId);
        cm.setCMemberAndSubscriptions(mr2.cMemberId, mr2, subList);
        cm.updateSubReq(mr2.cMemberId, subList);
    }
}