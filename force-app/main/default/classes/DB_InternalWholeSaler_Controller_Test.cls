/**
 * @description       : 
 * @author            : Rehan
 * @group             : 
 * @last modified on  : 02-11-2021
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   11-28-2020   Rehan                                 Initial Version
**/
@isTest
public class DB_InternalWholeSaler_Controller_Test 
{
    public static List<Id> getUserIdList()
    {
        List<Id> lstUserId = new List<Id>();
        Set<String> setUsers = new Set<String>();

        for(DB_InternalWholeSaler_Users__mdt objDB_InternalWholeSaler_Users : [SELECT Id, MasterLabel, DeveloperName FROM DB_InternalWholeSaler_Users__mdt])
        {
            setUsers.add(objDB_InternalWholeSaler_Users.MasterLabel);
        }

        for(User objUser : [Select Id, Name from User where Name IN: setUsers and isActive = true])
        {
            lstUserId.add(objUser.Id);
        }

        return lstUserId;
    }

    @TestSetup
    static void makeData()
    {
        NationalHolidays__c objNationalHolidays = new NationalHolidays__c();
        objNationalHolidays.Holiday_Date__c = Date.today();
        objNationalHolidays.HolidayDescription__c = 'Test Class Reord';
        objNationalHolidays.Year__c = Date.today().year();
        insert objNationalHolidays;

        ActivityType_Factors__c objActivityType_Factors = new ActivityType_Factors__c();
        objActivityType_Factors.Email_Subscription__c = 10;
        objActivityType_Factors.ETF_Leads__c = 10;
        objActivityType_Factors.Follow_up_Sales_Calls__c = 10;
        objActivityType_Factors.Meeting_Face_to_Face__c = 10;
        objActivityType_Factors.Profile_Metric__c = 10;
        objActivityType_Factors.Sales_Calls__c = 10;
        objActivityType_Factors.Sales_Email__c = 10;
        objActivityType_Factors.Scheduled_meeting_for_Ext_Specialist__c = 10;
        objActivityType_Factors.Service_Calls__c = 10;
        objActivityType_Factors.Zooms__c = 10;
        insert objActivityType_Factors;

        Integer intCurrentMonth = Date.today().month();
        String strName = intCurrentMonth + '_' + Date.today().year();
        DB_Matt_InterwholeSeller__c objDB_Matt_InterwholeSeller = new DB_Matt_InterwholeSeller__c();
        objDB_Matt_InterwholeSeller.Name = strName;
        objDB_Matt_InterwholeSeller.BradPope__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.ChristopherGagnier__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.IanAmberson__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.JacobWilson__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.JamesCooper__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.JarrettGatling__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.MadisonMorelli__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.MichaelHofbauer__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.NicholasPhillips__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.RyanHenshaw__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.VladKasyanov__c = intCurrentMonth;
        objDB_Matt_InterwholeSeller.Profiling__c = 1;
        objDB_Matt_InterwholeSeller.EmailSubscription__c = 1;

        insert objDB_Matt_InterwholeSeller;

        List<Id> lstUserId = new List<Id>();        
        lstUserId = getUserIdList();

        List<Task> lstTask = new List<Task>();
        List<Event> lstEvent = new List<Event>();
        List<PayeeUser_Attendance__c> lstInsertPayeeUserAttendance = new List<PayeeUser_Attendance__c>();
        List<PayeeSummary__c> lstPayeeSummary = new List<PayeeSummary__c>();

        for(User objUser : [Select Id from User where Id IN: lstUserId])
        {
            lstTask = new List<Task>();
            lstEvent = new List<Event>();
            lstInsertPayeeUserAttendance = new List<PayeeUser_Attendance__c>();
            lstPayeeSummary = new List<PayeeSummary__c>();
            system.runAs(objUser)
            {
                Task objTask = new Task();
                objTask.Subject = objUser.Id;
                objTask.ActivityDate = Date.today();
                objTask.OwnerId = objUser.Id;
                objTask.RIA_Activity_Score__c = 10;
                objTask.Status = 'Call Completed';
                objTask.US_Advisory_Call_Type__c = 'Sales Call';
                //objTask.Marketing_Automation__c = false;
                objTask.IsVEContact__c = false;
                objTask.IsTestContact__c = false;
                lstTask.add(objTask);

                Event objEvent = new Event();
                objEvent.ActivityDate=Date.today();
                objEvent.StartDateTime=Date.today();
                objEvent.EndDateTime=Date.today();
                objEvent.Result__c = 'Held';
                objEvent.RIA_Activity_Score__c = 10;
                objEvent.OwnerId = objUser.Id;
                objEvent.glance__c = true;
                objEvent.Subject = 'Activity for ' + objUser.Id + ' - ' + Date.today();
                lstEvent.add(objEvent);
                
                PayeeUser_Attendance__c objPayeeUser_Attendance = new PayeeUser_Attendance__c();
                objPayeeUser_Attendance.Payee_User__c = objUser.Id;
                objPayeeUser_Attendance.No_of_Activity_Records__c = 2;
                objPayeeUser_Attendance.Attendance_Date__c = Date.today();
                lstInsertPayeeUserAttendance.add(objPayeeUser_Attendance);

                PayeeSummary__c objPayeeSummary = new PayeeSummary__c();
                objPayeeSummary.Year__c=String.ValueOf(Date.today().Year());
                objPayeeSummary.Month__c=String.ValueOf(Date.today().Month());
                objPayeeSummary.User__c=objUser.Id;
                objPayeeSummary.TaskFAScore__c =1;
                objPayeeSummary.TaskFAQScore__c =1;
                objPayeeSummary.Approved_Monthly_Bonus__c  = 2000;
                objPayeeSummary.RIA_Activity_Score__c = 2000;
                objPayeeSummary.Total_Days_for_this_Calendar_Month__c = 22;
                objPayeeSummary.No_Of_Days_in_Office__c = 22;
                objPayeeSummary.TaskRIAScore__c =1;
                objPayeeSummary.TaskRIAQScore__c = 1; 
                objPayeeSummary.timeTradeQCount__c = 1;
                objPayeeSummary.GlaceQCount__c =  1;
                objPayeeSummary.IWSQTasksCount__c = 1; 
                objPayeeSummary.IWSToExternalTasksQCount__c = 1;
                objPayeeSummary.ETF_Task_Referral_Q_Count__c = 1;
                objPayeeSummary.ETF_Referral_Q_Count_4_Xternals_ETF_Spl__c = 1;
                objPayeeSummary.ETF_Referral_Q_Count_4_Xternals_RIA_Spl__c = 1;
                lstPayeeSummary.add(objPayeeSummary);

                if(!lstTask.isEmpty())
                    insert lstTask;
                    
                if(!lstEvent.isEmpty())
                    insert lstEvent;

                if(!lstInsertPayeeUserAttendance.isEmpty())
                    insert lstInsertPayeeUserAttendance;
                
                if(!lstPayeeSummary.isEmpty())
                    insert lstPayeeSummary;
            }
        }

        

        User objUser = [Select Id from User where Name = 'Brad Pope' limit 1];
        System.runAs(objUser)
        {
            List<Task> lstTaskUser = new List<Task>();
            List<Event> lstEventUser = new List<Event>();
            lstInsertPayeeUserAttendance = new List<PayeeUser_Attendance__c>();
            lstPayeeSummary = new List<PayeeSummary__c>();

            Task objTask1 = new Task();
            objTask1.Subject = objUser.Id;
            objTask1.ActivityDate = Date.today();
            objTask1.OwnerId = objUser.Id;
            objTask1.RIA_Activity_Score__c = 10;
            objTask1.IsVEContact__c = false ;
            objTask1.IsTestContact__c = false;
            objTask1.Status = 'Call Completed';
            objTask1.US_Advisory_Call_Type__c = 'Sales Call';
            lstTaskUser.add(objTask1);

            Task objTask = new Task();
            objTask.Subject = objUser.Id;
            objTask.ActivityDate = Date.today();
            objTask.OwnerId = objUser.Id;
            objTask.RIA_Activity_Score__c = 10;
            objTask.IsVEContact__c = false ;
            objTask.IsTestContact__c = false;
            objTask.Status = 'Call Completed';
            objTask.US_Advisory_Call_Type__c = 'Sales Call';
            lstTaskUser.add(objTask);

            Event objEvent1 = new Event();
			objEvent1.ActivityDate=Date.today();
			objEvent1.StartDateTime=Date.today();
			objEvent1.EndDateTime=Date.today();
            objEvent1.Result__c = 'Held';
			objEvent1.OwnerId = objUser.Id;
            objEvent1.RIA_Activity_Score__c = 10;
            objEvent1.IsVEContact__c = false ;
            objEvent1.IsTestContact__c = false;
            objEvent1.glance__c = true;
			objEvent1.Subject = 'Activity for ' + objUser.Id + ' - ' + Date.today();
            lstEventUser.add(objEvent1);
            
            Event objEvent = new Event();
			objEvent.ActivityDate=Date.today();
			objEvent.StartDateTime=Date.today();
			objEvent.EndDateTime=Date.today();
            objEvent.Result__c = 'Held';
			objEvent.OwnerId = objUser.Id;
            objEvent.RIA_Activity_Score__c = 10;
            objEvent.IsVEContact__c = false ;
            objEvent.IsTestContact__c = false;
            objEvent.glance__c = true;
			objEvent.Subject = 'Activity for ' + objUser.Id + ' - ' + Date.today();
            lstEventUser.add(objEvent);

            insert lstTaskUser;
            insert lstEventUser;

            Subscription__c subs = new Subscription__c(Name='abcd',Subscription_Display_Name__c='ABC', IsActive__c=true);
            insert subs;        

            Subscription_Group__c subGrp = new Subscription_Group__c(Name='test',IsActive__c=true);
            insert subGrp;

            Account acc = new Account(Name = 'Test Account 101', Channel__c = 'RIA', BillingCountry='US');
            insert acc;

            Contact con = new Contact(AccountId=acc.Id, FirstName='ABC', LastName='ABC', MailingCountry='US',Email='abc@gmail.com',Subscription_Group__c=subGrp.Id);
            insert con;

            Subscription_Member__c subMem = new Subscription_Member__c(Subscription__c =subs.id, Contact__c =con.Id, Email__c ='abc@gmail.com', subscribed__c=true);
            insert subMem;

            Fund__c fnd=new Fund__c(Name='Test',Fund_Type__c='MF',Fund_Group__c='International',Share_Class__c='A',Fund_Ticker_Symbol__c='MFTST');
            insert fnd;

            Fund_Interest__c fndInt=new Fund_Interest__c(Contact__c=con.Id,Fund__c=fnd.Id,Level_Of_Interest__c='Interested',Account__c=acc.Id,Fund_Client__c=True);
            insert fndInt;

            ContactScoreTracker__c objContactScoreTracker = new ContactScoreTracker__c();
            objContactScoreTracker = new ContactScoreTracker__c(Contact__c = con.Id, TrackingFieldAPIName__c = 'Outside_Research__c');
            insert objContactScoreTracker;

            PayeeUser_Attendance__c objPayeeUser_Attendance = new PayeeUser_Attendance__c();
            objPayeeUser_Attendance.Payee_User__c = objUser.Id;
            objPayeeUser_Attendance.No_of_Activity_Records__c = 2;
            objPayeeUser_Attendance.Attendance_Date__c = Date.today();
            lstInsertPayeeUserAttendance.add(objPayeeUser_Attendance);
            insert lstInsertPayeeUserAttendance;


            PayeeSummary__c objPayeeSummary = new PayeeSummary__c();
            objPayeeSummary.Year__c=String.ValueOf(Date.today().Year());
            objPayeeSummary.Month__c=String.ValueOf(Date.today().Month());
            objPayeeSummary.User__c=objUser.Id;
            objPayeeSummary.TaskFAScore__c =1;
            objPayeeSummary.TaskFAQScore__c =1;
            objPayeeSummary.Approved_Monthly_Bonus__c  = 2000;
            objPayeeSummary.RIA_Activity_Score__c = 2000;
            objPayeeSummary.Total_Days_for_this_Calendar_Month__c = 22;
            objPayeeSummary.No_Of_Days_in_Office__c = 22;
            objPayeeSummary.TaskRIAScore__c =1;
            objPayeeSummary.TaskRIAQScore__c = 1; 
            objPayeeSummary.timeTradeQCount__c = 1;
            objPayeeSummary.GlaceQCount__c =  1;
            objPayeeSummary.IWSQTasksCount__c = 1; 
            objPayeeSummary.IWSToExternalTasksQCount__c = 1;
            objPayeeSummary.ETF_Task_Referral_Q_Count__c = 1;
            objPayeeSummary.ETF_Referral_Q_Count_4_Xternals_ETF_Spl__c = 1;
            objPayeeSummary.ETF_Referral_Q_Count_4_Xternals_RIA_Spl__c = 1;
            lstPayeeSummary.add(objPayeeSummary);
            insert lstPayeeSummary;

        }
    }

    @isTest
    public static void testGetDrillDownUserRecords()
    {
        User objUser = [Select Id from User where Name = 'Brad Pope' limit 1];
        system.runAs(objUser)
        {
            test.startTest();

            List<ActivityType_Factors__c> lstActivityType_Factors = [Select Id from ActivityType_Factors__c];
            system.debug('>>>>>>>>>>>>>>>>>>>' + lstActivityType_Factors);

            Integer intCurrentMonth = Date.today().month();
            Integer intCurrentYear = Date.today().year();
            List<Date> lstDate = new List<Date>();
            
            Task objTask = new Task();
            objTask.Subject = objUser.Id;
            objTask.ActivityDate = Date.today();
            objTask.OwnerId = objUser.Id;
            objTask.RIA_Activity_Score__c = 10;
            objTask.IsVEContact__c = false ;
            objTask.IsTestContact__c = false;
            objTask.Status = 'Call Completed';
            objTask.US_Advisory_Call_Type__c = 'Sales Call';
            insert objTask;

            Event objEvent1 = new Event();
			objEvent1.ActivityDate=Date.today();
			objEvent1.StartDateTime=Date.today();
			objEvent1.EndDateTime=Date.today();
            objEvent1.Result__c = 'Held';
			objEvent1.OwnerId = objUser.Id;
            objEvent1.RIA_Activity_Score__c = 10;
            objEvent1.IsVEContact__c = false ;
            objEvent1.IsTestContact__c = false;
            objEvent1.glance__c = true;
			objEvent1.Subject = 'Activity for ' + objUser.Id + ' - ' + Date.today();
            insert objEvent1;

            List<Task> lstTask = [SELECT Id, Owner.Name, OwnerId, CreatedById, US_Advisory_Call_Type__c, 
                                    ActivityDate, Activity_Type__c, Subject,CreatedBy.Name, 
                                    CreatedDate, RIA_Activity_Score__c, FA_Activity_Score__c, CreatedByModelName__c , ETF_Team__c
                                    FROM Task where CreatedById =: objUser.Id ];

            List<Event> lstEvent = [SELECT Id, Owner.Name, OwnerId, CreatedById, US_Advisory_Call_Type__c,
                                    ActivityDate, Activity_Type__c, Subject,CreatedBy.Name, Glance__c,
                                    CreatedDate, RIA_Activity_Score__c, FA_Activity_Score__c, CreatedByModelName__c , ETF_Team__c
                                    from Event where CreatedById =: objUser.Id ];

            system.debug('>>>>>>>>>>>lstTask>>>>>>>>>>>' + lstTask);
            system.debug('>>>>>>>>>>>lstEvent>>>>>>>>>>>' + lstEvent);
            system.debug('>>>>>>>>>>>intCurrentMonth>>>>>>>>>>>' + intCurrentMonth);
            system.debug('>>>>>>>>>>>intCurrentYear>>>>>>>>>>>' + intCurrentYear);



            DB_InternalWholeSaler_Controller.getDrillDownUserRecords(objUser.Id, intCurrentMonth ,intCurrentYear);
            test.stopTest();
        }
        
    }
    

    @isTest
    public static void testGetOnLoadRecords() 
    {
        User objUser = [Select Id from User where Name = 'Brad Pope' limit 1];
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        Map<String, Object> objGetOnLoadData = new Map<String, Object>();
        List<Map<String, String>> lstDrillDownOptions = DB_InternalWholeSaler_Controller.getDrillDownUserOptions();
        List<String> lstDefaultUser = new List<String>();
        lstDefaultUser.add(UserInfo.getUserId());
        lstDefaultUser.add(objUser.Id);

        String strUserId = '';
        if(!lstDrillDownOptions.isEmpty())
            strUserId = lstDrillDownOptions[0].values()[0];
        objGetOnLoadData = DB_InternalWholeSaler_Controller.getOnLoadRecords(intCurrentMonth, intCurrentYear, strUserId, lstDefaultUser);
    }
    

    @isTest
    public static void testGetDrillDownAllUserRecords_Quarterly()
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();

        Set<String> setUsers = new Set<String>();

        List<Id>  lstUserIds = getUserIdList();

        DB_InternalWholeSaler_Controller.getDrillDownAllUserRecords_Quarterly(lstUserIds, intCurrentMonth, intCurrentYear);

        List<DB_InternalWholeSaler_Controller.ActivityObject> lstDrillDownAllUserRecordsMonthly = new List<DB_InternalWholeSaler_Controller.ActivityObject>();
        lstDrillDownAllUserRecordsMonthly = DB_InternalWholeSaler_Controller.getDrillDownAllUserRecords_Monthly(lstUserIds, intCurrentMonth, intCurrentYear);

        List<Map<String, String>> lstDrillDownOptions = new List<Map<String, String>>();
        DB_InternalWholeSaler_Controller.getDrillDownUserOptions( lstDrillDownAllUserRecordsMonthly ); //

        DB_InternalWholeSaler_Controller.getCalculatedScore( lstDrillDownAllUserRecordsMonthly ); //
      
    }

    
    @isTest
    public static void testGetCurrentMonthPayeeAttendanceRecordsDynamic()
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        List<Date> lstDate = new List<Date>();
        DB_InternalWholeSaler_Controller.getCurrentMonthPayeeAttendanceRecordsDynamic(intCurrentMonth,intCurrentYear);
        
    }

    

    @isTest
    public static void testGetAllDatesFromSelectedMonth() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        List<Date> lstDate = new List<Date>();
        lstDate = DB_InternalWholeSaler_Controller.getAllDatesFromSelectedMonth(intCurrentMonth,intCurrentYear);
    }

    @isTest
    public static void testGetCurrentMonthPayeeAttendanceRecords() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        Map<String, List<String>> mapUserRecords = new Map<String, List<String>>();
        mapUserRecords = DB_InternalWholeSaler_Controller.getCurrentMonthPayeeAttendanceRecords(intCurrentMonth,intCurrentYear);
    }

    @isTest
    public static void testGetUserWiseChartCurrentMonth() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords = DB_InternalWholeSaler_Controller.getUserWiseChartCurrentMonth(lstUserId, 
                            DB_InternalWholeSaler_Controller.getUserDaysValues(intCurrentMonth,intCurrentYear), 
                            DB_InternalWholeSaler_Controller.getActivityTypeValues(intCurrentMonth,intCurrentYear), 
                                intCurrentMonth, intCurrentYear);
    }

    @isTest
    public static void testGetUserWiseChartQuarterly() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords = DB_InternalWholeSaler_Controller.getUserWiseChartQuarterly(lstUserId, 
                            DB_InternalWholeSaler_Controller.getUserDaysValues(intCurrentMonth,intCurrentYear), 
                            DB_InternalWholeSaler_Controller.getActivityTypeValues(intCurrentMonth,intCurrentYear), 
                                intCurrentMonth, intCurrentYear);
    }

    @isTest
    public static void testSaveActivityTypeAndUserDays() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();

        DB_InternalWholeSaler_Controller.UserDays objUserDays = DB_InternalWholeSaler_Controller.getUserDaysValues(intCurrentMonth, intCurrentYear);
        DB_InternalWholeSaler_Controller.ActivityType objActivityType = DB_InternalWholeSaler_Controller.getActivityTypeValues(intCurrentMonth,intCurrentYear);

        DB_InternalWholeSaler_Controller.saveActivityTypeAndUserDays(objActivityType, objUserDays);


        User objUser = [Select Id from User where Name = 'Matt Bartlett' limit 1];

        System.runAs(objUser)
        {
            objUserDays = DB_InternalWholeSaler_Controller.getUserDaysValues(intCurrentMonth, intCurrentYear);
            objActivityType = DB_InternalWholeSaler_Controller.getActivityTypeValues(intCurrentMonth,intCurrentYear);

            DB_InternalWholeSaler_Controller.saveActivityTypeAndUserDays(objActivityType, objUserDays);
        }

    }

    @isTest
    public static void testGetUserDaysApprovedStatus_MattBartlett() 
    {
        DB_InternalWholeSaler_Controller.UserDays objUserDaysReturn;

        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();

        DB_InternalWholeSaler_Controller.UserDays objUserDays = DB_InternalWholeSaler_Controller.getUserDaysValues(intCurrentMonth, intCurrentYear);

        User objUser = [Select Id, Name, Username, Email from User where Email = 'mbartlett@vaneck.com' OR Email = 'mbartlett@vaneck.com.invalid' limit 1];

        system.debug('????????????objUser???????????' + objUser);
        System.runAs(objUser)
        {
            List<PayeeSummary__c> lstPayeeSummary = new List<PayeeSummary__c>();
            for(PayeeSummary__c objPayeeSummary : [Select Id, Status__c, Approved_Monthly_Bonus__c , 
                                                    RIA_Activity_Score__c, RIA_Monthly_Bonus__c , LastDayOfMonth__c from PayeeSummary__c])
            {
                Date today = date.today().addDays(-30); 
                Test.setCreatedDate(objPayeeSummary.Id, DateTime.newInstance(today.year(), today.month(), today.day()));

                if(objPayeeSummary.Approved_Monthly_Bonus__c == objPayeeSummary.RIA_Activity_Score__c)
                {
                    objPayeeSummary.Status__c = 'Override-Approved';
                    objPayeeSummary.No_Of_Days_in_Office__c = 22;
                    objPayeeSummary.Total_Days_for_this_Calendar_Month__c  = 22;
                    lstPayeeSummary.add(objPayeeSummary);
                }
            }

            update lstPayeeSummary;

            objUserDays = DB_InternalWholeSaler_Controller.getUserDaysValues(intCurrentMonth, intCurrentYear);

            objUserDaysReturn = DB_InternalWholeSaler_Controller.getUserDaysApprovedStatus(intCurrentMonth, intCurrentYear, objUserDays);
        }
        

        objUserDaysReturn = DB_InternalWholeSaler_Controller.getUserDaysApprovedStatus(intCurrentMonth, intCurrentYear, objUserDays);

    }

    @isTest
    public static void testGetUserDaysApprovedStatus_BradPope() 
    {
        DB_InternalWholeSaler_Controller.UserDays objUserDaysReturn;

        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();

        DB_InternalWholeSaler_Controller.UserDays objUserDays = DB_InternalWholeSaler_Controller.getUserDaysValues(intCurrentMonth, intCurrentYear);

        User objUser = [Select Id, Name, Username, Email from User where Name = 'Brad Pope' limit 1];

        system.debug('????????????objUser???????????' + objUser);
        System.runAs(objUser)
        {
            objUserDays = DB_InternalWholeSaler_Controller.getUserDaysValues(intCurrentMonth, intCurrentYear);
            objUserDaysReturn = DB_InternalWholeSaler_Controller.getUserDaysApprovedStatus(intCurrentMonth, intCurrentYear, objUserDays);
        }

        objUserDaysReturn = DB_InternalWholeSaler_Controller.getUserDaysApprovedStatus(intCurrentMonth, intCurrentYear, objUserDays);
    }

    

    @isTest
    public static void testGetUserWiseChartMeetingAndZoomMetrics() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords = DB_InternalWholeSaler_Controller.getUserWiseChartMeetingAndZoomMetrics(lstUserId, 
                            DB_InternalWholeSaler_Controller.getUserDaysValues(intCurrentMonth,intCurrentYear), 
                            DB_InternalWholeSaler_Controller.getActivityTypeValues(intCurrentMonth,intCurrentYear), 
                                intCurrentMonth, intCurrentYear);
    }

    @isTest
    public static void testGetUserWiseChartProfileMetrics() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords = DB_InternalWholeSaler_Controller.getUserWiseChartProfileMetrics(lstUserId, 
                            DB_InternalWholeSaler_Controller.getUserDaysValues(intCurrentMonth,intCurrentYear), 
                            DB_InternalWholeSaler_Controller.getActivityTypeValues(intCurrentMonth,intCurrentYear), 
                                intCurrentMonth, intCurrentYear);
    }

    @isTest
    public static void testGetUserWiseChartETFLeadsMetrics() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords = DB_InternalWholeSaler_Controller.getUserWiseChartETFLeadsMetrics(lstUserId, 
                            DB_InternalWholeSaler_Controller.getUserDaysValues(intCurrentMonth,intCurrentYear), 
                            DB_InternalWholeSaler_Controller.getActivityTypeValues(intCurrentMonth,intCurrentYear), 
                                intCurrentMonth, intCurrentYear);
    }

    @isTest
    public static void testGetSelectedYearQuarter() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearQuarter(3, 2021);
        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearQuarter(6, 2021);
        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearQuarter(9, 2021);
        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearQuarter(12, 2021);
    }

    @isTest
    public static void testGetSelectedYearOnlyQuarter() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        String jsonUserRecords = '';
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearOnlyQuarter(3, 2021);
        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearOnlyQuarter(6, 2021);
        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearOnlyQuarter(9, 2021);
        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearOnlyQuarter(12, 2021);
    }

    
    @isTest
    public static void testGetSelectedYearQuarterDates() 
    {
        Integer intCurrentMonth = Date.today().month();
        Integer intCurrentYear = Date.today().year();
        Map<String, DB_InternalWholeSaler_Controller.QuarterDates> jsonUserRecords = new Map<String, DB_InternalWholeSaler_Controller.QuarterDates>();
        List<String> lstUserId = new List<String>();
        Set<String> setUsers = new Set<String>();

        lstUserId = getUserIdList();

        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearQuarterDates(3, 2021);
        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearQuarterDates(6, 2021);
        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearQuarterDates(9, 2021);
        jsonUserRecords = DB_InternalWholeSaler_Controller.getSelectedYearQuarterDates(12, 2021);
    }
        

}